using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SensibilityManager : MonoBehaviour
{
    public static SensibilityManager instance;

    public static float sensibilityX = 1f;
    public static float sensibilityY = 1f;

    public static bool invertX = false;
    public static bool invertY = false;

    public static Action<float> OnSensibilityXSet;
    public static Action<float> OnSensibilityYSet;

    public static Action<bool> OnInvertXSet;
    public static Action<bool> OnInvertYSet;

    private void Awake()
    {
        SetSingleton();

        CheckInitializeSensibilityXPlayerPrefs();
        CheckInitializeSensibilityYPlayerPrefs();

        CheckInitializeInvertXPlayerPrefs();
        CheckInitializeInvertYPlayerPrefs();

        LoadSensibilityXPlayerPrefs();
        LoadSensibilityYPlayerPrefs();

        LoadInvertXPlayerPrefs();
        LoadInvertYPlayerPrefs();
    }

    private void SetSingleton()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }

    #region SetSensibilities
    public void SetSensibilityX(float senX)
    {
        sensibilityX = senX;

        OnSensibilityXSet?.Invoke(sensibilityX);

        //SaveSensibilityXPlayerPrefs();
    }
    public void SetSensibilityY(float senY)
    {
        sensibilityY = senY;

        OnSensibilityYSet?.Invoke(sensibilityY);

        //SaveSensibilityYPlayerPrefs();
    }
    #endregion

    #region LoadSensibilitiesPlayerPrefs
    public void LoadSensibilityXPlayerPrefs()
    {
        sensibilityX = PlayerPrefs.GetFloat("SensibilityX");

        OnSensibilityXSet?.Invoke(sensibilityX);
    }
    public void LoadSensibilityYPlayerPrefs()
    {
        sensibilityY = PlayerPrefs.GetFloat("SensibilityY");

        OnSensibilityYSet?.Invoke(sensibilityY);
    }
    #endregion

    #region SaveSensibilitiesPlayerPrefs
    public void SaveSensibilityXPlayerPrefs()
    {
        PlayerPrefs.SetFloat("SensibilityX", sensibilityX);
    }
    public void SaveSensibilityYPlayerPrefs()
    {
        PlayerPrefs.SetFloat("SensibilityY", sensibilityY);
    }
    #endregion

    #region InitializeSensibilitiesPlayerPrefs
    public void CheckInitializeSensibilityXPlayerPrefs()
    {
        if (!PlayerPrefs.HasKey("SensibilityX"))
        {
            PlayerPrefs.SetFloat("SensibilityX", 1);
        }
    }
    public void CheckInitializeSensibilityYPlayerPrefs()
    {
        if (!PlayerPrefs.HasKey("SensibilityY"))
        {
            PlayerPrefs.SetFloat("SensibilityY", 1);
        }
    }
    #endregion

    #region SetInvert
    public void SetInvertX(bool invX)
    {
        invertX = invX;

        OnInvertXSet?.Invoke(invertX);

        //SaveInvertXPlayerPrefs();
    }
    public void SetInvertY(bool invY)
    {
        invertY = invY;

        OnInvertYSet?.Invoke(invertY);

        //SaveInvertYPlayerPrefs();
    }
    #endregion

    #region LoadInvertPlayerPrefs
    public void LoadInvertXPlayerPrefs()
    {
        string value = PlayerPrefs.GetString("InvertX");

        invertX = (value == "true");

        OnInvertXSet?.Invoke(invertX);
    }
    public void LoadInvertYPlayerPrefs()
    {
        string value = PlayerPrefs.GetString("InvertY");

        invertY = (value == "true");

        OnInvertYSet?.Invoke(invertY);
    }
    #endregion

    #region SaveInvertPlayerPrefs
    public void SaveInvertXPlayerPrefs()
    {
        string value = invertX ? "true" : "false";

        PlayerPrefs.SetString("InvertX", value);
    }
    public void SaveInvertYPlayerPrefs()
    {
        string value = invertY ? "true" : "false";

        PlayerPrefs.SetString("InvertY", value);
    }
    #endregion

    #region InitializeInvertPlayerPrefs
    public void CheckInitializeInvertXPlayerPrefs()
    {
        if (!PlayerPrefs.HasKey("InvertX"))
        {
            PlayerPrefs.SetString("InvertX", "false");
        }
    }
    public void CheckInitializeInvertYPlayerPrefs()
    {
        if (!PlayerPrefs.HasKey("InvertY"))
        {
            PlayerPrefs.SetString("InvertY", "false");
        }
    }
    #endregion
}
