using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class ScenesManager : MonoBehaviour
{
    public static ScenesManager instance;

    [SerializeField] private CanvasGroup blackPanelCanvasGroup;

    public static Action OnSceneTransitionStart;
    public static Action OnSceneTransitionEnd;
    public enum SceneState {Idle, TransitionIn, TransitionOut}
    public SceneState sceneState;

    private void Awake()
    {
        SetSingleton();
    }

    private void SetSingleton()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    #region SceneState
    private void SetSceneState(SceneState state)
    {
        sceneState = state;
    }

    private bool CanMakeTransition()
    {
        return (sceneState == SceneState.Idle);
    }

    #endregion

    #region FadeCoroutines
    private IEnumerator FadeIn(float fadeInTime)
    {
        SetSceneState(SceneState.TransitionIn);

        OnSceneTransitionStart?.Invoke();

        blackPanelCanvasGroup.alpha = 1f;
        blackPanelCanvasGroup.blocksRaycasts = true;
        float time = 0f;

        while (time <= fadeInTime)
        {
            time += Time.deltaTime;
            blackPanelCanvasGroup.alpha = 1 - time / fadeInTime;

            yield return null;
        }

        blackPanelCanvasGroup.alpha = 0f;
        blackPanelCanvasGroup.blocksRaycasts = false;

        SetSceneState(SceneState.Idle);

        OnSceneTransitionEnd?.Invoke();
    }
    private IEnumerator FadeOut(float fadeOutTime)
    {
        SetSceneState(SceneState.TransitionOut);

        OnSceneTransitionStart?.Invoke();

        blackPanelCanvasGroup.alpha = 0f;
        blackPanelCanvasGroup.blocksRaycasts = true;
        float time = 0f;

        while (time <= fadeOutTime)
        {
            time += Time.unscaledDeltaTime;
            blackPanelCanvasGroup.alpha = time / fadeOutTime;

            yield return null;
        }

        blackPanelCanvasGroup.alpha = 1f;
        blackPanelCanvasGroup.blocksRaycasts = false;

        OnSceneTransitionEnd?.Invoke();

        SetSceneState(SceneState.Idle);
    }

    public void StartFadeInCoroutine(float fadeInTime)
    {
        if (!CanMakeTransition()) return;
        StartCoroutine(FadeIn(fadeInTime));
    }
    public void StartFadeOutCoroutine(float fadeOutTime)
    {
        if (!CanMakeTransition()) return;
        StartCoroutine(FadeOut(fadeOutTime));
    }
    #endregion

    #region FadeScene
    public IEnumerator FadeToTargetScene(string sceneName, float fadeOutTime) //Corrutina de cambio de escena
    {
        yield return StartCoroutine(FadeOut(fadeOutTime));
        SceneManager.LoadScene(sceneName); //Se carga la escena deseada
    }
    public void StartFadeToTargetScene(string sceneName, float fadeOutTime)
    {
        if (!CanMakeTransition()) return;
        StartCoroutine(FadeToTargetScene(sceneName,fadeOutTime));
    }
    #endregion

    #region SimpleTransition
    public IEnumerator SimpleTransitionToTargetScene(string sceneName, float blackOutTime)
    {
        SetSceneState(SceneState.TransitionOut);

        OnSceneTransitionStart?.Invoke();

        blackPanelCanvasGroup.alpha = 1f;
        blackPanelCanvasGroup.blocksRaycasts = true;

        yield return new WaitForSeconds(blackOutTime);

        SceneManager.LoadScene(sceneName); //Se carga la escena deseada

        SetSceneState(SceneState.Idle);

        OnSceneTransitionEnd?.Invoke();
    }

    public void StartSimpleTransitionToTargetScene(string sceneName, float blackOutTime)
    {
        if (!CanMakeTransition()) return;
        StartCoroutine(SimpleTransitionToTargetScene(sceneName, blackOutTime));
    }
    #endregion

    #region ReloadScene
    public void FadeReloadCurrentScene(float fadeOutTime)
    {
        if (!CanMakeTransition()) return;
        StartCoroutine(FadeToTargetScene(SceneManager.GetActiveScene().name, fadeOutTime));
    }
    public void SimpleReloadCurrentScene(float blackOutTime)
    {
        if (!CanMakeTransition()) return;
        StartCoroutine(SimpleTransitionToTargetScene(SceneManager.GetActiveScene().name, blackOutTime));
    }
    #endregion
}

