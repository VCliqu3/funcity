using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    private AudioSource musicSource;
    private AudioSource sfxSource;
    private AudioSource dialogueSource;

    public float musicVolume;
    public float sfxVolume;
    public float dialogueVolume;

    public static Action<float> OnMusicVolumeSet;
    public static Action<float> OnSFXVolumeSet;
    public static Action<float> OnDialogueVolumeSet;

    private void Awake()
    {
        SetSingleton();

        musicSource = GetComponentInChildren<MusicGameObjectController>().GetComponent<AudioSource>();
        sfxSource = GetComponentInChildren<SFXGameObjectController>().GetComponent<AudioSource>();
        dialogueSource = GetComponentInChildren<DialogueGameObjectController>().GetComponent<AudioSource>();

        CheckInitializeSFXVolumePlayerPrefs();
        CheckInitializeMusicVolumePlayerPrefs();
        CheckInitializeDialogueVolumePlayerPrefs();

        LoadMusicVolumePlayerPrefs();
        LoadSFXVolumePlayerPrefs();
        LoadDialogueVolumePlayerPrefs();
    }

    private void SetSingleton()
    {  
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        
    }
    public void PlayMusic(AudioClip music)
    {
        if (!music)
        {
            musicSource.Stop();
            musicSource.clip = null;
        }

        if(musicSource.clip != music)
        {
            musicSource.Stop();
            musicSource.clip = music;
            musicSource.Play();
        }
    }
    public void PlaySFX(AudioClip clip)
    {
        sfxSource.PlayOneShot(clip);
    }
    public void PlayDialogue(AudioClip dialogue)
    {
        dialogueSource.PlayOneShot(dialogue);
    }

    public void StopMusic()
    {
        musicSource.Stop();
    }
    public void StopSFX()
    {
        sfxSource.Stop();
    }
    public void StopDialogue()
    {
        dialogueSource.Stop();
    }

    #region SetVolumes
    public void SetMusicVolume(float volume)
    {
        musicVolume = volume;
        OnMusicVolumeSet?.Invoke(musicVolume);

        //SaveMusicVolumePlayerPrefs();
    }
    public void SetSFXVolume(float volume)
    {
        sfxVolume = volume;
        OnSFXVolumeSet?.Invoke(sfxVolume);

        //SaveSFXVolumePlayerPrefs();
    }
    public void SetDialogueVolume(float volume)
    {
        dialogueVolume = volume;
        OnDialogueVolumeSet?.Invoke(dialogueVolume);
        //SaveDialogueVolumePlayerPrefs();
    }
    #endregion

    #region LoadVolumesPlayerPrefs
    public void LoadMusicVolumePlayerPrefs()
    {
        musicVolume = PlayerPrefs.GetFloat("MusicVolume");
        OnMusicVolumeSet?.Invoke(musicVolume);
    }
    public void LoadSFXVolumePlayerPrefs()
    {
        sfxVolume  = PlayerPrefs.GetFloat("SFXVolume");
        OnSFXVolumeSet?.Invoke(sfxVolume);
    }
    public void LoadDialogueVolumePlayerPrefs()
    {
        dialogueVolume = PlayerPrefs.GetFloat("DialogueVolume");
        OnDialogueVolumeSet?.Invoke(dialogueVolume);
    }
    #endregion

    #region SaveVolumesPlayerPrefs
    public void SaveMusicVolumePlayerPrefs()
    {
        PlayerPrefs.SetFloat("MusicVolume", musicVolume);
    }
    public void SaveSFXVolumePlayerPrefs()
    {
        PlayerPrefs.SetFloat("SFXVolume", sfxVolume);
    }
    public void SaveDialogueVolumePlayerPrefs()
    {
        PlayerPrefs.SetFloat("DialogueVolume", dialogueVolume);
    }
    #endregion

    #region InitializeVolumesPlayerPrefs
    public void CheckInitializeMusicVolumePlayerPrefs()
    {
        if (!PlayerPrefs.HasKey("MusicVolume"))
        {
            PlayerPrefs.SetFloat("MusicVolume", 1);
        }
    }
    public void CheckInitializeSFXVolumePlayerPrefs()
    {
        if (!PlayerPrefs.HasKey("SFXVolume"))
        {
            PlayerPrefs.SetFloat("SFXVolume", 1);
        }
    }
    public void CheckInitializeDialogueVolumePlayerPrefs()
    {
        if (!PlayerPrefs.HasKey("DialogueVolume"))
        {
            PlayerPrefs.SetFloat("DialogueVolume", 1);
        }
    }
    #endregion
}
