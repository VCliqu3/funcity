using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorManager : MonoBehaviour
{
    public static CursorManager instance;

    [SerializeField] private bool startWithCursorUnlocked;

    private void Awake()
    {
        SetSingleton();
    }

    private void Start()
    {
        SetCursorLockedAndVisible(startWithCursorUnlocked);
    }

    private void SetSingleton()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void SetCursorLockedAndVisible(bool isUnlocked)
    {
        Cursor.lockState = isUnlocked ? CursorLockMode.None : CursorLockMode.Locked;
        Cursor.visible = isUnlocked;
    }
}
