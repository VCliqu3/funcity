using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    private UIInputHandler _UIInputHandler;
    public enum GameState {Playing,ReadingNote,OnInspection,OnJumpScare,Paused,Dead,OnCinematic,OnCutscene}
    public GameState gameState;

    [SerializeField] private float gameFadeInTime;
    [SerializeField] private float gameFadeOutTime;
    [SerializeField] private float deathBlackOutTime;

    [Header("Next Scene Settings")]
    [SerializeField] private string nextScene;
    [SerializeField] private float nextSceneFadeTime;

    public static Action OnGameStart;
    public static Action<float> OnGameSceneStart;

    public static Action<GameState,GameState> OnGameStateChanged;

    public static Action OnPauseInput;
    public static Action OnResumeInput;

    public static Action OnGameJumpScare;
    public static Action OnGameDeath;
    public static Action<float> OnGameSceneDeath;
    public static Action OnGameFinalCinematicStart;
    public static Action OnGameFinalCinematicEnd;

    private GameState previousGameState;
    private GameState beforePauseGameState;

    private void OnEnable()
    {
        ScenesManager.OnSceneTransitionStart += DoWhenSceneTransitionStart;
        ScenesManager.OnSceneTransitionEnd += DoWhenSceneTransitionEnd;

        InGameCheckpointManager.OnCheckpointLoad += DoWhenCheckpointLoad;

        PauseCanvasController.OnPauseMenuOpened += OnPauseMenuOpened;
        PauseCanvasController.OnPauseMenuClosed += OnPauseMenuClose;

        AngelicaDeath.OnAngelicaJumpscare += OnJumpScare;
        AngelicaDeath.OnAngelicaDeath += OnDeath;

        Cutscene.OnCutsceneStart += DoWhenCutsceneStart;
        Cutscene.OnCutsceneEnd += DoWhenCutsceneEnd;

        NoteCanvasController.OnNoteOpened += DoWhenNoteOpened;
        NoteCanvasController.OnNoteClosed += DoWhenNoteClosed;

        InspectionCanvasController.OnInspectionOpened += DoWhenInspectionOpened;
        InspectionCanvasController.OnInspectionClosed += DoWhenInspectionClosed;

        FinalCinematic.OnCinematicStart += DoWhenFinalCinematicStart;
        FinalCinematic.OnCinematicEnd += DoWhenFinalCinematicEnd;
    }
    private void OnDisable()
    {
        ScenesManager.OnSceneTransitionStart -= DoWhenSceneTransitionStart;
        ScenesManager.OnSceneTransitionEnd -= DoWhenSceneTransitionEnd;

        InGameCheckpointManager.OnCheckpointLoad -= DoWhenCheckpointLoad;

        PauseCanvasController.OnPauseMenuOpened -= OnPauseMenuOpened;
        PauseCanvasController.OnPauseMenuClosed -= OnPauseMenuClose;

        AngelicaDeath.OnAngelicaJumpscare -= OnJumpScare;
        AngelicaDeath.OnAngelicaDeath -= OnDeath;

        Cutscene.OnCutsceneStart -= DoWhenCutsceneStart;
        Cutscene.OnCutsceneEnd -= DoWhenCutsceneEnd;

        NoteCanvasController.OnNoteOpened -= DoWhenNoteOpened;
        NoteCanvasController.OnNoteClosed -= DoWhenNoteClosed;

        InspectionCanvasController.OnInspectionOpened -= DoWhenInspectionOpened;
        InspectionCanvasController.OnInspectionClosed -= DoWhenInspectionClosed;

        FinalCinematic.OnCinematicStart -= DoWhenFinalCinematicStart;
        FinalCinematic.OnCinematicEnd -= DoWhenFinalCinematicEnd;
    }

    private void Awake()
    {
        SetSingleton();
        _UIInputHandler = FindObjectOfType<UIInputHandler>();
    }

    private void Start()
    {
        SetGameState(GameState.Playing);
        Time.timeScale = 1f;
        
        OnGameStart?.Invoke();

        ScenesManager.instance.StartFadeInCoroutine(gameFadeInTime);
    }

    private void Update()
    {
        CheckPauseResume();

        CheckReloadCurrentScene();
    }

    private void SetSingleton()
    {
        if (instance == null)
        {
            instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    #region SetGameState
    public void SetGameState(GameState gameState)
    {
        previousGameState = this.gameState;
        this.gameState = gameState;

        OnGameStateChanged?.Invoke(previousGameState, this.gameState);
    }

    public IEnumerator SetGameStateAfterOneFrame(GameState gameState)
    {
        yield return new WaitForEndOfFrame();
        SetGameState(gameState);
    }
    #endregion

    #region Pause
    private void CheckPauseResume()
    {
        if (_UIInputHandler.GetCancelInputDown())
        {
            switch (gameState)
            {
                case GameState.Playing:
                case GameState.ReadingNote:
                case GameState.OnInspection:
                case GameState.OnCinematic:
                case GameState.OnCutscene:
                    OnPauseInput?.Invoke();
                    break;
                case GameState.Paused:
                    OnResumeInput?.Invoke();
                    break;
                default:
                    break;
            }
        }
    }

    private void OnPauseMenuOpened()
    {
        beforePauseGameState = gameState;

        Time.timeScale = 0f;
        SetGameState(GameState.Paused);
    }

    private void OnPauseMenuClose()
    {
        Time.timeScale = 1f;
        SetGameState(beforePauseGameState);
    }

    #endregion

    #region Death
    private void OnJumpScare()
    {
        SetGameState(GameState.OnJumpScare);
        OnGameJumpScare?.Invoke();
    }

    private void OnDeath()
    {
        SetGameState(GameState.Dead);

        OnGameDeath?.Invoke();

        ScenesManager.instance.SimpleReloadCurrentScene(deathBlackOutTime);
    }

    #endregion

    #region Cinematic
    private void DoWhenFinalCinematicStart()
    {
        SetGameState(GameState.OnCinematic);

        OnGameFinalCinematicStart?.Invoke();
    }

    private void DoWhenFinalCinematicEnd()
    {     
        ScenesManager.instance.StartFadeToTargetScene(nextScene, nextSceneFadeTime);

        OnGameFinalCinematicEnd?.Invoke();
    }
    #endregion

    #region Cutscenes
    private void DoWhenCutsceneStart()
    {
        SetGameState(GameState.OnCutscene);
    }

    private void DoWhenCutsceneEnd()
    {
        SetGameState(GameState.Playing);
    }
    #endregion

    #region SceneTransitions
    private void DoWhenSceneTransitionStart()
    {
        //SetGameState(GameState.SceneTransition);
    }

    private void DoWhenSceneTransitionEnd()
    {      
        //SetGameState(previousGameState);
    }

    #endregion

    #region Checkpoints

    private void DoWhenCheckpointLoad(int checkpointNumber, Transform checkpointLinkedTransform)
    {
         MoveAndRotatePlayer(checkpointLinkedTransform);
    }

    public void MoveAndRotatePlayer(Transform targetTransform)
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        CharacterController characterController = player.GetComponent<CharacterController>();
        characterController.enabled = false;
        
        player.transform.SetPositionAndRotation(targetTransform.position, targetTransform.rotation);

        characterController.enabled = true;
    }

    #endregion

    #region Notes
    private void DoWhenNoteOpened(InteractableNote interactableNote)
    {
        SetGameState(GameState.ReadingNote);
    }

    private void DoWhenNoteClosed(InteractableNote interactableNote)
    {
        if (gameState == GameState.OnJumpScare) return;
        if (gameState == GameState.Dead) return;

        StartCoroutine(SetGameStateAfterOneFrame(GameState.Playing));
    }
    #endregion

    #region Inspection
    private void DoWhenInspectionOpened(PickableObject pickableObject)
    {
        SetGameState(GameState.OnInspection);
    }

    private void DoWhenInspectionClosed(PickableObject pickableObject)
    {
        if (gameState == GameState.OnJumpScare) return;
        if (gameState == GameState.Dead) return;

        StartCoroutine(SetGameStateAfterOneFrame(GameState.Playing));
    }
    #endregion

    public void TransitionToTargetScene(string sceneName)
    {
        ScenesManager.instance.StartFadeToTargetScene(sceneName, gameFadeOutTime);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void CheckReloadCurrentScene()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            ScenesManager.instance.FadeReloadCurrentScene(gameFadeOutTime);
        }
    }
}
