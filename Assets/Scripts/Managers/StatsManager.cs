using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsManager : MonoBehaviour
{
    public static StatsManager instance;

    public static int tries = 0;
    public static int deaths = 0;

    private void OnEnable()
    {
        GameManager.OnGameStart += AddOneTry;
        GameManager.OnGameJumpScare += AddOneDeath;
    }
    private void OnDisable()
    {
        GameManager.OnGameStart += AddOneTry;
        GameManager.OnGameJumpScare += AddOneDeath;
    }

    private void Awake()
    {
        SetSingleton();

        CheckInitializeTriesPlayerPrefs();
        CheckInitializeDeathsPlayerPrefs();
    }

    private void SetSingleton()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void CheckInitializeTriesPlayerPrefs()
    {
        if (!PlayerPrefs.HasKey("Tries"))
        {
            PlayerPrefs.SetInt("Tries", 0);
        }
    }

    private void CheckInitializeDeathsPlayerPrefs()
    {
        if (!PlayerPrefs.HasKey("Deaths"))
        {
            PlayerPrefs.SetInt("Deaths", 0);
        }
    }

    public void LoadTriesPlayerPrefs()
    {
        tries = PlayerPrefs.GetInt("Tries");
    }
    public void LoadDeathsPlayerPrefs()
    {
        deaths = PlayerPrefs.GetInt("Deaths");
    }

    public void SaveTriesPlayerPrefs()
    {
        PlayerPrefs.SetInt("Tries", tries);
    }
    public void SaveDeathsPlayerPrefs()
    {
        PlayerPrefs.SetInt("Deaths", deaths);
    }

    private void AddOneTry()
    {
        tries++;
        SaveTriesPlayerPrefs();
    }
    private void AddOneDeath()
    {
        deaths++;
        SaveDeathsPlayerPrefs();
    }
}
