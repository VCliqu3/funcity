public class GameConstants
{
    // all the constant string used across the game
    public const string c_AxisNameVertical = "Vertical";
    public const string c_AxisNameHorizontal = "Horizontal";
    public const string c_MouseAxisNameVertical = "Mouse Y";
    public const string c_MouseAxisNameHorizontal = "Mouse X";
    public const string c_AxisNameJoystickLookVertical = "Look Y";
    public const string c_AxisNameJoystickLookHorizontal = "Look X";

    public const string c_ButtonNameSprint = "Sprint";
    public const string c_ButtonNameJump = "Jump";
    public const string c_ButtonNameCrouch = "Crouch";

    public const string c_ButtonNameFlashlight = "Flashlight";
    public const string c_ButtonNameInteract = "Interact";
    public const string c_ButtonNamePickUp = "Pick Up";    
    public const string c_ButtonNameDrop = "Drop";
    public const string c_ButtonNameCloseNote = "Close Note";
    public const string c_ButtonNameUse = "Use";
    public const string c_ButtonNameInspect = "Inspect";
    public const string c_ButtonNameCloseInspection = "Close Inspection";

    public const string c_ButtonNameGamepadFire = "Gamepad Fire";
    public const string c_ButtonNameGamepadAim = "Gamepad Aim";
    public const string c_ButtonNameSwitchWeapon = "Mouse ScrollWheel";
    public const string c_ButtonNameGamepadSwitchWeapon = "Gamepad Switch";
    public const string c_ButtonNameNextWeapon = "NextWeapon";
    public const string c_ButtonNamePauseMenu = "Pause Menu";
    public const string c_ButtonNameSubmit = "Submit";
    public const string c_ButtonNameCancel = "Cancel";
    public const string c_ButtonReload = "Reload";

    public const string c_ButtonNameNextScene = "Next Scene";
    public const string c_ButtonNameSkip = "Skip";
}