using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputHandler : MonoBehaviour
{
    private GameObject player;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private bool CanProcessInput()
    {
        if (ScenesManager.instance.sceneState == ScenesManager.SceneState.TransitionIn) return false;
        return (GameManager.instance.gameState == GameManager.GameState.Playing); //Se procesara el input del Player solo si el cursor esta dentro del juego
    }

    #region PlayerMovement
    public Vector3 GetMoveInput()
    {
        if (CanProcessInput())
        {
            float x = Input.GetAxisRaw(GameConstants.c_AxisNameHorizontal);
            float z = Input.GetAxisRaw(GameConstants.c_AxisNameVertical);

            Vector3 movementVector = player.transform.right * x + player.transform.forward * z;

            movementVector = Vector3.ClampMagnitude(movementVector, 1); //Restringir el movimiento a magnitud 1

            return movementVector;
        }

        return Vector3.zero;
    }
    #endregion

    #region PlayerLook

    public float GetLookInputHorizontal()
    {
        return GetLookAxis(GameConstants.c_MouseAxisNameHorizontal, SensibilityManager.sensibilityX, SensibilityManager.invertX);
    }

    public float GetLookInputVertical()
    {
        return GetLookAxis(GameConstants.c_MouseAxisNameVertical, SensibilityManager.sensibilityY, SensibilityManager.invertY);
    }

    public  float GetLookAxis(string mouseInputName, float lookSensitivity, bool invertAxis)
    {
        if (CanProcessInput())
        {
            float axisInput = Input.GetAxisRaw(mouseInputName);

            // handle inverting vertical input
            if (invertAxis)
                axisInput *= -1f;

            // apply sensitivity multiplier
            axisInput *= lookSensitivity;
            
            return axisInput;
        }

        return 0f;
    }
    #endregion

    #region PlayerJump
    public bool GetJumpInputDown()
    {
        if (CanProcessInput())
        {
            return Input.GetButtonDown(GameConstants.c_ButtonNameJump);
        }

        return false;
    }

    public bool GetJumpInputHeld()
    {
        if (CanProcessInput())
        {
            return Input.GetButton(GameConstants.c_ButtonNameJump);
        }

        return false;
    }

    #endregion

    #region PlayerSprint
    public bool GetSprintInputHeld()
    {
        if (CanProcessInput())
        {
            return Input.GetButton(GameConstants.c_ButtonNameSprint);
        }

        return false;
    }

    #endregion

    #region PlayerCrouch
    public bool GetCrouchInputDown()
    {
        if (CanProcessInput())
        {
            return Input.GetButtonDown(GameConstants.c_ButtonNameCrouch);
        }

        return false;
    }

    public bool GetCrouchInputReleased()
    {
        if (CanProcessInput())
        {
            return Input.GetButtonUp(GameConstants.c_ButtonNameCrouch);
        }

        return false;
    }
    #endregion

    #region PlayerFlashlight
    public bool GetFlashlightInputDown()
    {
        if (CanProcessInput())
        {
            return Input.GetButtonDown(GameConstants.c_ButtonNameFlashlight);
        }

        return false;
    }

    #endregion

    #region PlayerInteract
    public bool GetInteractInputDown()
    {
        if (CanProcessInput())
        {
            return Input.GetButtonDown(GameConstants.c_ButtonNameInteract);
        }

        return false;
    }

    #endregion

    #region PlayerPickUp
    public bool GetPickUpInputDown()
    {
        if (CanProcessInput())
        {
            return Input.GetButtonDown(GameConstants.c_ButtonNamePickUp);
        }

        return false;
    }

    #endregion

    #region PlayerDrop
    public bool GetDropInputDown()
    {
        if (CanProcessInput())
        {
            return Input.GetButtonDown(GameConstants.c_ButtonNameDrop);
        }

        return false;
    }

    #endregion

    #region PlayerUse
    public bool GetUseInputDown()
    {
        if (CanProcessInput())
        {
            return Input.GetButtonDown(GameConstants.c_ButtonNameUse);
        }

        return false;
    }

    #endregion

    #region PlayerInspect
    public bool GetInspectInputDown()
    {
        if (CanProcessInput())
        {
            return Input.GetButtonDown(GameConstants.c_ButtonNameInspect);
        }

        return false;
    }

    #endregion
}
