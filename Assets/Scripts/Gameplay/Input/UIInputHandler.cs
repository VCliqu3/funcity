using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIInputHandler : MonoBehaviour
{
    private bool CanProcessInput()
    {
        if (ScenesManager.instance.sceneState != ScenesManager.SceneState.Idle) return false;
        return true;
    }

    #region Cancel
    public bool GetCancelInputDown()
    {
        if (CanProcessInput())
        {
            return Input.GetButtonDown(GameConstants.c_ButtonNameCancel);
        }

        return false;
    }
    #endregion

    #region Notes
    public bool GetInteractInputDown()
    {
        if (CanProcessInput())
        {
            return Input.GetButtonDown(GameConstants.c_ButtonNameInteract);
        }

        return false;
    }

    public bool GetCloseNoteInputDown()
    {
        if (CanProcessInput())
        {
            return Input.GetButtonDown(GameConstants.c_ButtonNameCloseNote);
        }

        return false;
    }
    #endregion

    #region Inspection
    public bool GetIspectInputDown()
    {
        if (CanProcessInput())
        {
            return Input.GetButtonDown(GameConstants.c_ButtonNameInspect);
        }

        return false;
    }

    public bool GetCloseInspectionInputDown()
    {
        if (CanProcessInput())
        {
            return Input.GetButtonDown(GameConstants.c_ButtonNameCloseInspection);
        }

        return false;
    }
    #endregion
}
