using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentralSquareWanderingDeath : AngelicaDeath
{
    [Header("TV Set Persecution Death Settings")]
    [SerializeField] private float yOffsetAboveWanderingAngelica;

    protected override void OnEnable()
    {
        base.OnEnable();
        WanderingAngelica.OnAngelicaChasePlayer += DoWhenAngelicaChasePlayer;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        WanderingAngelica.OnAngelicaChasePlayer -= DoWhenAngelicaChasePlayer;
    }

    protected void DoWhenAngelicaChasePlayer(Transform transform)
    {
        this.transform.position = transform.position + new Vector3(0f, yOffsetAboveWanderingAngelica, 0f);
        this.transform.rotation = transform.rotation;

        StartCoroutine(DeathCoroutine());
    }

    protected IEnumerator DeathCoroutine()
    {
        base.PreDeathLogic();

        angelicaAnimator.Play(angelicaDeathAnimation);

        yield return StartCoroutine(PlayerLookTowards());

        cameraAnimationHolder.SetParent(null);
        cameraAnimationHolder.LookAt(transform);
        playerCamera.localRotation = Quaternion.Euler(Vector3.zero);
        playerCamera.localPosition = Vector3.zero;

        cameraAnimationHolderAnimator.SetTrigger(playerDeathAnimationTriggerName);

        yield return new WaitForSeconds(cameraAnimationHolderDuration);

        OnAngelicaDeath?.Invoke();
    }
}
