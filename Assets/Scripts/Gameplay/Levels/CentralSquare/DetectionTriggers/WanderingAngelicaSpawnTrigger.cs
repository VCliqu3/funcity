using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class WanderingAngelicaSpawnTrigger : ColliderDetectionTrigger
{
    public static Action OnPlayerEnter;
    public UnityEvent OnTrigger;

    protected override void DoWhenPlayerDetected()
    {
        base.DoWhenPlayerDetected();

        OnPlayerEnter?.Invoke();
        OnTrigger?.Invoke();

        Debug.Log("Wandering Angelica should spawn");
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        ExitHospitalCardPanel.OnCardPlaced += DoWhenExitHospitalCardPlaced;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        ExitHospitalCardPanel.OnCardPlaced -= DoWhenExitHospitalCardPlaced;
    }

    private void DoWhenExitHospitalCardPlaced()
    {
        canBeTriggered = true;
    }
}
