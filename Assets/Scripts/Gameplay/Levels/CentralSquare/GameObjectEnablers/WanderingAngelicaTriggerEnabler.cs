using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderingAngelicaTriggerEnabler : GameObjectEnabler
{
    private void OnEnable()
    {
        ExitHospitalTrigger.OnPlayerEnter += EnableAllGameObjects;
    }

    private void OnDisable()
    {
        ExitHospitalTrigger.OnPlayerEnter -= EnableAllGameObjects;
    }
}
