using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderingAngelicaEnabler : GameObjectEnabler
{
    private void OnEnable()
    {
        WanderingAngelicaSpawnTrigger.OnPlayerEnter += EnableAllGameObjects;
    }

    private void OnDisable()
    {
        WanderingAngelicaSpawnTrigger.OnPlayerEnter -= EnableAllGameObjects;
    }
}
