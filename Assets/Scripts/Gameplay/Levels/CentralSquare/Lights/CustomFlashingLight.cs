using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CustomFlashingLightRelation
{
    public float timeOn;
    public float timeOff;
}

public class CustomFlashingLight : MonoBehaviour
{
    [Header("General Settings")]
    [SerializeField] private bool startOn; 
    [SerializeField] private bool enableFlashing;

    [SerializeField] private List<Light> lights = new List<Light>();

    [Header("Parammeters")]
    [SerializeField] private CustomFlashingLightRelation[] customFlashingLightRelation;

    private Material material;

    private void Awake()
    {
        material = GetComponentInChildren<Renderer>().material;
    }

    private void Start()
    {
        TurnLights(startOn);

        if(enableFlashing) StartCoroutine(FlashLights());
    }

    private IEnumerator FlashLights()
    {
        while (true)
        {
            foreach(CustomFlashingLightRelation relation in customFlashingLightRelation)
            {
                TurnLights(true);

                yield return new WaitForSeconds(relation.timeOn);

                TurnLights(false);

                yield return new WaitForSeconds(relation.timeOff);
            }       
        }
    }

    private void TurnLights(bool state)
    {
        foreach(Light light in lights)
        {
            light.enabled = state;
        }

        if (!material) return;

        if (state)
        {
            material.EnableKeyword("_EMISSION");
        }
        else
        {
            material.DisableKeyword("_EMISSION");
        }
    }
}
