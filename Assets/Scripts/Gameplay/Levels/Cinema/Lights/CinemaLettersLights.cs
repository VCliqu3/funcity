using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinemaLettersLights : LightSource
{
    [Header("Load Checkpoint Settings")]
    [SerializeField] private int linkedcheckpointNumber;

    private Material material;

    private void OnEnable()
    {
        InGameCheckpointManager.OnCheckpointLoad += CheckActivateByCheckpointLoad;

        ExitTVSetTrigger.OnPlayerEnter += DoWhenExitTVSet;
    }

    private void OnDisable()
    {
        InGameCheckpointManager.OnCheckpointLoad -= CheckActivateByCheckpointLoad;

        ExitTVSetTrigger.OnPlayerEnter -= DoWhenExitTVSet;
    }

    protected override void Awake()
    {
        base.Awake();
        material = GetComponentInChildren<Renderer>().material;
    }

    private void DoWhenExitTVSet()
    {
        StartCoroutine(TurnLettersOn());
    }

    private IEnumerator TurnLettersOn()
    {
        TurnLights(true);

        yield return new WaitForSeconds(0.1f);

        TurnLights(false);

        yield return new WaitForSeconds(0.2f);

        TurnLights(true);

        yield return new WaitForSeconds(0.1f);

        TurnLights(false);

        yield return new WaitForSeconds(0.2f);

        TurnLights(true);

        yield return new WaitForSeconds(0.2f);

        TurnLights(false);

        yield return new WaitForSeconds(0.6f);

        TurnLights(true);
    }

    public override void TurnLights(bool state)
    {
        base.TurnLights(state);

        if (state)
        {
            material.EnableKeyword("_EMISSION");
        }
        else
        {
            material.DisableKeyword("_EMISSION");
        }
    }

    private void CheckActivateByCheckpointLoad(int checkpointNumber, Transform checkpointTransform)
    {
        if (linkedcheckpointNumber != checkpointNumber) return;

        DoWhenExitTVSet();
    }
}
