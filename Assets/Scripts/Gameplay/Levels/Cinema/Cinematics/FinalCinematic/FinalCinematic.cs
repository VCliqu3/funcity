using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FinalCinematic : Cinematic
{
    public static Action OnCinematicStart;
    public static Action OnCinematicEnd;
    public static Action OnAngelicaAscended;

    private void OnEnable()
    {
        FinalCinematicTrigger.OnPlayerEnter += StartCinematicCoroutine;
    }

    private void OnDisable()
    {
        FinalCinematicTrigger.OnPlayerEnter -= StartCinematicCoroutine;
    }

    protected void StartCinematicCoroutine()
    {
        StartCoroutine(CinematicCoroutine());
    }

    protected IEnumerator CinematicCoroutine()
    {
        OnCinematicStart?.Invoke();

        base.PreDeathLogic();

        yield return StartCoroutine(PlayerLookAndMoveTowards());

        cameraAnimationHolder.SetParent(null);
        cameraAnimationHolder.position = playerMoveTowardsPos.position;
        cameraAnimationHolder.LookAt(playerLookTowardsPos);

        playerCamera.localRotation = Quaternion.Euler(Vector3.zero);
        playerCamera.localPosition = Vector3.zero;

        cameraAnimationHolderAnimator.SetTrigger("FinalCinematic");

        yield return new WaitForSeconds(2f);

        OnAngelicaAscended?.Invoke();

        yield return new WaitForSeconds(1f);

        OnCinematicEnd?.Invoke();
    }
}
