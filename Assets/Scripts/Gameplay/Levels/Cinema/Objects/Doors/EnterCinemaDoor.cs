using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterCinemaDoor : InteractableDoor
{
    protected override void OnEnable()
    {
        base.OnEnable();
        ExitTVSetTrigger.OnPlayerEnter += DoWhenPlayerExitTVSet;
        EnterCinemaTrigger.OnPlayerEnter += DoWhenPlayerEnterCinema;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        ExitTVSetTrigger.OnPlayerEnter -= DoWhenPlayerExitTVSet;
        EnterCinemaTrigger.OnPlayerEnter -= DoWhenPlayerEnterCinema;
    }

    private void DoWhenPlayerExitTVSet()
    {
        if (!isOpen) OpenDoor();

        EnableInteractions();
    }

    private void DoWhenPlayerEnterCinema()
    {
        if (isOpen) CloseDoor();

        DisableInteractions();
    }
}
