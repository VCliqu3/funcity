using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnterCinemaTrigger : ColliderDetectionTrigger
{
    public static Action OnPlayerEnter;

    protected override void DoWhenPlayerDetected()
    {
        base.DoWhenPlayerDetected();

        OnPlayerEnter?.Invoke();

        Debug.Log("Cinema Doors Should Close");
    }
}
