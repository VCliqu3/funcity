using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurgeryRoomEncounterClock : EncounterClock
{
    private void OnEnable()
    {
        GameManager.OnGameStart += ClearClock;
        SurgeryRoomEncounter.OnSurgeryRoomEncounterStartingTime += InitialSetClockTime;
        SurgeryRoomEncounter.OnCounterChanged += SetClockTime;
        SurgeryRoomEncounter.OnSurgeryRoomEncounterEnd += ClearClock;
    }

    private void OnDisable()
    {
        GameManager.OnGameStart -= ClearClock;
        SurgeryRoomEncounter.OnSurgeryRoomEncounterStartingTime -= InitialSetClockTime;
        SurgeryRoomEncounter.OnCounterChanged -= SetClockTime;
        SurgeryRoomEncounter.OnSurgeryRoomEncounterEnd -= ClearClock;
    }
}
