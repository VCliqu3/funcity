using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SurgeryRoomEncounter : TimedEncounter
{
    [Header("Surgery Room Encounter Settings")]
    [SerializeField] private int correctLiverID;

    public static Action OnSurgeryRoomEncounterStarting;
    public static Action<int> OnSurgeryRoomEncounterStartingTime;
    public static Action OnSurgeryRoomEncounterStart;
    public static Action OnSurgeryRoomEncounterEnding;
    public static Action OnSurgeryRoomEncounterEnd;

    public static Action OnSurgeryRoomEncounterCompleted;
    public static Action OnSurgeryRoomEncounterFailed;

    public static Action OnTimeToDie;

    public static Action<int> OnCounterChanged;

    protected void OnEnable()
    {
        SurgeryRoomEncounterTrigger.OnPlayerEnter += DoWhenPlayerEnterTrigger;
        Liver.OnLiverPickedUp += CheckIfShouldComplete;
    }

    protected void OnDisable()
    {
        SurgeryRoomEncounterTrigger.OnPlayerEnter -= DoWhenPlayerEnterTrigger;
        Liver.OnLiverPickedUp -= CheckIfShouldComplete;
    }

    protected void CheckIfShouldComplete(int ID)
    {
        if (encounterState != EncounterState.Playing) return;
        if (correctLiverID != ID) return;

        SetEncounterResultState(EncounterResultState.Completed);
        SetEncounterState(EncounterState.Ending);
    }

    #region OnEncounter States
    protected override void OnEncounterStarting()
    {
        base.OnEncounterStarting();

        SetLiversPickupState(false);
        SetSurgeryRoomSlidingDoorsInteractableState(false);

        OnSurgeryRoomEncounterStarting?.Invoke();
        OnSurgeryRoomEncounterStartingTime?.Invoke(Mathf.RoundToInt(completeTime));
    }

    protected override void OnEncounterStart()
    {
        base.OnEncounterStart();

        SetLiversPickupState(true);
        SetSurgeryRoomSlidingDoorsInteractableState(true);

        OnSurgeryRoomEncounterStart?.Invoke();     
    }
    protected override void OnEncounterEnding()
    {
        base.OnEncounterEnding();
        OnSurgeryRoomEncounterEnding?.Invoke();
    }
    protected override void OnEncounterEnd()
    {
        base.OnEncounterEnd();
        OnSurgeryRoomEncounterEnd?.Invoke();
    }

    #endregion

    #region OnEncounter Result States
    protected override void OnEncounterCompleted()
    {
        base.OnEncounterCompleted();
        OnSurgeryRoomEncounterCompleted?.Invoke();
    }
    protected override void OnEncounterFailed()
    {
        base.OnEncounterFailed();
        OnSurgeryRoomEncounterFailed?.Invoke();
    }
    #endregion

    private void SetLiversPickupState(bool state)
    {
        Liver[] livers = FindObjectsOfType<Liver>();

        foreach (Liver liver in livers)
        {
            liver.pickUpEnabled = state;
        }
    }
    protected override void OnTimeToDiePassed()
    {
        base.OnTimeToDiePassed();

        OnTimeToDie?.Invoke();
    }

    private void SetSurgeryRoomSlidingDoorsInteractableState(bool state)
    {
        SurgeryRoomSlidingDoor[] slidingDoors = FindObjectsOfType<SurgeryRoomSlidingDoor>();

        foreach (SurgeryRoomSlidingDoor slidingDoor in slidingDoors)
        {
            slidingDoor.interactionsEnabled = state;
        }
    }
    protected override void OnTimeCounterChanged(int time)
    {
        OnCounterChanged?.Invoke(time);
    }
}
