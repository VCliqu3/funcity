using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BabyRoomEncounter : TimedEncounter
{
    [Header("Baby Room Encounter Settings")]
    [SerializeField] private List<Baby> babiesToStartCrying = new List<Baby>();

    public static Action OnBabyRoomEncounterStarting;
    public static Action<int> OnBabyRoomEncounterStartingTime;
    public static Action OnBabyRoomEncounterStart;
    public static Action OnBabyRoomEncounterEnding;
    public static Action OnBabyRoomEncounterEnd;

    public static Action OnBabyRoomEncounterCompleted;
    public static Action OnBabyRoomEncounterFailed;

    public static Action OnTimeToDie;

    public static Action<int> OnCounterChanged;

    protected void OnEnable()
    {
        BabyRoomEncounterTrigger.OnPlayerEnter += DoWhenPlayerEnterTrigger;
        Baby.OnBabyCryingStateToggled += CheckIfShouldComplete;
    }

    protected void OnDisable()
    {
        BabyRoomEncounterTrigger.OnPlayerEnter -= DoWhenPlayerEnterTrigger;
        Baby.OnBabyCryingStateToggled -= CheckIfShouldComplete;
    }

    protected void CheckIfShouldComplete(Baby baby)
    {
        if (encounterState != EncounterState.Playing) return;

        Baby[] babies = FindObjectsOfType<Baby>();

        foreach(Baby baby_ in babies)
        {
            if (baby_.isCrying) return;
        }

        SetEncounterResultState(EncounterResultState.Completed);
        SetEncounterState(EncounterState.Ending);
    }

    #region OnEncounter States
    protected override void OnEncounterStarting()
    {
        base.OnEncounterStarting();

        SetAllBalanceCalibratorsInteractions(false);
        SetBabyLeversInteractableState(false);
        SetBabiesToStartCryingState(true);

        OnBabyRoomEncounterStarting?.Invoke();
        OnBabyRoomEncounterStartingTime?.Invoke(Mathf.RoundToInt(completeTime));
    }

    protected override void OnEncounterStart()
    {
        base.OnEncounterStart();

        SetBabyLeversInteractableState(true);

        OnBabyRoomEncounterStart?.Invoke();
    }
    protected override void OnEncounterEnding()
    {
        base.OnEncounterEnding();

        SetAllBabiesEnableCryingToggle(false);
        SetAllBabiesCryingState(false);

        OnBabyRoomEncounterEnding?.Invoke();
    }
    protected override void OnEncounterEnd()
    {
        base.OnEncounterEnd();
        OnBabyRoomEncounterEnd?.Invoke();
    }

    #endregion

    #region OnEncounter Result States
    protected override void OnEncounterCompleted()
    {
        base.OnEncounterCompleted();

        SetAllBalanceCalibratorsInteractions(true);

        OnBabyRoomEncounterCompleted?.Invoke();
    }
    protected override void OnEncounterFailed()
    {
        base.OnEncounterFailed();
        OnBabyRoomEncounterFailed?.Invoke();
    }
    #endregion

    private void SetBabyLeversInteractableState(bool state)
    {
        BabyLever[] babyLevers = FindObjectsOfType<BabyLever>();

        foreach (BabyLever babyLever in babyLevers)
        {
            babyLever.interactionsEnabled = state;
        }
    }

    private void SetBabiesToStartCryingState(bool state)
    {
        foreach (Baby baby in babiesToStartCrying)
        {
            baby.ForceCryingState(state);
        }
    }

    private void SetAllBabiesCryingState(bool state)
    {
        Baby[] babies = FindObjectsOfType<Baby>();

        foreach (Baby baby in babies)
        {
            baby.ForceCryingState(state);
        }
    }

    private void SetAllBabiesEnableCryingToggle(bool state)
    {
        Baby[] babies = FindObjectsOfType<Baby>();

        foreach (Baby baby in babies)
        {
            baby.enableCryingToggle = state;
        }
    }

    private void SetAllBalanceCalibratorsInteractions(bool state)
    {
        BalanceCalibrator[] balanceCalibrators = FindObjectsOfType<BalanceCalibrator>();

        foreach (BalanceCalibrator balanceCalibrator in balanceCalibrators)
        {
            balanceCalibrator.interactionsEnabled = state;
        }
    }
    protected override void OnTimeToDiePassed()
    {
        base.OnTimeToDiePassed();

        OnTimeToDie?.Invoke();
    }

    protected override void OnTimeCounterChanged(int time)
    {
        OnCounterChanged?.Invoke(time);
    }
}
