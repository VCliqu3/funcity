using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabyRoomEncounterClock : EncounterClock
{
    private void OnEnable()
    {
        BabyRoomEncounter.OnBabyRoomEncounterStarting += ClearClock;
        BabyRoomEncounter.OnBabyRoomEncounterStartingTime += InitialSetClockTime;
        BabyRoomEncounter.OnCounterChanged += SetClockTime;
        BabyRoomEncounter.OnBabyRoomEncounterEnd += ClearClock;
    }

    private void OnDisable()
    {
        BabyRoomEncounter.OnBabyRoomEncounterStarting -= ClearClock;
        BabyRoomEncounter.OnBabyRoomEncounterStartingTime -= InitialSetClockTime;
        BabyRoomEncounter.OnCounterChanged -= SetClockTime;
        BabyRoomEncounter.OnBabyRoomEncounterEnd -= ClearClock;
    }
}
