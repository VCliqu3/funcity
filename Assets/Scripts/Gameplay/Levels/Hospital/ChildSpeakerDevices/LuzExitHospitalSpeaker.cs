using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuzExitHospitalSpeaker : ChildSpeakerDevice
{
    private void OnEnable()
    {
        ExitHospitalCardPanel.OnCardPlaced += DoWhenCardPlaced;
    }
    private void OnDisable()
    {
        ExitHospitalCardPanel.OnCardPlaced -= DoWhenCardPlaced;
    }
    private void DoWhenCardPlaced()
    {
        StartCoroutine(PlacedCardCoroutine());
    }

    private IEnumerator PlacedCardCoroutine()
    {
        yield return new WaitForSeconds(1f);

        PlayClip(0);
        yield return StartCoroutine(WaitTime(0));

        PlayClip(1);
        yield return StartCoroutine(WaitTime(1));

        PlayClip(2);
        yield return StartCoroutine(WaitTime(2));

        PlayClip(3);
    }
}
