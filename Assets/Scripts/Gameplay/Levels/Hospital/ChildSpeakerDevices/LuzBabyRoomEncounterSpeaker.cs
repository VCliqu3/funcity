using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuzBabyRoomEncounterSpeaker : ChildSpeakerDevice
{
    private void OnEnable()
    {
        BabyRoomEncounter.OnBabyRoomEncounterStarting += DoWhenBabyRoomEncounterStarting;
    }
    private void OnDisable()
    {
        BabyRoomEncounter.OnBabyRoomEncounterStarting -= DoWhenBabyRoomEncounterStarting;
    }

    private void DoWhenBabyRoomEncounterStarting()
    {
        StartCoroutine(BabyRoomCoroutine());
    }

    private IEnumerator BabyRoomCoroutine()
    {
        yield return new WaitForSeconds(1f);

        PlayClip(0);
    }
}
