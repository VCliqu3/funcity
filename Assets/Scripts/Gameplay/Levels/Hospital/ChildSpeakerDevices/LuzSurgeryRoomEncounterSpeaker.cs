using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuzSurgeryRoomEncounterSpeaker : ChildSpeakerDevice
{
    private void OnEnable()
    {
        SurgeryRoomEncounter.OnSurgeryRoomEncounterStarting += DoWhenSurgeryRoomEncounterStarting;
    }
    private void OnDisable()
    {
        SurgeryRoomEncounter.OnSurgeryRoomEncounterStarting -= DoWhenSurgeryRoomEncounterStarting;
    }

    private void DoWhenSurgeryRoomEncounterStarting()
    {
        StartCoroutine(SurgeryRoomCoroutine());
    }

    private IEnumerator SurgeryRoomCoroutine()
    {
        yield return new WaitForSeconds(0.5f);

        PlayClip(0);

        yield return StartCoroutine(WaitTime(0));

        PlayClip(1);
    }
}
