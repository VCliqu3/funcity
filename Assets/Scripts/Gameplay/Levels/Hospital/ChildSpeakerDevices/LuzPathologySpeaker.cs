using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuzPathologySpeaker : ChildSpeakerDevice
{
    private void OnEnable()
    {
        EnterPathologyTrigger.OnPlayerEnter += DoWhenPlayerEnterPathology;
    }
    private void OnDisable()
    {
        EnterPathologyTrigger.OnPlayerEnter -= DoWhenPlayerEnterPathology;
    }

    private void DoWhenPlayerEnterPathology()
    {
        StartCoroutine(EnterPathologyCoroutine());
    }

    private IEnumerator EnterPathologyCoroutine()
    {
        yield return new WaitForSeconds(1f);

        PlayClip(0);
        yield return StartCoroutine(WaitTime(0));

        PlayClip(1);
        yield return StartCoroutine(WaitTime(1));

        PlayClip(2);
    }
}
