using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LuzReceptionSpeaker : ChildSpeakerDevice
{
    public static Action OnDialogueCompleted;

    private void OnEnable()
    {
        EnterHospitalTrigger.OnPlayerEnter += DoWhenPlayerFirstEnterHospital;
    }
    private void OnDisable()
    {
        EnterHospitalTrigger.OnPlayerEnter -= DoWhenPlayerFirstEnterHospital;
    }

    private void DoWhenPlayerFirstEnterHospital()
    {
        StartCoroutine(FirstEnterHospitalCoroutine());
    }

    private IEnumerator FirstEnterHospitalCoroutine()
    {
        yield return new WaitForSeconds(1.5f);

        PlayClip(0);
        yield return StartCoroutine(WaitTime(0));

        PlayClip(1);
        yield return StartCoroutine(WaitTime(1));

        yield return new WaitForSeconds(2.5f);

        PlayClip(2);
        yield return StartCoroutine(WaitTime(2));

        PlayClip(3);
        yield return StartCoroutine(WaitTime(3));

        PlayClip(4);
        yield return StartCoroutine(WaitTime(4));

        OnDialogueCompleted?.Invoke();

        yield return new WaitForSeconds(0.25f);
        PlayClip(5);
    }
}
