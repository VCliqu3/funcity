using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuzOphthalmologySpeaker : ChildSpeakerDevice
{
    private void OnEnable()
    {
        EnterOphthalmologyTrigger.OnPlayerEnter += DoWhenPlayerEnterOphtamology;
    }
    private void OnDisable()
    {
        EnterOphthalmologyTrigger.OnPlayerEnter -= DoWhenPlayerEnterOphtamology;
    }

    private void DoWhenPlayerEnterOphtamology()
    {
        StartCoroutine(EnterOphtalmologyCoroutine());
    }

    private IEnumerator EnterOphtalmologyCoroutine()
    {
        yield return new WaitForSeconds(1f);

        PlayClip(0);
    }
}
