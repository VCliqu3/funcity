using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnterPathologyTrigger : ColliderDetectionTrigger
{
    [Header("PlayerPrefs Trigger")]
    [SerializeField] private bool disableByPlayerPrefs;
    [SerializeField] private string playerPrefsKeyName;
    [SerializeField] private bool hasBeenTriggeredPlayerPrefs;
    [SerializeField] private int maxCheckpointToDisableByPlayerPrefs;

    public static Action OnPlayerEnter;
    public static Action<bool> OnPlayerPrefsChecked;

    protected override void OnEnable()
    {
        base.OnEnable();
        LiverCardPanel.OnCardPlaced += OnLiverCardPlaced;
        InGameCheckpointManager.OnCheckpointLoad += CheckClearPlayerPrefsByCheckpointLoad;

    }
    protected override void OnDisable()
    {
        base.OnDisable();
        LiverCardPanel.OnCardPlaced -= OnLiverCardPlaced;
        InGameCheckpointManager.OnCheckpointLoad -= CheckClearPlayerPrefsByCheckpointLoad;
    }
    protected override void Awake()
    {
        base.Awake();
        CheckInitializePlayerPrefsKey();
    }

    protected override void DoWhenPlayerDetected()
    {
        if (hasBeenTriggeredPlayerPrefs && disableByPlayerPrefs) return;

        base.DoWhenPlayerDetected();

        OnPlayerEnter?.Invoke();

        SetPlayerPrefsKey(true);
    }

    private void OnLiverCardPlaced()
    {
        canBeTriggered = false;
    }

    #region PlayerPrefsEventTriggerMethods

    private void CheckClearPlayerPrefsByCheckpointLoad(int checkpointNumber, Transform checkpointTransform)
    {
        if (checkpointNumber <= maxCheckpointToDisableByPlayerPrefs)
        {
            SetPlayerPrefsKey(false);
            UpdatePlayerPrefsBoolean();
        }

        OnPlayerPrefsChecked?.Invoke(hasBeenTriggeredPlayerPrefs);

        //FindObjectOfType<PathologyHallDoor>().CheckShouldClosePlayerPrefs(hasBeenTriggeredPlayerPrefs);
    }

    private void CheckInitializePlayerPrefsKey()
    {
        if (!PlayerPrefs.HasKey(playerPrefsKeyName))
        {
            PlayerPrefs.SetInt(playerPrefsKeyName, 0);
        }

        UpdatePlayerPrefsBoolean();
    }

    private void SetPlayerPrefsKey(bool state)
    {
        int value = state == false ? 0 : 1;
        PlayerPrefs.SetInt(playerPrefsKeyName, value);

        UpdatePlayerPrefsBoolean();
    }

    private void UpdatePlayerPrefsBoolean()
    {
        hasBeenTriggeredPlayerPrefs = PlayerPrefs.GetInt(playerPrefsKeyName) == 0 ? false : true;
    }
    #endregion
}
