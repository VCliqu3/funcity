using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnterOphthalmologyTrigger : ColliderDetectionTrigger
{
    public static Action OnPlayerEnter;

    protected override void DoWhenPlayerDetected()
    {
        base.DoWhenPlayerDetected();

        OnPlayerEnter?.Invoke();
    }
}
