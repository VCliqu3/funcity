using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ExitHospitalDoorCloseTrigger : ColliderDetectionTrigger
{
    public static Action OnPlayerEnter;

    protected override void DoWhenPlayerDetected()
    {
        base.DoWhenPlayerDetected();

        OnPlayerEnter?.Invoke();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        ExitHospitalCardPanel.OnCardPlaced += DoWhenExitHospitalCardPlaced;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        ExitHospitalCardPanel.OnCardPlaced -= DoWhenExitHospitalCardPlaced;
    }

    private void DoWhenExitHospitalCardPlaced()
    {
        canBeTriggered = true;
    }
}
