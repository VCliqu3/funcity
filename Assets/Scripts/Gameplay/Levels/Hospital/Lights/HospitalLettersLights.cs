using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HospitalLettersLights : LightSource
{
    [Header("Load Checkpoint Settings")]
    [SerializeField] private int linkedcheckpointNumber;

    private Material material;

    private void OnEnable()
    {
        InGameCheckpointManager.OnCheckpointLoad += CheckActivateByCheckpointLoad;

        ExitTicketOfficeTrigger.OnPlayerEnter += DoWhenExitTicketOffice;
        ExitHospitalCardPanel.OnCardPlaced += DoWhenExitHospitalCardPlaced;
    }

    private void OnDisable()
    {
        InGameCheckpointManager.OnCheckpointLoad -= CheckActivateByCheckpointLoad;

        ExitTicketOfficeTrigger.OnPlayerEnter -= DoWhenExitTicketOffice;
        ExitHospitalCardPanel.OnCardPlaced -= DoWhenExitHospitalCardPlaced;
    }

    protected override void Awake()
    {
        base.Awake();
        material = GetComponentInChildren<Renderer>().material;
    }

    private void DoWhenExitTicketOffice()
    {
        StartCoroutine(TurnLettersOn());
    }

    private IEnumerator TurnLettersOn()
    {
        TurnLights(true);

        yield return new WaitForSeconds(0.3f);

        TurnLights(false);

        yield return new WaitForSeconds(0.2f);

        TurnLights(true);

        yield return new WaitForSeconds(0.3f);

        TurnLights(false);

        yield return new WaitForSeconds(0.15f);

        TurnLights(true);

        yield return new WaitForSeconds(0.2f);

        TurnLights(false);

        yield return new WaitForSeconds(0.3f);

        TurnLights(true);    
    }

    private void DoWhenExitHospitalCardPlaced()
    {
        TurnLights(false);
    }

    public override void TurnLights(bool state)
    {
        base.TurnLights(state);

        if (state)
        {
            material.EnableKeyword("_EMISSION");
        }
        else
        {
            material.DisableKeyword("_EMISSION");
        }
    }
    private void CheckActivateByCheckpointLoad(int checkpointNumber, Transform checkpointTransform)
    {
        if (linkedcheckpointNumber != checkpointNumber) return;

        DoWhenExitTicketOffice();
    }
}
