using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurgeryRoomLight : LightSource
{
    private Material material;

    private void OnEnable()
    {
        SurgeryRoomEncounter.OnSurgeryRoomEncounterStart += DoWhenSurgeryRoomEncounterStart;
        SurgeryRoomEncounter.OnSurgeryRoomEncounterEnd += DoWhenSurgeryRoomEncounterEnd;
    }

    private void OnDisable()
    {
        SurgeryRoomEncounter.OnSurgeryRoomEncounterStart -= DoWhenSurgeryRoomEncounterStart;
        SurgeryRoomEncounter.OnSurgeryRoomEncounterEnd -= DoWhenSurgeryRoomEncounterEnd;
    }

    protected override void Awake()
    {
        base.Awake();
        material = GetComponentInChildren<Renderer>().material;
    }

    private void DoWhenSurgeryRoomEncounterStart()
    {
        TurnLights(true);
    }

    private void DoWhenSurgeryRoomEncounterEnd()
    {
        TurnLights(false);      
    }

    public override void TurnLights(bool state)
    {
        base.TurnLights(state);

        if (state)
        {
            material.EnableKeyword("_EMISSION");
        }
        else
        {
            material.DisableKeyword("_EMISSION");
        }
    }
}
