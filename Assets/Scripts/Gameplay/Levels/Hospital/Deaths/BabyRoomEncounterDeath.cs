using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabyRoomEncounterDeath : AngelicaDeath
{
    protected override void OnEnable()
    {
        base.OnEnable();
        BabyRoomEncounter.OnTimeToDie += StartDeathCoroutine;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        BabyRoomEncounter.OnTimeToDie -= StartDeathCoroutine;
    }

    protected void StartDeathCoroutine()
    {
        StartCoroutine(DeathCoroutine());
    }

    protected IEnumerator DeathCoroutine()
    {
        base.PreDeathLogic();

        angelicaAnimator.Play(angelicaDeathAnimation);

        yield return StartCoroutine(PlayerLookTowards());

        cameraAnimationHolder.SetParent(null);
        cameraAnimationHolder.LookAt(transform);
        playerCamera.localRotation = Quaternion.Euler(Vector3.zero);
        playerCamera.localPosition = Vector3.zero;

        yield return StartCoroutine(PlayerLookTowards());

        cameraAnimationHolderAnimator.SetTrigger(playerDeathAnimationTriggerName);

        yield return new WaitForSeconds(cameraAnimationHolderDuration);

        OnAngelicaDeath?.Invoke();
    }
}
