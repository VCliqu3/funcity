    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HospitalHallJumpscare : MonoBehaviour
{
    [SerializeField] private GameObject angelicaModel;

    [SerializeField] private List<LightSource> lightSources = new List<LightSource>();
    [SerializeField] private float jumpscareDuration;
    [SerializeField] private float timeFlashingLights;

    [SerializeField] private AudioClip jumpscareAudioClip;

    [Header("Angelica Animation Settings")]
    [SerializeField] private Animator angelicaAnimator;

    private AudioSource audioSource;
    private PlayerFlashlight playerFlashlight;

    private void OnEnable()
    {
        HospitalHallJumpscareTrigger.OnPlayerEnter += StartJumpscare;
    }

    private void OnDisable()
    {
        HospitalHallJumpscareTrigger.OnPlayerEnter -= StartJumpscare;
    }

    private void Awake()
    {
        audioSource = GetComponentInChildren<AudioSource>();
        playerFlashlight = FindObjectOfType<PlayerFlashlight>();
    }

    private void StartJumpscare()
    {
        StartCoroutine(Jumpscare());
    }

    private IEnumerator Jumpscare()
    {
        bool prevFlashlightState = playerFlashlight.flashlightOn;
        playerFlashlight.ForceFlashlightState(false);
        playerFlashlight.flashlightToggleEnabled = false;

        angelicaModel.SetActive(true);
        angelicaAnimator.Play("Idle");

        if (jumpscareAudioClip) audioSource.PlayOneShot(jumpscareAudioClip);

        float jumpscareTimeCounter = 0f;
        float lightFlashTimeCounter = 0f;

        while (jumpscareTimeCounter < jumpscareDuration)
        {
            if(lightFlashTimeCounter >= timeFlashingLights)
            {
                ToggleAllLightSources();
                lightFlashTimeCounter = 0f;
            }

            lightFlashTimeCounter += Time.deltaTime;
            jumpscareTimeCounter += Time.deltaTime;

            yield return null;
        }

        TurnAllLightSources(false);
        angelicaModel.SetActive(false);

        playerFlashlight.ForceFlashlightState(prevFlashlightState);
        playerFlashlight.flashlightToggleEnabled = true;
    }

    private void ToggleAllLightSources()
    {
        foreach(LightSource lightSource in lightSources)
        {
            lightSource.ToggleLights();
        }
    }
    private void TurnAllLightSources(bool state)
    {
        foreach (LightSource lightSource in lightSources)
        {
            lightSource.TurnLights(state);
        }
    }
}
