using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HospitalParkingJumpscare : MonoBehaviour
{
    [SerializeField] private List<LightSource> lightSources = new List<LightSource>();
    [SerializeField] private float jumpscareDuration;

    [SerializeField] private AudioSource carAudioSource;
    [SerializeField] private AudioClip carAlarmClip;
    [SerializeField] private AudioClip stopCarAlarmClip;

    private void OnEnable()
    {
        HospitalParkingJumpscareTrigger.OnPlayerEnter += StartJumpscare;
    }

    private void OnDisable()
    {
        HospitalParkingJumpscareTrigger.OnPlayerEnter -= StartJumpscare;
    }

    private void StartJumpscare()
    {
        StartCoroutine(Jumpscare());
    }

    private IEnumerator Jumpscare()
    {
        float jumpscareTimeCounter = 0f;

        TurnAllLightSources(true);

        carAudioSource.loop = true;
        carAudioSource.clip = carAlarmClip;
        carAudioSource.Play();

        Debug.Log("Car Alarm Ringing");

        while (jumpscareTimeCounter < jumpscareDuration)
        {          
            jumpscareTimeCounter += Time.deltaTime;

            yield return null;
        }

        TurnAllLightSources(false);


        carAudioSource.loop = false;
        carAudioSource.clip = null;
        carAudioSource.Stop();

        if (stopCarAlarmClip) carAudioSource.PlayOneShot(stopCarAlarmClip);

        Debug.Log("Car Alarm Stopped");
    }

    private void TurnAllLightSources(bool state)
    {
        foreach (LightSource lightSource in lightSources)
        {
            lightSource.TurnLights(state);
        }
    }
}
