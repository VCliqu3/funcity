using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HospitalDoctorOfficeJumpscare : MonoBehaviour
{
    [SerializeField] private GameObject angelicaModel;

    [SerializeField] private List<LightSource> lightSources = new List<LightSource>();
    [SerializeField] private float jumpscareDuration;

    [SerializeField] private AudioClip jumpscareAudioClip;

    [Header("Angelica Animation Settings")]
    [SerializeField] private Animator angelicaAnimator;

    private AudioSource audioSource;
    private PlayerFlashlight playerFlashlight;
    private GameObject player;

    private void OnEnable()
    {
        HospitalDoctorOfficeJumpscareTrigger.OnPlayerEnter += StartJumpscare;
    }

    private void OnDisable()
    {
        HospitalDoctorOfficeJumpscareTrigger.OnPlayerEnter -= StartJumpscare;
    }

    private void Awake()
    {
        audioSource = GetComponentInChildren<AudioSource>();
        playerFlashlight = FindObjectOfType<PlayerFlashlight>();

        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void StartJumpscare()
    {
        StartCoroutine(Jumpscare());
    }

    private IEnumerator Jumpscare()
    {
        bool prevFlashlightState = playerFlashlight.flashlightOn;
        playerFlashlight.ForceFlashlightState(true);
        playerFlashlight.flashlightToggleEnabled = false;

        TurnAllLightSources(true);
        angelicaModel.SetActive(true);

        angelicaAnimator.Play("Spawn");

        if (jumpscareAudioClip) audioSource.PlayOneShot(jumpscareAudioClip);

        float time = 0f;

        while(time <= jumpscareDuration)
        {
            angelicaModel.transform.LookAt(player.transform);

            time += Time.deltaTime;
            yield return null;
        }

        TurnAllLightSources(false);
        angelicaModel.SetActive(false);

        playerFlashlight.ForceFlashlightState(prevFlashlightState);
        playerFlashlight.flashlightToggleEnabled = true;
    }

    private void ToggleAllLightSources()
    {
        foreach (LightSource lightSource in lightSources)
        {
            lightSource.ToggleLights();
        }
    }
    private void TurnAllLightSources(bool state)
    {
        foreach (LightSource lightSource in lightSources)
        {
            lightSource.TurnLights(state);
        }
    }
}
