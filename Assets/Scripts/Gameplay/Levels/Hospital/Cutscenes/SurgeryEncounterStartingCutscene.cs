using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurgeryEncounterStartingCutscene : Cutscene
{
    protected override void OnEnable()
    {
        base.OnEnable();
        SurgeryRoomEncounter.OnSurgeryRoomEncounterStarting += StartCutscene;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        SurgeryRoomEncounter.OnSurgeryRoomEncounterStarting -= StartCutscene;
    }

    protected void StartCutscene()
    {
        StartCoroutine(CutsceneCoroutine());
    }

    private IEnumerator CutsceneCoroutine()
    {
        PreCutsceneLogic();

        yield return StartCoroutine(PlayerLookTowards(cutsceneRelations[0]));
        yield return new WaitForSeconds(1.5f);
        yield return StartCoroutine(PlayerLookTowards(cutsceneRelations[1]));
        yield return new WaitForSeconds(1f);

        PostCutsceneLogic();
    }
}
