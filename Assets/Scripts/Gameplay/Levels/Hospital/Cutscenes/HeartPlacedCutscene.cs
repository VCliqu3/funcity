using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartPlacedCutscene : Cutscene
{
    [SerializeField] private UsableObject heart;

    protected override void OnEnable()
    {
        base.OnEnable();
        PlayerUse.OnObjectUsed += StartCutscene;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        PlayerUse.OnObjectUsed -= StartCutscene;
    }

    protected void StartCutscene(UsableObject usableObject)
    {
        if (usableObject != heart) return;
        StartCoroutine(CutsceneCoroutine());
    }

    private IEnumerator CutsceneCoroutine()
    {
        PreCutsceneLogic();

        yield return new WaitForSeconds(0.5f);
        yield return StartCoroutine(PlayerLookTowards(cutsceneRelations[0]));
        yield return new WaitForSeconds(0.5f);

        PostCutsceneLogic();
    }
}
