using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitHospitalTriggerEnabler : GameObjectEnabler
{
    [SerializeField] private int linkedcheckpointNumber;

    private void OnEnable()
    {
        ExitTVSetCardPanel.OnCardPlaced += EnableAllGameObjects;
        InGameCheckpointManager.OnCheckpointLoad += CheckIfShouldEnableByCheckpoint;
    }

    private void OnDisable()
    {
        ExitTVSetCardPanel.OnCardPlaced -= EnableAllGameObjects;
        InGameCheckpointManager.OnCheckpointLoad -= CheckIfShouldEnableByCheckpoint;
    }

    private void CheckIfShouldEnableByCheckpoint(int checkpointNumber, Transform checkpointTransform)
    {
        if(linkedcheckpointNumber == checkpointNumber)
        {
            EnableAllGameObjects();
        }
    }
}
