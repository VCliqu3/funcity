using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LungCardEnabler : GameObjectEnabler
{
    private void OnEnable()
    {
        LungHole.OnLungPlaced += EnableAllGameObjects;
    }

    private void OnDisable()
    {
        LungHole.OnLungPlaced -= EnableAllGameObjects;
    }
}
