using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiverCardEnabler : GameObjectEnabler
{
    private void OnEnable()
    {
        LiverHole.OnLiverPlaced += EnableAllGameObjects;
    }

    private void OnDisable()
    {
        LiverHole.OnLiverPlaced -= EnableAllGameObjects;
    }
}
