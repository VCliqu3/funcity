using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartCardEnabler : GameObjectEnabler
{
    private void OnEnable()
    {
        HeartHole.OnHeartPlaced += EnableAllGameObjects;
    }

    private void OnDisable()
    {
        HeartHole.OnHeartPlaced -= EnableAllGameObjects;
    }
}
