using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class BalanceCalibratorCorrectCalibrationRelation
{
    public BalanceCalibrator balanceCalibrator;
    [Range(0,9)] public int correctCalibration;
}

public class BalanceCalibratorsPuzzle : InteractionPuzzle
{
    [SerializeField] private List<BalanceCalibratorCorrectCalibrationRelation> balanceCalibratorCorrectCalibrationRelation;

    public static Action<BalanceCalibrator> OnBalanceCalibratorsPuzzleCompleted;

    protected override void OnEnable()
    {
        base.OnEnable();
        BalanceCalibrator.OnBalanceCalibrated += CheckBalanceCalibrations;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        BalanceCalibrator.OnBalanceCalibrated -= CheckBalanceCalibrations;
    }

    private void CheckBalanceCalibrations(BalanceCalibrator balanceCalibrator, int calibration)
    {
        if (puzzleIsCompleted) return;

        foreach (BalanceCalibratorCorrectCalibrationRelation relation in balanceCalibratorCorrectCalibrationRelation)
        {
            if (relation.balanceCalibrator.currentCalibration != relation.correctCalibration) return;
        }

        CompletePuzzle();

        OnBalanceCalibratorsPuzzleCompleted?.Invoke(balanceCalibrator);
    }
}
