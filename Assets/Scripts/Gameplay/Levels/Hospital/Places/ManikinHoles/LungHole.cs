using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LungHole : ManikinHole
{
    public static Action OnLungPlaced;

    public override void OnPlacePlaced(PlayerUse playerUse, PlaceableObject placeableObject)
    {
        base.OnPlacePlaced(playerUse, placeableObject);

        OnLungPlaced?.Invoke();
    }
}