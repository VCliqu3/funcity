using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HeartHole : ManikinHole
{
    public static Action OnHeartPlaced;

    public override void OnPlacePlaced(PlayerUse playerUse, PlaceableObject placeableObject)
    {
        base.OnPlacePlaced(playerUse, placeableObject);

        OnHeartPlaced?.Invoke();
    }
}
