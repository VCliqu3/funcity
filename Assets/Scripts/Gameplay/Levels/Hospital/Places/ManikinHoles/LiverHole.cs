using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LiverHole : ManikinHole
{
    public static Action OnLiverPlaced;

    public override void OnPlacePlaced(PlayerUse playerUse, PlaceableObject placeableObject)
    {
        base.OnPlacePlaced(playerUse, placeableObject);

        OnLiverPlaced?.Invoke();
    }
}