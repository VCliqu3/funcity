using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LiverCardPanel : CardPanel
{
    public static Action OnCardPlaced;

    public override void OnPlacePlaced(PlayerUse playerUse, PlaceableObject placeableObject)
    {
        base.OnPlacePlaced(playerUse, placeableObject);

        OnCardPlaced?.Invoke();
    }
}
