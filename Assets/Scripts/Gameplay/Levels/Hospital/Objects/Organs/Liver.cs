using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Liver : Organ
{
    [Header("Liver Config")]
    [SerializeField] private int liverID;

    public static Action<int> OnLiverPickedUp;

    public override void Place(PlayerUse playerUse, PlaceablePlace placeablePlace)
    {
        base.Place(playerUse, placeablePlace);
    }

    public override void PickUp(PlayerPickUp playerPickUp, Transform pickUpSocket)
    {
        base.PickUp(playerPickUp, pickUpSocket);

        OnLiverPickedUp?.Invoke(liverID);
    }
}
