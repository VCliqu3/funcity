using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Baby : MonoBehaviour
{
    [SerializeField] private List<BabyLever> controllingBabyLevers = new List<BabyLever>();
    [SerializeField] private Light babyLight;

    public bool enableCryingToggle = true;
    public bool isCrying;
    public bool cryingSoundPlaying;

    private AudioSource audioSource;
    [SerializeField] private AudioClip cryingBabyAudioClip;

    public static Action<Baby> OnBabyCryingStateToggled;

    private void OnEnable()
    {
        BabyLever.OnBabyLeverInteracted += CheckIfShouldToggle;
    }
    private void OnDisable()
    {
        BabyLever.OnBabyLeverInteracted -= CheckIfShouldToggle;
    }

    private void Awake()
    {
        audioSource = GetComponentInChildren<AudioSource>();
    }

    private void Start()
    {
        ForceCryingState(false);
    }

    private void Update()
    {
        CheckCryingSoundPlaying();
    }

    private void CheckIfShouldToggle(BabyLever babylever)
    {
        if (!enableCryingToggle) return;

        if (controllingBabyLevers.Contains(babylever))
        {
            ToggleBabyState();
        }
    }

    private void ToggleBabyState()
    {
        isCrying = !isCrying;
        if (babyLight) babyLight.enabled = !babyLight.enabled;

        OnBabyCryingStateToggled?.Invoke(this);
    }

    public void ForceCryingState(bool state)
    {
        isCrying = state;
        if (babyLight) babyLight.enabled = state;
    }

    private void CheckCryingSoundPlaying()
    {
        if(isCrying && !cryingSoundPlaying)
        {
            audioSource.clip = cryingBabyAudioClip;
            audioSource.Play();

            cryingSoundPlaying = true;
        }

        if(!isCrying && cryingSoundPlaying)
        {
            audioSource.clip = null;
            audioSource.Stop();

            cryingSoundPlaying = false;
        }
    }
}
