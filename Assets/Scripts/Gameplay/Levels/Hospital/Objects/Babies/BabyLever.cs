using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BabyLever : InteractableLever
{
    public static Action<BabyLever> OnBabyLeverInteracted;

    public override void Interact(PlayerInteract playerInteract)
    {
        base.Interact(playerInteract);

        OnBabyLeverInteracted?.Invoke(this);
    }
}
