using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterHospitalDoor : InteractableDoor
{
    protected override void OnEnable()
    {
        base.OnEnable();
        ExitTicketOfficeTrigger.OnPlayerEnter += DoWhenExitTicketOffice;
        EnterHospitalTrigger.OnPlayerEnter += DoWhenPlayerEnterHospital;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        ExitTicketOfficeTrigger.OnPlayerEnter -= DoWhenExitTicketOffice;
        EnterHospitalTrigger.OnPlayerEnter -= DoWhenPlayerEnterHospital;
    }

    private void DoWhenExitTicketOffice()
    {
        startOpen = true;
        if (!isOpen) OpenDoor();
        EnableInteractions();
    }

    private void DoWhenPlayerEnterHospital()
    {
        if (isOpen) CloseDoor();
        DisableInteractions();
    }
}
