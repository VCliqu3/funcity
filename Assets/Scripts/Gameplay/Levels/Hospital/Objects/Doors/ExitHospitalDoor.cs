using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitHospitalDoor : InteractableDoor
{
    protected override void OnEnable()
    {
        base.OnEnable();
        ExitHospitalCardPanel.OnCardPlaced += DoWhenCardPlaced;
        ExitHospitalDoorCloseTrigger.OnPlayerEnter += DoWhenPlayerExitHospital;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        ExitHospitalCardPanel.OnCardPlaced -= DoWhenCardPlaced;
        ExitHospitalDoorCloseTrigger.OnPlayerEnter -= DoWhenPlayerExitHospital;
    }

    private void DoWhenCardPlaced()
    {
        if(!isOpen) OpenDoor();
        EnableInteractions(); 
    }

    private void DoWhenPlayerExitHospital()
    {
        if (isOpen) CloseDoor();
        DisableInteractions();
    }
}
