using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiverDoor : InteractableDoor
{
    protected override void OnEnable()
    {
        base.OnEnable();
        LiverCardPanel.OnCardPlaced += DoWhenCardPlaced;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        LiverCardPanel.OnCardPlaced -= DoWhenCardPlaced;
    }

    private void DoWhenCardPlaced()
    {
        startOpen = true;
        if (!isOpen) OpenDoor();
        EnableInteractions();
    }
}
