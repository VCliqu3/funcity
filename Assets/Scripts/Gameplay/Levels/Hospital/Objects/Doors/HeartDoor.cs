using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartDoor : InteractableDoor
{
    protected override void OnEnable()
    {
        base.OnEnable();
        HeartCardPanel.OnCardPlaced += DoWhenCardPlaced;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        HeartCardPanel.OnCardPlaced -= DoWhenCardPlaced;
    }

    private void DoWhenCardPlaced()
    {
        startOpen = true;
        if (!isOpen) OpenDoor();
        EnableInteractions();
    }
}
