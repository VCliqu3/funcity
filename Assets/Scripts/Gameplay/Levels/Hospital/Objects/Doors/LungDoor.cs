using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LungDoor : InteractableDoor
{
    protected override void OnEnable()
    {
        base.OnEnable();
        LungCardPanel.OnCardPlaced += DoWhenCardPlaced;
        SurgeryRoomEncounter.OnSurgeryRoomEncounterStarting += DoWhenSurgeryRoomEncounterStarting;
        SurgeryRoomEncounter.OnSurgeryRoomEncounterCompleted += DoWhenSurgeryRoomEncounterCompleted;

    }

    protected override void OnDisable()
    {
        base.OnDisable();
        LungCardPanel.OnCardPlaced -= DoWhenCardPlaced;
        SurgeryRoomEncounter.OnSurgeryRoomEncounterStarting -= DoWhenSurgeryRoomEncounterStarting;
        SurgeryRoomEncounter.OnSurgeryRoomEncounterCompleted -= DoWhenSurgeryRoomEncounterCompleted;
    }

    private void DoWhenCardPlaced()
    {
        startOpen = true;
        OpenDoor();
        EnableInteractions();
    }

    private void DoWhenSurgeryRoomEncounterStarting()
    {
        if (isOpen) CloseDoor();

        DisableInteractions();
    }

    private void DoWhenSurgeryRoomEncounterCompleted()
    {
        startOpen = true;
        if (!isOpen) OpenDoor();
        EnableInteractions();
    }
}
