using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HospitalHallDoor : InteractableDoor
{
    protected override void OnEnable()
    {
        base.OnEnable();
        LuzReceptionSpeaker.OnDialogueCompleted += DoWhenLuzReceptionDialogueCompleted;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        LuzReceptionSpeaker.OnDialogueCompleted -= DoWhenLuzReceptionDialogueCompleted;
    }
    private void DoWhenLuzReceptionDialogueCompleted()
    {
        startOpen = true;
        if (!isOpen) OpenDoor();
        EnableInteractions();
    }
}
