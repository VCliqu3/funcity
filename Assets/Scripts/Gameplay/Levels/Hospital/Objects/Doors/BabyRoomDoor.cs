using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabyRoomDoor : InteractableDoor
{
    protected override void OnEnable()
    {
        base.OnEnable();
        BabyRoomEncounter.OnBabyRoomEncounterStarting += DoWhenBabyRoomEncounterStarting;
        BabyRoomEncounter.OnBabyRoomEncounterCompleted += DoWhenBabyRoomEncounterCompleted;

    }

    protected override void OnDisable()
    {
        base.OnDisable();
        BabyRoomEncounter.OnBabyRoomEncounterStarting -= DoWhenBabyRoomEncounterStarting;
        BabyRoomEncounter.OnBabyRoomEncounterCompleted -= DoWhenBabyRoomEncounterCompleted;
    }

    private void DoWhenBabyRoomEncounterStarting()
    {
        if (isOpen) CloseDoor();

        DisableInteractions();
    }

    private void DoWhenBabyRoomEncounterCompleted()
    {
        if (!isOpen) OpenDoor();

        EnableInteractions();
    }
}
