using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathologyHallDoor : InteractableDoor
{
    [Header("Pathology Hall Door Settings")]
    [SerializeField] private AudioClip fastDoorCloseClip;

    [Header("PlayerPrefs Trigger")]
    [SerializeField] private int minCheckPointToIgnorePlayerPrefs;

    protected override void OnEnable()
    {
        base.OnEnable();
        EnterPathologyTrigger.OnPlayerEnter += DoWhenPlayerEnterPathology;
        LiverCardPanel.OnCardPlaced += DoWhenCardPlaced;
        EnterPathologyTrigger.OnPlayerPrefsChecked += CheckShouldClosePlayerPrefs;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        EnterPathologyTrigger.OnPlayerEnter -= DoWhenPlayerEnterPathology;
        LiverCardPanel.OnCardPlaced -= DoWhenCardPlaced;
        EnterPathologyTrigger.OnPlayerPrefsChecked -= CheckShouldClosePlayerPrefs;
    }

    private void DoWhenPlayerEnterPathology()
    {
        rotationSpeed = 4f;

        if(isOpen) CloseDoor();

        DisableInteractions();

        if (fastDoorCloseClip) audioSource.PlayOneShot(fastDoorCloseClip);
    }

    private void DoWhenCardPlaced()
    {
        if (!isOpen) OpenDoor();

        EnableInteractions();
    }

    public void CheckShouldClosePlayerPrefs(bool hasBeenTriggeredPlayerPrefs)
    {
        if (!hasBeenTriggeredPlayerPrefs) return;

        if (InGameCheckpointManager.instance.currentCheckpointNumber >= minCheckPointToIgnorePlayerPrefs) return;

        startOpen = false;

        if (isOpen) CloseDoor();

        DisableInteractions();
    }
}
