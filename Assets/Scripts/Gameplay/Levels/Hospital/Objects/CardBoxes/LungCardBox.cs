using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LungCardBox : InteractableLid
{
    [Header("CardBoxSettings")]
    [SerializeField] private AudioClip cardBoxOpenClip;

    protected override void OnEnable()
    {
        base.OnEnable();
        LungHole.OnLungPlaced += DoWhenLungPlaced;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        LungHole.OnLungPlaced -= DoWhenLungPlaced;
    }

    private void DoWhenLungPlaced()
    {
        startOpen = true;
        if (!isOpen) OpenLid();
        interactionsEnabled = true;

        if (cardBoxOpenClip) audioSource.PlayOneShot(cardBoxOpenClip);
    }

    protected override void OpenByCheckpointLoadLogic()
    {
        DoWhenLungPlaced();
    }
}
