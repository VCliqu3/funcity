using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiverCardBox : InteractableLid
{
    [Header("CardBoxSettings")]
    [SerializeField] private AudioClip cardBoxOpenClip;

    protected override void OnEnable()
    {
        base.OnEnable();
        LiverHole.OnLiverPlaced += DoWhenLiverPlaced;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        LiverHole.OnLiverPlaced -= DoWhenLiverPlaced;
    }

    private void DoWhenLiverPlaced()
    {
        startOpen = true;
        if (!isOpen) OpenLid();
        interactionsEnabled = true;

        if (cardBoxOpenClip) audioSource.PlayOneShot(cardBoxOpenClip);
    }
    protected override void OpenByCheckpointLoadLogic()
    {
        DoWhenLiverPlaced();
    }
}
