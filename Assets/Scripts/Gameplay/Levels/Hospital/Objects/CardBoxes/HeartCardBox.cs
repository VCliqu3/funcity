using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartCardBox : InteractableLid
{
    [Header("CardBoxSettings")]
    [SerializeField] private AudioClip cardBoxOpenClip;

    protected override void OnEnable()
    {
        base.OnEnable();
        HeartHole.OnHeartPlaced += DoWhenHeartPlaced;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        HeartHole.OnHeartPlaced -= DoWhenHeartPlaced;
    }

    private void DoWhenHeartPlaced()
    {
        startOpen = true;
        if (!isOpen) OpenLid();
        interactionsEnabled = true;

        if (cardBoxOpenClip) audioSource.PlayOneShot(cardBoxOpenClip);
    }

    protected override void OpenByCheckpointLoadLogic()
    {
        DoWhenHeartPlaced();
    }
}
