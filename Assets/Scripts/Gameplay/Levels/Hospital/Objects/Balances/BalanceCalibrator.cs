using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BalanceCalibrator : InteractableObject
{
    [Header ("Balance Calibrator Settings")]
    [SerializeField] private Transform calibrator;
    [SerializeField] private Transform calibratorMinPos;
    [SerializeField] private Transform calibratorMaxPos;
    [SerializeField] private float calibratorMovementSpeed;
    [SerializeField,Range(0,9)] private int startCalibration;
    public int currentCalibration;

    private int minCalibrtation = 0;
    private int maxCalibration = 9;
    public bool calibrationGoingUp;
    private Vector3 calibratorTargetPos;

    public static Action<BalanceCalibrator,int> OnBalanceCalibrated;

    protected override void Start()
    {
        base.Start();
        ForceCalibration(startCalibration);
    }

    protected override void Update()
    {
        base.Update();

        CheckCalibratorMovement();
    }

    private void Calibrate(int cal)
    {
        Vector3 calibratorDisplacementVector = calibratorMaxPos.position - calibratorMinPos.position;
        calibratorTargetPos = calibratorMinPos.position + cal * calibratorDisplacementVector / (maxCalibration-minCalibrtation);
        currentCalibration = cal;

        if (currentCalibration <= minCalibrtation) calibrationGoingUp = true;
        if (currentCalibration >= maxCalibration) calibrationGoingUp = false;

        OnBalanceCalibrated?.Invoke(this, currentCalibration);
    }

    private void CheckCalibratorMovement()
    {
        if (calibrator.position != calibratorTargetPos)
        {
            calibrator.position = Vector3.Lerp(calibrator.position, calibratorTargetPos, calibratorMovementSpeed * Time.deltaTime);
        }
    }
    public override void Interact(PlayerInteract playerInteract)
    {
        base.Interact(playerInteract);

        if (calibrationGoingUp)
        {
            Calibrate(currentCalibration + 1);
        }
        else
        {
            Calibrate(currentCalibration - 1);
        }
    }

    private void ForceCalibration(int cal)
    {
        Vector3 calibratorDisplacementVector = calibratorMaxPos.position - calibratorMinPos.position;
        calibratorTargetPos = calibratorMinPos.position + cal * calibratorDisplacementVector / (maxCalibration - minCalibrtation);

        calibrator.position = calibratorTargetPos;
        currentCalibration = cal;

        if (currentCalibration < maxCalibration) calibrationGoingUp = true;
    }
}
