using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HospitalBalanceDrawer : InteractableDrawer
{
    [Header("Hospital Balance Drawer Settings")]
    [SerializeField] private BalanceCalibrator linkedBalanceCalibrator;
    [SerializeField] private AudioClip openHospitalBalanceDrawerClip;

    private void OnEnable()
    {
        BalanceCalibratorsPuzzle.OnBalanceCalibratorsPuzzleCompleted += CheckIfShouldOpen;
    }

    private void OnDisable()
    {
        BalanceCalibratorsPuzzle.OnBalanceCalibratorsPuzzleCompleted -= CheckIfShouldOpen;
    }

    private void CheckIfShouldOpen(BalanceCalibrator balanceCalibrator)
    {
        if(linkedBalanceCalibrator == balanceCalibrator)
        {
            if(!isOpen) OpenDrawer();
            interactionsEnabled = true;

            if (openHospitalBalanceDrawerClip) audioSource.PlayOneShot(openHospitalBalanceDrawerClip);
        }
    }
}
