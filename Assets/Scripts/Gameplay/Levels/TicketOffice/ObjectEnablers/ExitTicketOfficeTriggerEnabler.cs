using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitTicketOfficeTriggerEnabler : GameObjectEnabler
{
    [SerializeField] private int linkedcheckpointNumber;

    private void OnEnable()
    {
        TicketOfficeGatePanel.OnCardPlaced += DoWhenCardPlaced;
        InGameCheckpointManager.OnCheckpointLoad += CheckIfShouldEnableByCheckpoint;
    }

    private void OnDisable()
    {
        TicketOfficeGatePanel.OnCardPlaced -= DoWhenCardPlaced;
        InGameCheckpointManager.OnCheckpointLoad -= CheckIfShouldEnableByCheckpoint;
    }

    private void DoWhenCardPlaced(TicketOfficeGatePanel ticketOfficeGatePanel)
    {
        EnableAllGameObjects();
    }

    private void CheckIfShouldEnableByCheckpoint(int checkpointNumber, Transform checkpointTransform)
    {
        if (linkedcheckpointNumber == checkpointNumber)
        {
            EnableAllGameObjects();
        }
    }
}
