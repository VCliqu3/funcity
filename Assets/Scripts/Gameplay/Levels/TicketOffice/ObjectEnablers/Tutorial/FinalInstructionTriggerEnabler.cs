using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalInstructionTriggerEnabler : GameObjectEnabler
{
    private void OnEnable()
    {
        TicketOfficeGatePanel.OnCardPlaced += DoWhenCardPlaced;
    }

    private void OnDisable()
    {
        TicketOfficeGatePanel.OnCardPlaced -= DoWhenCardPlaced;
    }

    private void DoWhenCardPlaced(TicketOfficeGatePanel ticketOfficePanel)
    {
        EnableAllGameObjects();
    }
}
