using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropInstructionTriggerEnabler : GameObjectEnabler
{
    private void OnEnable()
    {
        TicketOfficeCard.OnTicketOfficeCardPickUp += EnableAllGameObjects;
    }

    private void OnDisable()
    {
        TicketOfficeCard.OnTicketOfficeCardPickUp -= EnableAllGameObjects;
    }
}
