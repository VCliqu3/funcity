using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpInstructionTriggerEnabler : GameObjectEnabler
{
    private void OnEnable()
    {
        LockerDoorsPuzzle.OnLockerDoorsPuzzleCompleted += EnableAllGameObjects;
    }

    private void OnDisable()
    {
        LockerDoorsPuzzle.OnLockerDoorsPuzzleCompleted -= EnableAllGameObjects;
    }
}
