using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceInstructionTriggerEnabler : GameObjectEnabler
{
    private void OnEnable()
    {
        PickableObject.OnObjectPickedUp += DoWhenObjectPickedUp;
    }

    private void OnDisable()
    {
        PickableObject.OnObjectPickedUp -= DoWhenObjectPickedUp;
    }

    private void DoWhenObjectPickedUp(PickableObject pickableObject)
    {
        EnableAllGameObjects();
    }
}
