using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TutorialNote : InteractableNote
{
    public static Action OnTutorialNoteInteracted;

    public override void Interact(PlayerInteract playerInteract)
    {
        base.Interact(playerInteract);

        OnTutorialNoteInteracted?.Invoke();
    }
}
