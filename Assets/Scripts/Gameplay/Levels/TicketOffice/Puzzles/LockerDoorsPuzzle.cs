using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

[System.Serializable]
public class LockerDoorCorrectStateRelation
{
    public LockerDoor lockerDoor;
    public bool correctState;
}

public class LockerDoorsPuzzle : InteractionPuzzle
{
    [SerializeField] private List<LockerDoorCorrectStateRelation> lockerDoorCorrectStateRelation;

    public static Action OnLockerDoorsPuzzleCompleted;
    public UnityEvent OnCompleted;

    protected override void OnEnable()
    {
        base.OnEnable();
        LockerDoor.OnLockerDoorInteracted += CheckLockerDoorStates;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        LockerDoor.OnLockerDoorInteracted -= CheckLockerDoorStates;
    }
    private void CheckLockerDoorStates(LockerDoor lockerDoor, bool isOpen)
    {
        if (puzzleIsCompleted) return;

        foreach (LockerDoorCorrectStateRelation relation in lockerDoorCorrectStateRelation)
        {
            if (relation.lockerDoor.isOpen != relation.correctState) return;
        }

        CompletePuzzle();

        OnLockerDoorsPuzzleCompleted?.Invoke();
        OnCompleted?.Invoke();
    }
}
