using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ExitTicketOfficeTrigger : ColliderDetectionTrigger
{
    public static Action OnPlayerEnter;

    protected override void DoWhenPlayerDetected()
    {
        base.DoWhenPlayerDetected();

        OnPlayerEnter?.Invoke();
    }
    protected override void OnEnable()
    {
        base.OnEnable();
        TicketOfficeGatePanel.OnCardPlaced += DoWhenTicketOfficeCardPlaced;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        TicketOfficeGatePanel.OnCardPlaced -= DoWhenTicketOfficeCardPlaced;
    }

    private void DoWhenTicketOfficeCardPlaced(TicketOfficeGatePanel ticketOfficeGatePanel)
    {
        canBeTriggered = true;
    }
}
