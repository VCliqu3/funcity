using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockersPuzzleCompletedCutscene : Cutscene
{
    protected override void OnEnable()
    {
        base.OnEnable();
        LockerDoorsPuzzle.OnLockerDoorsPuzzleCompleted += StartCutscene;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        LockerDoorsPuzzle.OnLockerDoorsPuzzleCompleted -= StartCutscene;
    }

    protected void StartCutscene()
    {
        StartCoroutine(CutsceneCoroutine());
    }

    private IEnumerator CutsceneCoroutine()
    {
        PreCutsceneLogic();

        yield return new WaitForSeconds(0.5f);
        yield return StartCoroutine(PlayerLookTowards(cutsceneRelations[0]));
        yield return new WaitForSeconds(0.5f);

        PostCutsceneLogic();
    }
}
