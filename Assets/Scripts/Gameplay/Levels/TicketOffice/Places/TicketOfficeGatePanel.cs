using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class TicketOfficeGatePanel : PlaceablePlace
{
    public static Action<TicketOfficeGatePanel> OnCardPlaced;
    public UnityEvent OnPlaced;

    public override void OnPlacePlaced(PlayerUse playerUse, PlaceableObject placeableObject)
    {
        base.OnPlacePlaced(playerUse, placeableObject);

        OnCardPlaced?.Invoke(this);
        OnPlaced?.Invoke();
    }
}
