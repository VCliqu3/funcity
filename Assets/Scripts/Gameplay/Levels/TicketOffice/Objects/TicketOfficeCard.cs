using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class TicketOfficeCard : Card
{
    public static Action OnTicketOfficeCardPickUp;
    public static Action OnTicketOfficeCardDrop;

    public UnityEvent OnPickUp;

    public override void Place(PlayerUse playerUse, PlaceablePlace placeablePlace)
    {
        base.Place(playerUse, placeablePlace);
    }

    public override void PickUp(PlayerPickUp playerPickUp, Transform pickUpSocket)
    {
        base.PickUp(playerPickUp, pickUpSocket);

        OnTicketOfficeCardPickUp?.Invoke();
        OnPickUp?.Invoke();
    }

    public override void Drop(PlayerPickUp playerPickUp)
    {
        base.Drop(playerPickUp);

        OnTicketOfficeCardDrop?.Invoke();
    }
}
