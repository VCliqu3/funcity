using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TicketOfficeGate : InteractableDoor
{
    [Header ("Ticket Office Gate Settings")]
    [SerializeField] private TicketOfficeGatePanel linkedPanel;

    protected override void OnEnable()
    {
        base.OnEnable();
        TicketOfficeGatePanel.OnCardPlaced += DoWhenCardPlaced;
        ExitTicketOfficeTrigger.OnPlayerEnter += DoWhenTriggerEnter;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        TicketOfficeGatePanel.OnCardPlaced -= DoWhenCardPlaced;
        ExitTicketOfficeTrigger.OnPlayerEnter -= DoWhenTriggerEnter;
    }

    private void DoWhenCardPlaced(TicketOfficeGatePanel panel)
    {
        if (linkedPanel != panel) return;
        OpenDoor();
    }

    private void DoWhenTriggerEnter()
    {
        if(isOpen) CloseDoor();
    }
}
