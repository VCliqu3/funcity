using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LockerDoor : InteractableDoor
{
    public static Action<LockerDoor,bool> OnLockerDoorInteracted;

    public override void Interact(PlayerInteract playerInteract)
    {
        base.Interact(playerInteract);

        OnLockerDoorInteracted?.Invoke(this, isOpen);
    }
}
