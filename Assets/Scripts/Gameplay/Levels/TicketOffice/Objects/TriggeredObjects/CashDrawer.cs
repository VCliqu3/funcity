using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CashDrawer : InteractableDrawer
{
    [Header("Cash Drawer Settings")]
    [SerializeField] private AudioClip cashClip;

    private void OnEnable()
    {
        LockerDoorsPuzzle.OnLockerDoorsPuzzleCompleted += DoWhenPuzzleCompleted;
    }

    private void OnDisable()
    {
        LockerDoorsPuzzle.OnLockerDoorsPuzzleCompleted -= DoWhenPuzzleCompleted;
    }

    private void DoWhenPuzzleCompleted()
    {
        if(!isOpen) OpenDrawer();
        interactionsEnabled = true;

        if (cashClip) audioSource.PlayOneShot(cashClip);
    }
}
