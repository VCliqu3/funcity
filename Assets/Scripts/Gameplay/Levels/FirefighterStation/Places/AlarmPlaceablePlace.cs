using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlarmPlaceablePlace : PlaceablePlace
{
    public override void OnPlacePlaced(PlayerUse playerUse, PlaceableObject placeableObject)
    {
        base.OnPlacePlaced(playerUse, placeableObject);

        //Debug.Log("Object placed in alarm");
    }
}
