using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlassPanel : PuzzleEnabledUsablePlace
{
    [SerializeField] private GameObject physicalGlassPanel;

    public override void OnPlaceUsed(PlayerUse playerUse, UsableObject usableObject)
    {
        base.OnPlaceUsed(playerUse, usableObject);

        Debug.Log("Using " + gameObject.name);

        Destroy(physicalGlassPanel);
    }
}


