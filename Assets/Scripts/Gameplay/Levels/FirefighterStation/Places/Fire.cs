using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : PuzzleEnabledUsablePlace
{
    [SerializeField] private GameObject physicalFire;

    public override void OnPlaceUsed(PlayerUse playerUse, UsableObject usableObject)
    {
        base.OnPlaceUsed(playerUse, usableObject);

        Debug.Log("Using " + gameObject.name);

        Destroy(physicalFire);
    }
}


