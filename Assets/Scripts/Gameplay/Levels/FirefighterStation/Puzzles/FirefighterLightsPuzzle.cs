using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FireFighterLightCorrectStateRelation
{
    public FirefighterLight firefighterLight;
    public bool correctState;
}

public class FirefighterLightsPuzzle : InteractionPuzzle
{
    [SerializeField] private List<FireFighterLightCorrectStateRelation> fireFighterLightCorrectStateRelation;

    protected override void CheckObjectInteracted(InteractableObject interactableObject)
    {
        base.CheckObjectInteracted(interactableObject);
    }

    protected override bool CheckIfThisPuzzleCompleted()
    {
        if (base.CheckIfThisPuzzleCompleted()) return true;

        foreach(FireFighterLightCorrectStateRelation relation in fireFighterLightCorrectStateRelation)
        {
            if(relation.firefighterLight.lightOn != relation.correctState) return false; 
        }

        CompletePuzzle();

        return true;
    }
}

