using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlarmPuzzle : PlacePuzzle
{
    protected override void CheckObjectUsedInPlace(UsableObject usableObject, UsablePlace usablePlace)
    {
        base.CheckObjectUsedInPlace(usableObject, usablePlace);
    }

    protected override bool CheckIfThisPuzzleCompleted()
    {
        if(base.CheckIfThisPuzzleCompleted()) return true;

        return false;
    }
}
