using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Extinguisher : PuzzleEnabledUsableObject
{
    public override void Use(PlayerUse playerUse, UsablePlace usablePlace)
    {
        base.Use(playerUse, usablePlace);

        Debug.Log("Using " + gameObject.name);

        playerPickUp.currentPickedUpObject = null;
        Destroy(gameObject);
    }

    public override void UseWithoutUsablePlace(PlayerUse playerUse)
    {
        base.UseWithoutUsablePlace(playerUse);

        Debug.Log("Using " + gameObject.name + "Without Usable Place");
    }
}
