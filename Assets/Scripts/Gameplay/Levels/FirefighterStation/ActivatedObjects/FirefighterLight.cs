using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FirefighterLight : InteractionTriggeredObject
{
    [SerializeField] private Light _light;
    [SerializeField] private bool startOn;
    public bool lightOn;

    public static Action<FirefighterLight> OnFirefighterLightToggled;

    private void Start()
    {
        ForceLightState(startOn);
    }

    protected override bool CheckIfThisObjectShouldTrigger()
    {
        if(!(base.CheckIfThisObjectShouldTrigger())) return false;

        ToggleLight();
        
        TriggerObject();

        return true;
    }

    private void ToggleLight()
    {
        lightOn = !lightOn;
        UpdateLightState();

        OnFirefighterLightToggled?.Invoke(this);
    }


    private void ForceLightState(bool state)
    {
        lightOn = state;
        UpdateLightState();
    }

    private void UpdateLightState()
    {
        _light.enabled = lightOn;
    }
}