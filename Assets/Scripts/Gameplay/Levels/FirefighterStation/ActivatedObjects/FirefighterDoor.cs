using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirefighterDoor : PuzzleTriggeredGate
{
    protected override bool CheckIfThisObjectShouldTrigger()
    {
        if ((!base.CheckIfThisObjectShouldTrigger())) return false;

        return true;
    }
}
