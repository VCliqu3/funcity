using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TVSetWarehouseEncounterDeath : AngelicaDeath
{
    protected override void OnEnable()
    {
        base.OnEnable();
        TVSetWarehouseEncounter.OnTimeToDie += StartDeathCoroutine;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        TVSetWarehouseEncounter.OnTimeToDie -= StartDeathCoroutine;
    }

    protected void StartDeathCoroutine()
    {
        StartCoroutine(DeathCoroutine());
    }

    protected IEnumerator DeathCoroutine()
    {
        base.PreDeathLogic();

        angelicaAnimator.Play(angelicaDeathAnimation);

        yield return StartCoroutine(PlayerLookTowards());

        cameraAnimationHolder.SetParent(null);
        cameraAnimationHolder.LookAt(transform);
        playerCamera.localRotation = Quaternion.Euler(Vector3.zero);
        playerCamera.localPosition = Vector3.zero;

        cameraAnimationHolderAnimator.SetTrigger(playerDeathAnimationTriggerName);

        yield return new WaitForSeconds(cameraAnimationHolderDuration);

        OnAngelicaDeath?.Invoke();
    }
}
