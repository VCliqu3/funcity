using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TVSetLettersLights : LightSource
{
    [Header("Load Checkpoint Settings")]
    [SerializeField] private int linkedcheckpointNumber;

    private Material material;

    private void OnEnable()
    {
        InGameCheckpointManager.OnCheckpointLoad += CheckActivateByCheckpointLoad;

        ExitHospitalTrigger.OnPlayerEnter += DoWhenExitHospital;
        ExitTVSetCardPanel.OnCardPlaced += DoWhenExitTVSetCardPlaced;
    }

    private void OnDisable()
    {
        InGameCheckpointManager.OnCheckpointLoad -= CheckActivateByCheckpointLoad;

        ExitHospitalTrigger.OnPlayerEnter -= DoWhenExitHospital;
        ExitTVSetCardPanel.OnCardPlaced -= DoWhenExitTVSetCardPlaced;
    }

    protected override void Awake()
    {
        base.Awake();
        material = GetComponentInChildren<Renderer>().material;
    }

    private void DoWhenExitHospital()
    {
        StartCoroutine(TurnLettersOn());
    }

    private IEnumerator TurnLettersOn()
    {
        TurnLights(true);

        yield return new WaitForSeconds(0.2f);

        TurnLights(false);

        yield return new WaitForSeconds(0.3f);

        TurnLights(true);

        yield return new WaitForSeconds(0.1f);

        TurnLights(false);

        yield return new WaitForSeconds(0.2f);

        TurnLights(true);

        yield return new WaitForSeconds(0.4f);

        TurnLights(false);

        yield return new WaitForSeconds(0.4f);

        TurnLights(true);
    }

    private void DoWhenExitTVSetCardPlaced()
    {
        TurnLights(false);
    }

    public override void TurnLights(bool state)
    {
        base.TurnLights(state);

        if (state)
        {
            material.EnableKeyword("_EMISSION");
        }
        else
        {
            material.DisableKeyword("_EMISSION");
        }
    }

    private void CheckActivateByCheckpointLoad(int checkpointNumber, Transform checkpointTransform)
    {
        if (linkedcheckpointNumber != checkpointNumber) return;
        DoWhenExitHospital();
    }
}
