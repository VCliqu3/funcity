using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotativeCamerasPuzzleCompleteLights : MonoBehaviour
{
    [SerializeField] private List<Light> lights = new List<Light>();

    private void OnEnable()
    {
        RotativeCamerasPuzzle.OnRotativeCamerasPuzzleCompleted += ToggleLights;
    }

    private void OnDisable()
    {
        RotativeCamerasPuzzle.OnRotativeCamerasPuzzleCompleted -= ToggleLights;
    }

    public virtual void ToggleLights()
    {
        foreach (Light light in lights)
        {
            light.enabled = !light.enabled;
        }
    }
}
