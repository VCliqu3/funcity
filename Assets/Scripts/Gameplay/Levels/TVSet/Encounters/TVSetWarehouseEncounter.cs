using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TVSetWarehouseEncounter : RegularEncounter
{
    [Header("TV Warehouse Encounter Settings")]
    [SerializeField] private int maxFails = 1;
    [SerializeField] private int fails = 0;
    [SerializeField] private List<TVSetWarehouseDrawer> correctDrawers = new List<TVSetWarehouseDrawer>();
    [SerializeField] private List<TVSetWarehouseDrawer> openedDrawers = new List<TVSetWarehouseDrawer>();

    public static Action OnTVSetWarehouseEncounterStarting;
    public static Action OnTVSetWarehouseEncounterStart;
    public static Action OnTVSetWarehouseEncounterEnding;
    public static Action OnTVSetWarehouseEncounterEnd;

    public static Action OnTVSetWarehouseEncounterCompleted;
    public static Action OnTVSetWarehouseEncounterFailed;

    public static Action OnTimeToDie;

    protected void OnEnable()
    {
        TVSetWarehouseEncounterTrigger.OnPlayerEnter += DoWhenPlayerEnterTrigger;
        TVSetWarehouseDrawer.OnTVSetWarehouseDrawerOpened += WhenDrawerOpened;
    }

    protected void OnDisable()
    {
        TVSetWarehouseEncounterTrigger.OnPlayerEnter -= DoWhenPlayerEnterTrigger;
        TVSetWarehouseDrawer.OnTVSetWarehouseDrawerOpened -= WhenDrawerOpened;
    }

    protected void WhenDrawerOpened(TVSetWarehouseDrawer drawer, bool hasBeenOpenedBefore)
    {
        if (encounterState != EncounterState.Playing) return;
        if (hasBeenOpenedBefore) return;

        openedDrawers.Add(drawer);

        if (!correctDrawers.Contains(drawer))
        {
            fails++;

            if (fails >= maxFails)
            {
                SetEncounterResultState(EncounterResultState.Failed);
                SetEncounterState(EncounterState.Ending);
                return;
            }
        }

        foreach (TVSetWarehouseDrawer correctDrawer in correctDrawers)
        {
            if (!openedDrawers.Contains(correctDrawer)) return;
        }

        SetEncounterResultState(EncounterResultState.Completed);
        SetEncounterState(EncounterState.Ending);
    }

    #region OnEncounter States
    protected override void OnEncounterStarting()
    {
        base.OnEncounterStarting();

        SetTVWarehouseDrawersInteractableState(false);

        //CloseTVWarehouseDrawers();

        OnTVSetWarehouseEncounterStarting?.Invoke();
    }

    protected override void OnEncounterStart()
    {
        base.OnEncounterStart();

        SetTVWarehouseDrawersInteractableState(true);

        OnTVSetWarehouseEncounterStart?.Invoke();
    }
    protected override void OnEncounterEnding()
    {
        base.OnEncounterEnding();

        OnTVSetWarehouseEncounterEnding?.Invoke();
    }
    protected override void OnEncounterEnd()
    {
        base.OnEncounterEnd();
        OnTVSetWarehouseEncounterEnd?.Invoke();
    }

    #endregion

    #region OnEncounter Result States
    protected override void OnEncounterCompleted()
    {
        base.OnEncounterCompleted();

        OnTVSetWarehouseEncounterCompleted?.Invoke();
    }
    protected override void OnEncounterFailed()
    {
        base.OnEncounterFailed();
        OnTVSetWarehouseEncounterFailed?.Invoke();
    }
    #endregion

    private void SetTVWarehouseDrawersInteractableState(bool state)
    {
        TVSetWarehouseDrawer[] drawers = FindObjectsOfType<TVSetWarehouseDrawer>();

        foreach (TVSetWarehouseDrawer drawer in drawers)
        {
            drawer.interactionsEnabled = state;
        }
    }

    private void CloseTVWarehouseDrawers()
    {
        TVSetWarehouseDrawer[] drawers = FindObjectsOfType<TVSetWarehouseDrawer>();

        foreach (TVSetWarehouseDrawer drawer in drawers)
        {
            drawer.CloseDrawer();
        }
    }

    protected override void OnTimeToDiePassed()
    {
        base.OnTimeToDiePassed();

        OnTimeToDie?.Invoke();
    }
}
