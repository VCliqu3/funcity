using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TVSetParkingJumpscareTrigger : ColliderDetectionTrigger
{
    public static Action OnPlayerEnter;

    protected override void DoWhenPlayerDetected()
    {
        base.DoWhenPlayerDetected();

        OnPlayerEnter?.Invoke();
    }
    protected override void OnEnable()
    {
        base.OnEnable();
        AngelicaTVSetParkingSpawnTrigger.OnPlayerEnter += DoWhenTVSetParkingAngelicaSpawned;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        AngelicaTVSetParkingSpawnTrigger.OnPlayerEnter -= DoWhenTVSetParkingAngelicaSpawned;
    }

    private void DoWhenTVSetParkingAngelicaSpawned()
    {
        canBeTriggered = true;
    }
}
