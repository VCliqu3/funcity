using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class AngelicaTVSetParkingSpawnTrigger : ColliderDetectionTrigger
{
    [Header("PlayerPrefs Trigger")]
    [SerializeField] private bool disableByPlayerPrefs;
    [SerializeField] private string playerPrefsKeyName;
    [SerializeField] private bool hasBeenTriggeredPlayerPrefs;
    [SerializeField] private int maxCheckpointToDisableByPlayerPrefs;

    public static Action OnPlayerEnter;
    public UnityEvent OnTrigger;

    #region PlayerPrefsEventTrigger Enable, Disable & Awake
    protected override void OnEnable()
    {
        base.OnEnable();
        InGameCheckpointManager.OnCheckpointLoad += CheckClearPlayerPrefsByCheckpointLoad;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        InGameCheckpointManager.OnCheckpointLoad -= CheckClearPlayerPrefsByCheckpointLoad;
    }

    protected override void Awake()
    {
        base.Awake();
        CheckInitializePlayerPrefsKey();
    }
    #endregion

    protected override void DoWhenPlayerDetected()
    {
        if (hasBeenTriggeredPlayerPrefs && disableByPlayerPrefs) return;

        base.DoWhenPlayerDetected();

        OnPlayerEnter?.Invoke();
        OnTrigger?.Invoke();

        SetPlayerPrefsKey(true);
    }

    #region PlayerPrefsEventTriggerMethods

    private void CheckClearPlayerPrefsByCheckpointLoad(int checkpointNumber, Transform checkpointTransform)
    {
        if (checkpointNumber <= maxCheckpointToDisableByPlayerPrefs)
        {
            SetPlayerPrefsKey(false);
            UpdatePlayerPrefsBoolean();
        }
    }

    private void CheckInitializePlayerPrefsKey()
    {
        if (!PlayerPrefs.HasKey(playerPrefsKeyName))
        {
            PlayerPrefs.SetInt(playerPrefsKeyName, 0);
        }

        UpdatePlayerPrefsBoolean();
    }

    private void SetPlayerPrefsKey(bool state)
    {
        int value = state == false ? 0 : 1;
        PlayerPrefs.SetInt(playerPrefsKeyName, value);

        UpdatePlayerPrefsBoolean();
    }

    private void UpdatePlayerPrefsBoolean()
    {
        hasBeenTriggeredPlayerPrefs = PlayerPrefs.GetInt(playerPrefsKeyName) == 0 ? false : true;
    }
    #endregion
}
