using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ExitTVSetDoorCloseTrigger : ColliderDetectionTrigger
{
    public static Action OnPlayerEnter;

    protected override void DoWhenPlayerDetected()
    {
        base.DoWhenPlayerDetected();

        OnPlayerEnter?.Invoke();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        ExitTVSetCardPanel.OnCardPlaced += DoWhenExitTVSetCardPlaced;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        ExitTVSetCardPanel.OnCardPlaced -= DoWhenExitTVSetCardPlaced;
    }

    private void DoWhenExitTVSetCardPlaced()
    {
        canBeTriggered = true;
    }
}
