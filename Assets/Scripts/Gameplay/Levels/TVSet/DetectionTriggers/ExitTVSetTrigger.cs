using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ExitTVSetTrigger : ColliderDetectionTrigger
{
    public static Action OnPlayerEnter;

    protected override void DoWhenPlayerDetected()
    {
        base.DoWhenPlayerDetected();

        OnPlayerEnter?.Invoke();

        Debug.Log("Angelica should despawn");
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        ExitTVSetCardPanel.OnCardPlaced += DoWhenExitTVSetCardPlaced;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        ExitTVSetCardPanel.OnCardPlaced -= DoWhenExitTVSetCardPlaced;
    }

    private void DoWhenExitTVSetCardPlaced()
    {
        canBeTriggered = true;
    }
}
