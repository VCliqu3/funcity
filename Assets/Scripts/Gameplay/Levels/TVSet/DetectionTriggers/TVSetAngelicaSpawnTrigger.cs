using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class TVSetAngelicaSpawnTrigger : ColliderDetectionTrigger
{
    public static Action OnPlayerEnter;

    public UnityEvent OnTrigger;

    protected override void DoWhenPlayerDetected()
    {
        base.DoWhenPlayerDetected();

        OnPlayerEnter?.Invoke();
        OnTrigger?.Invoke();

        Debug.Log("Angelica should spawn");
    }
    protected override void OnEnable()
    {
        base.OnEnable();
        ExitTVSetCard.OnCardPickUp += DoWhenExitTVSetCardPickUp;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        ExitTVSetCard.OnCardPickUp -= DoWhenExitTVSetCardPickUp;
    }

    private void DoWhenExitTVSetCardPickUp()
    {
        canBeTriggered = true;
    }
}
