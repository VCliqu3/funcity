using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class PanelPuzzle : InteractionPuzzle
{
    [SerializeField] private List<PanelButton> correctSequence = new List<PanelButton>();
    [SerializeField] private List<PanelButton> pressedSequence = new List<PanelButton>();

    [SerializeField] private PanelButton resetButton;

    [SerializeField] private AudioClip wrongSequenceAudioClip;

    [SerializeField] private Card hallCard;

    private AudioSource audioSource;

    public static Action OnPanelPuzzleCompleted;
    public static Action OnPanelPuzzleReset;

    public UnityEvent OnComplete;

    protected override void OnEnable()
    {
        base.OnEnable();
        PanelButton.OnButtonPressed += CheckButtonsPressed;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        PanelButton.OnButtonPressed -= CheckButtonsPressed;
    }

    private void Awake()
    {
        audioSource = GetComponentInChildren<AudioSource>();
    }

    private void CheckButtonsPressed(PanelButton panelButton)
    {
        if (puzzleIsCompleted) return;

        if (resetButton == panelButton)
        {
            ResetSequence();
            return;
        }

        pressedSequence.Add(panelButton);

        if (pressedSequence.Count == correctSequence.Count) CheckPressedSequence();

    }

    private void CheckPressedSequence()
    {
        for (int i = 0; i < correctSequence.Count; i++)
        {
            if (!correctSequence[i].Equals(pressedSequence[i]))
            {
                if (wrongSequenceAudioClip) audioSource.PlayOneShot(wrongSequenceAudioClip);

                ResetSequence();
                OnPanelPuzzleReset?.Invoke();
                return; 
            }
        }

        CompletePuzzle();
        OnPanelPuzzleCompleted?.Invoke();
        OnComplete?.Invoke();
    }

    private void ResetSequence()
    {
        pressedSequence.Clear();
        OnPanelPuzzleReset?.Invoke();
    }

    protected override void CompleteByCheckpointLoadLogic()
    {
        hallCard.onEnableEventsEnabled = false;

        CompletePuzzle();
        OnPanelPuzzleCompleted?.Invoke();
        OnComplete?.Invoke();
    }
}
