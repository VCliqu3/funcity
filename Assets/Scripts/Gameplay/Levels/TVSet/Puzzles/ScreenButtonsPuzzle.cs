using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class ScreenButtonCorrectStateRelation
{
    public ScreenButton screenButton;
    public bool correctState;
}

public class ScreenButtonsPuzzle : InteractionPuzzle
{
    [SerializeField] private List<ScreenButtonCorrectStateRelation> screenButtonCorrectStateRelation;

    public static Action<bool> OnScreenButtonsPuzzleCompleted;
    protected override void OnEnable()
    {
        base.OnEnable();
        ScreenButton.OnScreenToggled += CheckScreenStates;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        ScreenButton.OnScreenToggled -= CheckScreenStates;
    }
    private void CheckScreenStates(ScreenButton screenButton, bool isOn)
    {
        if (puzzleIsCompleted) return;

        foreach (ScreenButtonCorrectStateRelation relation in screenButtonCorrectStateRelation)
        {
            if (relation.screenButton.screenIsOn != relation.correctState) return;
        }

        CompletePuzzle();
        OnScreenButtonsPuzzleCompleted?.Invoke(false); //false si se completo naturalmente
    }

    protected override void CompleteByCheckpointLoadLogic()
    {
        CompletePuzzle();
        OnScreenButtonsPuzzleCompleted?.Invoke(true); //true si se autoCompleto (por checkpoint)
    }
}
