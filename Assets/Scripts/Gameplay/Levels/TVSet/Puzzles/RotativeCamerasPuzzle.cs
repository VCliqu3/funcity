using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

[System.Serializable]
public class RotativeCameraCorrectRotationIndexRelation
{
    public RotativeCamera rotativeCamera;
    [Range(1,12)] public int correctRotationIndex;
}

public class RotativeCamerasPuzzle : InteractionPuzzle
{
    [SerializeField] private List<RotativeCameraCorrectRotationIndexRelation> rotativeCameraCorrectRotationIndexRelation;

    [SerializeField] private Card exitTVSetCard;

    public UnityEvent OnComplete;

    public static Action OnRotativeCamerasPuzzleCompleted;
    protected override void OnEnable()
    {
        base.OnEnable();
        RotativeCamera.OnCameraRotated += CheckCameraRotations;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        RotativeCamera.OnCameraRotated -= CheckCameraRotations;
    }
    private void CheckCameraRotations(RotativeCamera balanceCalibrator, int rotationIndex)
    {
        if (puzzleIsCompleted) return;

        foreach (RotativeCameraCorrectRotationIndexRelation relation in rotativeCameraCorrectRotationIndexRelation)
        {
            if (relation.rotativeCamera.currentRotationIndex != relation.correctRotationIndex) return;
        }

        CompletePuzzle();
        OnRotativeCamerasPuzzleCompleted?.Invoke();
        OnComplete?.Invoke();
    }

    protected override void CompleteByCheckpointLoadLogic()
    {
        exitTVSetCard.onEnableEventsEnabled = false;

        CompletePuzzle();
        OnRotativeCamerasPuzzleCompleted?.Invoke();
        OnComplete?.Invoke();
    }
}
