using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngelicaPersecutionTriggerEnabler : GameObjectEnabler
{
    [SerializeField] private PickableObject exitTVSetCard;
    private void OnEnable()
    {
        PickableObject.OnObjectPickedUp += ChekIfObjectPickedUpIsCard;
    }

    private void OnDisable()
    {
        PickableObject.OnObjectPickedUp -= ChekIfObjectPickedUpIsCard;
    }

    private void ChekIfObjectPickedUpIsCard(PickableObject pickableObject)
    {
        if (pickableObject == exitTVSetCard)
        {
            EnableAllGameObjects();
            Debug.Log("Trigger Enabled");
        }
    }
}
