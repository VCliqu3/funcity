using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitTVSetTriggerEnabler : GameObjectEnabler
{
    [SerializeField] private int linkedcheckpointNumber;

    private void OnEnable()
    {
        EnterTVSetTrigger.OnPlayerEnter += EnableAllGameObjects;

        InGameCheckpointManager.OnCheckpointLoad += CheckIfShouldEnableByCheckpoint;
    }

    private void OnDisable()
    {
        EnterTVSetTrigger.OnPlayerEnter -= EnableAllGameObjects;

        InGameCheckpointManager.OnCheckpointLoad -= CheckIfShouldEnableByCheckpoint;
    }

    private void CheckIfShouldEnableByCheckpoint(int checkpointNumber, Transform checkpointTransform)
    {
        if (linkedcheckpointNumber == checkpointNumber)
        {
            EnableAllGameObjects();
        }
    }
}
