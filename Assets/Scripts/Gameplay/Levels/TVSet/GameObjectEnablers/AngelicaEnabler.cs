using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngelicaEnabler : GameObjectEnabler
{
    private void OnEnable()
    {
        TVSetAngelicaSpawnTrigger.OnPlayerEnter += EnableAllGameObjects;
    }

    private void OnDisable()
    {
        TVSetAngelicaSpawnTrigger.OnPlayerEnter -= EnableAllGameObjects;
    }
}
