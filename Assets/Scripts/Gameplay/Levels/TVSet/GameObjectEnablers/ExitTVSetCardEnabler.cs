using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitTVSetCardEnabler : GameObjectEnabler
{
    private void OnEnable()
    {
        RotativeCamerasPuzzle.OnRotativeCamerasPuzzleCompleted += EnableAllGameObjects;
    }

    private void OnDisable()
    {
        RotativeCamerasPuzzle.OnRotativeCamerasPuzzleCompleted -= EnableAllGameObjects;
    }

}
