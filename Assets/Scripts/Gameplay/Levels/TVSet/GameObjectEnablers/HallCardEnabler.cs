using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HallCardEnabler : GameObjectEnabler
{
    private void OnEnable()
    {
        PanelPuzzle.OnPanelPuzzleCompleted += EnableAllGameObjects;
    }

    private void OnDisable()
    {
        PanelPuzzle.OnPanelPuzzleCompleted -= EnableAllGameObjects;
    }
}
