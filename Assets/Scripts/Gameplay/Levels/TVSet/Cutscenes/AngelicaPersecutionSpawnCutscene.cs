using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngelicaPersecutionSpawnCutscene : Cutscene
{
    protected override void OnEnable()
    {
        base.OnEnable();
        PersecutionAngelica.OnAngelicaSpawn += StartCutscene;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        PersecutionAngelica.OnAngelicaSpawn -= StartCutscene;
    }

    protected void StartCutscene()
    {
        StartCoroutine(CutsceneCoroutine());
    }

    private IEnumerator CutsceneCoroutine()
    {
        PreCutsceneLogic();

        yield return StartCoroutine(PlayerLookTowards(cutsceneRelations[0]));
        yield return new WaitForSeconds(1f);

        PostCutsceneLogic();
    }
}
