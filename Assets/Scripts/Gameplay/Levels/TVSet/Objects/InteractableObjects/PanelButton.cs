using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PanelButton : InteractableButton
{
    [Header("Panel Button Settings")]
    [SerializeField] private List<Light> lights = new List<Light>();

    private bool lightsEnabled;

    public static Action<PanelButton> OnButtonPressed;

    private bool puzzleCompleted = false;

    private void OnEnable()
    {
        PanelPuzzle.OnPanelPuzzleReset += DoWhenReset;
        PanelPuzzle.OnPanelPuzzleCompleted += DoWhenPuzzleCompleted;
    }
    private void OnDisable()
    {
        PanelPuzzle.OnPanelPuzzleReset -= DoWhenReset;
        PanelPuzzle.OnPanelPuzzleCompleted -= DoWhenPuzzleCompleted;
    }

    protected override void Start()
    {
        base.Start();

        if (!puzzleCompleted)
        {
            TurnLights(false);
            lightsEnabled = true;
        }
    }

    public override void Interact(PlayerInteract playerInteract)
    {
        base.Interact(playerInteract);

        if(lightsEnabled) TurnLights(true);

        OnButtonPressed?.Invoke(this);
    }

    private void DoWhenReset()
    {
        TurnLights(false);
    }

    private void DoWhenPuzzleCompleted()
    {
        lightsEnabled = false;
        puzzleCompleted = true;
    }

    private void TurnLights(bool state)
    {
        foreach(Light light in lights)
        {
            light.enabled = state;
        }
    }
}
