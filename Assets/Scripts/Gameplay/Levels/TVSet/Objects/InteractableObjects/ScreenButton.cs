using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ScreenButton : InteractableButton
{
    [Header("Screen Button Settings")]
    [SerializeField] private TVSetPuzzleScreen puzzleScreen;
    [SerializeField] private bool startScreenOn;

    public bool screenIsOn;

    public static Action<ScreenButton,bool> OnScreenToggled;

    protected override void Start()
    {
        base.Start();
        //ForceScreenState(startScreenOn);
    }

    public override void Interact(PlayerInteract playerInteract)
    {
        base.Interact(playerInteract);

        ToggleScreen();
    }

    private void ToggleScreen()
    {
        screenIsOn = !screenIsOn;
        UpdateScreenState();

        OnScreenToggled?.Invoke(this, screenIsOn);
    }

    private void ForceScreenState(bool state)
    {
        screenIsOn = state;
        UpdateScreenState();
    }

    private void UpdateScreenState()
    {
        if(puzzleScreen) puzzleScreen.SetScreenFlashing(screenIsOn);
    }
}
