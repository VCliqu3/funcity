using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RotativeCamera : InteractableObject
{
    [Header("Rotative Camera Settings")]
    [SerializeField] private float cameraRotationSpeed;
    [SerializeField, Range(1, 12)] private int startRotationIndex;
    public int currentRotationIndex;

    [SerializeField] private AudioClip cameraRotationAudioClip;

    private int minRotationIndex = 1;
    private int maxRotationIndex = 12;
    private Quaternion cameraTargetRotation;

    public static Action<RotativeCamera, int> OnCameraRotated;

    protected override void Start()
    {
        base.Start();
        ForceCameraRotation(startRotationIndex);
    }

    protected override void Update()
    {
        base.Update();

        CheckCameraRotation();
    }
    private void RotateCamera(int rotationIndex)
    {
        float angleToRotate = 360f/ (maxRotationIndex - minRotationIndex + 1);

        cameraTargetRotation = Quaternion.Euler(transform.localRotation.x, angleToRotate * rotationIndex, transform.localRotation.z);
        currentRotationIndex = rotationIndex;

        if (cameraRotationAudioClip) audioSource.PlayOneShot(cameraRotationAudioClip);

        OnCameraRotated?.Invoke(this, currentRotationIndex);
    }

    private void CheckCameraRotation()
    {
        if (transform.localRotation != cameraTargetRotation)
        {
            transform.localRotation = Quaternion.Lerp(transform.localRotation, cameraTargetRotation, cameraRotationSpeed * Time.deltaTime);
        }
    }
    public override void Interact(PlayerInteract playerInteract)
    {
        base.Interact(playerInteract);

        if (currentRotationIndex == maxRotationIndex)
        {
            RotateCamera(minRotationIndex);
        }
        else
        {
            RotateCamera(currentRotationIndex + 1);
        }
    }

    private void ForceCameraRotation(int rotationIndex)
    {
        float angleToRotate = 360f / (maxRotationIndex - minRotationIndex + 1);

        cameraTargetRotation = Quaternion.Euler(transform.localRotation.x, angleToRotate * rotationIndex, transform.localRotation.z);
        currentRotationIndex = rotationIndex;
    }
}
