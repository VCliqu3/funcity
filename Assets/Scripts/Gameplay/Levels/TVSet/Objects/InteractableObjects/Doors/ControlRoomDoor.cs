using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlRoomDoor : InteractableDoor
{
    protected override void OnEnable()
    {
        base.OnEnable();
        AlexReceptionSpeaker.OnDialogueCompleted += DoWhenAlexReceptionDialogueCompleted;

        PersecutionAngelica.OnAngelicaSpawn += DoWhenAngelicaSpawn;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        AlexReceptionSpeaker.OnDialogueCompleted -= DoWhenAlexReceptionDialogueCompleted;

        PersecutionAngelica.OnAngelicaSpawn -= DoWhenAngelicaSpawn;
    }

    private void DoWhenAlexReceptionDialogueCompleted()
    {
        startOpen = true;
        if (!isOpen) OpenDoor();
        EnableInteractions();
    }

    private void DoWhenAngelicaSpawn()
    {
        //if (!isOpen) OpenDoor();
        EnableInteractions();
    }
}
