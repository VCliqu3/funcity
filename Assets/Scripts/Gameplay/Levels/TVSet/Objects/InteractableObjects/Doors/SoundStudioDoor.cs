using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundStudioDoor : InteractableDoor
{
    protected override void OnEnable()
    {
        base.OnEnable();
        SoundStudioCardPanel.OnCardPlaced += DoWhenCardPlaced;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        SoundStudioCardPanel.OnCardPlaced -= DoWhenCardPlaced;
    }

    private void DoWhenCardPlaced()
    {
        OpenDoor();
        EnableInteractions();
    }
}
