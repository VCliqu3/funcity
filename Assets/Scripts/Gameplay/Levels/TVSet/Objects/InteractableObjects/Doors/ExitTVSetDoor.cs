using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitTVSetDoor : InteractableDoor
{
    protected override void OnEnable()
    {
        base.OnEnable();
        ExitTVSetCardPanel.OnCardPlaced += DoWhenCardPlaced;
        ExitTVSetDoorCloseTrigger.OnPlayerEnter += DoWhenPlayerExitTVSet;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        ExitTVSetCardPanel.OnCardPlaced -= DoWhenCardPlaced;
        ExitTVSetDoorCloseTrigger.OnPlayerEnter -= DoWhenPlayerExitTVSet;
    }

    private void DoWhenCardPlaced()
    {
        OpenDoor();

        EnableInteractions();
    }

    private void DoWhenPlayerExitTVSet()
    {
        if (isOpen) CloseDoor();

        DisableInteractions();
    }
}
