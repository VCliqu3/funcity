using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterTVSetDoor : InteractableDoor
{
    protected override void OnEnable()
    {
        base.OnEnable();
        ExitHospitalTrigger.OnPlayerEnter += DoWhenPlayerExitHospital;
        EnterTVSetTrigger.OnPlayerEnter += DoWhenPlayerEnterTVSet;

    }

    protected override void OnDisable()
    {
        base.OnDisable();
        ExitHospitalTrigger.OnPlayerEnter -= DoWhenPlayerExitHospital;
        EnterTVSetTrigger.OnPlayerEnter -= DoWhenPlayerEnterTVSet;
    }

    private void DoWhenPlayerExitHospital()
    {
        startOpen = true;
        if (!isOpen) OpenDoor();
        EnableInteractions();
    }

    private void DoWhenPlayerEnterTVSet()
    {
        if (isOpen) CloseDoor();
        DisableInteractions();
    }
}
