using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HallDoor : InteractableDoor
{
    protected override void OnEnable()
    {
        base.OnEnable();
        HallCardPanel.OnCardPlaced += DoWhenCardPlaced;
        PersecutionAngelica.OnAngelicaSpawn += DoWhenAngelicaSpawn;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        HallCardPanel.OnCardPlaced -= DoWhenCardPlaced;
        PersecutionAngelica.OnAngelicaSpawn -= DoWhenAngelicaSpawn;
    }

    private void DoWhenCardPlaced()
    {
        OpenDoor();
        EnableInteractions();
    }

    private void DoWhenAngelicaSpawn()
    {
        //if (!isOpen) OpenDoor();
        EnableInteractions();
    }
}
