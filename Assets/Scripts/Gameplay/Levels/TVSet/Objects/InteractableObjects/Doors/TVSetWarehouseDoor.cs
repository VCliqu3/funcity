using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TVSetWarehouseDoor : InteractableDoor
{
    protected override void OnEnable()
    {
        base.OnEnable();
        TVSetWarehouseEncounter.OnTVSetWarehouseEncounterStarting += DoWhenTVWarehouseEncounterStarting;
        TVSetWarehouseEncounter.OnTVSetWarehouseEncounterCompleted += DoWhenTVWarehouseEncounterCompleted;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        TVSetWarehouseEncounter.OnTVSetWarehouseEncounterStarting -= DoWhenTVWarehouseEncounterStarting;
        TVSetWarehouseEncounter.OnTVSetWarehouseEncounterCompleted -= DoWhenTVWarehouseEncounterCompleted;
    }

    private void DoWhenTVWarehouseEncounterStarting()
    {
        if (isOpen) CloseDoor();

        DisableInteractions();
    }

    private void DoWhenTVWarehouseEncounterCompleted()
    {
        if (!isOpen) OpenDoor();

        EnableInteractions();
    }
}
