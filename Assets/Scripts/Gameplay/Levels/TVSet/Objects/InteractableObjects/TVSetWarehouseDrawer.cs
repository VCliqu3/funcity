using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TVSetWarehouseDrawer : InteractableDrawer
{
    [Header("TVSetWarehouseDrawerConfig")]

    public static Action<TVSetWarehouseDrawer,bool> OnTVSetWarehouseDrawerOpened;
    private bool hasBeenOpenedBefore = false;

    public override void OpenDrawer()
    {
        base.OpenDrawer();

        OnTVSetWarehouseDrawerOpened?.Invoke(this,hasBeenOpenedBefore);

        hasBeenOpenedBefore = true;
    }
}
