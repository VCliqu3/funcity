using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class ExitTVSetCard : Card
{
    public static Action OnCardPickUp;

    public override void PickUp(PlayerPickUp playerPickUp, Transform pickUpSocket)
    {
        base.PickUp(playerPickUp, pickUpSocket);
        OnCardPickUp?.Invoke();
    }
}
