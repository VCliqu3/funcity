using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TVSetPuzzleScreen : MonoBehaviour
{
    [Header ("Puzzle Screen Settings")]

    [SerializeField] private Light screenLight;

    [SerializeField] private AudioClip turnOnAudioClip;
    [SerializeField] private AudioClip turnOffAudioClip;

    private AudioSource audioSource;

    private Material material;
    private bool enableFlashing;

    private IEnumerator screenFlashCoroutine;

    private void Awake()
    {
        material = GetComponent<Renderer>().material;
        audioSource = GetComponentInChildren<AudioSource>();

        screenFlashCoroutine = ScreenFlash();
    }

    public void SetScreenFlashing(bool state)
    {
        if (state)
        {
            StopCoroutine(screenFlashCoroutine);
            screenFlashCoroutine = ScreenFlash();
            StartCoroutine(screenFlashCoroutine);

            if (turnOnAudioClip) audioSource.PlayOneShot(turnOnAudioClip);
        }
        else
        {
            StopCoroutine(screenFlashCoroutine);
            
            TurnScreen(false);

            if (turnOffAudioClip) audioSource.PlayOneShot(turnOffAudioClip);
        }     
    }

    private IEnumerator ScreenFlash()
    {
        while (true)
        {
            TurnScreen(true);

            yield return new WaitForSeconds(0.9f);

            TurnScreen(false);

            yield return new WaitForSeconds(0.05f);

            TurnScreen(true);

            yield return new WaitForSeconds(0.5f);

            TurnScreen(false);

            yield return new WaitForSeconds(0.05f);

            TurnScreen(true);

            yield return new WaitForSeconds(1.25f);

            TurnScreen(false);

            yield return new WaitForSeconds(0.05f);

            TurnScreen(true);

            yield return new WaitForSeconds(0.3f);

            TurnScreen(false);

            yield return new WaitForSeconds(0.05f);

            TurnScreen(true);

            yield return new WaitForSeconds(0.5f);

            TurnScreen(false);

            yield return new WaitForSeconds(0.05f);

            TurnScreen(true);

            yield return new WaitForSeconds(1.5f);

            TurnScreen(false);

            yield return new WaitForSeconds(0.05f);
        }
    }

    private void TurnScreen(bool state)
    {
        screenLight.enabled = state;

        if (state)
        {
            material.EnableKeyword("_EMISSION");
        }
        else
        {
            material.DisableKeyword("_EMISSION");
        }
    }
    
}
