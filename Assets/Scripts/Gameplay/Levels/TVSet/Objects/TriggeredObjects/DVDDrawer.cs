using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DVDDrawer : InteractableDrawer
{
    [Header("DVD Drawer Settings")]
    [SerializeField] private AudioClip openDVDDrawerClip;

    [SerializeField] private Color lockedColor;
    [SerializeField] private Color unlockedColor;
    [SerializeField] private List<Light> lights = new List<Light>();
    
    private bool puzzleCompleted;

    private Material material;

    private void OnEnable()
    {
        ScreenButtonsPuzzle.OnScreenButtonsPuzzleCompleted += DoWhenPuzzleCompleted;
    }

    private void OnDisable()
    {
        ScreenButtonsPuzzle.OnScreenButtonsPuzzleCompleted -= DoWhenPuzzleCompleted;
    }

    protected override void Awake()
    {
        base.Awake();
        material = GetComponentInParent<Renderer>().material;
    }

    protected override void  Start()
    {
        base.Start();

        if(!puzzleCompleted) SetDVDPlayerLights(false);
    }

    private void DoWhenPuzzleCompleted(bool autocompleted)
    {
        startOpen = true;
        if(!isOpen) OpenDrawer();
        interactionsEnabled = true;
        puzzleCompleted = true;

        SetDVDPlayerLights(true);

        audioSource.Stop();

        if (!autocompleted) 
        { 
            if (openDVDDrawerClip) audioSource.PlayOneShot(openDVDDrawerClip);
        }
    }

    private void SetDVDPlayerLights(bool state)
    {
        if (state)
        {
            ChangeLightsColor(unlockedColor);
            ChangeMaterialEmissionColor(unlockedColor);
        }
        else
        {
            ChangeLightsColor(lockedColor);
            ChangeMaterialEmissionColor(lockedColor);
        }
    }

    private void ChangeLightsColor(Color color)
    {
        foreach (Light light in lights)
        {
            light.color = color;
        }
    }

    private void ChangeMaterialEmissionColor(Color color)
    {
        material.SetColor("_EmissionColor", color);
    }
}
