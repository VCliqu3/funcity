using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMapScreen : MonoBehaviour
{
    [Header("Camera Map Screen Settings")]

    [SerializeField] private bool enableFlashing;

    [SerializeField] private Light screenLight;
    [SerializeField] private AudioClip turnOnAudioClip;
    [SerializeField] private AudioClip turnOffAudioClip;

    private AudioSource audioSource;

    private Material material;

    private void Awake()
    {
        material = GetComponent<Renderer>().material;
        audioSource = GetComponentInChildren<AudioSource>();
    }

    private void Start()
    {
        if (enableFlashing) StartCoroutine(ScreenFlash());
    }

    private IEnumerator ScreenFlash()
    {
        while (true)
        {
            TurnScreen(true);

            yield return new WaitForSeconds(0.9f);

            TurnScreen(false);

            yield return new WaitForSeconds(0.05f);

            TurnScreen(true);

            yield return new WaitForSeconds(0.5f);

            TurnScreen(false);

            yield return new WaitForSeconds(0.05f);

            TurnScreen(true);

            yield return new WaitForSeconds(1.25f);

            TurnScreen(false);

            yield return new WaitForSeconds(0.05f);

            TurnScreen(true);

            yield return new WaitForSeconds(0.3f);

            TurnScreen(false);

            yield return new WaitForSeconds(0.05f);

            TurnScreen(true);

            yield return new WaitForSeconds(0.5f);

            TurnScreen(false);

            yield return new WaitForSeconds(0.05f);

            TurnScreen(true);

            yield return new WaitForSeconds(1.5f);

            TurnScreen(false);

            yield return new WaitForSeconds(0.05f);
        }
    }

    private void TurnScreen(bool state)
    {
        screenLight.enabled = state;

        if (state)
        {
            material.EnableKeyword("_EMISSION");
            if (turnOnAudioClip) audioSource.PlayOneShot(turnOnAudioClip);
        }
        else
        {
            material.DisableKeyword("_EMISSION");
            if (turnOffAudioClip) audioSource.PlayOneShot(turnOffAudioClip);
        }
    }

}