using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlexExitTVSetSpeaker : ChildSpeakerDevice
{
    private void OnEnable()
    {
        ExitTVSetCardPanel.OnCardPlaced += DoWhenExitTVSetCardPlaced;
    }
    private void OnDisable()
    {
        ExitTVSetCardPanel.OnCardPlaced -= DoWhenExitTVSetCardPlaced;
    }

    private void DoWhenExitTVSetCardPlaced()
    {
        StartCoroutine(ExitTVSetCoroutine());
    }

    private IEnumerator ExitTVSetCoroutine()
    {
        yield return new WaitForSeconds(0.25f);

        PlayClip(0);
    }
}
