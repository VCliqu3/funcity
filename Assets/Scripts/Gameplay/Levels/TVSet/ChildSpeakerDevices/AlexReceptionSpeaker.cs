using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AlexReceptionSpeaker : ChildSpeakerDevice
{
    public static Action OnDialogueCompleted;

    private void OnEnable()
    {
        EnterTVSetTrigger.OnPlayerEnter += DoWhenPlayerFirstEnterTVSet;
    }
    private void OnDisable()
    {
        EnterTVSetTrigger.OnPlayerEnter -= DoWhenPlayerFirstEnterTVSet;
    }
    private void DoWhenPlayerFirstEnterTVSet()
    {
        if (!speakerEnabled)
        {
            Debug.Log("Speaker not enabled");
            return;
        }
        StartCoroutine(FirstEnterTVSetCoroutine());
    }

    private IEnumerator FirstEnterTVSetCoroutine()
    {
        yield return new WaitForSeconds(2.5f);

        PlayClip(0);
        yield return StartCoroutine(WaitTime(0));

        PlayClip(1);
        yield return StartCoroutine(WaitTime(1));

        PlayClip(2);
        yield return StartCoroutine(WaitTime(2));

        PlayClip(3);
        yield return StartCoroutine(WaitTime(3));

        PlayClip(4);
        yield return StartCoroutine(WaitTime(4));

        PlayClip(5);
        
        OnDialogueCompleted?.Invoke();
    }
}
