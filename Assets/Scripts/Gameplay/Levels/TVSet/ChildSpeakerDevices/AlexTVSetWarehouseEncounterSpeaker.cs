using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlexTVSetWarehouseEncounterSpeaker : ChildSpeakerDevice
{
    private void OnEnable()
    {
        TVSetWarehouseEncounter.OnTVSetWarehouseEncounterStarting += DoWhenTVSetWarehouseEncounterStarting;
    }
    private void OnDisable()
    {
        TVSetWarehouseEncounter.OnTVSetWarehouseEncounterStarting -= DoWhenTVSetWarehouseEncounterStarting;
    }

    private void DoWhenTVSetWarehouseEncounterStarting()
    {
        StartCoroutine(TVSetWarehouseEncounterCoroutine());
    }

    private IEnumerator TVSetWarehouseEncounterCoroutine()
    {
        yield return new WaitForSeconds(0.5f);

        PlayClip(0);

        yield return StartCoroutine(WaitTime(0));

        PlayClip(1);
    }
}
