using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlexCameraRoomSpeaker : ChildSpeakerDevice
{
    private void OnEnable()
    {
        EnterCameraRoomTrigger.OnPlayerEnter += DoWhenPlayerEnterCameraRoom;
    }
    private void OnDisable()
    {
        EnterCameraRoomTrigger.OnPlayerEnter -= DoWhenPlayerEnterCameraRoom;
    }

    private void DoWhenPlayerEnterCameraRoom()
    {
        StartCoroutine(EnterCameraRoomCoroutine());
    }

    private IEnumerator EnterCameraRoomCoroutine()
    {
        yield return new WaitForSeconds(0.5f);

        PlayClip(0);
    }
}
