using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TVSetParkingAngelica : MonoBehaviour
{
    [SerializeField] private Transform playerCamera;
    [SerializeField] private float movementSpeed;
    [SerializeField] private float lookSpeed;

    [SerializeField] private AudioClip jumpscareClip;

    [SerializeField] private AngelicaState angelicaState;
    private enum AngelicaState {Crying, OnJumpscare}

    [Header("Angelica Animation Settings")]
    [SerializeField] private Animator angelicaAnimator;

    private GameObject player;
    private PlayerFlashlight playerFlashlight;
    private AudioSource audioSource;
    private bool hasStartedCrying;
    private bool hasStartedJumpscare;

    public static Action OnAngelicaStartJumpscare;

    private void OnEnable()
    {
        SetAngelicaState(AngelicaState.Crying);
        TVSetParkingJumpscareTrigger.OnPlayerEnter += StartJumpscare;
    }
    private void OnDisable()
    {
        TVSetParkingJumpscareTrigger.OnPlayerEnter -= StartJumpscare;
    }

    private void Awake()
    {
        audioSource = GetComponentInChildren<AudioSource>();
        player = GameObject.FindGameObjectWithTag("Player");
        playerFlashlight = FindObjectOfType<PlayerFlashlight>();
    }

    private void Update()
    {
        AngelicaLogic();
    }

    private void AngelicaLogic()
    {
        switch (angelicaState)
        {
            case AngelicaState.Crying:
                CryingLogic();
                break;
            case AngelicaState.OnJumpscare:
                JumpscareLogic();
                break;
            default:
                break;
        }
    }

    private void SetAngelicaState(AngelicaState state)
    {
        angelicaState = state;
    }

    private void StartJumpscare()
    {
        SetAngelicaState(AngelicaState.OnJumpscare);
    }

    private void CryingLogic()
    {
        if (!hasStartedCrying)
        {
            WhenAngelicaStartCrying();    
            hasStartedCrying = true;      
        }
    }

    private void JumpscareLogic()
    {
        if (!hasStartedJumpscare)
        {
            WhenAngelicaStartJumpscare();
            hasStartedJumpscare = true;          
        }

        JumpscarePlayer();
    }

    private void WhenAngelicaStartCrying()
    {
        Debug.Log("AngelicaStartCrying");
        angelicaAnimator.Play("Cry");
    }

    private void WhenAngelicaStartJumpscare()
    {
        Debug.Log("AngelicaStartJumpscare");
        OnAngelicaStartJumpscare?.Invoke();

        if (jumpscareClip) audioSource.PlayOneShot(jumpscareClip);

        angelicaAnimator.CrossFade("Scream", 0.1f);

        playerFlashlight.flashlightToggleEnabled = false;
        playerFlashlight.ForceFlashlightState(true);
    }

    private void JumpscarePlayer()
    {
        MoveTowardsTarget(player.transform, movementSpeed);
        LookAtTarget(player.transform, lookSpeed);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (angelicaState != AngelicaState.OnJumpscare) return;
            WhenAngelicaReachPlayer();
        }
    }

    private void WhenAngelicaReachPlayer()
    {
        Debug.Log("PlayerReached");

        playerFlashlight.flashlightToggleEnabled = true;

        audioSource.transform.SetParent(transform.parent);
        gameObject.SetActive(false);
    }

    private void LookAtTarget(Transform target, float lookSpeed)
    {
        Vector3 targetPos = new Vector3(target.position.x, transform.position.y, target.position.z);
        Vector3 directionToTarget = targetPos - transform.position;

        if (directionToTarget == Vector3.zero) return;

        Quaternion rotationToTarget = Quaternion.LookRotation(directionToTarget, Vector3.up);
        transform.localRotation = Quaternion.Lerp(transform.localRotation, rotationToTarget, Time.deltaTime * lookSpeed);
    }

    private void MoveTowardsTarget(Transform target, float movementSpeed)
    {
        Vector3 targetPos = new Vector3(target.position.x, transform.position.y, target.position.z);
        transform.position = Vector3.MoveTowards(transform.position, targetPos, Time.deltaTime * movementSpeed);
    }
}
