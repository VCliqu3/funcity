using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableButton : InteractableObject
{
    [Header("Interactable Button Settings")]
    [SerializeField] private Transform extendedPos;
    [SerializeField] private Transform pushedPos;
    [SerializeField] private float movementSpeed = 5f;

    [SerializeField] private AudioClip pushAudioClip;

    protected override void Update()
    {
        base.Update();
        CheckPosition();
    }

    public override void Interact(PlayerInteract playerInteract)
    {
        base.Interact(playerInteract);
        PushButton();
    }

    private void PushButton()
    {
        transform.position = pushedPos.position;

        if (pushAudioClip) audioSource.PlayOneShot(pushAudioClip);
    }

    private void CheckPosition()
    {
        if (transform.position == extendedPos.position) return;
        transform.position = Vector3.Lerp(transform.position, extendedPos.position, Time.deltaTime * movementSpeed);
    }


}
