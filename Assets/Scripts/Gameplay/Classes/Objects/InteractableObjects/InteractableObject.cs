using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class InteractableObject : MonoBehaviour, IInteractable
{
    [Header("Interactable Object Settings")]
    public bool interactionsEnabled;
    protected AudioSource audioSource;

    protected GameObject player;
    protected PlayerInteract playerInteract;

    [SerializeField] private bool sightIndicatorEnabled;
    [SerializeField] private string textOnSight;

    public static Action<InteractableObject,string> OnInteractableObjectOnSight;
    public static Action<InteractableObject> OnInteractableObjectLeaveSight;
    public static Action<InteractableObject> OnObjectInteracted;
    public static Action<InteractableObject> OnObjectFailInteracted;

    [SerializeField] private AudioClip interactionAudioClip;
    [SerializeField] private AudioClip failedInteractionAudioClip;

    protected virtual void Awake() 
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerInteract = player.GetComponent<PlayerInteract>();

        audioSource = GetComponentInChildren<AudioSource>();

        if (!audioSource) Debug.Log("NoAudioSource" + gameObject.name);
    }
    protected virtual void Start() { }
    protected virtual void Update() { }

    public virtual void OnPlayerEnterInteractionDetectionRange() { }
    public virtual void OnPlayerLeaveInteractionDetectionRange() { }
    public virtual void OnPlayerEnterInteractionSightRange() 
    {
        if (!sightIndicatorEnabled) return;
        if (!playerInteract.interactionsEnabled) return;
        if (!interactionsEnabled) return;
        
         OnInteractableObjectOnSight?.Invoke(this,textOnSight);      
    }
    public virtual void OnPlayerLeaveInteractionSightRange() 
    {
        OnInteractableObjectLeaveSight?.Invoke(this);
    }

    public virtual void Interact(PlayerInteract playerInteract) 
    {
        if(interactionAudioClip) audioSource.PlayOneShot(interactionAudioClip);

        OnObjectInteracted?.Invoke(this);
    }
    public virtual void FailInteract(PlayerInteract playerInteract)
    {
        if (failedInteractionAudioClip) audioSource.PlayOneShot(failedInteractionAudioClip);

        OnObjectFailInteracted?.Invoke(this);
    }

    protected void EnableInteractions()
    {
        interactionsEnabled = true;
    }
    protected void DisableInteractions()
    {
        interactionsEnabled = false;
    }
    protected void AddToInteractiveLayer()
    {
        gameObject.layer = LayerMask.NameToLayer("Interactable");
    }
    protected void RemoveFromInteractiveLayer()
    {
        gameObject.layer = LayerMask.NameToLayer("Default");
    }
   
}
