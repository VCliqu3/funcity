using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class InteractableNote : InteractableObject
{
    [Header("Interactable Note Settings")]
    [SerializeField] private int noteNumber;
    [SerializeField] private Sprite noteSprite;
    [SerializeField] private AudioClip readAudioClip;
    [SerializeField] private AudioClip closeAudioClip;

    public static Action<InteractableNote, Sprite, int> OnNoteInteracted;

    protected virtual void OnEnable()
    {
        NoteCanvasController.OnNoteClosed += OnNoteClosed;
    }
    protected virtual void OnDisable()
    {
        NoteCanvasController.OnNoteClosed -= OnNoteClosed;
    }

    protected override void Start()
    {
        base.Start();
    }

    public override void Interact(PlayerInteract playerInteract)
    {
        base.Interact(playerInteract);
        ReadNote();
    }

    private void ReadNote()
    {
        OnNoteInteracted?.Invoke(this, noteSprite, noteNumber);

        if (readAudioClip) audioSource.PlayOneShot(readAudioClip);
    }

    private void OnNoteClosed(InteractableNote interactableNote)
    {
        if(this == interactableNote)
        {
            if (closeAudioClip) audioSource.PlayOneShot(closeAudioClip);
        }    
    }
}
