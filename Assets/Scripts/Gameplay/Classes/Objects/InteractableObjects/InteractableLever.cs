using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableLever : InteractableObject
{
    [Header("Interactable Lever Settings")]
    [SerializeField] public bool isOn;
    [SerializeField] private bool startOn;

    [SerializeField] private float onRotation;
    [SerializeField] private float offRotation;
    [SerializeField] private float rotationSpeed = 4;

    private float targetAngle;

    [SerializeField] private AudioClip toggleAudioClip;

    protected override void Start()
    {
        base.Start();
        ForceLeverState(startOn);
    }

    protected override void Update()
    {
        base.Update();
        CheckRotation();
    }

    private void CheckRotation()
    {
        Quaternion targetRotation = Quaternion.Euler(transform.rotation.x, transform.localRotation.y, targetAngle);
        
        if (transform.localRotation == targetRotation) return;

        transform.localRotation = Quaternion.Lerp(transform.localRotation, targetRotation, Time.deltaTime * rotationSpeed);
        
    }

    public override void Interact(PlayerInteract playerInteract)
    {
        base.Interact(playerInteract);

        ToggleLeverState();
    }

    private void ForceLeverState(bool state)
    {
        isOn = state;
        UpdateTargetAngle();
    }

    private void ToggleLeverState()
    {
        isOn = !isOn;
        UpdateTargetAngle();

        if (toggleAudioClip) audioSource.PlayOneShot(toggleAudioClip);
    }

    private void UpdateTargetAngle()
    {
        if (isOn)
        {
            targetAngle = onRotation;
            return;
        }

        targetAngle = offRotation;
    } 
}
