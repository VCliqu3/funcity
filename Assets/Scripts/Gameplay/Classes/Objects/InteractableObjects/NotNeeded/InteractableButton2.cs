using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InteractableButton2 : InteractableObject
{
    [Header("Interactable Button Configuration")]
    [SerializeField] private Transform buttonPivot;
    [SerializeField] private bool enablePushLerp;
    [SerializeField] private float pushDisplacement = -0.04f;
    [SerializeField] private float recoilSpeed = 1f;

    public static Action<InteractableButton2> OnButtonPush;

    protected override void Update()
    {
        base.Update();
        CheckPush();
    }

    public override void Interact(PlayerInteract playerInteract)
    {
        base.Interact(playerInteract);

        if (enablePushLerp) { buttonPivot.localPosition = new Vector3(buttonPivot.localPosition.x, pushDisplacement, buttonPivot.localPosition.z); }

        OnButtonPush?.Invoke(this);
       
    }

    private void CheckPush()
    {
        if (!enablePushLerp) return;
        if (buttonPivot.localPosition == Vector3.zero) return;

        buttonPivot.localPosition = Vector3.Lerp(buttonPivot.localPosition, Vector3.zero, Time.deltaTime * recoilSpeed);      
    }
}
