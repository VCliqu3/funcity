using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InteractableLever2 : InteractableObject
{
    [Header("Interactable Lever Configuration")]
    [SerializeField] private bool isOn;
    [SerializeField] private bool initialState;

    [SerializeField] private Transform leverPivot;

    [SerializeField] private float onRotation;
    [SerializeField] private float offRotation;
    [SerializeField] private bool enableRotationLerp;
    [SerializeField] private float rotationSpeed;

    private float targetAngle;

    public static Action<InteractableLever2, bool> OnLeverInteracted;

    protected override void Start()
    {
        base.Start();

        ForceLeverState(initialState);
    }

    protected override void Update()
    {
        base.Update();

        CheckRotation();
    }

    public override void Interact(PlayerInteract playerInteract)
    {
        base.Interact(playerInteract);

        ToggleLeverState();

        OnLeverInteracted?.Invoke(this, isOn);
    }

    private void ForceLeverState(bool state)
    {
        isOn = state;
        UpdateTargetAngle();
    }

    private void ToggleLeverState()
    {
        isOn = !isOn;
        UpdateTargetAngle();
    }

    private void UpdateTargetAngle()
    {
        if (isOn)
        {
            targetAngle = onRotation;
            return;
        }

        targetAngle = offRotation;
    }

    private void CheckRotation()
    {
        Quaternion targetRotation = Quaternion.Euler(targetAngle, transform.localRotation.y, transform.localRotation.z);

        if (!enableRotationLerp)
        {
            leverPivot.localRotation = targetRotation;
        }
        else
        {
            if (leverPivot.localRotation == targetRotation) return;
            leverPivot.localRotation = Quaternion.Lerp(leverPivot.localRotation, targetRotation, Time.deltaTime * rotationSpeed);
        }
    }
}
