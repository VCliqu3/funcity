using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableLid : InteractableObject
{
    [Header("Interactable Lid Settings")]
    [SerializeField] protected bool startOpen = false;
    public bool isOpen;
    [SerializeField] private float closedAngle = 0f;
    [SerializeField] private float openedAngle = 90f;
    [SerializeField] private float rotationSpeed = 2f;

    [SerializeField] private MovementType movementType;

    [SerializeField] private AudioClip openAudioClip;
    [SerializeField] private AudioClip closeAudioClip;

    private enum MovementType {Lerp, Slerp, RotateTowards }

    [Header("Checpoint Load Settings")]
    [SerializeField] private bool openByCheckpointLoad;
    [SerializeField] private int checkpointNumberToEnable;

    [SerializeField] protected LoadType loadType;
    protected enum LoadType { OnlyThisCheckpoint, AllCheckpointsAfterThis }

    private float targetAngle;
    protected virtual void OnEnable()
    {
        InGameCheckpointManager.OnCheckpointLoad += CheckOpenByCheckpointLoad;
    }

    protected virtual void OnDisable()
    {
        InGameCheckpointManager.OnCheckpointLoad -= CheckOpenByCheckpointLoad;
    }

    protected override void Start()
    {
        base.Start();

        SetStartRotation();
    }

    protected override void Update()
    {
        base.Update();
        CheckRotation();
    }

    private void CheckRotation()
    {
        Quaternion targetRotation = Quaternion.Euler(targetAngle, transform.localRotation.y, transform.localRotation.z);

        if (transform.localRotation == targetRotation) return;

        switch (movementType)
        {
            case MovementType.Lerp:
                transform.localRotation = Quaternion.Lerp(transform.localRotation, targetRotation, Time.deltaTime * rotationSpeed);
                break;
            case MovementType.Slerp:
                transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRotation, Time.deltaTime * rotationSpeed);
                break;
            case MovementType.RotateTowards:
                transform.localRotation = Quaternion.RotateTowards(transform.localRotation, targetRotation, Time.deltaTime * rotationSpeed);
                break;
            default:
                transform.localRotation = Quaternion.Lerp(transform.localRotation, targetRotation, Time.deltaTime * rotationSpeed);
                break;
        }
    }

    public void OpenLid()
    {
        targetAngle = openedAngle;
        isOpen = true;

        if (openAudioClip) audioSource.PlayOneShot(openAudioClip);
    }

    public void CloseLid()
    {
        targetAngle = closedAngle;
        isOpen = false;

        if (closeAudioClip) audioSource.PlayOneShot(closeAudioClip);
    }

    public override void Interact(PlayerInteract playerInteract)
    {
        base.Interact(playerInteract);

        ToggleState();
    }

    public virtual void ToggleState()
    {
        if (isOpen) CloseLid();
        else OpenLid();
    }

    private void SetStartRotation()
    {
        targetAngle = startOpen ? openedAngle : closedAngle;
        isOpen = startOpen;

        Quaternion targetRotation = Quaternion.Euler(targetAngle, transform.localRotation.y, transform.localRotation.z);
        transform.localRotation = targetRotation;
    }

    public override void FailInteract(PlayerInteract playerInteract)
    {
        base.FailInteract(playerInteract);

        Debug.Log("Cant Use Lid");
    }

    protected virtual void CheckOpenByCheckpointLoad(int checkpointNumber, Transform checkpointTransform)
    {
        if (!openByCheckpointLoad) return;

        switch (loadType)
        {
            case (LoadType.OnlyThisCheckpoint):
                if (checkpointNumber == checkpointNumberToEnable) OpenByCheckpointLoadLogic();
                break;

            case (LoadType.AllCheckpointsAfterThis):
            default:
                if (checkpointNumber >= checkpointNumberToEnable) OpenByCheckpointLoadLogic();
                break;
        }
    }

    protected virtual void OpenByCheckpointLoadLogic()
    {
        startOpen = true;
        if (!isOpen) OpenLid();
        interactionsEnabled = true;
    }
}
