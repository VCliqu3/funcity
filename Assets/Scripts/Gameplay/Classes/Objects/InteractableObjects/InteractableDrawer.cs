using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableDrawer : InteractableObject
{
    [Header ("Interactable Drawer Settings")]

    [SerializeField] protected bool startOpen = false;
    public bool isOpen;
    [SerializeField] private Transform closedPos;
    [SerializeField] private Transform openedPos;
    [SerializeField] private float movementSpeed = 5f;

    private Vector3 targetPos;

    [SerializeField] private AudioClip openAudioClip;
    [SerializeField] private AudioClip closeAudioClip;

    protected override void Start()
    {
        base.Start();
        SetStartPosition();
    }
    protected override void Update()
    {
        base.Update();
        CheckPosition();
    }
    public override void Interact(PlayerInteract playerInteract)
    {
        base.Interact(playerInteract);
        ToggleState();
    }

    private void CheckPosition()
    {
        if (transform.position == targetPos) return;
        transform.position = Vector3.Lerp(transform.position, targetPos, Time.deltaTime * movementSpeed);
    }

    private void SetStartPosition()
    {
        targetPos = startOpen ? openedPos.position : closedPos.position;
        isOpen = startOpen;

        transform.position = targetPos;
    }
    public virtual void OpenDrawer()
    {
        targetPos = openedPos.position;
        isOpen = true;

        if (openAudioClip) audioSource.PlayOneShot(openAudioClip);
    }

    public virtual void CloseDrawer()
    {
        targetPos = closedPos.position;
        isOpen = false;

        if (closeAudioClip) audioSource.PlayOneShot(closeAudioClip);
    }

    public virtual void ToggleState()
    {
        if (isOpen) CloseDrawer();
        else OpenDrawer();
    }

    public override void FailInteract(PlayerInteract playerInteract)
    {
        base.FailInteract(playerInteract);

        Debug.Log("Cant Open Drawer");
    }
}
