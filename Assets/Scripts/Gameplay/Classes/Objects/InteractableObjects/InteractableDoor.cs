using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableDoor : InteractableObject
{
    [Header("Interactable Door Settings")]
    [SerializeField] protected bool startOpen = false;
    public bool isOpen;
    [SerializeField] protected float closedAngle = 0f;
    [SerializeField] protected float openedAngle = 90f;
    [SerializeField] protected float rotationSpeed = 2f;

    [SerializeField] protected MovementType movementType;

    [SerializeField] protected AudioClip openAudioClip;
    [SerializeField] protected AudioClip closeAudioClip;
    protected enum MovementType {Lerp,Slerp,RotateTowards}

    [Header("Checpoint Load Settings")]
    [SerializeField] private bool openByCheckpointLoad;
    [SerializeField] private int checkpointNumberToEnable;

    [SerializeField] protected LoadType loadType;
    protected enum LoadType {OnlyThisCheckpoint, AllCheckpointsAfterThis}

    protected float targetAngle;

    protected virtual void OnEnable()
    {
        InGameCheckpointManager.OnCheckpointLoad += CheckOpenByCheckpointLoad;
    }

    protected virtual void OnDisable()
    {
        InGameCheckpointManager.OnCheckpointLoad -= CheckOpenByCheckpointLoad;
    }

    protected override void Start()
    {
        base.Start();

        ForceRotation(startOpen);
    }

    protected override void Update()
    {
        base.Update();
        CheckRotation();
    }

    private void CheckRotation()
    {
        Quaternion targetRotation = Quaternion.Euler(transform.localRotation.x, targetAngle, transform.localRotation.z);

        if (transform.localRotation == targetRotation) return;

        switch (movementType)
        {
            case MovementType.Lerp:
                transform.localRotation = Quaternion.Lerp(transform.localRotation, targetRotation, Time.deltaTime * rotationSpeed);
                break;
            case MovementType.Slerp:
                transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRotation, Time.deltaTime * rotationSpeed);
                break;
            case MovementType.RotateTowards:
                transform.localRotation = Quaternion.RotateTowards(transform.localRotation, targetRotation, Time.deltaTime * rotationSpeed);
                break;
            default:
                transform.localRotation = Quaternion.Lerp(transform.localRotation, targetRotation, Time.deltaTime * rotationSpeed);
                break;
        }

    }

    public void OpenDoor()
    {
        targetAngle = openedAngle;
        isOpen = true;

        if (!audioSource) return;
        if (openAudioClip) audioSource.PlayOneShot(openAudioClip);
    }

    public void CloseDoor()
    {
        targetAngle = closedAngle;
        isOpen = false;

        if (closeAudioClip) audioSource.PlayOneShot(closeAudioClip);
    }

    public override void Interact(PlayerInteract playerInteract)
    {
        base.Interact(playerInteract);

        ToggleState();
    }

    public virtual void ToggleState()
    {
        if (isOpen) CloseDoor();
        else OpenDoor();
    }

    protected virtual void ForceRotation(bool state)
    {
        targetAngle = state ? openedAngle : closedAngle;
        isOpen = state;

        Quaternion targetRotation = Quaternion.Euler(transform.localRotation.x, targetAngle, transform.localRotation.z);
        transform.localRotation = targetRotation;
    }

    public override void FailInteract(PlayerInteract playerInteract)
    {
        base.FailInteract(playerInteract);

        Debug.Log("Cant Use Door");
    }

    protected virtual void CheckOpenByCheckpointLoad(int checkpointNumber, Transform checkpointTransform)
    {
        if (!openByCheckpointLoad) return;

        switch (loadType)
        {
            case (LoadType.OnlyThisCheckpoint):
                if (checkpointNumber == checkpointNumberToEnable) OpenByCheckpointLoadLogic();
                break;

            case (LoadType.AllCheckpointsAfterThis):
            default:
                if (checkpointNumber >= checkpointNumberToEnable) OpenByCheckpointLoadLogic();
                break;
        }
    }

    protected virtual void OpenByCheckpointLoadLogic() 
    {
        startOpen = true;
        ForceRotation(true);
        EnableInteractions();
    }
}
