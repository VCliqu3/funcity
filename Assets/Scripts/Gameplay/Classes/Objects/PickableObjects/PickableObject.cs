using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PickableObject : MonoBehaviour, IPickable
{
    [Header("Pickable Object Configuration")]
    public bool pickUpEnabled;
    protected AudioSource audioSource;

    [SerializeField] protected bool objectPickedUp = false;
    
    protected GameObject player;
    protected PlayerPickUp playerPickUp;
    protected Transform currentPickUpSocket;
    
    [HideInInspector] public Rigidbody _rigidbody;
    [HideInInspector] public BoxCollider _boxCollider;

    [SerializeField] private Vector3 pickedUpPositionOffset;
    [SerializeField] private Vector3 pickedUpRotation;

    [SerializeField] private PickableObjectConfig pickableObjectConfig;

    [SerializeField] private bool sightIndicatorEnabled;
    [SerializeField] private string textOnSight;

    [SerializeField] private bool dropIndicatorEnabled;
    [SerializeField] private string textToDrop;

    [SerializeField] private AudioClip pickedUpAudioClip;
    [SerializeField] private AudioClip droppedAudioClip;

    [Header("Inspection Configuration")]
    public bool enableInspection;
    [SerializeField] private bool enablePickUpInspection;
    [SerializeField] private bool hasBeenInspected;
    [SerializeField] string textToInspect;
    [SerializeField] private Sprite objectSprite;
    [SerializeField] private AudioClip openInspectionAudioClip;
    [SerializeField] private AudioClip closeInspectionAudioClip;

    public static Action<PickableObject,string> OnPickableObjectOnSight;
    public static Action<PickableObject> OnPickableObjectLeaveSight;

    public static Action<PickableObject> OnObjectPickedUp;    
    public static Action<PickableObject> OnObjectDropped;
    public static Action<PickableObject,Sprite> OnObjectInspected;

    public static Action<PickableObject,string> OnObjectPickedUpDropIndicator;
    public static Action<PickableObject, string> OnObjectPickedUpInspectIndicator;

    protected virtual void OnEnable()
    {
        InspectionCanvasController.OnInspectionClosed += OnInspectionClosed;
    }
    protected virtual void OnDisable()
    {
        InspectionCanvasController.OnInspectionClosed -= OnInspectionClosed;
    }

    protected virtual void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _boxCollider = GetComponent<BoxCollider>();
        player = GameObject.FindGameObjectWithTag("Player");
        playerPickUp = player.GetComponent<PlayerPickUp>();

        audioSource = GetComponentInChildren<AudioSource>();
    }

    protected virtual void Start() { }
    protected virtual void Update() 
    {
        CheckSmoothPickUp();
    }

    public virtual void OnPlayerEnterPickUpDetectionRange() { }
    public virtual void OnPlayerLeavePickUpDetectionRange() { }
    public virtual void OnPlayerEnterPickUpSightRange() 
    {
        if (!sightIndicatorEnabled) return;
        if (!playerPickUp.pickUpEnabled) return;
        if (!pickUpEnabled) return;

        OnPickableObjectOnSight?.Invoke(this,textOnSight);
    }
    public virtual void OnPlayerLeavePickUpSightRange() 
    {
        OnPickableObjectLeaveSight?.Invoke(this);
    }
    public virtual void PickUp(PlayerPickUp playerPickUp, Transform pickUpSocket) 
    {
        playerPickUp.currentPickedUpObject = this;

        ChangeLayerAndChildLayers("Hand");

        _boxCollider.isTrigger = true;
        _rigidbody.isKinematic = true;    

        transform.SetParent(pickUpSocket);
        currentPickUpSocket = pickUpSocket;
        objectPickedUp = true;

        if (!playerPickUp.smoothPickUp)
        {
            transform.localPosition = pickedUpPositionOffset;
            transform.localRotation = Quaternion.Euler(pickedUpRotation);
        }

        if(pickedUpAudioClip) audioSource.PlayOneShot(pickedUpAudioClip);

        if(dropIndicatorEnabled) OnObjectPickedUpDropIndicator?.Invoke(this,textToDrop);
        if (enableInspection) OnObjectPickedUpInspectIndicator?.Invoke(this, textToInspect);
        OnObjectPickedUp?.Invoke(this);

        if (enablePickUpInspection && !hasBeenInspected) Inspect(playerPickUp);
    }

    public virtual void CheckSmoothPickUp()
    {
        if (!playerPickUp.smoothPickUp) return;

        if (!objectPickedUp) return;

        if (currentPickUpSocket == null) return;

        if (!(Vector3.Distance(transform.localPosition,pickedUpPositionOffset)<=playerPickUp.isPickingUpThreshold))
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, pickedUpPositionOffset, Time.deltaTime * playerPickUp.pickUpSpeed);
            transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(pickedUpRotation), Time.deltaTime * playerPickUp.pickUpSpeed);
            playerPickUp.isPickingUp = true;
        }
        else
        {
            playerPickUp.isPickingUp = false;
        }
    }

    public virtual void Drop(PlayerPickUp playerPickUp) 
    {
        CharacterController playerCharacterController = player.GetComponent<CharacterController>();

        _boxCollider.isTrigger = false;
        _rigidbody.isKinematic = false;

        objectPickedUp = false;
        transform.SetParent(null);

        _rigidbody.velocity = playerCharacterController.velocity;

        _rigidbody.AddForce(currentPickUpSocket.up * pickableObjectConfig.upDropForce, ForceMode.Impulse);
        _rigidbody.AddForce(currentPickUpSocket.forward * pickableObjectConfig.forwardDropForce, ForceMode.Impulse);
        _rigidbody.AddForce(currentPickUpSocket.right * pickableObjectConfig.rightDropForce, ForceMode.Impulse);

        currentPickUpSocket = null;

        transform.localPosition += transform.TransformDirection(pickableObjectConfig.dropOffsetPosition);

        ChangeLayerAndChildLayers("Pickable"); //Se regresa a su layer inicial (Pickable)

        if(droppedAudioClip) audioSource.PlayOneShot(droppedAudioClip);

        OnObjectDropped?.Invoke(this);
    }
    protected void EnablePickUp()
    {
        pickUpEnabled = true;
    }
    protected void DisablePickUp()
    {
        pickUpEnabled = false;
    }
    protected void AddToPickableLayer()
    {
        gameObject.layer = LayerMask.NameToLayer("Pickable");
    }
    protected void RemoveFromPickableLayer()
    {
        gameObject.layer = LayerMask.NameToLayer("Default");
    }

    protected void ChangeLayerAndChildLayers(string layerName)
    {
        gameObject.layer = LayerMask.NameToLayer(layerName); //Prevenir Clipping del objeto sostenido

        foreach (Transform child in transform)
        {
            child.gameObject.layer = LayerMask.NameToLayer(layerName);
            
            foreach (Transform childOfChild in child.transform)
            {
                childOfChild.gameObject.layer = LayerMask.NameToLayer(layerName);
            }    
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        transform.SetParent(collision.transform);
    }

    public void Inspect(PlayerPickUp playerPickUp)
    {
        hasBeenInspected = true;

        playerPickUp.isInspecting = true;

        if (openInspectionAudioClip) audioSource.PlayOneShot(openInspectionAudioClip);

        OnObjectInspected?.Invoke(this, objectSprite);
    }

    private void OnInspectionClosed(PickableObject pickableObject)
    {
        if (this == pickableObject)
        {
            playerPickUp.isInspecting = false;
            if (closeInspectionAudioClip) audioSource.PlayOneShot(closeInspectionAudioClip);
        }
    }
}
