using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : PlaceableObject
{
    [Header("Card Configuration")]
    public bool onEnableEventsEnabled;
    [SerializeField] private AudioClip enableCardAudioClip;

    protected override void OnEnable()
    {
        base.OnEnable();
        CheckOnEnableCardEvents();
    }

    public override void Place(PlayerUse playerUse, PlaceablePlace placeablePlace)
    {
        base.Place(playerUse, placeablePlace);
    }

    private void CheckOnEnableCardEvents()
    {
        if (!onEnableEventsEnabled) return;

        if (enableCardAudioClip) audioSource.PlayOneShot(enableCardAudioClip);
        Debug.Log(gameObject.name + " enabled");
    }
}
