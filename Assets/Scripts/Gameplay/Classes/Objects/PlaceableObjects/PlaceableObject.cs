using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlaceableObject : UsableObject, IPlaceable
{
    [Header("Placeable Object Configuration")]
    public Vector3 placePositionOffset;
    public Vector3 placeRotation;
    private PlayerUse playerUse;

    public static Action<PlaceableObject,PlaceablePlace> OnObjectPlaced;
    public static Action<PlaceableObject, PlaceablePlace> OnObjectPlacedWrong;

    private Transform currentPlaceableSocket;

    protected override void Awake() 
    { 
        base.Awake();
        playerUse = player.GetComponent<PlayerUse>();
    }
    protected override void Start() { base.Start(); }
    protected override void Update() 
    { 
        base.Update();

        CheckSmoothPlace();
    }

    public override void Use(PlayerUse playerUse, UsablePlace usablePlace) 
    {
        base.Use(playerUse, usablePlace);

        if (usablePlace.TryGetComponent<PlaceablePlace>(out PlaceablePlace placeablePlace))
        {
            Place(playerUse, placeablePlace);
        }
    }

    public override void UseWrong(PlayerUse playerUse, UsablePlace usablePlace)
    {
        base.UseWrong(playerUse, usablePlace);

        if (usablePlace.TryGetComponent<PlaceablePlace>(out PlaceablePlace placeablePlace))
        {
            PlaceWrong(playerUse, placeablePlace);
        }
    }

    public virtual void Place(PlayerUse playerUse, PlaceablePlace placeablePlace)
    {
        ChangeLayerAndChildLayers("Default"); //Ahora pertenece al layer default

        playerUse.playerPickUp.currentPickedUpObject = null;
        objectPickedUp = false;
        currentPickUpSocket = null;

        pickUpEnabled = false;
        useEnabled = false;

        //transform.SetParent(placeablePlace.placeableSocket);      
        transform.SetParent(null);

        currentPlaceableSocket = placeablePlace.placeableSocket;

        if (!playerUse.smoothPlace) 
        { 
            transform.position = currentPlaceableSocket.position + placePositionOffset;
            transform.rotation = currentPlaceableSocket.rotation;

            //transform.position = currentPlaceableSocket.position + placePositionOffset;
            //transform.rotation = Quaternion.Euler(placeRotation);
        }

        OnObjectPlaced?.Invoke(this, placeablePlace);
    }

    protected virtual void PlaceInSocket(PlayerUse playerUse, PlaceablePlace placeablePlace)
    {
        transform.SetParent(placeablePlace.placeableSocket);

        transform.localPosition = placePositionOffset;
        transform.localRotation = Quaternion.Euler(placeRotation);
    }

    public virtual void PlaceWrong(PlayerUse playerUse, PlaceablePlace placeablePlace)
    {
        OnObjectPlacedWrong?.Invoke(this, placeablePlace);
    }

    private void CheckSmoothPlace()
    {
        if (!playerUse.smoothPlace) return;

        if (currentPlaceableSocket == null) return;

        /*
        if (!(Vector3.Distance(transform.localPosition, placePositionOffset) <= playerUse.isPlacingThreshold))
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, placePositionOffset, Time.deltaTime * playerUse.placeSpeed);
            transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(placeRotation), Time.deltaTime * playerUse.placeSpeed);
            playerUse.isPlacing = true;
        }
        else
        {
            playerUse.isPlacing = false;
        }
        */

        /*
        if (!(Vector3.Distance(transform.position, currentPlaceableSocket.position + placePositionOffset) <= playerUse.isPlacingThreshold))
        {
            transform.position = Vector3.Lerp(transform.position, currentPlaceableSocket.position + placePositionOffset, Time.deltaTime * playerUse.placeSpeed);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(placeRotation), Time.deltaTime * playerUse.placeSpeed);
            playerUse.isPlacing = true;
        }
        else
        {
            playerUse.isPlacing = false;
        }
        */

        if (!(Vector3.Distance(transform.position, currentPlaceableSocket.position + placePositionOffset) <= playerUse.isPlacingThreshold))
        {
            transform.position = Vector3.Lerp(transform.position, currentPlaceableSocket.position + placePositionOffset, Time.deltaTime * playerUse.placeSpeed);
            transform.rotation = Quaternion.Lerp(transform.rotation, currentPlaceableSocket.rotation, Time.deltaTime * playerUse.placeSpeed);
            playerUse.isPlacing = true;
        }
        else
        {
            playerUse.isPlacing = false;
        }
    }
}
