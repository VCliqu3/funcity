using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UsableObject : PickableObject, IUsable
{
    [Header("Usable Object Configuration")]
    public bool useEnabled;

    [SerializeField] private AudioClip useAudioClip;
    [SerializeField] private AudioClip useWrongAudioClip;

    [HideInInspector] public bool useAnimationsEnabled = true;

    public static Action<UsableObject,UsablePlace> OnObjectUsed;
    public static Action<UsableObject,UsablePlace> OnObjectUsedWrong;

    public static Action<UsableObject> AnimationOnObjectUsed;
    public static Action<UsableObject> AnimationOnObjectUsedWrong;

    protected override void Awake() { base.Awake(); }
    protected override void Start() { base.Start(); }
    protected override void Update() { base.Update(); }

    public override void OnPlayerEnterPickUpDetectionRange() 
    {
        base.OnPlayerEnterPickUpDetectionRange();
    }
    public override void OnPlayerLeavePickUpDetectionRange() 
    {
        base.OnPlayerLeavePickUpDetectionRange();
    }
    public override void OnPlayerEnterPickUpSightRange() 
    {
        base.OnPlayerEnterPickUpSightRange();
    }
    public override void OnPlayerLeavePickUpSightRange() 
    {
        base.OnPlayerLeavePickUpSightRange();
    }

    public virtual void UseWithoutUsablePlace(PlayerUse playerUse) { }
    public virtual void Use(PlayerUse playerUse, UsablePlace usablePlace) 
    {
        ChangeLayerAndChildLayers("Default"); //Retirar si el objeto se puede usar mas de una vez

        if (useAudioClip) audioSource.PlayOneShot(useAudioClip);

        OnObjectUsed?.Invoke(this,usablePlace);

        if(useAnimationsEnabled) AnimationOnObjectUsed?.Invoke(this);
    }
    public virtual void UseWrong(PlayerUse playerUse, UsablePlace usablePlace)
    {
        Debug.Log("Im using " + gameObject.name + " in an incorrect place");

        if(useWrongAudioClip) audioSource.PlayOneShot(useWrongAudioClip);

        OnObjectUsedWrong?.Invoke(this, usablePlace);

        if(useAnimationsEnabled) AnimationOnObjectUsed?.Invoke(this);
    }
}
