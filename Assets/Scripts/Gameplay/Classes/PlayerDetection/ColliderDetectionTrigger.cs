using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderDetectionTrigger : DetectionTrigger
{
    protected void OnTriggerEnter(Collider collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (!canBeTriggered) return;
            if (hasTriggered && isOnlyTriggeredOnce) return;
            if (onCooldown) return;

            DoWhenPlayerDetected();
        }
    }
}
