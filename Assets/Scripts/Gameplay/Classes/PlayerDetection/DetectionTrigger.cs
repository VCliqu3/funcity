using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DetectionTrigger : MonoBehaviour
{
    [Header("Detection Trigger Settings")]
    [SerializeField] protected bool canBeTriggered = true;
    [SerializeField] protected bool isOnlyTriggeredOnce = true;
    [SerializeField] protected bool hasTriggered = false;
    [SerializeField] protected bool cooldownEnabled = false;
    [SerializeField] protected float cooldownTime = 100f;

    protected float cooldownCounter = 0f;
    protected bool onCooldown = false;

    [Header("Load Chedckpoint Settings")]
    [SerializeField] private bool disableByCheckpointLoad;
    [SerializeField] private int checkpointNumberToEnable;

    [SerializeField] protected LoadType loadType;
    protected enum LoadType {OnlyThisCheckpoint, AllCheckpointsUntilThis }

    public static Action<DetectionTrigger> OnPlayerDetected;

    protected virtual void OnEnable()
    {
        InGameCheckpointManager.OnCheckpointLoad += CheckDisableByCheckpointLoad;
    }
    protected virtual void OnDisable()
    {
        InGameCheckpointManager.OnCheckpointLoad -= CheckDisableByCheckpointLoad;
    }

    protected virtual void Awake() { }

    protected virtual void Start() { }

    protected virtual void Update()
    {
        CheckCooldown();
    }
    protected virtual void DoWhenPlayerDetected()
    {
        hasTriggered = true;

        CheckStartCooldown();

        OnPlayerDetected?.Invoke(this);
    }
    private void CheckCooldown()
    {
        if (!onCooldown) return;

        if (cooldownCounter >= 0)
        {
            cooldownCounter -= Time.deltaTime;
        }
        else
        {
            cooldownCounter = 0f;
            onCooldown = false;
        }
    }

    private void CheckStartCooldown()
    {
        if (!cooldownEnabled) return;

        onCooldown = true;
        cooldownCounter = cooldownTime;
    }

    protected void CheckDisableByCheckpointLoad(int checkpointNumber, Transform checkpointTransform)
    {
        if (!disableByCheckpointLoad) return;

        switch (loadType)
        {
            case (LoadType.OnlyThisCheckpoint):
                if (checkpointNumber != checkpointNumberToEnable) canBeTriggered = false;
                break;

            case (LoadType.AllCheckpointsUntilThis):
            default:
                if (checkpointNumber > checkpointNumberToEnable) canBeTriggered = false;
                break;
        }
    }
}
