using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerDetectionTrigger : MonoBehaviour
{
    [Header("Player Detecition Trigger Settings")]
    [SerializeField] private bool isOnlyTriggeredOnce = true;
    [SerializeField] private bool hasTriggered = false;
    [SerializeField] private bool cooldownEnabled = false;
    [SerializeField] private float cooldownTime = 100f;

    private float cooldownCounter = 0f;
    private bool onCooldown = false;

    public static Action<PlayerDetectionTrigger> OnPlayerDetected;

    protected virtual void Update()
    {
        CheckCooldown();
    }

    protected void OnTriggerEnter(Collider collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (hasTriggered && isOnlyTriggeredOnce) return;
            if (onCooldown) return;

            DoWhenPlayerDetected();
        }
    }

    protected virtual void DoWhenPlayerDetected()
    {
        hasTriggered = true;

        CheckStartCooldown();

        OnPlayerDetected?.Invoke(this);
    }

    private void CheckCooldown()
    {
        if (!onCooldown) return;

        if (cooldownCounter >= 0)
        {
            cooldownCounter -= Time.deltaTime;
        }
        else
        {
            cooldownCounter = 0f;
            onCooldown = false;
        }
    }

    private void CheckStartCooldown()
    {
        if (!cooldownEnabled) return;

        onCooldown = true;
        cooldownCounter = cooldownTime;
    }
}
