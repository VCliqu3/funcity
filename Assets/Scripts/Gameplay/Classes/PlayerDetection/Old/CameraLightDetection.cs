using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLightDetection : MonoBehaviour
{
    private Camera lightDetectionCamera;

    public bool lightDetectionEnabled;
    public float lightValue;

    private const int textureSize = 1;
    private Texture2D textureLight;
    private RenderTexture textureTemp;
    private Rect rectLight;
    private Color lightPixel;

    private void Awake()
    {
        lightDetectionCamera = GetComponent<Camera>();
    }

    private void Start()
    {
        textureLight = new Texture2D(textureSize, textureSize, TextureFormat.RGB24, false);
        textureTemp = new RenderTexture(textureSize, textureSize, 24);
        rectLight = new Rect(0f, 0f, textureSize, textureSize);
    }

    private void Update()
    {
        LightDetection();
    }

    private void LightDetection()
    {
        if(!lightDetectionEnabled) return;

        lightDetectionCamera.targetTexture = textureTemp; //La camara renderizara en textureTemp
        lightDetectionCamera.Render(); //La camara renderiza (en textureTemp)
       
        RenderTexture.active = textureTemp; //textureTemp es seleccionada como la textura renderizada activa, las siguientes operaciones de renderizado y lectura se realizaran de esta textura
        textureLight.ReadPixels(rectLight, 0, 0); //Se lee los pixels de la textura activa (textureTemp) en una porcion con las propiedades (tama�o y posicion) de rectLight, la informacion se transfiere a textureLight

        RenderTexture.active = null; //Se resetea la textura renderizada activa
        lightDetectionCamera.targetTexture = null; //Se resetea la textura en la cual la camara renderiza

        lightPixel = textureLight.GetPixel(textureSize/2, textureSize/2); //Se lee el pixel al medio de la textura

        lightValue = (lightPixel.r + lightPixel.g + lightPixel.b) / 3f; //Se obtiene el valor de la luz mediante un promedio del RGB
    }
}