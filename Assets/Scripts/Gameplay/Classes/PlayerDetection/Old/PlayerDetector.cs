using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDetector : MonoBehaviour
{
    [SerializeField] private bool detectorEnabled;

    public bool detectorOnCooldown;
    public bool flashlightSensitive;
    public bool sightSensitive;

    public float detectorCooldownCounter;

    void Update()
    {
        CheckDetectorCooldown();
    }

    public void StartDetectorCooldown(float cooldownTime)
    {
        detectorOnCooldown = true;
        detectorCooldownCounter = cooldownTime;
    }

    private void CheckDetectorCooldown()
    {
        if (!detectorOnCooldown) return;

        if (detectorCooldownCounter >= 0)
        {
            detectorCooldownCounter -= Time.deltaTime;
        }
        else
        {
            detectorCooldownCounter = 0f;
            detectorOnCooldown = false;
        }
    }
}
