using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerDetection : MonoBehaviour
{
    [Header("Player Detection Setings")]
    [SerializeField] private List<PlayerDetector> playerDetectors = new List<PlayerDetector>();
    [SerializeField,Range(2f,6f)] private float detectionDistanceThreshold;
    [SerializeField, Range(10f, 120f)] private float detectionCooldownTime;
    [SerializeField, Range(20f, 200f)] private float detectorCooldownTime;

    [Header("Sight Detection Setings")]
    [SerializeField] private Camera playerCamera;
    [SerializeField,Range(10f,30f)] private float sightAngleThreshold;

    private GameObject player;

    public PlayerDetector activeDetector;
    private float activeDetectorDistanceToPlayer;
    public bool detectionOnCooldown;
    public float detectionCooldownCounter;

    public static Action<PlayerDetector> OnPlayerDetected;

    private void OnEnable()
    {
        OnPlayerDetected += StartDetectionCooldown;
    }
    private void OnDisable()
    {
        OnPlayerDetected -= StartDetectionCooldown;
    }

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        CheckDetectionCooldown();

        ChooseActiveDetector();
        HandlePlayerDetection();
    }

    private void ChooseActiveDetector()
    {
        PlayerDetector auxCloserPlayerDetector = null;
        float leastDistanceToPlayer = float.MaxValue;

        foreach (PlayerDetector playerDetector in playerDetectors)
        {
            if (playerDetector.detectorOnCooldown) continue;

            float distance = Vector3.Distance(playerDetector.transform.position, player.transform.position);

            if (distance < leastDistanceToPlayer)
            {
                auxCloserPlayerDetector = playerDetector;
                leastDistanceToPlayer = distance;
            }
        }

        activeDetector = auxCloserPlayerDetector;     
        activeDetectorDistanceToPlayer = leastDistanceToPlayer;
    }

    private void HandlePlayerDetection()
    {
        if (detectionOnCooldown) return;

        if (!activeDetector) return;

        if (!(activeDetectorDistanceToPlayer <= detectionDistanceThreshold)) return;

        if (activeDetector.flashlightSensitive && !CheckPlayerFlashlightOn()) return;

        if (activeDetector.sightSensitive && !CheckPlayerLookingAtPoint(activeDetector.transform,sightAngleThreshold)) return;

        OnPlayerDetected?.Invoke(activeDetector);
    }

    private bool CheckPlayerFlashlightOn()
    {
        if (player.GetComponent<PlayerFlashlight>().flashlightOn) return true;

        return false;
    }

    private bool CheckPlayerLookingAtPoint(Transform point, float angleThreshold)
    {
        Vector3 straightDirection = (point.position - playerCamera.transform.position).normalized;
        Vector3 cameraDirection = playerCamera.transform.forward;

        float angle = Vector3.Angle(straightDirection, cameraDirection);

        if (angle < angleThreshold) return true;
        
        return false;
    }

    private void StartDetectionCooldown(PlayerDetector playerDetector)
    {
        Debug.Log(playerDetector.name + " has detected the player");

        detectionOnCooldown = true;
        detectionCooldownCounter = detectionCooldownTime;

        playerDetector.StartDetectorCooldown(detectorCooldownTime);
    }

    private void CheckDetectionCooldown()
    {
        if (!detectionOnCooldown) return;

        if(detectionCooldownCounter >= 0)
        {
            detectionCooldownCounter -= Time.deltaTime;
        }
        else
        {
            detectionCooldownCounter = 0f;
            detectionOnCooldown = false;
        }

    }
}
