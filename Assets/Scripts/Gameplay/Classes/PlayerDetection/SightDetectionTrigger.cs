using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SightDetectionTrigger : DetectionTrigger
{
    [Header("Sight Detector Setings")]
    [SerializeField, Range(0f, 10f)] protected float detectionDistance;
    [SerializeField, Range(0f, 180f)] protected float sightAngleTolerance;

    protected GameObject player;
    protected GameObject playerCamera;

    protected override void Awake()
    {
        base.Awake();

        player = GameObject.FindGameObjectWithTag("Player");
        playerCamera = GameObject.FindGameObjectWithTag("MainCamera");
    }

    protected override void Update()
    {
        base.Update();

        CheckDetection();
    }

    private void CheckDetection()
    {
        if (!canBeTriggered) return;
        if (hasTriggered && isOnlyTriggeredOnce) return;
        if (onCooldown) return;

        if (!CheckPlayerLookingAtPoint()) return;
        if (!CheckPlayerInDetectionDistance()) return;

        DoWhenPlayerDetected();
    }

    private bool CheckPlayerInDetectionDistance()
    {
        float currentDistanceToDetector = Vector3.Distance(transform.position, player.transform.position);

        if (currentDistanceToDetector <= detectionDistance) return true;

        return false;
    }

    private bool CheckPlayerLookingAtPoint()
    {
        Vector3 straightDirection = (transform.position - playerCamera.transform.position).normalized;
        Vector3 cameraDirection = playerCamera.transform.forward;

        float currentSightAngle = Vector3.Angle(straightDirection, cameraDirection);

        if (currentSightAngle <= sightAngleTolerance) return true;

        return false;
    }

}
