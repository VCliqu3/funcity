using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TutorialColliderDetectionTrigger : ColliderDetectionTrigger
{
    [Header("Tutorial Collider Detection Trigger Settings")]
    [SerializeField] private int tutorialPanelID;

    [Header("Simulate Enter Settings")]
    [SerializeField] private bool simulateEnterEnabled;
    [SerializeField] private float timeToSimulateEnter;

    public static Action<int> OnPlayerEnter;

    protected override void OnEnable()
    {
        base.OnEnable();
        if(simulateEnterEnabled) SimulateEnter();
    }
    protected override void DoWhenPlayerDetected()
    {
        base.DoWhenPlayerDetected();

        if(!simulateEnterEnabled) OnPlayerEnter?.Invoke(tutorialPanelID);
    }

    private void SimulateEnter()
    {
        StartCoroutine(SimulateEnterAfterTime());
    }

    private IEnumerator SimulateEnterAfterTime()
    {
        yield return new WaitForSeconds(timeToSimulateEnter);

        OnPlayerEnter?.Invoke(tutorialPanelID);
    }
}
