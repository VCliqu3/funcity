using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateInstructionTrigger : TutorialColliderDetectionTrigger
{
    protected override void OnEnable()
    {
        base.OnEnable();
        TicketOfficeCard.OnTicketOfficeCardPickUp += DoWhenTicketOfficeCardPickUp;
    }

    protected override void OnDisable()
    {
        base.OnEnable();
        TicketOfficeCard.OnTicketOfficeCardPickUp -= DoWhenTicketOfficeCardPickUp;
    }

    private void DoWhenTicketOfficeCardPickUp()
    {
        canBeTriggered = false;
    }
}
