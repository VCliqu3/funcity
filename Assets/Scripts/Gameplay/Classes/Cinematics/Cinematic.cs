using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Cinematic : MonoBehaviour
{
    [SerializeField] protected Transform playerCamera;
    [SerializeField] protected Transform cameraAnimationHolder;

    [Header("PlayerLookTowards Settings")]
    [SerializeField] protected Transform playerLookTowardsPos;
    [SerializeField, Range(5f, 20f)] private float playerLookTowardsSpeed = 10f;
    [SerializeField, Range(0.01f, 0.1f)] private float playerLookTowardsThreshold = 0.1f;

    [Header("PlayerMoveTowards Settings")]
    [SerializeField] protected Transform playerMoveTowardsPos;
    [SerializeField, Range(5f, 20f)] private float playerMoveTowardsSpeed = 10f;
    [SerializeField, Range(0.01f, 0.1f)] private float playerMoveTowardsThreshold = 0.1f;

    protected Animator cameraAnimationHolderAnimator;
    protected PlayerHorizontalMovement playerHorizontalMovement;
    protected PlayerLook playerLook;
    protected PlayerFlashlight playerFlashlight;

    protected virtual void Awake()
    {
        cameraAnimationHolderAnimator = cameraAnimationHolder.GetComponent<Animator>();

        playerHorizontalMovement = FindObjectOfType<PlayerHorizontalMovement>();
        playerLook = FindObjectOfType<PlayerLook>();
        playerFlashlight = FindObjectOfType<PlayerFlashlight>();
    }

    protected virtual void PreDeathLogic()
    {
        playerHorizontalMovement.playerHorizontalMovementEnabled = false;
        playerLook.playerLookEnabled = false;
        playerFlashlight.flashlightToggleEnabled = false;
        playerFlashlight.ForceFlashlightState(true);
    }

    protected IEnumerator PlayerLookTowards()
    {
        playerLook.playerLookEnabled = false;

        Quaternion targetRot = CalculateTargetCameraRotation(playerCamera, playerLookTowardsPos);

        while (playerCamera.rotation != targetRot)
        {
            playerCamera.rotation = Quaternion.Lerp(playerCamera.rotation, targetRot, playerLookTowardsSpeed * Time.deltaTime);

            float angle = Quaternion.Angle(playerCamera.rotation, targetRot);

            if (angle < playerLookTowardsThreshold)
            {
                playerCamera.rotation = targetRot;
                break;
            }

            yield return null;
        }

        Debug.Log("Player Look Towards Completed");
    }
    protected IEnumerator PlayerMoveTowards()
    {
        playerHorizontalMovement.playerHorizontalMovementEnabled = false;

        Vector3 targetPos = playerMoveTowardsPos.position;

        while (playerCamera.position != targetPos)
        {
            playerCamera.position = Vector3.Lerp(playerCamera.position, targetPos, playerMoveTowardsSpeed * Time.deltaTime);

            float distance = Vector3.Distance(playerCamera.position, targetPos);

            if (distance < playerMoveTowardsThreshold)
            {
                playerCamera.position = targetPos;
                break;
            }

            yield return null;

            Debug.Log("Player Move Towards Completed");
        }
    }

    protected IEnumerator PlayerLookAndMoveTowards()
    {
        playerLook.playerLookEnabled = false;
        playerHorizontalMovement.playerHorizontalMovementEnabled = false;
     
        Vector3 targetPos = playerMoveTowardsPos.position;

        while (playerCamera.position != targetPos)
        {
            Quaternion targetRot = CalculateTargetCameraRotation(playerCamera, playerLookTowardsPos);

            playerCamera.rotation = Quaternion.Lerp(playerCamera.rotation, targetRot, playerLookTowardsSpeed * Time.deltaTime);
            playerCamera.position = Vector3.Lerp(playerCamera.position, targetPos, playerMoveTowardsSpeed * Time.deltaTime);

            float angle = Quaternion.Angle(playerCamera.rotation, targetRot);
            float distance = Vector3.Distance(playerCamera.position, targetPos);

            if (angle < playerLookTowardsThreshold && distance < playerMoveTowardsThreshold)
            {
                playerCamera.rotation = targetRot;
                playerCamera.position = targetPos;
                break;
            }

            yield return null;
        }

        Debug.Log("Player Look And Move Towards Completed");
    }

    private Quaternion CalculateTargetCameraRotation(Transform camera, Transform target)
    {
        Vector3 directionToTarget = target.position - camera.position;

        float pitch = Mathf.Asin(directionToTarget.normalized.y) * Mathf.Rad2Deg;
        float yaw = Mathf.Atan2(directionToTarget.x, directionToTarget.z) * Mathf.Rad2Deg;

        Quaternion rotation = Quaternion.Euler(-pitch, yaw, 0);

        return rotation;
    }

    private Quaternion CalculateTargetPlayerRotation(Transform player, Transform target)
    {
        Vector3 directionToTarget = target.position - player.position;

        float yaw = Mathf.Atan2(directionToTarget.x, directionToTarget.z) * Mathf.Rad2Deg;

        Quaternion rotation = Quaternion.Euler(0, yaw, 0);

        return rotation;
    }

    private Quaternion CalculateTargetPitchRotation(Transform pitchHolder, Transform target)
    {
        Vector3 directionToTarget = target.position - pitchHolder.position;

        float pitch = Mathf.Asin(directionToTarget.normalized.y) * Mathf.Rad2Deg;

        Quaternion rotation = Quaternion.Euler(-pitch, 0, 0);

        return rotation;
    }
}
