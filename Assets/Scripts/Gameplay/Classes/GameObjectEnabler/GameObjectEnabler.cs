using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectEnabler : MonoBehaviour
{
    [SerializeField] protected List<GameObject> objectsToEnable = new List<GameObject>();

    protected void EnableAllGameObjects()
    {
        foreach(GameObject obj in objectsToEnable)
        {
            obj.SetActive(true);
        }
    }
}
