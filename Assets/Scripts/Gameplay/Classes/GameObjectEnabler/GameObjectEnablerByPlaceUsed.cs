using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectEnablerByPlaceUsed : GameObjectEnabler
{
    [SerializeField] private UsablePlace usablePlace;
    protected void OnEnable()
    {
        UsableObject.OnObjectUsed += CheckObjectUsed;
    }

    protected void OnDisable()
    {
        UsableObject.OnObjectUsed -= CheckObjectUsed;
    }

    protected void CheckObjectUsed(UsableObject usableObject, UsablePlace usablePlace)
    {
        if(usablePlace == this.usablePlace)
        {
            Debug.Log(gameObject.name + " Activated");

            foreach (GameObject obj in objectsToEnable)
            {
                obj.SetActive(true);
            }
        }
    }
}
