using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectEnablerByPuzzleCompleted : GameObjectEnabler
{
    [SerializeField] private Puzzle puzzle;
    protected void OnEnable()
    {
        Puzzle.OnPuzzleCompleted += CheckPuzzleCompleted;
    }

    protected void OnDisable()
    {
        Puzzle.OnPuzzleCompleted -= CheckPuzzleCompleted;
    }
    protected void CheckPuzzleCompleted(Puzzle puzzle)
    {
        if (puzzle == this.puzzle)
        {
            Debug.Log(gameObject.name + " Activated");

            foreach (GameObject obj in objectsToEnable)
            {
                obj.SetActive(true);
            }
        }
    }
}
