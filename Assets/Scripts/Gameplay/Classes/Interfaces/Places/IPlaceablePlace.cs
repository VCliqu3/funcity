using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlaceablePlace
{
    public void OnPlacePlaced(PlayerUse playerUse, PlaceableObject placeableObject);
}
