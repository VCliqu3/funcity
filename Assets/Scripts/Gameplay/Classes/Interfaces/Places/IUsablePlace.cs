using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUsablePlace
{
    public void OnPlayerEnterUsablePlaceDetectionRange();
    public void OnPlayerLeaveUsablePlaceDetectionRange();
    public void OnPlayerEnterUsablePlaceSightRange();
    public void OnPlayerLeaveUsablePlaceSightRange();
    public void TryUseHere(PlayerUse playerUse, UsableObject usableObject, bool autoUse);
    public void OnPlaceUsed(PlayerUse playerUse, UsableObject usableObject);
    public void OnPlaceUsedWrong(PlayerUse playerUse, UsableObject usableObject);
}
