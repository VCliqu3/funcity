using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUsable 
{
    public void UseWithoutUsablePlace(PlayerUse playerUse);
    public void Use(PlayerUse playerUse, UsablePlace usablePlace);
    public void UseWrong(PlayerUse playerUse, UsablePlace usablePlace);
}
