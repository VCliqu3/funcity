using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlaceable
{
    public void Place(PlayerUse playerUse, PlaceablePlace placeablePlace);
    public void PlaceWrong(PlayerUse playerUse, PlaceablePlace placeablePlace);
}
