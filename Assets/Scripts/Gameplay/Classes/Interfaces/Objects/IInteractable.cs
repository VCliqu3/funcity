using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable 
{
    public void OnPlayerEnterInteractionDetectionRange();
    public void OnPlayerLeaveInteractionDetectionRange();
    public void OnPlayerEnterInteractionSightRange();
    public void OnPlayerLeaveInteractionSightRange();
    public void Interact(PlayerInteract playerInteract);
}
