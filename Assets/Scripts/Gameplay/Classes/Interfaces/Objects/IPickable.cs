using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPickable 
{
    public void OnPlayerEnterPickUpDetectionRange();
    public void OnPlayerLeavePickUpDetectionRange();
    public void OnPlayerEnterPickUpSightRange();
    public void OnPlayerLeavePickUpSightRange();
    public void PickUp(PlayerPickUp playerPickUp, Transform pickUpSocket);
    public void Drop(PlayerPickUp playerPickUp);
}
