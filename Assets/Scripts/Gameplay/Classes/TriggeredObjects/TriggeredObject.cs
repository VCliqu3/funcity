using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TriggeredObject : MonoBehaviour
{
    [Header("Triggered Object Settings")]
    [SerializeField] private bool isOnlyTriggeredOnce;
    [SerializeField] protected bool hasBeenTriggered;

    public static Action<TriggeredObject> OnObjectTriggered;
    private void OnEnable()
    {
        UsableObject.OnObjectUsed += CheckObjectUsedInPlace;
        Puzzle.OnPuzzleCompleted += CheckIfPuzzleCompleted;
        InteractableObject.OnObjectInteracted += CheckIfObjectInteracted;
    }

    private void OnDisable()
    {
        UsableObject.OnObjectUsed -= CheckObjectUsedInPlace;
        Puzzle.OnPuzzleCompleted -= CheckIfPuzzleCompleted;
        InteractableObject.OnObjectInteracted -= CheckIfObjectInteracted;
    }

    protected virtual bool CheckIfThisObjectShouldTrigger() 
    {
        if (hasBeenTriggered && isOnlyTriggeredOnce) return false;

        return true;  
    }

    protected virtual void TriggerObject()
    {
        hasBeenTriggered = true;
        OnObjectTriggered?.Invoke(this);
    }

    protected virtual void CheckIfPuzzleCompleted(Puzzle puzzle) {}
    protected virtual void CheckObjectUsedInPlace(UsableObject usableObject, UsablePlace usablePlace) { }
    protected virtual void CheckIfObjectInteracted(InteractableObject interactableObject) { }
}
