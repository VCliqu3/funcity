using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionTriggeredObject : TriggeredObject
{
    [Header("Interactable Objects To Trigger")]
    [SerializeField] private List<InteractableObject> interactableObjectsToTrigger = new List<InteractableObject>();

    protected override void CheckIfObjectInteracted(InteractableObject interactableObject)
    {
        base.CheckIfObjectInteracted(interactableObject);

        if (!interactableObjectsToTrigger.Contains(interactableObject)) return;

        CheckIfThisObjectShouldTrigger();
    }

    protected override bool CheckIfThisObjectShouldTrigger()
    {
        if((!base.CheckIfThisObjectShouldTrigger())) return false;

        //TriggerObject() Debe ir al final de la condicion de trigger

        return true;
    }
}
