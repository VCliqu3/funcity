using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleTriggeredGate : PuzzleTriggeredObject
{
    [SerializeField] private bool startClosed = true;
    [SerializeField] private float closedAngle = 0f;
    [SerializeField] private float openedAngle = 90f;
    [SerializeField] private float rotationSpeed = 1f;

    private float targetAngle;

    private void Start()
    {
        SetStartRotation();
    }

    void Update()
    {
        CheckRotation();
    }

    private void CheckRotation()
    {
        Quaternion targetRotation = Quaternion.Euler(transform.localRotation.x, targetAngle, transform.localRotation.z);

        if (transform.localRotation == targetRotation) return;
        transform.localRotation = Quaternion.Lerp(transform.localRotation, targetRotation, Time.deltaTime * rotationSpeed);
    }

    public void OpenGate()
    {
        targetAngle = openedAngle;
    }

    public void CloseGate()
    {
        targetAngle = closedAngle;
    }

    protected override bool CheckIfThisObjectShouldTrigger()
    {
        if (!(base.CheckIfThisObjectShouldTrigger())) return false;

        OpenGate();

        return true;
    }

    private void SetStartRotation()
    {
        targetAngle = startClosed ? closedAngle : openedAngle;
    }
}
