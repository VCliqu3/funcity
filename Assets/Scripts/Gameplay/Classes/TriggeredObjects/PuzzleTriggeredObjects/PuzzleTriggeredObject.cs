using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleTriggeredObject : TriggeredObject
{
    [Header("Puzzles To Trigger")]
    [SerializeField] private List<Puzzle> puzzlesToTrigger = new List<Puzzle>();

    protected override void CheckIfPuzzleCompleted(Puzzle puzzle)
    {
        base.CheckIfPuzzleCompleted(puzzle);

        if (!puzzlesToTrigger.Contains(puzzle)) return;

        CheckIfThisObjectShouldTrigger();
    }

    protected override bool CheckIfThisObjectShouldTrigger()
    {
        if(!base.CheckIfThisObjectShouldTrigger()) return false;

        foreach (Puzzle puzzle in puzzlesToTrigger)
        {
            if (!puzzle.puzzleIsCompleted) return false;
        }

        TriggerObject();

        return true;
    }
}
