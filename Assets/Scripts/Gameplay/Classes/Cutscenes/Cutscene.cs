using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class CutsceneTransformSpeedRelation
{
    public Transform target;
    [Range(1f,20f)] public float speed;
}

public class Cutscene : MonoBehaviour
{
    [SerializeField] protected Transform player;
    [SerializeField] protected Transform pitchHolder;
    [SerializeField] protected CutsceneTransformSpeedRelation[] cutsceneRelations;

    [Header("PlayerLookTowards Settings")]
    [SerializeField, Range(0.01f, 0.1f)] private float playerLookTowardsThreshold = 0.1f;

    [Header("Checkpoint Load Settings")]
    [SerializeField] protected bool cutsceneEnabled = true;
    [SerializeField] private bool disableByCheckpointLoad;
    [SerializeField] private int checkpointNumberToEnable;

    [SerializeField] protected LoadType loadType;
    protected enum LoadType { OnlyThisCheckpoint, AllCheckpointsUntilThis }

    protected PlayerLook playerLook;
    protected PlayerHorizontalMovement playerHorizontalMovement;
    protected PlayerFlashlight playerFlashlight;

    public static Action OnCutsceneStart;
    public static Action OnCutsceneEnd;

    protected virtual void OnEnable()
    {
        InGameCheckpointManager.OnCheckpointLoad += CheckDisableByCheckpointLoad;
    }
    protected virtual void OnDisable()
    {
        InGameCheckpointManager.OnCheckpointLoad -= CheckDisableByCheckpointLoad;
    }

    protected virtual void Awake()
    {
        playerLook = FindObjectOfType<PlayerLook>();
        playerHorizontalMovement = FindObjectOfType<PlayerHorizontalMovement>();
        playerFlashlight = FindObjectOfType<PlayerFlashlight>();
    }

    protected virtual void PreCutsceneLogic()
    {
        OnCutsceneStart?.Invoke();

        playerLook.playerLookEnabled = false;

        playerHorizontalMovement.playerHorizontalMovementEnabled = false;
        playerHorizontalMovement.horizontalSpeed = 0f;

        playerFlashlight.flashlightToggleEnabled = false;
        playerFlashlight.ForceFlashlightState(true);
    }

    protected virtual void PostCutsceneLogic()
    {
        playerLook.yAccumulator = 0f;
        playerLook.cameraVerticalAngle = AngleTo1stQuadrant(pitchHolder.localEulerAngles.x);

        playerLook.playerLookEnabled = true;

        playerHorizontalMovement.playerHorizontalMovementEnabled = true;

        playerFlashlight.flashlightToggleEnabled = true;

        OnCutsceneEnd?.Invoke();
    }

    protected IEnumerator PlayerLookTowards(CutsceneTransformSpeedRelation relation)
    {
        Transform target = relation.target;
        float playerLookTowardsSpeed = relation.speed;

        Quaternion targetPlayerRotation = CalculateTargetPlayerRotation(player.transform, target);
        Quaternion targetPitchHolderRotation = CalculateTargetPitchRotation(pitchHolder.transform, target);

        while (player.localRotation != targetPlayerRotation)
        {
            player.localRotation = Quaternion.Lerp(player.localRotation, targetPlayerRotation, playerLookTowardsSpeed * Time.deltaTime);
            pitchHolder.localRotation = Quaternion.Lerp(pitchHolder.localRotation, targetPitchHolderRotation, playerLookTowardsSpeed * Time.deltaTime);

            float anglePlayer = Quaternion.Angle(player.rotation, targetPlayerRotation);

            if (anglePlayer < playerLookTowardsThreshold)
            {
                player.localRotation = targetPlayerRotation;
                pitchHolder.localRotation = targetPitchHolderRotation;
                break;
            }

            yield return null;
        }

        Debug.Log("Player Look And Move Towards Completed");
    }

    private Quaternion CalculateTargetCameraRotation(Transform camera, Transform target)
    {
        Vector3 directionToTarget = target.position - camera.position;

        float pitch = Mathf.Asin(directionToTarget.normalized.y) * Mathf.Rad2Deg;
        float yaw = Mathf.Atan2(directionToTarget.x, directionToTarget.z) * Mathf.Rad2Deg;

        Quaternion rotation = Quaternion.Euler(-pitch, yaw, 0);

        return rotation;
    }

    private Quaternion CalculateTargetPlayerRotation(Transform player, Transform target)
    {
        Vector3 directionToTarget = target.position - player.position;

        float yaw = Mathf.Atan2(directionToTarget.x, directionToTarget.z) * Mathf.Rad2Deg;

        Quaternion rotation = Quaternion.Euler(0, yaw, 0);

        return rotation;
    }

    private Quaternion CalculateTargetPitchRotation(Transform pitchHolder, Transform target)
    {
        Vector3 directionToTarget = target.position - pitchHolder.position;

        float pitch = Mathf.Asin(directionToTarget.normalized.y) * Mathf.Rad2Deg;

        Quaternion rotation = Quaternion.Euler(-pitch, 0, 0);

        return rotation;
    }

    protected void CheckDisableByCheckpointLoad(int checkpointNumber, Transform checkpointTransform)
    {
        if (!disableByCheckpointLoad) return;

        switch (loadType)
        {
            case (LoadType.OnlyThisCheckpoint):
                if (checkpointNumber != checkpointNumberToEnable) cutsceneEnabled = false;
                break;

            case (LoadType.AllCheckpointsUntilThis):
            default:
                if (checkpointNumber > checkpointNumberToEnable) cutsceneEnabled = false;
                break;
        }
    }

    protected float AngleTo1stQuadrant(float angle)
    {
        float newAngle = angle % 360;

        if (Mathf.Abs(newAngle) > 90)
        {
            newAngle = newAngle - 360;
        }
        return newAngle;
    }
}
