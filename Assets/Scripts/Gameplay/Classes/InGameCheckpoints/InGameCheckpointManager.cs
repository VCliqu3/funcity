using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class CheckPointTrasformNumberRelation
{
    public int checkpointNumber;
    public Transform linkedCheckpointTransform;
}

public class InGameCheckpointManager : MonoBehaviour
{
    public static InGameCheckpointManager instance;
    
    [SerializeField] private bool enableCheckpoints = true;
    public int currentCheckpointNumber;

    public bool updateCheckpointsWithoutDatabase;

    [SerializeField] private CheckPointTrasformNumberRelation[] checkpointRelations;

    public static Action<int,Transform> OnCheckpointLoad;
    public static Action<int,Transform> OnCheckpointUpdate;

    private void OnEnable()
    {
        GameManager.OnGameStart += DoWhenGameStart;
        
        ExitTicketOfficeTrigger.OnPlayerEnter += DoWhenPlayerExitTicketOffice;

        EnterHospitalTrigger.OnPlayerEnter += DoWhenPlayerEnterHospital;
        SurgeryRoomEncounterTrigger.OnPlayerEnter += DoWhenPlayerEnterSurgeryRoom;

        SecondHospitalSectionTrigger.OnPlayerEnter += DoWhenPlayerEnterSecondHallSection;
        BabyRoomEncounterTrigger.OnPlayerEnter += DoWhenPlayerEnterBabyRoom;
        BabyRoomEncounter.OnBabyRoomEncounterCompleted += DoWhenBabyRoomEncounterCompleted;

        ExitHospitalTrigger.OnPlayerEnter += DoWhenPlayerExitHospital;

        EnterTVSetTrigger.OnPlayerEnter += DoWhenPlayerEnterTVSet;
        TVSetWarehouseEncounterTrigger.OnPlayerEnter += DoWhenPlayerEnterTVSetWarehouse;
        EnterTVSetHallTrigger.OnPlayerEnter += DoWhenPlayerEnterTVSetHall;
        TVSetAngelicaSpawnTrigger.OnPlayerEnter += DoWhenPersecutionAngelicaSpawn;

        ExitTVSetTrigger.OnPlayerEnter += DoWhenPlayerExitTVSet;      

        GameManager.OnGameFinalCinematicEnd += ClearCheckpointNumber;
    }
    private void OnDisable()
    {
        GameManager.OnGameStart -= DoWhenGameStart;
        
        ExitTicketOfficeTrigger.OnPlayerEnter -= DoWhenPlayerExitTicketOffice;

        EnterHospitalTrigger.OnPlayerEnter -= DoWhenPlayerEnterHospital;
        SurgeryRoomEncounterTrigger.OnPlayerEnter -= DoWhenPlayerEnterSurgeryRoom;

        SecondHospitalSectionTrigger.OnPlayerEnter -= DoWhenPlayerEnterSecondHallSection;
        BabyRoomEncounterTrigger.OnPlayerEnter -= DoWhenPlayerEnterBabyRoom;
        BabyRoomEncounter.OnBabyRoomEncounterCompleted -= DoWhenBabyRoomEncounterCompleted;

        ExitHospitalTrigger.OnPlayerEnter -= DoWhenPlayerExitHospital;

        EnterTVSetTrigger.OnPlayerEnter -= DoWhenPlayerEnterTVSet;
        TVSetWarehouseEncounterTrigger.OnPlayerEnter -= DoWhenPlayerEnterTVSetWarehouse;
        EnterTVSetHallTrigger.OnPlayerEnter -= DoWhenPlayerEnterTVSetHall;
        TVSetAngelicaSpawnTrigger.OnPlayerEnter -= DoWhenPersecutionAngelicaSpawn;

        ExitTVSetTrigger.OnPlayerEnter -= DoWhenPlayerExitTVSet;
        

        GameManager.OnGameFinalCinematicEnd -= ClearCheckpointNumber;
    }
    private void Awake()
    {
        SetSingleton();       
    }

    private void Update()
    {
        CheckClearCheckpoint(); //Resetear checkpoint con la tecla "l"
    }

    private void SetSingleton()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private bool CheckUsingDatabase()
    {
        return LoadedDatabaseManager.instance.enableDataLoad && LoadedDatabaseManager.instance.hasLoadedData;
    }

    #region General

    private void UpdateCheckpointNumber(int checkpointNumber) 
    {
        if (CheckUsingDatabase())
        {
            UpdateCheckpointNumberToDatabase(UserManager.instance.username, checkpointNumber, UpdateCheckpointNumberLocal);
        }
        else 
        {
            UpdateCheckpointNumberPlayerPrefs(checkpointNumber);
        }
    }

    private void UpdateCheckpointNumberLocal(int checkpointNumber)
    {
        UserManager.instance.checkpointNumber = checkpointNumber;
        currentCheckpointNumber = checkpointNumber;

        CheckPointTrasformNumberRelation checkpointRelation = Array.Find(checkpointRelations, x => x.checkpointNumber == currentCheckpointNumber);

        if (checkpointRelation == null)
        {
            Debug.Log("Checkpoint Relation not found - Checkpoint Number: " + currentCheckpointNumber);
            return;
        }

        OnCheckpointUpdate?.Invoke(checkpointNumber, checkpointRelation.linkedCheckpointTransform);
    }

    private void ClearCheckpointNumber()
    {
        if (CheckUsingDatabase())
        {
            int firstCheckpoint = 0;
            UpdateCheckpointNumberToDatabase(UserManager.instance.username, firstCheckpoint, UpdateCheckpointNumberLocal);
        }
        else
        {
            ClearCheckpointNumberPlayerPrefs();
        }
    }

    #endregion

    #region PlayerPrefs

    public void CheckInitializeCheckpointPlayerPrefs()
    {
        if (CheckUsingDatabase()) return;
        if (!updateCheckpointsWithoutDatabase) return;

        if (!PlayerPrefs.HasKey("CheckpointNumber"))
        {
            PlayerPrefs.SetInt("CheckpointNumber", 0);
        }
    }

    public void UpdateCheckpointNumberPlayerPrefs(int checkpointNumber)
    {
        if (!updateCheckpointsWithoutDatabase) return;

        currentCheckpointNumber = checkpointNumber;
        PlayerPrefs.SetInt("CheckpointNumber", checkpointNumber);

        CheckPointTrasformNumberRelation checkpointRelation = Array.Find(checkpointRelations, x => x.checkpointNumber == currentCheckpointNumber);

        if(checkpointRelation == null)
        {
            Debug.Log("Checkpoint Relation not found - Checkpoint Number: " + currentCheckpointNumber);
            return;
        }

        OnCheckpointUpdate?.Invoke(checkpointNumber, checkpointRelation.linkedCheckpointTransform);
    }

    public void LoadCheckpointPlayerPrefs()
    {
        if (!updateCheckpointsWithoutDatabase) return;

        currentCheckpointNumber = PlayerPrefs.GetInt("CheckpointNumber");

        CheckPointTrasformNumberRelation checkpointRelation = Array.Find(checkpointRelations, x => x.checkpointNumber == currentCheckpointNumber);

        if(checkpointRelation == null)
        {
            Debug.Log("Checkpoint Relation not found - Checkpoint Number: " + currentCheckpointNumber);
            return;
        }

        OnCheckpointLoad?.Invoke(currentCheckpointNumber, checkpointRelation.linkedCheckpointTransform);
    }

    private void ClearCheckpointNumberPlayerPrefs()
    {
        if (!updateCheckpointsWithoutDatabase) return;

        PlayerPrefs.DeleteKey("CheckpointNumber");
    }
    #endregion

    #region Database

    private void LoadCheckpointNumberFromLoadedDatabase()
    {
        currentCheckpointNumber = UserManager.instance.checkpointNumber;

        CheckPointTrasformNumberRelation checkpointRelation = Array.Find(checkpointRelations, x => x.checkpointNumber == currentCheckpointNumber);

        if (checkpointRelation == null)
        {
            Debug.Log("Checkpoint Relation not found - Checkpoint Number: " + currentCheckpointNumber);
            return;
        }

        OnCheckpointLoad?.Invoke(currentCheckpointNumber, checkpointRelation.linkedCheckpointTransform);
    }

    private void UpdateCheckpointNumberToDatabase(string username, int checkpointNumber, Action<int> OnFinish)
    {
        DatabaseManager.instance.UpdateCheckpointNumberToDatabase(username, checkpointNumber, OnFinish);
    }
    #endregion

    #region Subscribers
    private void DoWhenGameStart()
    {
        if (!enableCheckpoints) return;

        if (CheckUsingDatabase())
        {
            LoadCheckpointNumberFromLoadedDatabase();

        }
        else
        {
            CheckInitializeCheckpointPlayerPrefs();
            LoadCheckpointPlayerPrefs();
        }
    }

    private void DoWhenPlayerExitTicketOffice()
    {
        UpdateCheckpointNumber(1);
    }
    private void DoWhenPlayerEnterHospital()
    {
        UpdateCheckpointNumber(2);
    }

    private void DoWhenPlayerEnterSurgeryRoom()
    {
        UpdateCheckpointNumber(3);
    }
    private void DoWhenPlayerEnterSecondHallSection()
    {
        UpdateCheckpointNumber(4);
    }

    private void DoWhenPlayerEnterBabyRoom()
    {
        UpdateCheckpointNumber(5);
    }

    private void DoWhenBabyRoomEncounterCompleted()
    {
        UpdateCheckpointNumber(6);
    }

    private void DoWhenPlayerExitHospital()
    {
        UpdateCheckpointNumber(7);
    }

    private void DoWhenPlayerEnterTVSet()
    {
        UpdateCheckpointNumber(8);
    }

    private void DoWhenPlayerEnterTVSetWarehouse()
    {
        UpdateCheckpointNumber(9);
    }
    private void DoWhenPlayerEnterTVSetHall()
    {
        UpdateCheckpointNumber(10);
    }
    private void DoWhenPersecutionAngelicaSpawn()
    {
        UpdateCheckpointNumber(11);
    }

    private void DoWhenPlayerExitTVSet()
    {
        UpdateCheckpointNumber(12);
    }
    #endregion

    private void CheckClearCheckpoint()
    {
        if (!Input.GetKeyDown(KeyCode.L)) return;

        ClearCheckpointNumber();
    }
}
