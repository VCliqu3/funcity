using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EncounterClock : MonoBehaviour
{
    [SerializeField] private TMP_Text clockText;

    [SerializeField] private AudioClip changeTimeClip;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponentInChildren<AudioSource>();
    }

    protected void SetClockTime(int time)
    {
        if (changeTimeClip) audioSource.PlayOneShot(changeTimeClip);

        int minutes = time / 60;
        int seconds = time - 60 * minutes;

        string minutesText = minutes < 10 ? "0" + minutes : minutes.ToString();
        string secondsText = seconds < 10? "0" + seconds : seconds.ToString();

        if(time == 0)
        {
            ClearClock();
            return;
        }

        SetClockText(minutesText + ":" + secondsText);
    }

    protected void InitialSetClockTime(int time)
    {
        int minutes = time / 60;
        int seconds = time - 60 * minutes;

        string minutesText = minutes < 10 ? "0" + minutes : minutes.ToString();
        string secondsText = seconds < 10 ? "0" + seconds : seconds.ToString();

        if (time == 0)
        {
            ClearClock();
            return;
        }

        SetClockText(minutesText + ":" + secondsText);
    }

    protected void ClearClock()
    {
        SetClockText("");
    }

    protected void SetClockText(string text)
    {
        clockText.text = text;
    }
}
