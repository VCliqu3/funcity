using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RegularEncounter : Encounter
{
    [Header("Regular Encounter Settings")]
    [SerializeField] protected float timeStarting;
    [SerializeField] protected float timeEnding;

    //[SerializeField] protected GameObject deathScareTrigger;
    [SerializeField] protected float timeToDie;

    protected float time = 0f;
    private bool hasStarted = false;

    protected override void Start()
    {
        base.Start();
        ResetTimer();
    }

    protected override void NotStartedEncounterLogic() { }
    protected override void StartingEncounterLogic()
    {
        if (time <= timeStarting)
        {
            if (time <= 0f)
            {
                OnEncounterStarting();
                Debug.Log("Encounter Starting");
            }

            time += Time.deltaTime;
        }
        else
        {
            ResetTimer();
            SetEncounterState(EncounterState.Playing);
        }
    }
    protected override void PlayingEncounterLogic() 
    {
        if (!hasStarted)
        {
            OnEncounterStart();
            Debug.Log("Encounter Started");
            hasStarted = true;
        }
    }
    protected override void EndingEncounterLogic()
    {
        if (time <= timeEnding)
        {
            if (time <= 0f)
            {
                OnEncounterEnding();
                Debug.Log("Encounter Ending");
            }

            time += Time.deltaTime;
        }
        else
        {
            ResetTimer();
            SetEncounterState(EncounterState.Ended);
        }
    }
    protected override void EndedEncounterLogic()
    {
        if (hasEnded) return;

        hasEnded = true;

        OnEncounterEnd();
        Debug.Log("Encounter Ended");

        switch (encounterResultState)
        {
            case EncounterResultState.Completed:
                OnEncounterCompleted();
                Debug.Log("Encounter Completed");
                break;
            case EncounterResultState.Failed:
                OnEncounterFailed();
                Debug.Log("Encounter Failed");
                StartCoroutine(WaitToDieCoroutine());
                break;
            default:
                break;
        }
    }

    protected override void SetEncounterState(EncounterState encounterState)
    {
        base.SetEncounterState(encounterState);
        ResetTimer();
    }

    protected void ResetTimer()
    {
        time = 0f;
    }

    protected virtual IEnumerator WaitToDieCoroutine()
    {
        yield return new WaitForSeconds(timeToDie);

        OnTimeToDiePassed();
    }

    protected virtual void OnTimeToDiePassed() { }

}
