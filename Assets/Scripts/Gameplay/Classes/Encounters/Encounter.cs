using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Encounter : MonoBehaviour
{
    protected enum EncounterState {NotStarted, Starting, Playing, Ending, Ended}
    protected enum EncounterResultState {NotEnded, Completed, Failed}

    [SerializeField] protected EncounterState encounterState;
    [SerializeField] protected EncounterResultState encounterResultState;

    protected bool hasEnded = false;

    protected virtual void Start()
    {
        SetEncounterState(EncounterState.NotStarted);
        SetEncounterResultState(EncounterResultState.NotEnded);
    }

    protected virtual void Update()
    {
        EncounterUpdateLogic();
    }

    protected void EncounterUpdateLogic()
    {
        switch (encounterState)
        {
            case EncounterState.NotStarted:
                NotStartedEncounterLogic();
                break;
            case EncounterState.Starting:
                StartingEncounterLogic();
                break;
            case EncounterState.Playing:
                PlayingEncounterLogic();
                break;
            case EncounterState.Ending:
                EndingEncounterLogic();
                break;
            case EncounterState.Ended:
                EndedEncounterLogic();
                break;
            default:
                break;
        }
    }

    #region UpdateLogics

    protected virtual void NotStartedEncounterLogic() { }
    protected virtual void StartingEncounterLogic() { }
    protected virtual void PlayingEncounterLogic() { }
    protected virtual void EndingEncounterLogic() { }
    protected virtual void EndedEncounterLogic() { }
    #endregion

    #region OnEncounter States
    protected virtual void OnEncounterStarting() { }
    protected virtual void OnEncounterStart() { }
    protected virtual void OnEncounterEnding() { }
    protected virtual void OnEncounterEnd() { }
    #endregion

    #region OnEncounter Result States
    protected virtual void OnEncounterCompleted() { }
    protected virtual void OnEncounterFailed() { }

    #endregion

    protected virtual void DoWhenPlayerEnterTrigger()
    {
        if (encounterState != EncounterState.NotStarted) return;

        SetEncounterState(EncounterState.Starting);
    }

    protected virtual void SetEncounterState(EncounterState encounterState)
    {
        this.encounterState = encounterState;
    }

    protected virtual void SetEncounterResultState(EncounterResultState encounterResultState)
    {
        this.encounterResultState = encounterResultState;
    }

    protected void CompleteEncounter()
    {
        SetEncounterResultState(EncounterResultState.Completed);
        OnEncounterCompleted();
    }

    protected void FailEncounter()
    {
        SetEncounterResultState(EncounterResultState.Failed);
        OnEncounterFailed();
    }
}
