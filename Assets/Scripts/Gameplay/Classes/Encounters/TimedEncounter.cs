using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TimedEncounter : RegularEncounter
{
    [Header("Timed Encounter Settings")]
    [SerializeField] protected float completeTime;

    private int currentTimeCounter;
    private int previousTimeCounter;

    protected override void Start()
    {
        base.Start();
    }

    protected override void NotStartedEncounterLogic() 
    { 
        base.NotStartedEncounterLogic(); 
    }
    protected override void StartingEncounterLogic() 
    {
        base.StartingEncounterLogic();
    }
    protected override void PlayingEncounterLogic() 
    {
        base.PlayingEncounterLogic();

        if (time <= completeTime)
        {
            time += Time.deltaTime;

            currentTimeCounter = Mathf.CeilToInt(completeTime - time);
            if(currentTimeCounter != previousTimeCounter) OnTimeCounterChanged(currentTimeCounter);
            previousTimeCounter = currentTimeCounter;
        }
        else
        {
            SetEncounterResultState(EncounterResultState.Failed);
            SetEncounterState(EncounterState.Ending);
        }
    }
    protected override void EndingEncounterLogic() 
    {
       base.EndingEncounterLogic();
    }
    protected override void EndedEncounterLogic() 
    {
        base.EndedEncounterLogic();
    }
    protected virtual void OnTimeCounterChanged(int time) {}
}
