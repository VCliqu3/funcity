using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSource : MonoBehaviour
{
    [SerializeField] private List<Light> lights = new List<Light>();
    [SerializeField] private bool lightsStartOn;

    private bool lightsOn;
    private AudioSource audioSource;

    [SerializeField] private AudioClip turnOnLightClip;
    [SerializeField] private AudioClip turnOffLightClip;

    protected virtual void Awake()
    {
        audioSource = GetComponentInChildren<AudioSource>();
    }

    private void Start()
    {
        TurnLights(lightsStartOn);
    }
    
    public virtual void TurnLights(bool state)
    {
        foreach(Light light in lights)
        {
            light.enabled = state;
        }

        lightsOn = state;

        PlayClips(lightsOn);
    }

    public virtual void ToggleLights()
    {
        foreach (Light light in lights)
        {
            light.enabled = !light.enabled;
        }

        lightsOn = !lightsOn;

        PlayClips(lightsOn);
    }

    public void PlayClips(bool state)
    {
        if (state)
        {
            if (turnOnLightClip) audioSource.PlayOneShot(turnOnLightClip);
        }
        else
        {
            if (turnOffLightClip) audioSource.PlayOneShot(turnOffLightClip);
        }
    }
}

