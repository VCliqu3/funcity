using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AngelicaDeath : MonoBehaviour
{
    [Header("Angelica Death Settings")]
    [SerializeField] protected Transform playerCamera;
    [SerializeField] protected Transform cameraAnimationHolder;
    [SerializeField] protected GameObject angelicaModel;

    [SerializeField, Range(5f, 20f)] private float playerLookTowardsSpeed = 10f;
    [SerializeField, Range(0.01f, 0.1f)] private float playerLookTowardsThreshold = 0.1f;

    [SerializeField, Range(.1f,4f)] protected float angelicaDistanceToAppear;
    [SerializeField] protected float cameraAnimationHolderDuration;

    [SerializeField] protected string playerDeathAnimationTriggerName;
    [SerializeField] private AudioClip deathAudioClip;

    [Header("Angelica Animation Settings")]
    [SerializeField] protected Animator angelicaAnimator;
    [SerializeField] protected string angelicaDeathAnimation;

    protected Animator cameraAnimationHolderAnimator;
    protected PlayerHorizontalMovement playerHorizontalMovement;
    protected PlayerLook playerLook;
    protected PlayerFlashlight playerFlashlight;

    protected AudioSource audioSource;

    public static Action OnAngelicaDeath;
    public static Action OnAngelicaJumpscare;

    protected virtual void OnEnable()
    {

    }

    protected virtual void OnDisable() { }

    protected virtual void Awake()
    {
        cameraAnimationHolderAnimator = cameraAnimationHolder.GetComponent<Animator>();

        playerHorizontalMovement = FindObjectOfType<PlayerHorizontalMovement>();
        playerLook = FindObjectOfType<PlayerLook>();
        playerFlashlight = FindObjectOfType<PlayerFlashlight>();

        audioSource = GetComponentInChildren<AudioSource>();
    }

    protected virtual void PreDeathLogic()
    {
        OnAngelicaJumpscare?.Invoke();

        angelicaModel.SetActive(true);

        if (deathAudioClip) audioSource.PlayOneShot(deathAudioClip);

        playerHorizontalMovement.playerHorizontalMovementEnabled = false;
        playerLook.playerLookEnabled = false;
        playerFlashlight.flashlightToggleEnabled = false;
        playerFlashlight.ForceFlashlightState(false);

        transform.LookAt(playerCamera);
        TransformMoveTowards(transform, playerCamera, angelicaDistanceToAppear);    
    }

    protected IEnumerator PlayerLookTowards()
    {
        playerLook.playerLookEnabled = false;
        playerHorizontalMovement.playerHorizontalMovementEnabled = false;

        Quaternion targetRot = CalculateTargetCameraRotation(playerCamera, transform);

        while (playerCamera.rotation != targetRot)
        {
            playerCamera.rotation = Quaternion.Lerp(playerCamera.rotation, targetRot, playerLookTowardsSpeed * Time.deltaTime);

            float angle = Quaternion.Angle(playerCamera.rotation, targetRot);

            if (angle < playerLookTowardsThreshold)
            {
                playerCamera.rotation = targetRot;
                break;
            }

            yield return null;
        }

        Debug.Log("Player Look Towards Completed");
    }

    private Quaternion CalculateTargetCameraRotation(Transform camera, Transform target)
    {
        Vector3 directionToTarget = target.position - camera.position;

        float pitch = Mathf.Asin(directionToTarget.normalized.y) * Mathf.Rad2Deg;
        float yaw = Mathf.Atan2(directionToTarget.x, directionToTarget.z) * Mathf.Rad2Deg;

        Quaternion rotation = Quaternion.Euler(-pitch, yaw, 0);

        return rotation;
    }

    protected void TransformMoveTowards(Transform movingTransform, Transform fixedTransform, float distanceBetweenTransforms)
    {
        Vector3 direction = (movingTransform.position - fixedTransform.position).normalized;
        movingTransform.position = fixedTransform.position + distanceBetweenTransforms * direction;
    }
}
