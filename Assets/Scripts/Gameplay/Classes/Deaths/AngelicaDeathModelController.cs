using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngelicaDeathModelController : MonoBehaviour
{
    [SerializeField] private string animationNameToPlay;
    private Animator _animator;
    private void OnEnable()
    {
        PlayAnimation(animationNameToPlay);
    }

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    private void PlayAnimation(string animationName)
    {
        _animator.Play(animationName);
    }
}
