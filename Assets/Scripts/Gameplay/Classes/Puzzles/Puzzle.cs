using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Puzzle : MonoBehaviour
{
    public bool puzzleIsCompleted;

    [Header("Checpoint Load Settings")]
    [SerializeField] private bool completeByCheckpointLoad;
    [SerializeField] private int checkpointNumberToComplete;

    [SerializeField] protected LoadType loadType;
    protected enum LoadType { OnlyThisCheckpoint, AllCheckpointsAfterThis }

    public static Action<Puzzle> OnPuzzleCompleted;

    protected virtual void OnEnable()
    {
        InGameCheckpointManager.OnCheckpointLoad += CheckCompleteByCheckpointLoad;

        UsableObject.OnObjectUsed += CheckObjectUsedInPlace;
        InteractableObject.OnObjectInteracted += CheckObjectInteracted;
        TriggeredObject.OnObjectTriggered += CheckObjectTriggered;
        OnPuzzleCompleted += CheckIfPuzzleCompleted;
    }

    protected virtual void OnDisable()
    {
        InGameCheckpointManager.OnCheckpointLoad -= CheckCompleteByCheckpointLoad;

        UsableObject.OnObjectUsed -= CheckObjectUsedInPlace;
        InteractableObject.OnObjectInteracted -= CheckObjectInteracted;
        TriggeredObject.OnObjectTriggered -= CheckObjectTriggered;
        OnPuzzleCompleted -= CheckIfPuzzleCompleted;
    }

    protected virtual bool CheckIfThisPuzzleCompleted() 
    { 
        if (puzzleIsCompleted) return true;

        return false;
    }

    protected virtual void CompletePuzzle()
    {
        puzzleIsCompleted = true;
        OnPuzzleCompleted?.Invoke(this);
    }

    protected virtual void CheckIfPuzzleCompleted(Puzzle puzzle) { }
    protected virtual void CheckObjectUsedInPlace(UsableObject usableObject, UsablePlace usablePlace) { }
    protected virtual void CheckObjectInteracted(InteractableObject interactableObject) { }
    protected virtual void CheckObjectTriggered(TriggeredObject triggeredOnject) { }

    protected virtual void CheckCompleteByCheckpointLoad(int checkpointNumber, Transform checkpointTransform)
    {
        if (!completeByCheckpointLoad) return;

        switch (loadType)
        {
            case (LoadType.OnlyThisCheckpoint):
                if (checkpointNumber == checkpointNumberToComplete) CompleteByCheckpointLoadLogic();
                break;

            case (LoadType.AllCheckpointsAfterThis):
            default:
                if (checkpointNumber >= checkpointNumberToComplete) CompleteByCheckpointLoadLogic();
                break;
        }
    }

    protected virtual void CompleteByCheckpointLoadLogic()
    {
        CompletePuzzle();
    }
}
