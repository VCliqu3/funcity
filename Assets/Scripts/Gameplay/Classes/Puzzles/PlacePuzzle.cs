using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacePuzzle : Puzzle
{
    [Header("Places")]
    [SerializeField] private List<UsablePlace> placesUsedInPuzzle = new List<UsablePlace>();

    protected override void CheckObjectUsedInPlace(UsableObject usableObject, UsablePlace usablePlace)
    {
        base.CheckObjectUsedInPlace(usableObject, usablePlace);

        CheckIfThisPuzzleCompleted();
    }

    protected override bool CheckIfThisPuzzleCompleted()
    {
        if(base.CheckIfThisPuzzleCompleted()) return true;

        foreach(UsablePlace usablePlace in placesUsedInPuzzle)
        {
            if (!usablePlace.placeUsedSuccessfully) return false;
        }

        CompletePuzzle();

        return true;
    }
}
