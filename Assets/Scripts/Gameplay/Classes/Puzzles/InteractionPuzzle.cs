using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionPuzzle : Puzzle
{
    protected override void CheckObjectInteracted(InteractableObject interactableObject)
    {
        base.CheckObjectInteracted(interactableObject);

        CheckIfThisPuzzleCompleted();
    }

    protected override void CheckObjectTriggered(TriggeredObject triggeredObject)
    {
        base.CheckObjectTriggered(triggeredObject);

        CheckIfThisPuzzleCompleted();
    }

    protected override bool CheckIfThisPuzzleCompleted()
    {
        if (base.CheckIfThisPuzzleCompleted()) return true;      

        return false;
    }
}
