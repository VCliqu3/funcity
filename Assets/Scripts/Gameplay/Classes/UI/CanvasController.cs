using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasController : MonoBehaviour
{
    protected Canvas canvas;
    protected CanvasGroup canvasGroup;

    public bool isInteractable;
    public bool isActive;
    public int sortOrder;

    protected UIInputHandler _UIInputHandler;

    protected virtual void OnEnable()
    {
        GameManager.OnGameStateChanged += CheckIfShouldBeActive;
    }
    protected virtual void OnDisable()
    {
        GameManager.OnGameStateChanged -= CheckIfShouldBeActive;
    }

    protected virtual void Awake()
    {
        _UIInputHandler = FindObjectOfType<UIInputHandler>();

        canvas = GetComponent<Canvas>();
        canvasGroup = GetComponent<CanvasGroup>();
    }

    protected virtual void Start() 
    {
        sortOrder = canvas.sortingOrder;
    }

    protected virtual void Update() { }

    protected void SetCanvasGroup(bool state)
    {
        canvasGroup.alpha = state ? 1f : 0f;
        canvasGroup.interactable = state;
        canvasGroup.blocksRaycasts = state;

        isActive = state;
    }
    protected virtual void CheckIfShouldBeActive(GameManager.GameState previousState, GameManager.GameState newState) { }

    protected bool CheckIfIsOnTopAmongActives()
    {
        if (!isActive) return false;

        int highestActiveSortingOrder = int.MinValue;

        CanvasController[] canvasControllers = FindObjectsOfType<CanvasController>();

        foreach(CanvasController canvasController in canvasControllers)
        {
            if (!canvasController.isActive) continue;
            if (!canvasController.isInteractable) continue;
            if (!(canvasController.sortOrder > highestActiveSortingOrder)) continue;

            highestActiveSortingOrder = canvasController.sortOrder;
        }

        if (sortOrder >= highestActiveSortingOrder) return true;

        return false;
    }
}

