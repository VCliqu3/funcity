using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class ChildSpeakerDialogRelation
{
    public AudioClip audioClip;
    public string dialog;
    public float timeShowingDialog;
}

public class ChildSpeakerDevice : MonoBehaviour
{
    [SerializeField] protected ChildSpeakerDialogRelation[] childSpeakerDialogRelations;
    [SerializeField] protected bool speakerEnabled = true;

    protected AudioSource audioSource;

    public static Action<string, float> OnChildSpeakerDeviceClipPlay;

    protected virtual void Awake()
    {
        audioSource = GetComponentInChildren<AudioSource>();
    }

    protected void PlayClip(int relationIndex)
    {
        if (!speakerEnabled) return;

        if (relationIndex > childSpeakerDialogRelations.Length - 1)
        {
            Debug.Log("IndexOutOfArray: " + relationIndex);
            return;
        }

        ChildSpeakerDialogRelation relation = childSpeakerDialogRelations[relationIndex];

        if (relation.audioClip) audioSource.PlayOneShot(relation.audioClip);

        OnChildSpeakerDeviceClipPlay?.Invoke(relation.dialog, relation.timeShowingDialog);
        Debug.Log(gameObject.name + " playing");
    }

    protected IEnumerator WaitTime(int relationIndex)
    {
        if (relationIndex > childSpeakerDialogRelations.Length - 1)
        {
            Debug.Log("IndexOutOfArray: " + relationIndex);
            yield break;
        }

        ChildSpeakerDialogRelation relation = childSpeakerDialogRelations[relationIndex];

        yield return new WaitForSeconds(relation.timeShowingDialog);
        yield return new WaitForSeconds(1.7f);
    }
}
