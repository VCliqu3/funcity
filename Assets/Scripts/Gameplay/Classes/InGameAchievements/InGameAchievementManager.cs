using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameAchievementManager : MonoBehaviour
{
    public static InGameAchievementManager instance;

    public bool completeAchievementsWithoutDatabase;

    private void Awake()
    {
        SetSingleton();
    }

    private void SetSingleton()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

    }
}
