using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InGameAchievement : MonoBehaviour
{
    [SerializeField] private int achievementNumber;

    public static Action<int,string,Sprite,AudioClip> OnAchievementAchieved;

    protected virtual void OnEnable() { }
    protected virtual void OnDisable() { }
    protected virtual void Awake() { }
    protected virtual void Start() { }
    protected virtual void CheckCompleteAchievement() 
    {
        Achievement achievement = Array.Find(AchievementManager.instance.achievementsArray, x => x.achievementNumber == achievementNumber);

        if (achievement == null)
        {
            Debug.Log("Achievement not found -  AchievementNumber: " + achievementNumber);
            return;
        }

        if (achievement.isCompleted) return;

        if (CheckUsingDatabase())
        {
            UpdateAchievementToDatabase(UserManager.instance.username, achievement.achievementNumber, UpdateAchievementLocal);
        }
        else
        {
            if(InGameAchievementManager.instance.completeAchievementsWithoutDatabase) UpdateAchievementLocal(achievement.achievementNumber);
        }
    }

    private void UpdateAchievementToDatabase(string username, int achievementNumber, Action<int> OnFinish)
    {
        DatabaseManager.instance.UpdateAchievementToDatabase(username,achievementNumber, OnFinish);
    }

    private void UpdateAchievementLocal(int achievementNumber)
    {
        Achievement achievement = Array.Find(AchievementManager.instance.achievementsArray, x => x.achievementNumber == achievementNumber);

        achievement.isCompleted = true;

        OnAchievementAchieved?.Invoke(achievement.achievementNumber, achievement.achievementName, achievement.achievementSprite, achievement.achievementClip);
    }
    private bool CheckUsingDatabase()
    {
        return LoadedDatabaseManager.instance.enableDataLoad && LoadedDatabaseManager.instance.hasLoadedData;
    }
}
