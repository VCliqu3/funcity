using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterTVSetAchievement : InGameAchievement
{
    //Al entrar al set de TV

    protected override void OnEnable()
    {
        base.OnEnable();
        EnterTVSetTrigger.OnPlayerEnter += CheckCompleteAchievement;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        EnterTVSetTrigger.OnPlayerEnter -= CheckCompleteAchievement;
    }

    protected override void CheckCompleteAchievement()
    {
        base.CheckCompleteAchievement();
    }
}
