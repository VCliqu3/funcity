using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterHospitalAchievement : InGameAchievement
{
    //Al entrar al hospital

    protected override void OnEnable()
    {
        base.OnEnable();
        EnterHospitalTrigger.OnPlayerEnter += CheckCompleteAchievement;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        EnterHospitalTrigger.OnPlayerEnter -= CheckCompleteAchievement;
    }

    protected override void CheckCompleteAchievement()
    {
        base.CheckCompleteAchievement();
    }
}
