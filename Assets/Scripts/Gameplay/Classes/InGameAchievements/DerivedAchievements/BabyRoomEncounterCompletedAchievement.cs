using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabyRoomEncounterCompletedAchievement : InGameAchievement
{
    //Al completar encuentro en cuarto de bebes

    protected override void OnEnable()
    {
        base.OnEnable();
        BabyRoomEncounter.OnBabyRoomEncounterCompleted += CheckCompleteAchievement;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        BabyRoomEncounter.OnBabyRoomEncounterCompleted -= CheckCompleteAchievement;
    }
    protected override void CheckCompleteAchievement()
    {
        base.CheckCompleteAchievement();
    }
}
