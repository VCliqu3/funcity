using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurgeryRoomEncounterCompletedAchievement : InGameAchievement
{
    //Al completar encuentro en sala de cirugia

    protected override void OnEnable()
    {
        base.OnEnable();
        SurgeryRoomEncounter.OnSurgeryRoomEncounterCompleted += CheckCompleteAchievement;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        SurgeryRoomEncounter.OnSurgeryRoomEncounterCompleted -= CheckCompleteAchievement;
    }

    protected override void CheckCompleteAchievement()
    {
        base.CheckCompleteAchievement();
    }
}
