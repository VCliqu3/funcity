using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngelicaAscendedAchievement : InGameAchievement
{
    //Al ascender Angelica

    protected override void OnEnable()
    {
        base.OnEnable();
        FinalCinematic.OnAngelicaAscended += CheckCompleteAchievement;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        FinalCinematic.OnAngelicaAscended -= CheckCompleteAchievement;
    }
    protected override void CheckCompleteAchievement()
    {
        base.CheckCompleteAchievement();
    }
}
