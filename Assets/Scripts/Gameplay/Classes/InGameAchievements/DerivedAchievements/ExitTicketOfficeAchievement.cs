using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitTicketOfficeAchievement : InGameAchievement
{
    //Al salir de la boleteria

    protected override void OnEnable()
    {
        base.OnEnable();
        ExitTicketOfficeTrigger.OnPlayerEnter += CheckCompleteAchievement;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        ExitTicketOfficeTrigger.OnPlayerEnter -= CheckCompleteAchievement;
    }
    protected override void CheckCompleteAchievement()
    {
        base.CheckCompleteAchievement();
    }
}
