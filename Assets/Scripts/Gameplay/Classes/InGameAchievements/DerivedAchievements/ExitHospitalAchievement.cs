using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitHospitalAchievement : InGameAchievement
{
    //Al salir del hospital

    protected override void OnEnable()
    {
        base.OnEnable();
        ExitHospitalTrigger.OnPlayerEnter += CheckCompleteAchievement;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        ExitHospitalTrigger.OnPlayerEnter -= CheckCompleteAchievement;
    }

    protected override void CheckCompleteAchievement()
    {
        base.CheckCompleteAchievement();
    }
}
