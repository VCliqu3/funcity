using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TVSetWarehouseEncounterCompletedAchievement : InGameAchievement
{
    //Al completar encuentro en almacen de set de TV

    protected override void OnEnable()
    {
        base.OnEnable();
        TVSetWarehouseEncounter.OnTVSetWarehouseEncounterCompleted += CheckCompleteAchievement;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        TVSetWarehouseEncounter.OnTVSetWarehouseEncounterCompleted -= CheckCompleteAchievement;
    }

    protected override void CheckCompleteAchievement()
    {
        base.CheckCompleteAchievement();
    }
}
