using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitTVSetAchievement : InGameAchievement
{
    //Al salir set de TV

    protected override void OnEnable()
    {
        base.OnEnable();
        ExitTVSetTrigger.OnPlayerEnter += CheckCompleteAchievement;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        ExitTVSetTrigger.OnPlayerEnter -= CheckCompleteAchievement;
    }
    protected override void CheckCompleteAchievement()
    {
        base.CheckCompleteAchievement();
    }
}
