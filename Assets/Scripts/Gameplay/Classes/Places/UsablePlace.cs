using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UsablePlace : MonoBehaviour, IUsablePlace
{
    [Header("Usable Place Settings")]
    public bool usablePlaceEnabled;
    public bool placeUsedSuccessfully;

    protected AudioSource audioSource;
    protected GameObject player;
    protected PlayerUse playerUse;
    protected PlayerPickUp playerPickUp;

    [SerializeField] protected List<PickableObject> usableObjectsInThisPlace = new List<PickableObject>();

    [SerializeField] private bool sightIndicatorEnabled;
    [SerializeField] private string textOnSight;

    [SerializeField] private AudioClip usedPlaceAudioClip;
    [SerializeField] private AudioClip usedWrongAudioClip;

    [Header("Checkpoint Auto Use")]
    [SerializeField] private bool enableAutoUseByCheckpoint;
    [SerializeField] private UsableObject usableObjectToAutoUse;
    [SerializeField] private int firstcheckpointNumberToAutoUse;

    public static Action<UsablePlace, string> OnUsablePlaceOnSight;
    public static Action<UsablePlace> OnUsablePlaceLeaveSight;

    protected virtual void OnEnable()
    {
        InGameCheckpointManager.OnCheckpointLoad += CheckAutoUseByCheckpoint;
    }

    protected virtual void OnDisable()
    {
        InGameCheckpointManager.OnCheckpointLoad -= CheckAutoUseByCheckpoint;
    }


    protected virtual void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerUse = player.GetComponent<PlayerUse>();
        playerPickUp = player.GetComponent<PlayerPickUp>();

        audioSource = GetComponentInChildren<AudioSource>();
    }

    public virtual void OnPlayerEnterUsablePlaceDetectionRange() { }
    public virtual void OnPlayerLeaveUsablePlaceDetectionRange() { }
    public virtual void OnPlayerEnterUsablePlaceSightRange()
    {
        if (!sightIndicatorEnabled) return;
        if (!playerUse.usablePlacesEnabled) return;
        if (!playerUse.useEnabled) return;
        if (!usablePlaceEnabled) return;
        if (!usableObjectsInThisPlace.Contains(playerPickUp.currentPickedUpObject)) return;

        OnUsablePlaceOnSight?.Invoke(this, textOnSight);
    }

    public virtual void OnPlayerLeaveUsablePlaceSightRange()
    {
        OnUsablePlaceLeaveSight?.Invoke(this);
    }
    public virtual void TryUseHere(PlayerUse playerUse, UsableObject usableObject, bool autoUse)
    {
        if (usableObjectsInThisPlace.Contains(usableObject))
        {
            OnPlaceUsed(playerUse, usableObject);
            usableObject.Use(playerUse, this); //Uso propio del objeto usable
        }
        else
        {
            OnPlaceUsedWrong(playerUse, usableObject);
            usableObject.UseWrong(playerUse, this);
        }

        if (!autoUse) PlayerUse.OnObjectUsed?.Invoke(usableObject);
    }

    public virtual void OnPlaceUsed(PlayerUse playerUse, UsableObject usableObject)
    {
        gameObject.layer = LayerMask.NameToLayer("Default"); //Ahora pertenece al layer default
        placeUsedSuccessfully = true;

        if (usedPlaceAudioClip) audioSource.PlayOneShot(usedPlaceAudioClip);
    }

    public virtual void OnPlaceUsedWrong(PlayerUse playerUse, UsableObject usableObject)
    {
        Debug.Log("Cant use that here");

        if (usedWrongAudioClip) audioSource.PlayOneShot(usedWrongAudioClip);
    }

    protected void EnableUsablePlace()
    {
        usablePlaceEnabled = true;
    }
    protected void DisableUsablePlace()
    {
        usablePlaceEnabled = false;
    }
    protected void AddToUsablePlaceLayer()
    {
        gameObject.layer = LayerMask.NameToLayer("UsablePlace");
    }
    protected void RemoveFromUsablePlaceLayer()
    {
        gameObject.layer = LayerMask.NameToLayer("Default");
    }

    private void CheckAutoUseByCheckpoint(int checkpointNumber, Transform checkpointTransform)
    {
        if (!enableAutoUseByCheckpoint) return;

        if (checkpointNumber >= firstcheckpointNumberToAutoUse)
        {
            usableObjectToAutoUse._boxCollider.isTrigger = true;
            usableObjectToAutoUse._rigidbody.isKinematic = true;
            usableObjectToAutoUse.useAnimationsEnabled = false;

            bool smoothPlaceState = playerUse.smoothPlace;
            playerUse.smoothPlace = false;

            TryUseHere(playerUse, usableObjectToAutoUse, true);

            playerUse.smoothPlace = smoothPlaceState;
        }
    }
}
