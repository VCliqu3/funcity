using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleEnabledUsablePlace : UsablePlace
{
    [Header("Puzzles To Enable")]
    [SerializeField] private List<Puzzle> puzzlesToEnable = new List<Puzzle>();

    protected override void OnEnable()
    {
        base.OnEnable();
        Puzzle.OnPuzzleCompleted += CheckIfShouldEnable;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        Puzzle.OnPuzzleCompleted -= CheckIfShouldEnable;
    }

    protected virtual void CheckIfShouldEnable(Puzzle _puzzle)
    {
        if (!puzzlesToEnable.Contains(_puzzle)) return;

        foreach (Puzzle puzzle in puzzlesToEnable)
        {
            if (!puzzle.puzzleIsCompleted) return;
        }

        ChangeToUsablePlaceLayer();
    }

    protected virtual void ChangeToUsablePlaceLayer()
    {
        gameObject.layer = LayerMask.NameToLayer("UsablePlace");
        Debug.Log(gameObject.name + " Enabled!");
    }
}
