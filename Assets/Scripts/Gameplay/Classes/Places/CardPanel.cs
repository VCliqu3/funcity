using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardPanel : PlaceablePlace
{
    [Header("Card Panel Settings")]
    [SerializeField] private Color lockedColor;
    [SerializeField] private Color unlockedColor;
    [SerializeField] private List<Light> lights = new List<Light>();

    private Material material;

    protected override void Awake()
    {
        base.Awake();
        material = GetComponent<Renderer>().material;
    }

    protected void Start()
    {
        //SetCardPanelLights(false);
    }

    public override void OnPlacePlaced(PlayerUse playerUse, PlaceableObject placeableObject)
    {
        base.OnPlacePlaced(playerUse, placeableObject);

        SetCardPanelLights(true);
    }

    private void SetCardPanelLights(bool state)
    {
        if (state)
        {
            ChangeLightsColor(unlockedColor);
            ChangeMaterialEmissionColor(unlockedColor);
        }
        else
        {
            ChangeLightsColor(lockedColor);
            ChangeMaterialEmissionColor(lockedColor);
        }
    }

    private void ChangeLightsColor(Color color)
    {
        foreach(Light light in lights)
        {
            light.color = color;
        }
    }

    private void ChangeMaterialEmissionColor(Color color)
    {
        material.SetColor("_EmissionColor", color);
    }

}
