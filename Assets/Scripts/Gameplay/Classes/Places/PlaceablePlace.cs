using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceablePlace : UsablePlace, IPlaceablePlace
{
    [Header("Placeable Place Settings")]
    public Transform placeableSocket;

    public override void OnPlaceUsed(PlayerUse playerUse, UsableObject usableObject)
    {
        base.OnPlaceUsed(playerUse, usableObject);

        if (usableObject.TryGetComponent<PlaceableObject>(out PlaceableObject placeableObject)) 
        {
            OnPlacePlaced(playerUse, placeableObject);
        }
    }

    public override void OnPlaceUsedWrong(PlayerUse playerUse, UsableObject usableObject)
    {
        base.OnPlaceUsedWrong(playerUse, usableObject);
        //Debug.Log("Cant place that here");
    }

    public virtual void OnPlacePlaced(PlayerUse playerUse, PlaceableObject placeableObject) 
    {
        gameObject.layer = LayerMask.NameToLayer("Default"); //Ahora pertenece al layer default
    }
}
