using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLook : MonoBehaviour
{
    [SerializeField] float rotationSpeed = 1000f;
    [SerializeField] float clampAngle = 90f;
    [SerializeField,Range(5f,25f)] float xSnappiness = 10f;
    [SerializeField,Range(5f,25f)] float ySnappiness = 10f;

    [SerializeField] private Transform playerCameraPitchHolder;

    private PlayerInputHandler playerImputHandler;

    public bool playerLookEnabled;
    [HideInInspector] public float cameraVerticalAngle = 0f;
    private float rotationMultiplier = 1f;

    private float xAccumulator = 0f;
    [HideInInspector] public float yAccumulator = 0f;

    private void Awake()
    {
        playerImputHandler = FindObjectOfType<PlayerInputHandler>();
    }

    private void LateUpdate()
    {
        CheckLook();
    }

    private void CheckLook()
    {
        if (playerLookEnabled)
        {
            SmoothLookX();
            SmoothLookY();
        }
    }

    private void LookX()
    {
        float mouseX = playerImputHandler.GetLookInputHorizontal() * rotationSpeed * rotationMultiplier * Time.deltaTime;

        Vector3 rotateXAngle = new Vector3(0f, mouseX, 0f);

        transform.Rotate(rotateXAngle, Space.Self);
    }

    private void LookY()
    {
        float mouseY = playerImputHandler.GetLookInputVertical() * rotationSpeed * rotationMultiplier * Time.deltaTime;

        cameraVerticalAngle -= mouseY;
        cameraVerticalAngle = Mathf.Clamp(cameraVerticalAngle, -clampAngle, clampAngle);

        playerCameraPitchHolder.localRotation = Quaternion.Euler(cameraVerticalAngle, 0f, 0f);
    }

    private void SmoothLookX()
    {
        float inputX = playerImputHandler.GetLookInputHorizontal();

        xAccumulator = Mathf.Lerp(xAccumulator, inputX, xSnappiness * Time.deltaTime);

        float mouseX = xAccumulator * rotationSpeed * rotationMultiplier * Time.deltaTime; //*Time.deltaTime  rotationSpeed =1 o 500;

        Vector3 rotateXAngle = new Vector3(0f, mouseX, 0f);

        transform.Rotate(rotateXAngle, Space.Self);
    }

    private void SmoothLookY()
    {
        float inputY = playerImputHandler.GetLookInputVertical();

        yAccumulator = Mathf.Lerp(yAccumulator, inputY, ySnappiness * Time.deltaTime);

        float mouseY = yAccumulator * rotationSpeed * rotationMultiplier * Time.deltaTime; //*Time.deltaTime rotationSpeed =1 o 500;

        cameraVerticalAngle -= mouseY;
        cameraVerticalAngle = Mathf.Clamp(cameraVerticalAngle, -clampAngle, clampAngle);

        playerCameraPitchHolder.localRotation = Quaternion.Euler(cameraVerticalAngle, 0f, 0f);
    }
}
