using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerJump : MonoBehaviour
{
    [Header("Jump Settings")]
    public bool playerJumpEnabled;
    [SerializeField, Range(0.75f,1.5f)] private float baseJumpHeight = 1f;
    [SerializeField, Range(0f, 0.5f)] private float minJumpHeight = 0f;
    [SerializeField, Range(-9.81f, -19.62f)] private float baseGravity = -19.62f;

    [HideInInspector] public Vector3 verticalVelocity;

    private PlayerInputHandler playerImputHandler;
    private CharacterController characterController;
    private PlayerFeet playerFeet;

    public static Action OnPlayerJump;

    private void Awake()
    {
        playerImputHandler = FindObjectOfType<PlayerInputHandler>();
        characterController = GetComponent<CharacterController>();
        playerFeet = GetComponentInChildren<PlayerFeet>();
    }

    private void OnEnable()
    {
        PlayerFeet.OnPlayerGrounded += ResetJump;
    }

    private void OnDisable()
    {
        PlayerFeet.OnPlayerGrounded -= ResetJump;
    }

    private void Start()
    {
        playerJumpEnabled = false;
    }

    private void Update()
    {
        CheckJump();
        CheckFall();
    }

    private void CheckJump()
    {
        if (!playerJumpEnabled) return;

        if (playerImputHandler.GetJumpInputDown() && playerFeet.isGrounded) // &&!playerStamina.playerFatigued
        {
            Jump();
        }   
    }

    private void CheckFall()
    {
        if (!playerFeet.isGrounded || verticalVelocity.y > 0f)
        {
            ApplyGravity();
        }
    }

    private void Jump()
    {
        verticalVelocity.y = 0f;
        verticalVelocity.y = Mathf.Sqrt(CalculateJumpHeight() * -2 * baseGravity);

        OnPlayerJump?.Invoke();
    }

    private void ResetJump()
    {
        verticalVelocity.y = 0f;
    }

    private void ApplyGravity()
    {
        verticalVelocity.y += baseGravity * Time.deltaTime;
        characterController.Move(verticalVelocity * Time.deltaTime);
    }

    private float CalculateJumpHeight()
    {
        float jumpHeight = minJumpHeight + baseJumpHeight; //* playerStamina.staminaRatio

        return jumpHeight;
    }
}
