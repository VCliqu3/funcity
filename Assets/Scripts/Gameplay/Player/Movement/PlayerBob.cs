using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBob : MonoBehaviour
{
    [SerializeField] private Transform playerCameraHeadBobHolder;
    [SerializeField] private Transform playerHandsBobHolder;

    [Header("Head Bob Settings")]
    [SerializeField] private bool bobEnabled;
    [SerializeField] private bool headBobEnabled;
    [SerializeField, Range(0f, 5f)] private float headBobSpeedThreshold = 3f;
    [SerializeField, Range(0f, 3f)] private float resetPositionSpeed = 1f;

    [Header("X Settings")]
    [SerializeField, Range(0f, 0.01f)] private float xAmplitude = 0.005f;
    [SerializeField, Range(0f, 30f)] private float xFrequency = 10f;
    [SerializeField] private float xSprintAmplitudeMultiplier = 1.2f;
    [SerializeField] private float xSprintFrequencyMultiplier = 1.2f;

    [Header("Y Settings")]
    [SerializeField, Range(0f, 0.01f)] private float yAmplitude = 0.005f;
    [SerializeField, Range(0f, 30f)] private float yFrequency = 10f;
    [SerializeField] private float ySprintAmplitudeMultiplier = 1.2f;
    [SerializeField] private float ySprintFrequencyMultiplier = 1.2f;

    [Header("Hands Bob Settings")]
    [SerializeField] private bool handsBobEnabled;
    [SerializeField, Range(0f, 5f)] private float handsXAmplitudeMultiplier;
    [SerializeField, Range(0f, 5f)] private float handsXAmplitudeSprintMultiplier;

    [SerializeField, Range(0f, 5f)] private float handsYAmplitudeMultiplier;
    [SerializeField, Range(0f, 5f)] private float handsYAmplitudeSprintMultiplier;

    [Header("Bob Regulation Settings")]
    [SerializeField, Range(0.01f,10f)] private float smoothDampSmoothTime = 1f;

    private Vector3 headStartPos;
    private Vector3 handsStartPos;

    private float resetPositionTime;
    private float timeReference;

    private PlayerHorizontalMovement playerHorizontalMovement;
    private PlayerFeet playerFeet;

    private Vector3 velocityRef;

    private void Awake()
    {
        playerHorizontalMovement = GetComponent<PlayerHorizontalMovement>();
        playerFeet = GetComponentInChildren<PlayerFeet>();
    }
    private void Start()
    {
        headStartPos = playerCameraHeadBobHolder.localPosition;
        handsStartPos = playerHandsBobHolder.localPosition;
    }
    private void Update()
    {
        CheckMotion();
        RegulateBob();
    }

    private void CheckMotion()
    {
        if (!bobEnabled) return;

        if (playerFeet.isGrounded && playerHorizontalMovement.currentHorizontalSpeed >= headBobSpeedThreshold)
        {
            if(headBobEnabled) PlayHeadMotion(FootstepMotion());
            if(handsBobEnabled) PlayHandsMotion(FootstepMotion());

            resetPositionTime = 0f;
        }
        else
        {
            ResetPositions();
        }
    }
    private void PlayHeadMotion(Vector3 motion)
    {
        playerCameraHeadBobHolder.localPosition += motion;
    }

    private void PlayHandsMotion(Vector3 motion)
    {
        float xHandsAmplitudeMultiplier = playerHorizontalMovement.movementState == PlayerHorizontalMovement.MovementState.Running ? handsXAmplitudeMultiplier * handsXAmplitudeSprintMultiplier : handsXAmplitudeMultiplier;
        float yHandsAmplitudeMultiplier = playerHorizontalMovement.movementState == PlayerHorizontalMovement.MovementState.Running ? handsYAmplitudeMultiplier * handsYAmplitudeSprintMultiplier : handsYAmplitudeMultiplier;

        playerHandsBobHolder.localPosition += new Vector3(motion.x * xHandsAmplitudeMultiplier, motion.y * yHandsAmplitudeMultiplier, motion.z);
    }

    private Vector3 FootstepMotion()
    {
        Vector3 headBobDifferential = Vector3.zero;

        float xAmplitudeMultiplier = playerHorizontalMovement.movementState == PlayerHorizontalMovement.MovementState.Running ? xSprintAmplitudeMultiplier : 1f;
        float yAmplitudeMultiplier = playerHorizontalMovement.movementState == PlayerHorizontalMovement.MovementState.Running ? ySprintAmplitudeMultiplier : 1f;

        float xFrequencyMultiplier = playerHorizontalMovement.movementState == PlayerHorizontalMovement.MovementState.Running ? xSprintFrequencyMultiplier : 1f;
        float yFrequencyMultiplier = playerHorizontalMovement.movementState == PlayerHorizontalMovement.MovementState.Running ? ySprintFrequencyMultiplier : 1f;

        headBobDifferential.x = Mathf.Cos(timeReference * xFrequency * xFrequencyMultiplier) * xAmplitude * xAmplitudeMultiplier;
        headBobDifferential.y = Mathf.Sin(timeReference * yFrequency * yFrequencyMultiplier) * yAmplitude * yAmplitudeMultiplier;

        timeReference += Time.deltaTime;

        return headBobDifferential;
    }

    private void ResetPositions()
    {
        resetPositionTime += Time.deltaTime;

        if (playerCameraHeadBobHolder.localPosition != headStartPos)
        {
            playerCameraHeadBobHolder.localPosition = Vector3.Lerp(playerCameraHeadBobHolder.localPosition, headStartPos, resetPositionTime * resetPositionSpeed);
        }
        else
        {
            timeReference = 0f;
        }

        if (playerHandsBobHolder.localPosition != handsStartPos)
        {
            playerHandsBobHolder.localPosition = Vector3.Lerp(playerHandsBobHolder.localPosition, handsStartPos, resetPositionTime * resetPositionSpeed);
        }
    }

    private void RegulateBob()
    {
        if (playerCameraHeadBobHolder.localPosition != headStartPos)
        {
            playerCameraHeadBobHolder.localPosition = Vector3.SmoothDamp(playerCameraHeadBobHolder.localPosition, headStartPos, ref velocityRef, smoothDampSmoothTime);
        }

        if (playerHandsBobHolder.localPosition != headStartPos)
        {
            playerHandsBobHolder.localPosition = Vector3.SmoothDamp(playerHandsBobHolder.localPosition, handsStartPos, ref velocityRef, smoothDampSmoothTime);
        }      
    }
}
