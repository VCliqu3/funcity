using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraStabilization : MonoBehaviour
{
    [SerializeField] private Transform playerCamera;
    [SerializeField] private Transform playerCameraStabilizationHolder;

    [Header("Camera Stabilization Settings")]
    [SerializeField] private bool cameraStabilizationEnabled;

    [SerializeField, Range(0f, 15)] private float cameraFocusDistance = 15f;

    // Update is called once per frame
    private void Update()
    {
        CheckFocusTarget();
    }

    private void CheckFocusTarget()
    {
        if (!cameraStabilizationEnabled) return;

        Vector3 focusPosition = new Vector3(transform.position.x, transform.position.y + playerCameraStabilizationHolder.localPosition.y, transform.position.z);
        focusPosition += playerCameraStabilizationHolder.forward * cameraFocusDistance;

        playerCamera.LookAt(focusPosition);
    }
}
