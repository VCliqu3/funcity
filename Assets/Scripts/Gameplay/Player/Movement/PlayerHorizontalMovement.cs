using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerHorizontalMovement : MonoBehaviour
{
    [Header("Horizontal Movement Settings")]
    public bool playerHorizontalMovementEnabled;
    public bool playerSprintEnabled;
    [SerializeField] private float walkingSpeed = 12f;
    [SerializeField] private float sprintSpeedMultiplier = 1.5f;
    public float currentHorizontalSpeed;  
    public enum MovementState {Idle, Walking, Running}
    public MovementState movementState;

    [Header("Movement Inertia Settings")]
    [SerializeField] private bool movementInertiaEnabled;
    [SerializeField] private float speedChangeRate;
    [SerializeField] private float speedOffset;

    public float targetSpeed;
    [HideInInspector] public float horizontalSpeed;

    [Header("Direction Inertia Settings")]
    [SerializeField] private bool directionInertiaEnabled;
    [SerializeField] private float directionChangeRate;

    private Vector3 targetDirection;
    private Vector3 direction;

    private PlayerInputHandler playerInputHandler;
    private CharacterController characterController;
    private PlayerFeet playerFeet;

    [Header("AudioClips")]
    [SerializeField] private AudioSource feetAudioSource;
    [SerializeField] private AudioClip walkingClip;
    [SerializeField] private AudioClip runningClip;
    [SerializeField] private float minStepPitch;
    [SerializeField] private float maxStepPitch;

    private float lastAudioTime = 0f;

    public static Action OnPlayerIdle;
    public static Action OnPlayerWalk;
    public static Action OnPlayerRun;

    private void Awake()
    {
        playerInputHandler = FindObjectOfType<PlayerInputHandler>();
        characterController = GetComponent<CharacterController>();
        playerFeet = GetComponentInChildren<PlayerFeet>();
    }

    private void Start()
    {
        playerHorizontalMovementEnabled = true;
        lastAudioTime = 0f;
    }

    private void Update()
    {
        CalculateCurrentSpeed();
        CheckMovementState();
        CheckMove();

        CheckFootstepChangePitch();
    }

    private void CalculateCurrentSpeed()
    {
        currentHorizontalSpeed = playerHorizontalMovementEnabled? characterController.velocity.magnitude : 0f;
    }

    private void CheckMovementState()
    {
        if(!playerHorizontalMovementEnabled || playerInputHandler.GetMoveInput() == Vector3.zero || !playerFeet.isGrounded || currentHorizontalSpeed == 0f)
        {
            if (movementState != MovementState.Idle) SetMovementState(MovementState.Idle);
            return;
        }

        if(playerSprintEnabled && playerInputHandler.GetSprintInputHeld())
        {
            if (movementState != MovementState.Running) SetMovementState(MovementState.Running);
            return;
        }

        if (movementState != MovementState.Walking) SetMovementState(MovementState.Walking);
    }

    private void CheckMove()
    {
        if (!playerHorizontalMovementEnabled) return;

        Move();
        
    }

    private void Move()
    {
        targetSpeed = movementState == MovementState.Running ? walkingSpeed * sprintSpeedMultiplier : walkingSpeed;

        if (playerInputHandler.GetMoveInput() == Vector3.zero) targetSpeed = 0f;

        if ((currentHorizontalSpeed < targetSpeed - speedOffset || currentHorizontalSpeed > targetSpeed + speedOffset) && movementInertiaEnabled)
        {
            horizontalSpeed = Mathf.Lerp(horizontalSpeed, targetSpeed, Time.deltaTime * speedChangeRate);
        }
        else
        {
            horizontalSpeed = targetSpeed;
        }

        if (playerInputHandler.GetMoveInput() != Vector3.zero) targetDirection = playerInputHandler.GetMoveInput();

        if (directionInertiaEnabled)
        {
            direction = Vector3.Lerp(direction, targetDirection, Time.deltaTime * directionChangeRate);
        }
        else
        {
            direction = targetDirection;
        }

        characterController.Move(horizontalSpeed * Time.deltaTime * direction);
    }

    private void SetMovementState(MovementState state)
    {
        movementState = state;

        MovementSoundLogic(state);

        switch (state)
        {
            case MovementState.Idle:
                OnPlayerIdle?.Invoke();
                break;
            case MovementState.Walking:
                OnPlayerWalk?.Invoke();
                break;
            case MovementState.Running:
                OnPlayerRun?.Invoke();
                break;
            default:
                break;

        }
    }

    private void MovementSoundLogic(MovementState state)
    {
        switch (state)
        {
            case MovementState.Walking:
                feetAudioSource.clip = walkingClip;
                feetAudioSource.Play();
                break;
            case MovementState.Running:
                feetAudioSource.clip = runningClip;
                feetAudioSource.Play();
                break;
            case MovementState.Idle:
                feetAudioSource.clip = null;
                feetAudioSource.Stop();
                break;
            default:
                break;
        }
    }

    private void CheckFootstepChangePitch()
    {
        if (feetAudioSource.isPlaying && feetAudioSource.loop)
        {
            if (feetAudioSource.time < lastAudioTime)
            {
                feetAudioSource.pitch = GetRandomPitch();
            }

            lastAudioTime = feetAudioSource.time;
        }
    }

    private float GetRandomPitch()
    {
        float randomPitch = UnityEngine.Random.Range(minStepPitch, maxStepPitch);
        return randomPitch;
    }
}
