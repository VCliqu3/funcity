using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerFeet : MonoBehaviour
{
    [SerializeField] private Vector3 checkBoxDimensions;

    [SerializeField] private LayerMask groundLayer;
    private bool prevoiusGroundedState;
    public bool isGrounded;

    public static Action OnPlayerGrounded;

    private void Update()
    {
        isGrounded = CheckGround();

        CheckLand();
    }

    private bool CheckGround()
    {
        RaycastHit[] hits = Physics.BoxCastAll(transform.position, checkBoxDimensions, Vector3.up, transform.rotation, groundLayer);

        foreach (RaycastHit hit in hits)
        {
            Collider hitCollider = hit.collider;
            //Debug.Log(hitCollider.name);

            if(groundLayer == (groundLayer | (1 << hitCollider.gameObject.layer)))
            {
                return true;
            }
        }

        return Physics.CheckBox(transform.position, checkBoxDimensions, transform.rotation, groundLayer); //Para mesh colliders
        
        //return false;
    }

    private void CheckLand()
    {
        if(isGrounded && !prevoiusGroundedState)
        {
            OnPlayerGrounded?.Invoke();
        }

        prevoiusGroundedState = isGrounded;
    }

    
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, 2*checkBoxDimensions);
    }
    
}
