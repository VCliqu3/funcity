using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHandsInertia : MonoBehaviour
{
    [Header("Hands Inertia Settings")]
    [SerializeField] private bool handsInertiaEnabled;
    [SerializeField] private Transform handsInertiaRefference;
    [SerializeField] public Transform handsInertiaSocket;
    [SerializeField,Range(1f,20f)] private float inertiaSpeed;

    [SerializeField] private bool clampEnabled;
    [SerializeField] private Vector2 clampVector;

    private Vector3 HIRCurrentRotation;
    private Vector3 HIRPrevRotation;
    private Vector3 rotationOffset;

    private Quaternion handsInertiaStartRotation;

    private void OnEnable()
    {
        ScenesManager.OnSceneTransitionEnd += DoWhenSceneTransitionEnd;
    }
    private void OnDisable()
    {
        ScenesManager.OnSceneTransitionEnd -= DoWhenSceneTransitionEnd;
    }

    private void Start()
    {
        handsInertiaStartRotation = handsInertiaSocket.localRotation;

        HIRCurrentRotation = handsInertiaRefference.rotation.eulerAngles;
        HIRPrevRotation = HIRCurrentRotation;
    }
    private void Update()
    {
        CalculateInertia();
        CheckEnableHandsInertia();
    }

    private void CalculateInertia()
    {
        if (!handsInertiaEnabled) 
        {
            handsInertiaSocket.localEulerAngles = Vector3.zero;
            return;
        }

        if(HIRCurrentRotation != HIRPrevRotation)
        {
            rotationOffset = HIRPrevRotation - HIRCurrentRotation;

            if (clampEnabled)
            {
                if (Mathf.Abs(handsInertiaSocket.localRotation.x) > Mathf.Abs(clampVector.x * Mathf.PI/180f)) rotationOffset.x = 0f; 
                if (Mathf.Abs(handsInertiaSocket.localRotation.y) > Mathf.Abs(clampVector.y * Mathf.PI/180f)) rotationOffset.y = 0f; 
            }

            handsInertiaSocket.localEulerAngles += rotationOffset;
        }

        handsInertiaSocket.localRotation = Quaternion.Slerp(handsInertiaSocket.localRotation,handsInertiaStartRotation, inertiaSpeed * Time.deltaTime);

        HIRPrevRotation = HIRCurrentRotation;
        HIRCurrentRotation = handsInertiaRefference.rotation.eulerAngles;
    }

    private void CheckEnableHandsInertia()
    {
        if(ScenesManager.instance.sceneState != ScenesManager.SceneState.TransitionIn)
        {
            handsInertiaEnabled = true;
        }
        else
        {
            handsInertiaEnabled = false;
        }
    }
    private void DoWhenSceneTransitionEnd()
    {
        HIRPrevRotation = HIRCurrentRotation;
    }
}
