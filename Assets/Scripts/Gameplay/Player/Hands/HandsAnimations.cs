using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandsAnimations : MonoBehaviour
{
    [SerializeField] private Animator handsAnimator;

    [SerializeField] private PickedUpObject pickedUpObject;
    private enum PickedUpObject {None, Organ, Card, Default}

    private void OnEnable()
    {
        PlayerInteract.OnPlayerInteract += DoWhenInteract;
        PlayerInteract.OnPlayerFailInteract += DoWhenFailInteract;

        PlayerPickUp.OnPlayerPickUp += DoWhenPickUp;
        PlayerPickUp.OnPlayerDrop += DoWhenDrop;

        UsableObject.AnimationOnObjectUsed += DoWhenObjectUsed;
        UsableObject.AnimationOnObjectUsedWrong += DoWhenObjectUsedWrong;

        PlayerFlashlight.OnFlashlightToggle += DoWhenFlashlightToggle;
    }
    private void OnDisable()
    {
        PlayerInteract.OnPlayerInteract -= DoWhenInteract;
        PlayerInteract.OnPlayerFailInteract -= DoWhenFailInteract;

        PlayerPickUp.OnPlayerPickUp -= DoWhenPickUp;
        PlayerPickUp.OnPlayerDrop -= DoWhenDrop;

        UsableObject.AnimationOnObjectUsed -= DoWhenObjectUsed;
        UsableObject.AnimationOnObjectUsedWrong -= DoWhenObjectUsedWrong;

        PlayerFlashlight.OnFlashlightToggle -= DoWhenFlashlightToggle;
    }

    private void DoWhenInteract(InteractableObject interactableObject)
    {
        if (interactableObject.TryGetComponent(out InteractableDoor door))
        {
            if (door.isOpen)
            {
                //Debug.Log("Hands Interact Door Opening Animation");
                return;
            }
            else
            {
                //Debug.Log("Hands Interact Door Closing Animation");
                return;
            }
        }

        if (interactableObject.TryGetComponent(out InteractableDrawer drawer))
        {
            if (drawer.isOpen)
            {
                //Debug.Log("Hands Interact Drawer Opening Animation");
                return;
            }
            else
            {
                //Debug.Log("Hands Interact Drawer Closing Animation");
                return;
            }
        }

        if (interactableObject.TryGetComponent(out InteractableLever lever))
        {
            if (!lever.isOn)
            {
                //Debug.Log("Hands Interact Lever Up Animation");
                return;
            }
            else
            {
                //Debug.Log("Hands Interact Lever Down Animation");
                return;
            }
        }

        if (interactableObject.TryGetComponent(out InteractableLid lid))
        {
            if (lid.isOpen)
            {
                //Debug.Log("Hands Interact Lid Open Animation");
                return;
            }
            else
            {
                //Debug.Log("Hands Interact Lid Close Animation");
                return;
            }
        }

        if (interactableObject.TryGetComponent(out InteractableNote note))
        {
            //Debug.Log("Hands Interact Note Open Animation");
            return;
        }

        if (interactableObject.TryGetComponent(out BalanceCalibrator balance))
        {       
            //Debug.Log("Hands Balance Calibration Animation");
            return;
        }

        if (interactableObject.TryGetComponent(out InteractableButton button))
        {
            //Debug.Log("Button Interact Animation");
            return;
        }

        if (interactableObject.TryGetComponent(out RotativeCamera camera))
        {          
            //Debug.Log("Camera Rotated Hands Animation");
            return;        
        }

        //Debug.Log("Hands Interact Animation");
    }

    private void DoWhenFailInteract(InteractableObject interactableObject)
    {
        if (interactableObject.TryGetComponent(out InteractableDoor door))
        {          
            //Debug.Log("Hands Fail Open Door Animation");
            return;
        }

        if (interactableObject.TryGetComponent(out InteractableDrawer drawer))
        {
            //Debug.Log("Hands Fail Open Drawer Animation");
            return;
        }

        if (interactableObject.TryGetComponent(out InteractableLever lever))
        {
            //Debug.Log("Hands Fail Pull Down Lever Animation");
            return;
        }

        if (interactableObject.TryGetComponent(out InteractableLid lid))
        {
            //Debug.Log("Hands Fail Open Lid Animation");
            return;
        }

        if (interactableObject.TryGetComponent(out BalanceCalibrator balance))
        {
            //Debug.Log("Hands Fail Calibrate Up Animation");
            return;
        }

        //Debug.Log("Hands Fail Interact Animation");
    }

    private void DoWhenPickUp(PickableObject pickableObject)
    {
        if (pickableObject.TryGetComponent(out Card card))
        {
            //Debug.Log("Hands PickUp Card Animation");
            SetPickedUpObject(PickedUpObject.Card);
            return;
        }

        if (pickableObject.TryGetComponent(out Organ organ))
        {
            //Debug.Log("Hands PickUp Organ Animation");
            SetPickedUpObject(PickedUpObject.Organ);
            return;
        }

        //Debug.Log("Hands PickUp Animation");
        SetPickedUpObject(PickedUpObject.Default);
    }

    private void DoWhenDrop(PickableObject pickableObject)
    {
        SetPickedUpObject(PickedUpObject.None);

        if (pickableObject.TryGetComponent(out Card card))
        {
            //Debug.Log("Hands Drop Card Animation");
            return;
        }

        if (pickableObject.TryGetComponent(out Organ organ))
        {
            //Debug.Log("Hands Drop Organ Animation");
            return;
        }

        //Debug.Log("Hands Drop Animation");
    }

    private void DoWhenObjectUsed(UsableObject usableObject)
    {
        SetPickedUpObject(PickedUpObject.None);

        if (usableObject.TryGetComponent(out Card card))
        {
            //Debug.Log("Hands Use Card Animation");
            return;
        }

        if (usableObject.TryGetComponent(out Organ organ))
        {
            //Debug.Log("Hands Use Organ Animation");
            return;
        }

        //Debug.Log("Hands Use Animation");
    }

    private void DoWhenObjectUsedWrong(UsableObject usableObject)
    {
        //Debug.Log("Hands UseWrong Animation");
    }

    private void DoWhenFlashlightToggle(bool state)
    {
        //Debug.Log("Hands FlashlightToggle Animation");
    }

    private void SetPickedUpObject(PickedUpObject pickedUpObject)
    {
        this.pickedUpObject = pickedUpObject;
    }

    private void OnHandAnimationCompleted()
    {
        switch (pickedUpObject)
        {        
            case PickedUpObject.Organ:
                //Debug.Log("Idle Hands Animation - Object Picked Up: Organ");
                break;
            case PickedUpObject.Card:
                //Debug.Log("Idle Hands Animation - Object Picked Up: Card");
                break;
            case PickedUpObject.Default:
                //Debug.Log("Idle Hands Animation - Object Picked Up: Default");
                break;
            case PickedUpObject.None:
            default:
                //Debug.Log("Idle Hands Animation - Object Picked Up: None");
                break;
        }
    }
}
