using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerFlashlight : MonoBehaviour
{
    [Header("Flashlight Settings")]
    public bool flashlightToggleEnabled;
    [SerializeField] private List<Light> flashlightLights = new List<Light>();
    [SerializeField] private bool startWithFlashlightOn;
    public bool flashlightOn;

    [Header("AudioClips")]
    [SerializeField] private AudioSource flashlightAudioSource;
    [SerializeField] private AudioClip toggleFlashlightClip;

    private PlayerInputHandler playerImputHandler;

    public static Action<bool> OnFlashlightToggle;

    private void Awake()
    {
        playerImputHandler = FindObjectOfType<PlayerInputHandler>();
    }

    private void Start()
    {
        ForceFlashlightState(startWithFlashlightOn);
    }

    void Update()
    {
        CheckFlashlightToggle();
    }

    private void CheckFlashlightToggle()
    {
        if (!flashlightToggleEnabled) return;

        if (playerImputHandler.GetFlashlightInputDown())
        {
            ToggleFlashlight();
        }
    }

    public void ForceFlashlightState(bool state)
    {
        flashlightOn = state;
        UpdateFlashlightState();
    }

    private void ToggleFlashlight()
    {
        flashlightOn = !flashlightOn;
        UpdateFlashlightState();

        OnFlashlightToggle?.Invoke(flashlightOn);

        if (toggleFlashlightClip) flashlightAudioSource.PlayOneShot(toggleFlashlightClip);
    }

    private void UpdateFlashlightState()
    {
        foreach(Light light in flashlightLights)
        {
            light.enabled = flashlightOn;
        }
    }
}
