using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class PlayerPickUp : MonoBehaviour
{
    public PickableObject currentPickableObjectOnSight;
    public PickableObject currentPickedUpObject;

    [Header("Pick Up Settings")]
    [SerializeField] private Camera playerCamera;
    [SerializeField] private Transform leftHandSocket;
    [SerializeField] private LayerMask pickableUpLayer;
    [SerializeField] private LayerMask ignoredLayers;
    public bool pickUpEnabled;
    public LayerMask handsLayer;
    [SerializeField, Range(0.5f, 6f)] private float pickUpDetectionRange;
    [SerializeField, Range(0.5f, 3.5f)] private float pickUpSightRange;

    [Header("Smooth Pick Up Settings")]
    public bool smoothPickUp;
    [Range(1f,20f)] public float pickUpSpeed;
    public bool dropWhilePickingUp;
    public bool isPickingUp;
    [Range(0.01f, 0.1f)] public float isPickingUpThreshold;
    public bool isInspecting;

    [Header("Inspection Settings")]
    public bool inspectionEnabled;

    private PlayerInputHandler playerInputHandler;
    private PlayerInteract playerInteract;

    private List<PickableObject> pickableObjectsDetected = new List<PickableObject>();

    public static Action<PickableObject> OnPlayerPickUp;
    public static Action<PickableObject> OnPlayerDrop;
    public static Action<PickableObject> OnPlayerInspect;

    private void Awake()
    {
        playerInputHandler = FindObjectOfType<PlayerInputHandler>();
        playerInteract = GetComponent<PlayerInteract>();
    }

    private void Start()
    {
        Physics.IgnoreLayerCollision(8, 13);
        Physics.IgnoreLayerCollision(8, 10);
        Physics.IgnoreLayerCollision(6, 10);
    }

    void LateUpdate() //Debe ocurrir luego del Update de PlayerInteract (si se usa la misma tecla)
    {
        CheckPickableObjectsOnDetectionRange();
        CheckPickableObjectOnSightRange();
        CheckPickUp();
    }

    private void CheckPickUp()
    {
        if (!pickUpEnabled) return;

        if (playerInputHandler.GetPickUpInputDown())
        {
            if(!currentPickedUpObject && currentPickableObjectOnSight)
            {
                if (!currentPickableObjectOnSight.pickUpEnabled) return;

                currentPickedUpObject = currentPickableObjectOnSight;
                currentPickedUpObject.PickUp(this,leftHandSocket);

                OnPlayerPickUp?.Invoke(currentPickedUpObject);
            }         
        }

        if (playerInputHandler.GetDropInputDown())
        {
            if (!currentPickedUpObject) return;
            if (isPickingUp && !dropWhilePickingUp) return;

            currentPickedUpObject.Drop(this);
            OnPlayerDrop?.Invoke(currentPickedUpObject);
            
            currentPickedUpObject = null;
        }

        if (!inspectionEnabled) return;

        if (playerInputHandler.GetInspectInputDown())
        {
            if (!currentPickedUpObject) return;
            if (!currentPickedUpObject.enableInspection) return;
            if (isPickingUp && !dropWhilePickingUp) return;
            if (isInspecting) return;

            currentPickedUpObject.Inspect(this);
            OnPlayerInspect?.Invoke(currentPickedUpObject);
        }
    }

    private void CheckPickableObjectOnSightRange()
    {
        if (!pickUpEnabled) return;

        GameObject objectInPickupRange = GetGameObjectInRange(pickUpSightRange,ignoredLayers);

        if (objectInPickupRange && CheckIsInLayer(objectInPickupRange,pickableUpLayer))
        {
            if (objectInPickupRange.TryGetComponent<PickableObject>(out PickableObject pickableObject))
            {
                if (pickableObject == currentPickableObjectOnSight) return;

                if (currentPickableObjectOnSight) currentPickableObjectOnSight.OnPlayerLeavePickUpSightRange();

                currentPickableObjectOnSight = pickableObject;
                currentPickableObjectOnSight.OnPlayerEnterPickUpSightRange();
            }
        }
        else if (currentPickableObjectOnSight)
        {
            currentPickableObjectOnSight.OnPlayerLeavePickUpSightRange();
            currentPickableObjectOnSight = null;
        }
    }
    private GameObject GetGameObjectInRange(float range, LayerMask ignoreLayer)
    {
        Ray cameraRay = playerCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)); //Rayo disparado desde la mitad de la pantalla

        if (Physics.Raycast(cameraRay, out RaycastHit hitInfo, range, ~(ignoreLayer)))
        {
            return hitInfo.transform.gameObject;
        }

        return null;
    }

    private bool CheckIsInLayer(GameObject obj, LayerMask layer)
    {
        return layer == (layer | (1 << obj.layer));
    }

    private void CheckPickableObjectsOnDetectionRange()
    {
        List<PickableObject> newPickableObjectsDetected = new List<PickableObject>();

        Collider[] pickableColliders = Physics.OverlapSphere(transform.position, pickUpDetectionRange, pickableUpLayer);

        foreach (Collider col in pickableColliders)
        {
            if (col.TryGetComponent<PickableObject>(out PickableObject pickableObjectDetected))
            {
                newPickableObjectsDetected.Add(pickableObjectDetected);
            }
        }

        List<PickableObject> enterDetectionPickableObjects = newPickableObjectsDetected.Except(pickableObjectsDetected).ToList();

        List<PickableObject> leftDetectionPickableObjects = pickableObjectsDetected.Except(newPickableObjectsDetected).ToList();

        foreach (PickableObject PickableObject in enterDetectionPickableObjects)
        {
            PickableObject.OnPlayerEnterPickUpDetectionRange();
        }

        foreach (PickableObject PickableObject in leftDetectionPickableObjects)
        {
            PickableObject.OnPlayerLeavePickUpDetectionRange();
        }

        pickableObjectsDetected = newPickableObjectsDetected;
    }
}
