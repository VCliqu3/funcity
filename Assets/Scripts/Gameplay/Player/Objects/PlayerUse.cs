using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class PlayerUse : MonoBehaviour
{
    [Header("Use Configuration")]
    private PlayerInputHandler playerInputHandler;
    [HideInInspector] public PlayerPickUp playerPickUp;
    
    [SerializeField] private Camera playerCamera;
    public bool useEnabled;
    public bool usablePlacesEnabled;
    [SerializeField] private LayerMask usablePlaceLayers;
    [SerializeField] private LayerMask ignoredLayers;

    [SerializeField, Range(0.5f, 6f)] private float usablePlaceDetectionRange;
    [SerializeField, Range(1f, 3.5f)] private float usablePlaceSightRange;

    private UsablePlace currentUsablePlaceOnSight;
    private List<UsablePlace> usablePlacesDetected = new List<UsablePlace>();

    [Header("Place Configuration")]

    public bool smoothPlace;
    [Range(0.5f, 20f)] public float placeSpeed;
    public bool isPlacing;
    [Range(0.01f, 0.1f)] public float isPlacingThreshold;

    public static Action<UsableObject> OnObjectUsed; 

    private void Awake()
    {
        playerPickUp = GetComponent<PlayerPickUp>();
        playerInputHandler = FindObjectOfType<PlayerInputHandler>();
    }

    void Update()
    {
        CheckUsablePlacesOnDetectionRange();
        CheckUsablePlaceOnSightRange();
        CheckUse();
    }

    private void CheckUse()
    {
        if (!useEnabled) return;

        if (playerInputHandler.GetUseInputDown())
        {
            if (!playerPickUp.currentPickedUpObject) return;

            if (playerPickUp.currentPickedUpObject.TryGetComponent<UsableObject>(out UsableObject usableObject))
            {
                if (!usableObject.useEnabled) return;

                if (currentUsablePlaceOnSight)
                {
                    if (!currentUsablePlaceOnSight.usablePlaceEnabled) return;

                    currentUsablePlaceOnSight.TryUseHere(this, usableObject, false);
                }
                else
                {
                    usableObject.UseWithoutUsablePlace(this);
                }
            }
        }
    }

    private void CheckUsablePlaceOnSightRange()
    {
        if (!usablePlacesEnabled) return;

        GameObject objectInUsablePlaceRange = GetGameObjectInRange(usablePlaceSightRange,ignoredLayers);

        if (objectInUsablePlaceRange && CheckIsInLayer(objectInUsablePlaceRange,usablePlaceLayers))
        {
            if (objectInUsablePlaceRange.TryGetComponent<UsablePlace>(out UsablePlace usablePlace))
            {
                if (usablePlace == currentUsablePlaceOnSight) return;

                if (currentUsablePlaceOnSight) currentUsablePlaceOnSight.OnPlayerLeaveUsablePlaceSightRange();

                currentUsablePlaceOnSight = usablePlace;
                currentUsablePlaceOnSight.OnPlayerEnterUsablePlaceSightRange();
            }
        }
        else if (currentUsablePlaceOnSight)
        {
            currentUsablePlaceOnSight.OnPlayerLeaveUsablePlaceSightRange();
            currentUsablePlaceOnSight = null;
        }
    }

    private GameObject GetGameObjectInRange(float range, LayerMask ignoreLayer)
    {
        Ray cameraRay = playerCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)); //Rayo disparado desde la mitad de la pantalla

        if (Physics.Raycast(cameraRay, out RaycastHit hitInfo, range, ~(ignoreLayer)))
        {
            return hitInfo.transform.gameObject;
        }

        return null;
    }

    private bool CheckIsInLayer(GameObject obj, LayerMask layer)
    {
        return layer == (layer | (1 << obj.layer));
    }

    private void CheckUsablePlacesOnDetectionRange()
    {
        List<UsablePlace> newUsablePlacesDetected = new List<UsablePlace>();

        Collider[] usablePlacesColliders = Physics.OverlapSphere(transform.position, usablePlaceDetectionRange, usablePlaceLayers);

        foreach (Collider col in usablePlacesColliders)
        {
            if (col.TryGetComponent<UsablePlace>(out UsablePlace usablePlaceDetected))
            {
                newUsablePlacesDetected.Add(usablePlaceDetected);
            }
        }

        List<UsablePlace> enterDetectionUsablePlaces= newUsablePlacesDetected.Except(usablePlacesDetected).ToList();

        List<UsablePlace> leftDetectionUsablePlaces = usablePlacesDetected.Except(newUsablePlacesDetected).ToList();

        foreach (UsablePlace usablePlace in enterDetectionUsablePlaces)
        {
            usablePlace.OnPlayerEnterUsablePlaceDetectionRange();
        }

        foreach (UsablePlace usablePlace in leftDetectionUsablePlaces)
        {
            usablePlace.OnPlayerLeaveUsablePlaceDetectionRange();
        }

        usablePlacesDetected = newUsablePlacesDetected;
    }
}
