using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class PlayerInteract : MonoBehaviour
{
    private PlayerInputHandler playerInputHandler;
    private PlayerPickUp playerPickUp;
    
    [SerializeField] private Camera playerCamera;
    [SerializeField] private LayerMask interactableLayers;
    [SerializeField] private LayerMask ignoredLayers;
    public bool interactionsEnabled;
    [SerializeField] private bool interactWhileObjectPickedUp;

    [SerializeField, Range(0.5f, 6f)] private float interactionDetectionRange;
    [SerializeField, Range(0.5f, 3.5f)] private float interactionSightRange;

    public InteractableObject currentInteractableObjectOnSight;
    private List<InteractableObject> interactableObjectsDetected = new List<InteractableObject>();

    public static Action<InteractableObject> OnPlayerInteract;
    public static Action<InteractableObject> OnPlayerFailInteract;

    private void Awake()
    {
        playerInputHandler = FindObjectOfType<PlayerInputHandler>();
        playerPickUp = GetComponent<PlayerPickUp>();
    }

    void Update()
    {
        CheckInteractableObjectsOnDetectionRange();
        CheckInteractableObjectOnSightRange();
        CheckInteract();
    }

    private void CheckInteract()
    {
        if (!interactionsEnabled) return;

        if (!interactWhileObjectPickedUp && playerPickUp.currentPickedUpObject) return;

        if (playerInputHandler.GetInteractInputDown())
        {
            if (!currentInteractableObjectOnSight) return;

            if (currentInteractableObjectOnSight.interactionsEnabled)
            {
                currentInteractableObjectOnSight.Interact(this);
                OnPlayerInteract?.Invoke(currentInteractableObjectOnSight);
            }
            else
            {
                currentInteractableObjectOnSight.FailInteract(this);
                OnPlayerFailInteract?.Invoke(currentInteractableObjectOnSight);
            }
        }
    }

    private void CheckInteractableObjectOnSightRange()
    {
        if (!interactionsEnabled) return;

        GameObject objectInInteractionRange = GetGameObjectInRange(interactionSightRange,ignoredLayers);

        if (objectInInteractionRange && CheckIsInLayer(objectInInteractionRange, interactableLayers))
        {
            if (objectInInteractionRange.TryGetComponent<InteractableObject>(out InteractableObject interactableObject))
            {
                if (interactableObject == currentInteractableObjectOnSight) return;

                if (currentInteractableObjectOnSight) currentInteractableObjectOnSight.OnPlayerLeaveInteractionSightRange();

                currentInteractableObjectOnSight = interactableObject;
                currentInteractableObjectOnSight.OnPlayerEnterInteractionSightRange();                
            }
        }
        else if (currentInteractableObjectOnSight)
        {
            currentInteractableObjectOnSight.OnPlayerLeaveInteractionSightRange();
            currentInteractableObjectOnSight = null;
        }   
    }

    private GameObject GetGameObjectInRange(float range, LayerMask ignoreLayer)
    {
        Ray cameraRay = playerCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)); //Rayo disparado desde la mitad de la pantalla

        if (Physics.Raycast(cameraRay, out RaycastHit hitInfo, range, ~(ignoreLayer)))
        {
            return hitInfo.transform.gameObject;
        }

        return null;
    }

    private bool CheckIsInLayer(GameObject obj, LayerMask layer)
    {
        return layer == (layer | (1 << obj.layer));
    }

    private void CheckInteractableObjectsOnDetectionRange()
    {
        List<InteractableObject> newInteractableObjectsDetected = new List<InteractableObject>();

        Collider[] interactableColliders = Physics.OverlapSphere(transform.position, interactionDetectionRange, interactableLayers);

        foreach (Collider col in interactableColliders)
        {
            if(col.TryGetComponent<InteractableObject>(out InteractableObject interactableObjectDetected))
            {   
                newInteractableObjectsDetected.Add(interactableObjectDetected);    
            }
        }

        List<InteractableObject> enterDetectionInteractableObjects = newInteractableObjectsDetected.Except(interactableObjectsDetected).ToList();
        List<InteractableObject> leftDetectionInteractableObjects = interactableObjectsDetected.Except(newInteractableObjectsDetected).ToList();

        foreach(InteractableObject interactableObject in enterDetectionInteractableObjects)
        {
            interactableObject.OnPlayerEnterInteractionDetectionRange();
        }

        foreach (InteractableObject interactableObject in leftDetectionInteractableObjects)
        {
            interactableObject.OnPlayerLeaveInteractionDetectionRange();
        }

        interactableObjectsDetected = newInteractableObjectsDetected;
    }
}
