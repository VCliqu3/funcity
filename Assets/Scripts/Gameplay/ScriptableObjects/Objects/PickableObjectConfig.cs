using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PickableObjectConfig", menuName = "ScriptableObjects/Config/Objects/PickableObjectConfig")]
public class PickableObjectConfig : ScriptableObject
{
    [Header("Drop Configuration")]
    public Vector3 dropOffsetPosition;
    public float forwardDropForce = 1f;
    public float upDropForce = 0f;
    public float rightDropForce = 0f;
}
