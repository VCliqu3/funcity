using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TutorialActions : MonoBehaviour
{
    [Header("Tutorial Actions-PanelIDs Link Settings")]
    [SerializeField] private int movePanelID;
    [SerializeField] private int runPanelID;
    [SerializeField] private int flashlightPanelID;
    [SerializeField] private int interactPanelID;
    [SerializeField] private int readNotePanelID;
    [SerializeField] private int pickUpPanelID;
    [SerializeField] private int dropPanelID;
    [SerializeField] private int placePanelID;

    public static Action<int> OnTutorialActionPerformed;

    private void OnEnable()
    {
        PlayerHorizontalMovement.OnPlayerWalk += OnMoveActionPerformed;
        PlayerHorizontalMovement.OnPlayerRun += OnMoveActionPerformed;

        PlayerHorizontalMovement.OnPlayerRun += OnRunActionPerformed;

        PlayerFlashlight.OnFlashlightToggle += OnFlaslightActionPerformed;

        LockerDoor.OnLockerDoorInteracted += OnInteractActionPerformed;

        InteractableNote.OnNoteInteracted += OnReadNoteActionPerformed;

        TicketOfficeCard.OnTicketOfficeCardPickUp += OnPickUpActionPerformed;

        TicketOfficeCard.OnTicketOfficeCardDrop += OnDropActionPerformed;

        PlaceableObject.OnObjectPlaced += OnPlaceActionPerformed;
    }

    private void OnDisable()
    {
        PlayerHorizontalMovement.OnPlayerWalk -= OnMoveActionPerformed;
        PlayerHorizontalMovement.OnPlayerRun -= OnMoveActionPerformed;

        PlayerHorizontalMovement.OnPlayerRun -= OnRunActionPerformed;

        PlayerFlashlight.OnFlashlightToggle -= OnFlaslightActionPerformed;

        LockerDoor.OnLockerDoorInteracted -= OnInteractActionPerformed;

        InteractableNote.OnNoteInteracted -= OnReadNoteActionPerformed;

        TicketOfficeCard.OnTicketOfficeCardPickUp -= OnPickUpActionPerformed;

        TicketOfficeCard.OnTicketOfficeCardDrop -= OnDropActionPerformed;

        PlaceableObject.OnObjectPlaced -= OnPlaceActionPerformed;
    }

    private void OnMoveActionPerformed()
    {
        OnTutorialActionPerformed?.Invoke(movePanelID);
    }
    private void OnRunActionPerformed()
    {
        OnTutorialActionPerformed?.Invoke(runPanelID);
    }

    private void OnFlaslightActionPerformed(bool state)
    {
        OnTutorialActionPerformed?.Invoke(flashlightPanelID);
    }

    private void OnInteractActionPerformed(LockerDoor lockerDoor, bool isOpen)
    {
        OnTutorialActionPerformed?.Invoke(interactPanelID);
    }
    private void OnReadNoteActionPerformed(InteractableNote interactableNote, Sprite sprite, int noteNumber)
    {
        OnTutorialActionPerformed?.Invoke(readNotePanelID);
    }

    private void OnPickUpActionPerformed()
    {
        OnTutorialActionPerformed?.Invoke(pickUpPanelID);
    }

    private void OnDropActionPerformed()
    {
        OnTutorialActionPerformed?.Invoke(dropPanelID);
    }

    private void OnPlaceActionPerformed(PlaceableObject placeableObject, PlaceablePlace placeablePlace)
    {
        OnTutorialActionPerformed?.Invoke(placePanelID);
    }
}
