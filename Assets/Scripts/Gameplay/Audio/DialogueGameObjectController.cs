using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueGameObjectController : SoundGameObjectController
{
    protected override void OnEnable()
    {
        base.OnEnable();
        AudioManager.OnDialogueVolumeSet += UpdateAudioSourceVolume;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        AudioManager.OnDialogueVolumeSet -= UpdateAudioSourceVolume;
    }
    protected override void Start()
    {
        base.Start();
        UpdateAudioSourceVolume(AudioManager.instance.dialogueVolume);
    }

    private void UpdateAudioSourceVolume(float volume)
    {
        audioSource.volume = volume;
    }
}
