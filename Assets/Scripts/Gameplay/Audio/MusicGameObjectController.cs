using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicGameObjectController : SoundGameObjectController
{
    protected override void OnEnable()
    {
        base.OnEnable();
        AudioManager.OnMusicVolumeSet += UpdateAudioSourceVolume;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        AudioManager.OnMusicVolumeSet -= UpdateAudioSourceVolume;
    }
    protected override void Start()
    {
        base.Start();
        UpdateAudioSourceVolume(AudioManager.instance.musicVolume);
    }

    private void UpdateAudioSourceVolume(float volume)
    {
        audioSource.volume = volume;
    }
}
