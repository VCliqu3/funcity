using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundGameObjectController : MonoBehaviour
{
    [SerializeField] private bool pauseSoundOnPause = true;
    protected AudioSource audioSource;

    protected virtual void OnEnable()
    {
        PauseCanvasController.OnPauseMenuOpened += OnPause;
        PauseCanvasController.OnPauseMenuClosed += OnResume;

        ScenesManager.OnSceneTransitionEnd += DoOnSceneTransition;
    }

    protected virtual void OnDisable()
    {
        PauseCanvasController.OnPauseMenuOpened -= OnPause;
        PauseCanvasController.OnPauseMenuClosed -= OnResume;

        ScenesManager.OnSceneTransitionEnd -= DoOnSceneTransition;
    }

    protected virtual void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    protected virtual void Start() { }

    protected virtual void OnPause()
    {
        if (!pauseSoundOnPause) return;
        audioSource.Pause();
    }

    protected virtual void OnResume()
    {
        if (!pauseSoundOnPause) return;
        audioSource.UnPause();
    }

    protected virtual void DoOnSceneTransition()
    {
        audioSource.UnPause();
    }
}
