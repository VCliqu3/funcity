using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXGameObjectController : SoundGameObjectController
{
    protected override void OnEnable()
    {
        base.OnEnable();
        AudioManager.OnSFXVolumeSet += UpdateAudioSourceVolume;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        AudioManager.OnSFXVolumeSet -= UpdateAudioSourceVolume;
    }
    protected override void Start()
    {
        base.Start();
        UpdateAudioSourceVolume(AudioManager.instance.sfxVolume);
    }

    private void UpdateAudioSourceVolume(float volume)
    {
        audioSource.volume = volume;
    }
}
