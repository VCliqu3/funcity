using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class TutorialPanelIDRelation
{
    public int panelID;
    public GameObject linkedPanel;
    public float activeTime;
    public bool actionPerformed;
}

public class TutorialPanelsController : MonoBehaviour
{
    [SerializeField] private TutorialPanelIDRelation[] tutorialPanelIDrelations;

    [SerializeField] private float fadeInTime;
    [SerializeField] private float fadeOutTime;
    [SerializeField] private float timeBetweenPanels;

    public TutorialPanelIDRelation activeRelation;
    public enum TutorialCanvasState {Idle, FadingIn, Showing, FadingOut }
    public TutorialCanvasState tutorialCanvasState;

    private CanvasGroup canvasGroup;

    public static Action<TutorialCanvasState> OnTutorialCanvasStateChanged;

    private void OnEnable()
    {
        TutorialColliderDetectionTrigger.OnPlayerEnter += DoWhenTutorialColliderTrigger;
        TutorialActions.OnTutorialActionPerformed += DoWhenTutorialActionPerformed;
    }

    private void OnDisable()
    {
        TutorialColliderDetectionTrigger.OnPlayerEnter -= DoWhenTutorialColliderTrigger;
        TutorialActions.OnTutorialActionPerformed -= DoWhenTutorialActionPerformed;
    }

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    private void Start()
    {
        InitializeCanvasGroup();
        SetTutorialCanvasState(TutorialCanvasState.Idle);
    }

    private void InitializeCanvasGroup()
    {
        canvasGroup.alpha = 0f;
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
    }
    public void SetTutorialCanvasState(TutorialCanvasState tutorialCanvasState)
    {
        this.tutorialCanvasState = tutorialCanvasState;
        OnTutorialCanvasStateChanged?.Invoke(this.tutorialCanvasState);
    }

    private void DoWhenTutorialColliderTrigger(int panelID)
    {
        if (InGameCheckpointManager.instance.currentCheckpointNumber != 0) return;

        TutorialPanelIDRelation tutorialPanelIDrelation = Array.Find(tutorialPanelIDrelations, x => x.panelID == panelID);

        if (tutorialPanelIDrelation != null)
        {
            StopAllCoroutines();
            StartCoroutine(ShowCompleteInstruction(tutorialPanelIDrelation));
        }
        else
        {
            Debug.Log("Invalid Tutorial Panel ID: " + panelID);
        }
    }
    private void DoWhenTutorialActionPerformed(int panelID)
    {
        TutorialPanelIDRelation tutorialPanelIDrelation = Array.Find(tutorialPanelIDrelations, x => x.panelID == panelID);

        if (tutorialPanelIDrelation != null)
        {
            //if (tutorialCanvasState == TutorialCanvasState.Idle) return;
            
            if (activeRelation != tutorialPanelIDrelation) return;
            if (tutorialCanvasState == TutorialCanvasState.FadingOut) return;
            if (tutorialCanvasState == TutorialCanvasState.FadingIn) return;
            if (activeRelation.actionPerformed) return;

            activeRelation.actionPerformed = true;

            StopAllCoroutines();
            StartCoroutine(HideInstruction());
        }
        else
        {
            Debug.Log("Invalid Tutorial Panel ID: " + panelID);
        }
    }

    #region FadeCoroutines
    private IEnumerator FadeIn(float fadeInTime)
    {
        canvasGroup.alpha = 0f;
        float time = 0f;

        while (time <= fadeInTime)
        {
            time += Time.deltaTime;
            canvasGroup.alpha = time / fadeInTime;

            yield return null;
        }

        canvasGroup.alpha = 1f;
    }

    private IEnumerator FadeOut(float fadeOutTime)
    {
        float startingAlpha = canvasGroup.alpha;
        float timeRemaining = fadeOutTime * canvasGroup.alpha;

        float time = 0f;

        while (time <= timeRemaining)
        {
            time += Time.deltaTime;
            canvasGroup.alpha = startingAlpha*(1 - time / timeRemaining);

            yield return null;
        }

        canvasGroup.alpha = 0f;
    }  
    #endregion

    private IEnumerator HideInstruction()
    {
        SetTutorialCanvasState(TutorialCanvasState.FadingOut);
        yield return StartCoroutine(FadeOut(fadeOutTime));

        SetTutorialCanvasState(TutorialCanvasState.Idle);
        activeRelation = null;
        DisableAllPanels();
    }
     
    private IEnumerator ShowCompleteInstruction(TutorialPanelIDRelation relation)
    {    
        if (tutorialCanvasState != TutorialCanvasState.Idle)
        {
            yield return StartCoroutine(HideInstruction());
            yield return new WaitForSeconds(timeBetweenPanels);
        }

        activeRelation = relation;

        if (activeRelation.actionPerformed)
        {
            activeRelation = null;
            yield break;
        }

        activeRelation.linkedPanel.SetActive(true);

        SetTutorialCanvasState(TutorialCanvasState.FadingIn);
        yield return StartCoroutine(FadeIn(fadeInTime));

        SetTutorialCanvasState(TutorialCanvasState.Showing);
        yield return new WaitForSeconds(activeRelation.activeTime);

        yield return StartCoroutine(HideInstruction());
    }

    private void DisableAllPanels()
    {
        foreach(TutorialPanelIDRelation relation in tutorialPanelIDrelations)
        {
            relation.linkedPanel.SetActive(false);
        }
    } 
}
