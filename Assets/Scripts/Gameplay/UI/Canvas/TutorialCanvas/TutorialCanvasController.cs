using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialCanvasController : CanvasController
{
    protected override void Start()
    {
        base.Start();
        SetCanvasGroup(true);
    }

    protected override void CheckIfShouldBeActive(GameManager.GameState previousState, GameManager.GameState newState)
    {
        switch (newState)
        {
            case GameManager.GameState.Playing:
            case GameManager.GameState.ReadingNote:
            case GameManager.GameState.OnInspection:
            case GameManager.GameState.OnCutscene:
                SetCanvasGroup(true);
                break;
            default:
                SetCanvasGroup(false);
                break;
        }
    }
}
