using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

[System.Serializable]
public class PauseMenuButtonPanelLink
{
    public Button button;
    public GameObject panel;
}

[System.Serializable]
public class PauseMenuButtonSuperposedPanelLink
{
    public Button button;
    public GameObject superposedPanel;
}

public class PauseMenuPanelsController : MonoBehaviour
{
    [SerializeField] private PauseMenuButtonPanelLink[] buttonPanelLinks;
    [SerializeField] private GameObject defaultPanel;
    
    [SerializeField] private PauseMenuButtonSuperposedPanelLink[] buttonSuperposedPanelLinks;
    
    [HideInInspector] public bool onSuperposedPanel = false;

    private GameObject activePanel;
    private GameObject activeSuperposedPanel;

    public void OnPauseMenuPanelButtonClicked()
    {
        Button clickedButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();

        if (clickedButton)
        {
            PauseMenuButtonPanelLink buttonPanelLink = Array.Find(buttonPanelLinks, x => x.button == clickedButton);

            if (buttonPanelLink != null)
            {
                GameObject panel = buttonPanelLink.panel;

                if(panel != activePanel)
                {
                    SwitchActivePanel(panel);
                }
            }
            else
            {
                Debug.Log("Link Not Found");
            }
        }
        else
        {
            Debug.Log("Button Not Found");
        }
    }
    public void OnPauseMenuOpened()
    {
        EnableDefaultPanel();
    }
    public void OnPauseMenuClosed()
    {
        DisableAllPanels();
    }
    private void EnableDefaultPanel()
    {
        if(activePanel) return;

        if (defaultPanel)
        {
            defaultPanel.SetActive(true);
            activePanel = defaultPanel;
        }
    }
    private void DisableAllPanels()
    {
        foreach (PauseMenuButtonPanelLink link in buttonPanelLinks)
        {
            link.panel.SetActive(false);
        }

        activePanel = null;
    }
    private void SwitchActivePanel(GameObject panel)
    {
        if(activePanel) activePanel.SetActive(false);

        panel.SetActive(true);

        activePanel = panel;
    }

    public void OnPauseMenuSuperposedPanelButtonClicked()
    {
        Button clickedButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();

        if (clickedButton)
        {
            PauseMenuButtonSuperposedPanelLink buttonSuperposedPanelLink = Array.Find(buttonSuperposedPanelLinks, x => x.button == clickedButton);

            if (buttonSuperposedPanelLink != null)
            {
                GameObject panel = buttonSuperposedPanelLink.superposedPanel;
                EnableSuperposedPanel(panel);
            }
            else
            {
                Debug.Log("Link Not Found");
            }
        }
        else
        {
            Debug.Log("Button Not Found");
        }
    }

    private void EnableSuperposedPanel(GameObject panel)
    {
        if (activeSuperposedPanel) activeSuperposedPanel.SetActive(false);

        panel.SetActive(true);
        activeSuperposedPanel = panel;
        onSuperposedPanel = true;
    }

    public void DisableActiveSuperposedPanel()
    {
        activeSuperposedPanel.SetActive(false);
        activeSuperposedPanel = null;
        onSuperposedPanel = false;
    }
}
