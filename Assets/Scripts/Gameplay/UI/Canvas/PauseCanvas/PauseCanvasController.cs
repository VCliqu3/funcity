using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PauseCanvasController : CanvasController
{
    private PauseMenuPanelsController pauseMenuPanelsController;

    public static Action OnPauseMenuOpened;
    public static Action OnPauseMenuClosed;

    protected override void OnEnable()
    {
        base.OnEnable();
        GameManager.OnPauseInput += DoWhenPauseInput;
        GameManager.OnResumeInput += DoWhenResumeInput;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        GameManager.OnPauseInput -= DoWhenPauseInput;
        GameManager.OnResumeInput -= DoWhenResumeInput;
    }

    protected override void Awake()
    {
        base.Awake();
        pauseMenuPanelsController = GetComponent<PauseMenuPanelsController> ();
    }

    protected override void Start()
    {
        base.Start();

        SetCanvasGroup(false);
    }

    private void DoWhenPauseInput()
    {
        OpenPauseMenu();
    }

    private void DoWhenResumeInput()
    {
        if (pauseMenuPanelsController.onSuperposedPanel)
        {
            pauseMenuPanelsController.DisableActiveSuperposedPanel();
            return;
        }

        ClosePauseMenu();
    }

    private void OpenPauseMenu()
    {
        SetCanvasGroup(true);
        CursorManager.instance.SetCursorLockedAndVisible(true);

        OnPauseMenuOpened?.Invoke();

        pauseMenuPanelsController.OnPauseMenuOpened();
    }

    public void ClosePauseMenu()
    {
        SetCanvasGroup(false);
        CursorManager.instance.SetCursorLockedAndVisible(false);

        OnPauseMenuClosed?.Invoke();

        pauseMenuPanelsController.OnPauseMenuClosed();
    }
}
