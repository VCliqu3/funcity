using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class DialoguePanelController : MonoBehaviour
{
    [SerializeField] private TMP_Text dialogueText;
    [SerializeField] private float timeBetweenDialogues;

    [SerializeField] private bool fadeEnabled;
    [SerializeField] private float fadeInTime;
    [SerializeField] private float fadeOutTime;
    public enum DialogueCanvasState { Idle, FadingIn, Showing, FadingOut }
    public DialogueCanvasState dialogueCanvasState;

    private string currentDialogue;
    private CanvasGroup canvasGroup;

    public static Action<DialogueCanvasState> OnDialogueCanvasStateChanged;

    private void OnEnable()
    {
        ChildSpeakerDevice.OnChildSpeakerDeviceClipPlay += ShowDialogue;
    }

    private void OnDisable()
    {
        ChildSpeakerDevice.OnChildSpeakerDeviceClipPlay -= ShowDialogue;
    }

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    private void Start()
    {
        InitializeCanvasGroup();
        SetDialogueCanvasState(DialogueCanvasState.Idle);
    }

    private void InitializeCanvasGroup()
    {
        canvasGroup.alpha = 0f;
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
    }

    public void SetDialogueCanvasState(DialogueCanvasState dialogueCanvasState)
    {
        this.dialogueCanvasState = dialogueCanvasState;
        OnDialogueCanvasStateChanged?.Invoke(this.dialogueCanvasState);
    }

    private void ShowDialogue(string dialogue, float timeShowing)
    {
        if (fadeEnabled)
        {
            StopAllCoroutines();
            StartCoroutine(ShowDialogueFade(dialogue, timeShowing));
        }
        else
        {
            StopAllCoroutines();
            StartCoroutine(ShowDialogueRegular(dialogue, timeShowing));
        }
    }

    private IEnumerator ShowDialogueFade(string dialogue, float timeShowing)
    {
        if (dialogueCanvasState != DialogueCanvasState.Idle)
        {
            yield return StartCoroutine(HideDialogueFade());
            yield return new WaitForSeconds(timeBetweenDialogues);
        }

        currentDialogue = dialogue;
        SetDialogueText(currentDialogue);

        SetDialogueCanvasState(DialogueCanvasState.FadingIn);
        yield return StartCoroutine(FadeIn(fadeInTime));

        SetDialogueCanvasState(DialogueCanvasState.Showing);
        yield return new WaitForSeconds(timeShowing);

        yield return StartCoroutine(HideDialogueFade());
    }

    private IEnumerator HideDialogueFade()
    {
        SetDialogueCanvasState(DialogueCanvasState.FadingOut);
        yield return StartCoroutine(FadeOut(fadeOutTime));

        SetDialogueCanvasState(DialogueCanvasState.Idle);

        currentDialogue = null;
        ClearDialogueText();
    }

    private IEnumerator ShowDialogueRegular(string dialogue,float timeShowing)
    {
        if (dialogueCanvasState != DialogueCanvasState.Idle)
        {
            HideDialogueRegular();
            yield return new WaitForSeconds(timeBetweenDialogues);
        }

        currentDialogue = dialogue;
        SetDialogueText(currentDialogue);
        
        SetCanvasGroupAlpha(1f);
        SetDialogueCanvasState(DialogueCanvasState.Showing);

        yield return new WaitForSeconds(timeShowing);

        HideDialogueRegular();
    }

    private void HideDialogueRegular()
    {
        SetCanvasGroupAlpha(0f);
        SetDialogueCanvasState(DialogueCanvasState.Idle);

        currentDialogue = null;
        ClearDialogueText();
    }

    #region FadeCoroutines
    private IEnumerator FadeIn(float fadeInTime)
    {
        canvasGroup.alpha = 0f;
        float time = 0f;

        while (time <= fadeInTime)
        {
            time += Time.deltaTime;
            canvasGroup.alpha = time / fadeInTime;

            yield return null;
        }

        canvasGroup.alpha = 1f;
    }

    private IEnumerator FadeOut(float fadeOutTime)
    {
        float startingAlpha = canvasGroup.alpha;
        float timeRemaining = fadeOutTime * canvasGroup.alpha;

        float time = 0f;

        while (time <= timeRemaining)
        {
            time += Time.deltaTime;
            canvasGroup.alpha = startingAlpha * (1 - time / timeRemaining);

            yield return null;
        }

        canvasGroup.alpha = 0f;
    }
    #endregion

    #region CanvasGroupAlpha
    private void SetCanvasGroupAlpha(float alpha)
    {
        canvasGroup.alpha = alpha;
    }
    #endregion

    #region DialogueText

    private void SetDialogueText(string dialogue)
    {
        dialogueText.text = dialogue;
    }

    private void ClearDialogueText()
    {
        dialogueText.text = "";
    }
    #endregion
}

