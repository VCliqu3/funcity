using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class NoteCanvasController : CanvasController
{
    [SerializeField] private Image noteImage;
    [SerializeField] private GameObject closePanel;

    [SerializeField] private bool disableNoteImageOnPause;
    [SerializeField] private bool disableClosePanelOnPause;

    private InteractableNote currentNote;

    public static Action<InteractableNote> OnNoteOpened;
    public static Action<InteractableNote> OnNoteClosed;

    protected override void OnEnable()
    {
        base.OnEnable();
        InteractableNote.OnNoteInteracted += OpenNote;

        PauseCanvasController.OnPauseMenuOpened += DoWhenPauseMenuOpened;
        PauseCanvasController.OnPauseMenuClosed += DoWhenPauseMenuClosed;

        GameManager.OnGameJumpScare += CloseNote;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        InteractableNote.OnNoteInteracted -= OpenNote;

        PauseCanvasController.OnPauseMenuOpened -= DoWhenPauseMenuOpened;
        PauseCanvasController.OnPauseMenuClosed -= DoWhenPauseMenuClosed;

        GameManager.OnGameJumpScare -= CloseNote;
    }

    protected override void Start()
    {
        base.Start();

        SetCanvasGroup(false);
        noteImage.preserveAspect = true;
    }

    protected override void Update()
    {
        base.Update();

        CheckCloseNote();
    }

    private void CheckCloseNote()
    {
        if (!CheckIfIsOnTopAmongActives()) return;

        if (_UIInputHandler.GetCloseNoteInputDown())
        {
            CloseNote();
        }
    }

    private void OpenNote(InteractableNote interactableNote, Sprite noteSprite, int noteNumber)
    {
        if (GameManager.instance.gameState != GameManager.GameState.Playing) return;

        noteImage.sprite = noteSprite;
        SetCanvasGroup(true);

        currentNote = interactableNote;
        OnNoteOpened?.Invoke(currentNote);
    }

    public void CloseNote()
    {
        if (!currentNote) return;

        noteImage.sprite = null;
        SetCanvasGroup(false);

        OnNoteClosed?.Invoke(currentNote);
        currentNote = null;
    }

    private void DoWhenPauseMenuOpened()
    {
        if(disableNoteImageOnPause) noteImage.enabled = false;
        if(disableClosePanelOnPause) closePanel.SetActive(false);
    }
    private void DoWhenPauseMenuClosed()
    {
        if (disableNoteImageOnPause) noteImage.enabled = true;
        if (disableClosePanelOnPause) closePanel.SetActive(true);
    }
}
