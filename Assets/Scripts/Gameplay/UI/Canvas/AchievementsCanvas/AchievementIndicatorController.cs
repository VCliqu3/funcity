using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AchievementIndicatorController : MonoBehaviour
{
    [SerializeField] private float movementSpeed;
    [SerializeField] private float showingPos;
    [SerializeField] private float hidePos;

    [SerializeField] private float timeShowing;
    [SerializeField] private float timeShowingOut;

    [SerializeField] private Image achievementImage;
    [SerializeField] private TMP_Text achievementTitle;

    [SerializeField] private MovementType movementType;
    private enum MovementType {Lerp, MoveTowards}

    private RectTransform rectTransform;
    private AudioSource audioSource;
    private Vector2 targetPos;

    private void OnEnable()
    {
        InGameAchievement.OnAchievementAchieved += DoWhenAchievementAchieved;
    }

    private void OnDisable()
    {
        InGameAchievement.OnAchievementAchieved -= DoWhenAchievementAchieved;
    }

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        audioSource = GetComponentInChildren<AudioSource>();
    }

    private void Start()
    {
        SetPosition(hidePos);
    }

    private void Update()
    {
        CheckPosition();
    }

    private void SetPosition(float position)
    {
        targetPos = new Vector2(rectTransform.anchoredPosition.x, position);
        rectTransform.anchoredPosition = targetPos;
    }

    private void CheckPosition()
    {
        if (rectTransform.anchoredPosition == targetPos) return;

        switch (movementType)
        {
            case MovementType.Lerp:
            default:
                rectTransform.anchoredPosition = Vector2.Lerp(rectTransform.anchoredPosition, targetPos, Time.unscaledDeltaTime * movementSpeed);
                break;
            case MovementType.MoveTowards:
                rectTransform.anchoredPosition = Vector2.MoveTowards(rectTransform.anchoredPosition, targetPos, Time.unscaledDeltaTime * movementSpeed);
                break;
        }
    }
       
    private void DoWhenAchievementAchieved(int achievementNumber, string achievementName, Sprite achievementSprite, AudioClip achievementClip)
    {
        StopAllCoroutines();
        StartCoroutine(ShowAchievement(achievementNumber,achievementName,achievementSprite,achievementClip));
    }

    private IEnumerator ShowAchievement(int achievementNumber, string achievementName, Sprite achievementSprite, AudioClip achievementClip)
    {
        SetPosition(hidePos);
        achievementImage.sprite = achievementSprite;
        achievementTitle.text = achievementName;
        if (achievementClip) audioSource.PlayOneShot(achievementClip);

        targetPos = new Vector2(rectTransform.anchoredPosition.x, showingPos);

        yield return new WaitForSecondsRealtime(timeShowing);

        targetPos = new Vector2(rectTransform.anchoredPosition.x, hidePos);

        yield return new WaitForSecondsRealtime(timeShowingOut);

        achievementImage.sprite = null;
        achievementTitle.text = "";
        SetPosition(hidePos);
    }
}
