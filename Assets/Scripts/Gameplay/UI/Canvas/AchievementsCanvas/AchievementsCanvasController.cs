using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementsCanvasController : CanvasController
{
    protected override void Start()
    {
        base.Start();
        SetCanvasGroup(true);
    }

    protected override void CheckIfShouldBeActive(GameManager.GameState previousState, GameManager.GameState newState)
    {
        switch (newState)
        {
            default:
                SetCanvasGroup(true);
                break;
        }
    }
}
