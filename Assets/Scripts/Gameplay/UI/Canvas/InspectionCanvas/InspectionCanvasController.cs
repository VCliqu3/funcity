using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class InspectionCanvasController : CanvasController
{
    [SerializeField] private Image objectImage;
    [SerializeField] private GameObject closePanel;

    [SerializeField] private bool disableInspectionImageOnPause;
    [SerializeField] private bool disableClosePanelOnPause;

    private PickableObject currentPickableObject;

    public static Action<PickableObject> OnInspectionOpened;
    public static Action<PickableObject> OnInspectionClosed;

    protected override void OnEnable()
    {
        base.OnEnable();
        PickableObject.OnObjectInspected += OpenInspection;

        PauseCanvasController.OnPauseMenuOpened += DoWhenPauseMenuOpened;
        PauseCanvasController.OnPauseMenuClosed += DoWhenPauseMenuClosed;

        GameManager.OnGameJumpScare += CloseInspection;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        PickableObject.OnObjectInspected -= OpenInspection;

        PauseCanvasController.OnPauseMenuOpened -= DoWhenPauseMenuOpened;
        PauseCanvasController.OnPauseMenuClosed -= DoWhenPauseMenuClosed;

        GameManager.OnGameJumpScare -= CloseInspection;
    }

    protected override void Start()
    {
        base.Start();

        SetCanvasGroup(false);
        objectImage.preserveAspect = true;
    }

    protected override void Update()
    {
        base.Update();

        CheckCloseInspection();
    }

    private void CheckCloseInspection()
    {
        if (!CheckIfIsOnTopAmongActives()) return;

        if (_UIInputHandler.GetCloseInspectionInputDown())
        {
            CloseInspection();
        }
    }

    private void OpenInspection(PickableObject pickableObject, Sprite objectSprite)
    {
        if (GameManager.instance.gameState != GameManager.GameState.Playing) return;

        objectImage.sprite = objectSprite;
        SetCanvasGroup(true);

        currentPickableObject = pickableObject;
        OnInspectionOpened?.Invoke(currentPickableObject);
    }

    public void CloseInspection()
    {
        if (!currentPickableObject) return;

        objectImage.sprite = null;
        SetCanvasGroup(false);

        OnInspectionClosed?.Invoke(currentPickableObject);
        currentPickableObject = null;
    }

    private void DoWhenPauseMenuOpened()
    {
        if (disableInspectionImageOnPause) objectImage.enabled = false;
        if (disableClosePanelOnPause) closePanel.SetActive(false);
    }
    private void DoWhenPauseMenuClosed()
    {
        if (disableInspectionImageOnPause) objectImage.enabled = true;
        if (disableClosePanelOnPause) closePanel.SetActive(true);
    }
}
