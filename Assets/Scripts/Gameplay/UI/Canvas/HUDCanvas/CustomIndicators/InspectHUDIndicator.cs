using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InspectHUDIndicator : HUDIndicator
{
    private void OnEnable()
    {
        PickableObject.OnObjectPickedUpInspectIndicator += EnableInspectPanel;
        PickableObject.OnObjectDropped += DisableInspectPanel;
        UsableObject.OnObjectUsed += DisableInspectPanelWhenObjectUsed;
    }

    private void OnDisable()
    {
        PickableObject.OnObjectPickedUpInspectIndicator -= EnableInspectPanel;
        PickableObject.OnObjectDropped -= DisableInspectPanel;
        UsableObject.OnObjectUsed -= DisableInspectPanelWhenObjectUsed;
    }

    private void EnableInspectPanel(PickableObject pickableObject, string text)
    {
        ShowIndication(text);
    }

    private void DisableInspectPanel(PickableObject pickableObject)
    {
        HideIndication();
    }
    private void DisableInspectPanelWhenObjectUsed(UsableObject usableObject, UsablePlace usablePlace)
    {
        HideIndication();
    }
}
