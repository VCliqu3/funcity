using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseHUDIndicator : HUDIndicator
{
    private void OnEnable()
    {
        UsablePlace.OnUsablePlaceOnSight += EnableUsePanel;
        UsablePlace.OnUsablePlaceLeaveSight += DisableUsePanel;
        PickableObject.OnObjectDropped += DisableUsePanelWhenObjectDropped;
    }

    private void OnDisable()
    {
        UsablePlace.OnUsablePlaceOnSight -= EnableUsePanel;
        UsablePlace.OnUsablePlaceLeaveSight -= DisableUsePanel;
        PickableObject.OnObjectDropped -= DisableUsePanelWhenObjectDropped;
    }

    private void EnableUsePanel(UsablePlace usablePlace, string text)
    {
        ShowIndication(text);
    }

    private void DisableUsePanel(UsablePlace usablePlace)
    {
        HideIndication();
    }
    private void DisableUsePanelWhenObjectDropped(PickableObject pickableObject)
    {
        HideIndication();
    }
}
