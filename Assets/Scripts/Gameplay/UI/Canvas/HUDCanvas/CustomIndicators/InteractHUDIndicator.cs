using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractHUDIndicator : HUDIndicator
{
    private void OnEnable()
    {
        InteractableObject.OnInteractableObjectOnSight += EnableInteractPanel;
        InteractableObject.OnInteractableObjectLeaveSight += DisableInteractPanel;
    }

    private void OnDisable()
    {
        InteractableObject.OnInteractableObjectOnSight -= EnableInteractPanel;
        InteractableObject.OnInteractableObjectLeaveSight -= DisableInteractPanel;
    }

    private void EnableInteractPanel(InteractableObject interactableObject, string text)
    {
        ShowIndication(text);
    }

    private void DisableInteractPanel(InteractableObject interactableObject)
    {
        HideIndication();
    }
}
