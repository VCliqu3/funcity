using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpHUDIndicator : HUDIndicator
{
    private PlayerPickUp playerPickUp;

    private void OnEnable()
    {
        PickableObject.OnPickableObjectOnSight += EnablePickUpPanel;
        PickableObject.OnPickableObjectLeaveSight += DisablePickUpPanel;
    }

    private void OnDisable()
    {
        PickableObject.OnPickableObjectOnSight -= EnablePickUpPanel;
        PickableObject.OnPickableObjectLeaveSight -= DisablePickUpPanel;
    }

    protected override void Awake()
    {
        base.Awake();
        playerPickUp = FindObjectOfType<PlayerPickUp>();
    }

    private void EnablePickUpPanel(PickableObject pickableObject, string text)
    {
        if (playerPickUp.currentPickedUpObject) return;
        ShowIndication(text);
    }

    private void DisablePickUpPanel(PickableObject pickableObject)
    {
        HideIndication();
    }
}
