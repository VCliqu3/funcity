using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropHUDIndicator : HUDIndicator
{
    private void OnEnable()
    {
        PickableObject.OnObjectPickedUpDropIndicator += EnableDropPanel;
        PickableObject.OnObjectDropped += DisableDropPanel;
        UsableObject.OnObjectUsed += DisableDropPanelWhenObjectUsed;
    }

    private void OnDisable()
    {
        PickableObject.OnObjectPickedUpDropIndicator -= EnableDropPanel;
        PickableObject.OnObjectDropped -= DisableDropPanel;
        UsableObject.OnObjectUsed -= DisableDropPanelWhenObjectUsed;
    }

    private void EnableDropPanel(PickableObject pickableObject, string text)
    {
        ShowIndication(text);
    }

    private void DisableDropPanel(PickableObject pickableObject)
    {
        HideIndication();
    }
    private void DisableDropPanelWhenObjectUsed(UsableObject usableObject, UsablePlace usablePlace)
    {
        HideIndication();
    }
}
