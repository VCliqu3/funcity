using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HUDCanvasController : CanvasController
{
    protected override void Start()
    {
        base.Start();
        SetCanvasGroup(true);
    }

    protected override void CheckIfShouldBeActive(GameManager.GameState previousState, GameManager.GameState newState)
    {
        switch (newState)
        {
            case GameManager.GameState.Playing:
                SetCanvasGroup(true);
                break;
            default:
                SetCanvasGroup(false);
                break;

        }
    }
}
