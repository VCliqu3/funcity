using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class HUDIndicator : MonoBehaviour
{
    [SerializeField] private TMP_Text indicatorText;

    [SerializeField] private float timeBetweenIndications;

    [SerializeField] private bool fadeEnabled;
    [SerializeField] private float fadeInTime;
    [SerializeField] private float fadeOutTime;

    public enum HUDIndicatorState {Idle, FadingIn, Showing, FadingOut}
    public HUDIndicatorState _HUDIndicatorState;

    private string currentIndication;
    private CanvasGroup canvasGroup;

    protected virtual void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }
    private void Start()
    {
        InitializeCanvasGroup();
        SetHUDIndicatorState(HUDIndicatorState.Idle);
    }

    private void InitializeCanvasGroup()
    {
        canvasGroup.alpha = 0f;
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
    }

    public void SetHUDIndicatorState(HUDIndicatorState _HUDIndicatorState)
    {
        this._HUDIndicatorState = _HUDIndicatorState;
    }

    protected void ShowIndication(string indication)
    {
        if (fadeEnabled)
        {
            StopAllCoroutines();
            StartCoroutine(ShowIndicatorFade(indication));
        }
        else
        {
            StopAllCoroutines();
            StartCoroutine(ShowIndicatorRegular(indication));
        }
    }

    protected void HideIndication()
    {
        if (fadeEnabled)
        {
            StopAllCoroutines();
            StartCoroutine(HideIndicatorFade());
        }
        else
        {
            StopAllCoroutines();
            HideIndicatorRegular();
        }
    }

    private IEnumerator ShowIndicatorFade(string indication)
    {
        if (_HUDIndicatorState != HUDIndicatorState.Idle)
        {
            yield return StartCoroutine(HideIndicatorFade());
            yield return new WaitForSeconds(timeBetweenIndications);
        }

        currentIndication = indication;
        SetIndicatorText(currentIndication);

        SetHUDIndicatorState(HUDIndicatorState.FadingIn);
        yield return StartCoroutine(FadeIn(fadeInTime));
        SetHUDIndicatorState(HUDIndicatorState.Showing);
    }

    private IEnumerator HideIndicatorFade()
    {
        SetHUDIndicatorState(HUDIndicatorState.FadingOut);
        yield return StartCoroutine(FadeOut(fadeOutTime));

        SetHUDIndicatorState(HUDIndicatorState.Idle);

        currentIndication = null;
        ClearIndicatorText();
    }

    private IEnumerator ShowIndicatorRegular(string indication)
    {
        if (_HUDIndicatorState != HUDIndicatorState.Idle)
        {
            HideIndicatorRegular();
            yield return new WaitForSeconds(timeBetweenIndications);
        }

        currentIndication = indication;
        SetIndicatorText(currentIndication);

        SetCanvasGroupAlpha(1f);
        SetHUDIndicatorState(HUDIndicatorState.Showing);
    }

    private void HideIndicatorRegular()
    {
        SetCanvasGroupAlpha(0f);
        SetHUDIndicatorState(HUDIndicatorState.Idle);

        currentIndication = null;
        ClearIndicatorText();
    }

    #region FadeCoroutines
    private IEnumerator FadeIn(float fadeInTime)
    {
        canvasGroup.alpha = 0f;
        float time = 0f;

        while (time <= fadeInTime)
        {
            time += Time.deltaTime;
            canvasGroup.alpha = time / fadeInTime;

            yield return null;
        }

        canvasGroup.alpha = 1f;
    }

    private IEnumerator FadeOut(float fadeOutTime)
    {
        float startingAlpha = canvasGroup.alpha;
        float timeRemaining = fadeOutTime * canvasGroup.alpha;

        float time = 0f;

        while (time <= timeRemaining)
        {
            time += Time.deltaTime;
            canvasGroup.alpha = startingAlpha * (1 - time / timeRemaining);

            yield return null;
        }

        canvasGroup.alpha = 0f;
    }
    #endregion

    #region CanvasGroupAlpha
    private void SetCanvasGroupAlpha(float alpha)
    {
        canvasGroup.alpha = alpha;
    }
    #endregion

    #region IndicatorText

    private void SetIndicatorText(string text)
    {
        indicatorText.text = text;
    }

    private void ClearIndicatorText()
    {
        indicatorText.text = "";
    }
    #endregion
}
