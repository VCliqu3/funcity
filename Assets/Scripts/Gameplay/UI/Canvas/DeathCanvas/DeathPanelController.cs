using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[System.Serializable]
public class DeathPhrasesRelation
{
    public string deathPhrase;
    public AudioClip deathPhraseClip;
    public TMP_FontAsset font;
}
public class DeathPanelController : MonoBehaviour
{
    [SerializeField] private TMP_Text deathText;

    [SerializeField] private float timeToFadeIn;
    [SerializeField] private float fadeInTime;
    [SerializeField] private float timeShowingDeathPhrase;
    [SerializeField] private float fadeOutTime;

    [SerializeField] private List<DeathPhrasesRelation> deathPhrasesRelations = new List<DeathPhrasesRelation>();

    private CanvasGroup canvasGroup;
    private AudioSource audioSource;

    private static int previousPhraseIndex = 0;

    private void OnEnable()
    {
        GameManager.OnGameDeath += ShowDeathPhrase;
    }

    private void OnDisable()
    {
        GameManager.OnGameDeath -= ShowDeathPhrase;
    }

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        audioSource = GetComponentInChildren<AudioSource>();
    }
    private void Start()
    {
        InitializeCanvasGroup();
    }
    private void InitializeCanvasGroup()
    {
        canvasGroup.alpha = 0f;
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
    }

    private void ShowDeathPhrase()
    {
        StartCoroutine(DeathPhraseCoroutine());
    }

    private IEnumerator DeathPhraseCoroutine()
    {
        DeathPhrasesRelation relation = ChooseRandomDeathPhrase();
        deathText.text = relation.deathPhrase;
        deathText.font = relation.font;

        yield return new WaitForSeconds(timeToFadeIn);

        if (relation.deathPhraseClip) audioSource.PlayOneShot(relation.deathPhraseClip);

        yield return StartCoroutine(FadeIn(fadeInTime));

        yield return new WaitForSeconds(timeShowingDeathPhrase);

        yield return StartCoroutine(FadeOut(fadeOutTime));
    }

    private DeathPhrasesRelation ChooseRandomDeathPhrase()
    {
        int chosenIndex = 0;

        while (chosenIndex == previousPhraseIndex)
        {
            chosenIndex = Random.Range(0, deathPhrasesRelations.Count - 1);
        }

        previousPhraseIndex = chosenIndex;
        return deathPhrasesRelations[chosenIndex];
    }

    #region FadeCoroutines
    private IEnumerator FadeIn(float fadeInTime)
    {
        canvasGroup.alpha = 0f;
        float time = 0f;

        while (time <= fadeInTime)
        {
            time += Time.deltaTime;
            canvasGroup.alpha = time / fadeInTime;

            yield return null;
        }

        canvasGroup.alpha = 1f;
    }

    private IEnumerator FadeOut(float fadeOutTime)
    {
        float startingAlpha = canvasGroup.alpha;
        float timeRemaining = fadeOutTime * canvasGroup.alpha;

        float time = 0f;

        while (time <= timeRemaining)
        {
            time += Time.deltaTime;
            canvasGroup.alpha = startingAlpha * (1 - time / timeRemaining);

            yield return null;
        }

        canvasGroup.alpha = 0f;
    }
    #endregion
}
