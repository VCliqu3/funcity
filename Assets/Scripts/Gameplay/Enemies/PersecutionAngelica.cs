using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.AI;

public class PersecutionAngelica : MonoBehaviour
{
    [Header("Persecution Angelica Settings")]
    [SerializeField] private bool ignoreEnvironmentCollision;

    [SerializeField] private Transform playerCamera;
    [SerializeField] private float timeSpawning;
    [SerializeField] private float timeDespawning;

    [SerializeField] private AngelicaState angelicaState;

    private enum AngelicaState {Spawn, Persecution, Despawn}

    [SerializeField] private GameObject target;
    [SerializeField] private NavMeshAgent agent;

    [SerializeField] private AudioClip spawnClip;
    [SerializeField] private AudioClip persecutionClip;
    [SerializeField] private AudioClip despawnClip;

    [Header("Angelica Animation Settings")]
    [SerializeField] private Animator angelicaAnimator;

    private AudioSource audioSource;
    private float time = 0;
    private bool hasStartedPersecution = false;

    public static Action OnAngelicaSpawn;
    public static Action OnAngelicaStartPersecution;
    public static Action<Transform> OnAngelicaChasePlayer;
    public static Action OnAngelicaStartDespawn;
    public static Action OnAngelicaDespawn;

    private void OnEnable()
    {
        CheckIgnoreEnvironmentCollision();

        SetAngelicaState(AngelicaState.Spawn);
        ExitTVSetTrigger.OnPlayerEnter += Despawn;
    }

    private void OnDisable()
    {
        ExitTVSetTrigger.OnPlayerEnter -= Despawn;
    }

    private void Awake()
    {
        audioSource = GetComponentInChildren<AudioSource>();
    }

    private void Start()
    {
        ResetTimer();
    }

    private void Update()
    {
        AngelicaLogic();
    }

    private void SetAngelicaState(AngelicaState state)
    {
        angelicaState = state;
    }

    private void AngelicaLogic()
    {
        switch (angelicaState)
        {
            case AngelicaState.Spawn:
                SpawningLogic();
                break;
            case AngelicaState.Persecution:
                PersecutionLogic();
                break;
            case AngelicaState.Despawn:
                DespawningLogic();
                break;
            default:
                break;
        }
    }

    #region SpawningLogic

    private void SpawningLogic()
    {
        if (time <= timeSpawning)
        {
            if (time <= 0f)
            {
                WhenAngelicaSpawn();
            }

            time += Time.deltaTime;
            LookAtPlayer();
        }
        else
        {
            ResetTimer();
            SetAngelicaState(AngelicaState.Persecution);
        }
    }

    private void WhenAngelicaSpawn()
    {
        Debug.Log("Angelica Spawned");
        OnAngelicaSpawn?.Invoke();

        if (spawnClip) audioSource.PlayOneShot(spawnClip);

        angelicaAnimator.Play("Spawn");
    }

    #endregion

    #region PersecutionLogic
    private void PersecutionLogic()
    {
        if (!hasStartedPersecution)
        {
            WhenAngelicaStartPersecution();
            hasStartedPersecution = true;

            //Animator.SetTrigger("Persecution");
        }

        NavMeshLogic();
    }

    private void WhenAngelicaStartPersecution()
    {
        Debug.Log("AngelicaStartPersecution");
        OnAngelicaStartPersecution?.Invoke();

        audioSource.Stop();
        audioSource.clip = persecutionClip;
        audioSource.Play();

        angelicaAnimator.CrossFade("Persecution", 0.3f);

    }
    private void NavMeshLogic()
    {
        float distanceToTarget = Vector3.Distance(transform.position, target.transform.position);
        Vector3 moveDirection = agent.velocity.normalized;

        agent.enabled = true;
        agent.SetDestination(target.transform.position);

        if (moveDirection != Vector3.zero)
        {
            transform.rotation = Quaternion.LookRotation(moveDirection);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (angelicaState != AngelicaState.Persecution) return;
            WhenAngelicaChasePlayer();
        }
    }

    private void WhenAngelicaChasePlayer()
    {
        OnAngelicaChasePlayer?.Invoke(transform);

        ResetTimer();
        agent.enabled = false;

        audioSource.Stop();
        audioSource.clip = null;

        Debug.Log("PlayerChased");
        gameObject.SetActive(false);
    }
    #endregion

    #region DespawningLogic

    private void DespawningLogic()
    {
        if (time <= timeDespawning)
        {
            if (time <= 0f)
            {
                WhenAngelicaDespawn();
            }

            time += Time.deltaTime;
            LookAtPlayer();
        }
        else
        {
            Debug.Log("Angelica Despawn");
            OnAngelicaDespawn?.Invoke();
            
            ResetTimer();
            gameObject.SetActive(false); //Por ahora, se desactiva inmediatamente al salir del estudio de TV
        }
    }


    private void WhenAngelicaDespawn()
    {
        Debug.Log("Angelica Despawning");

        agent.enabled = false;
        OnAngelicaStartDespawn?.Invoke();

        audioSource.Stop();
        if (despawnClip) audioSource.PlayOneShot(despawnClip);

        //Animator.SetTrigger("Despawn");
    }

    private void Despawn()
    {
        ResetTimer();
        SetAngelicaState(AngelicaState.Despawn);
    }

    #endregion

    private void ResetTimer()
    {
        time = 0f;
    }

    private void LookAtPlayer()
    {
        Vector3 lookPosition = new Vector3(playerCamera.position.x, transform.position.y, playerCamera.position.z);
        transform.LookAt(lookPosition);
    }

    private void CheckIgnoreEnvironmentCollision()
    {
        if (ignoreEnvironmentCollision)
        {
            Physics.IgnoreLayerCollision(14, 0);
            Physics.IgnoreLayerCollision(14, 13);
            Physics.IgnoreLayerCollision(14, 18);
            Physics.IgnoreLayerCollision(14, 7);
            Physics.IgnoreLayerCollision(14, 8);
            Physics.IgnoreLayerCollision(14, 9);
            Physics.IgnoreLayerCollision(14, 11);
        }
    }
}
