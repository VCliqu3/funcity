using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class WanderingAngelica : MonoBehaviour
{
    [Header("Wandering Angelica Settings")]
    [SerializeField] private bool ignoreEnvironmentCollision;

    [SerializeField] private Transform playerCamera;
    [SerializeField] private float timeSpawning;
    [SerializeField] private float timeStandingPatrol;
    [SerializeField] private float timeDespawning;

    [SerializeField] private float playerDetectionRange;

    [SerializeField] private Transform[] patrolPoints;

    [SerializeField] private float patrolMovementSpeed;
    [SerializeField] private float persecutionBaseMovementSpeed;
    [SerializeField] private float persecutionAcceleration;
    [SerializeField] private float persecutionMaxSpeed;
    private float persecutionSpeed;
    [SerializeField] private float patrolLookSpeed;
    [SerializeField] private float persecutionLookSpeed;

    [SerializeField] private AngelicaState angelicaState;
    private enum AngelicaState {Spawn, Patrol, Persecution, Despawn}

    [SerializeField] private PatrolState patrolState;
    private enum PatrolState {Moving, Standing}

    [SerializeField] private AudioClip spawnClip;
    [SerializeField] private AudioClip patrolClip;
    [SerializeField] private AudioClip persecutionClip;
    [SerializeField] private AudioClip despawnClip;

    [Header("Angelica Animation Settings")]
    [SerializeField] private Animator angelicaAnimator;

    private AudioSource audioSource;
    private float time = 0;
    private bool hasStartedPersecution = false;
    private bool hasStartedPatrol = false;
    private GameObject player;

    private int patrolPointIndex;

    public static Action OnAngelicaSpawn;
    public static Action OnAngelicaStartPatrol;
    public static Action OnAngelicaStartPersecution;
    public static Action<Transform> OnAngelicaChasePlayer;
    public static Action OnAngelicaStartDespawn;
    public static Action OnAngelicaDespawn;

    private void OnEnable()
    {
        CheckIgnoreEnvironmentCollision();

        SetAngelicaState(AngelicaState.Spawn);
        WanderingAngelicaInstantPersecutionTrigger.OnPlayerEnter += StartPersecution;
        EnterTVSetTrigger.OnPlayerEnter += Despawn;
    }

    private void OnDisable()
    {
        WanderingAngelicaInstantPersecutionTrigger.OnPlayerEnter -= StartPersecution;
        EnterTVSetTrigger.OnPlayerEnter -= Despawn;
    }
    private void Awake()
    {
        audioSource = GetComponentInChildren<AudioSource>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Start()
    {
        ResetTimer();
    }

    private void Update()
    {
        AngelicaLogic();
    }

    private void SetAngelicaState(AngelicaState state)
    {
        angelicaState = state;
    }

    private void SetPatrolState(PatrolState state)
    {
        patrolState = state;
    }

    private void AngelicaLogic()
    {
        switch (angelicaState)
        {    
            case AngelicaState.Spawn:
                SpawningLogic();
                break;
            case AngelicaState.Patrol:
                PatrolLogic();
                break;
            case AngelicaState.Persecution:
                PersecutionLogic();
                break;
            case AngelicaState.Despawn:
                DespawningLogic();
                break;
            default:
                break;
            
        }
    }
    #region SpawningLogic

    private void SpawningLogic()
    {
        if (time <= timeSpawning)
        {
            if (time <= 0f)
            {
                WhenAngelicaSpawn();
            }

            time += Time.deltaTime;
        }
        else
        {
            ResetTimer();
            SetAngelicaState(AngelicaState.Patrol);
            SetPatrolState(PatrolState.Moving);
        }
    }

    private void WhenAngelicaSpawn()
    {
        Debug.Log("Angelica Spawned");
        OnAngelicaSpawn?.Invoke();

        if (spawnClip) audioSource.PlayOneShot(spawnClip);

        angelicaAnimator.Play("Spawn");
    }

    #endregion

    #region PatrolLogic

    private void PatrolLogic()
    {
        if (!hasStartedPatrol)
        {
            WhenAngelicaStartPatrol();
            hasStartedPatrol = true;
        }

        Patrol(patrolPoints);
    }

    private void Patrol(Transform[] points)
    {
        if(patrolState == PatrolState.Standing)
        {
            time += Time.deltaTime;

            if (time <= timeStandingPatrol) return;
         
            SetPatrolState(PatrolState.Moving);
            ResetTimer();           
            
            angelicaAnimator.CrossFade("Walk", 0.5f);
        }

        Transform point = points[patrolPointIndex];
        Vector3 targetPos = new Vector3(point.position.x, transform.position.y, point.position.z);

        if(Vector3.Distance(transform.position, targetPos) <= 0.01f)
        {
            transform.position = targetPos;
            ResetTimer();
            SetPatrolState(PatrolState.Standing);

            patrolPointIndex = (patrolPointIndex + 1) % patrolPoints.Length;
            
            angelicaAnimator.CrossFade("Idle", 0.2f);
        }
        else
        {
            MoveTowardsTarget(point, patrolMovementSpeed);
            LookAtTarget(point, patrolLookSpeed);
        }

        CheckPlayerNearby();
    }

    private void CheckPlayerNearby()
    {
        if(Vector3.Distance(player.transform.position, transform.position) <= playerDetectionRange)
        {
            ResetTimer();
            StartPersecution();
        }
    }

    private void StartPersecution()
    {
        ResetTimer();
        SetAngelicaState(AngelicaState.Persecution);
    }

    private void WhenAngelicaStartPatrol()
    {
        SetPatrolState(PatrolState.Moving);

        Debug.Log("AngelicaStartPatrol");
        OnAngelicaStartPatrol?.Invoke();

        audioSource.clip = patrolClip;
        audioSource.Play();
        
        angelicaAnimator.CrossFade("Walk", 0.4f);
    }

    #endregion

    #region PersecutionLogic
    private void PersecutionLogic()
    {
        time += Time.deltaTime;

        persecutionSpeed = persecutionBaseMovementSpeed + persecutionAcceleration * time;
        persecutionSpeed = persecutionSpeed > persecutionMaxSpeed ? persecutionMaxSpeed : persecutionSpeed;

        if (!hasStartedPersecution)
        {
            WhenAngelicaStartPersecution();
            hasStartedPersecution = true;
        }

        PersecutePlayer();
    }

    private void WhenAngelicaStartPersecution()
    {
        Debug.Log("AngelicaStartPersecution");
        OnAngelicaStartPersecution?.Invoke();

        audioSource.Stop();
        audioSource.clip = persecutionClip;
        audioSource.Play();

        angelicaAnimator.CrossFade("Persecution", 0.3f);
    }

    private void PersecutePlayer()
    {
        MoveTowardsTarget(player.transform, persecutionSpeed);
        LookAtTarget(player.transform, persecutionLookSpeed);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (angelicaState != AngelicaState.Persecution) return;
            WhenAngelicaChasePlayer();
        }
    }

    private void WhenAngelicaChasePlayer()
    {
        OnAngelicaChasePlayer?.Invoke(transform);

        ResetTimer();

        audioSource.Stop();
        audioSource.clip = null;

        Debug.Log("PlayerChased");
        gameObject.SetActive(false);
    }
    #endregion

    #region DespawningLogic

    private void DespawningLogic()
    {
        if (time <= timeDespawning)
        {
            if (time <= 0f)
            {
                WhenAngelicaDespawn();
            }

            time += Time.deltaTime;
        }
        else
        {
            Debug.Log("Angelica Despawn");
            OnAngelicaDespawn?.Invoke();

            ResetTimer();
            gameObject.SetActive(false); 
        }
    }


    private void WhenAngelicaDespawn()
    {
        Debug.Log("Angelica Despawning");

        OnAngelicaStartDespawn?.Invoke();

        audioSource.Stop();
        if (despawnClip) audioSource.PlayOneShot(despawnClip);

        //Animator.SetTrigger("Despawn");
    }

    private void Despawn()
    {
        ResetTimer();
        SetAngelicaState(AngelicaState.Despawn);
    }

    #endregion

    private void ResetTimer()
    {
        time = 0f;
    }

    private void LookAtTarget(Transform target, float lookSpeed)
    {
        Vector3 targetPos = new Vector3(target.position.x, transform.position.y, target.position.z);
        Vector3 directionToTarget = targetPos - transform.position;

        if (directionToTarget == Vector3.zero) return;

        Quaternion rotationToTarget = Quaternion.LookRotation(directionToTarget, Vector3.up);
        transform.localRotation = Quaternion.Lerp(transform.localRotation, rotationToTarget, Time.deltaTime * lookSpeed);
    }

    private void MoveTowardsTarget(Transform target, float movementSpeed)
    {
        Vector3 targetPos = new Vector3(target.position.x, transform.position.y, target.position.z);
        transform.position = Vector3.MoveTowards(transform.position, targetPos, Time.deltaTime * movementSpeed);
    }

    private void CheckIgnoreEnvironmentCollision()
    {
        if (ignoreEnvironmentCollision)
        {
            Physics.IgnoreLayerCollision(14, 0);
            Physics.IgnoreLayerCollision(14, 13);
            Physics.IgnoreLayerCollision(14, 18);
            Physics.IgnoreLayerCollision(14, 7);
            Physics.IgnoreLayerCollision(14, 8);
            Physics.IgnoreLayerCollision(14, 9);
            Physics.IgnoreLayerCollision(14, 11);
        }
    }
}
