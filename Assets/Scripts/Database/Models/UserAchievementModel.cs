using System;

[Serializable]
public class UserAchievementModel
{
    public string username;
    public int achievementNumber;
    public int isCompleted;
}

