using System;

[Serializable]
public class AchievementModel
{
    public int achievementID;
    public int achievementNumber;
    public string achievementName;
    public int achievementDescription;
}

