using System;

[Serializable]
public class UserModel
{
    public int userID;
    public int username;
    public int checkpointNumber;
}
