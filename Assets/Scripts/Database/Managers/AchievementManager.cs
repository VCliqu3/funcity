using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class Achievement
{
    public int achievementNumber;
    public string achievementName;
    public string achievementDescription;
    public Sprite achievementSprite;
    public AudioClip achievementClip;
    public bool isCompleted;
}

public class AchievementManager : MonoBehaviour
{
    public static AchievementManager instance;

    public Achievement[] achievementsArray;

    private void Awake()
    {
        SetSingleton();
    }

    private void SetSingleton()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }
}
