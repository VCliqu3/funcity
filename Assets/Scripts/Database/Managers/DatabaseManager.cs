using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DatabaseManager : MonoBehaviour
{
    public static DatabaseManager instance;

    private UploadUserController uploadUserController;
    private UploadAchievementsController uploadAchivementsController;
    private GetUserAchievementsByUsernameController getUserAchievementsByUsernameController;
    private UpdateAchievementController updateAchievementController;
    private UpdateCheckpointNumberController updateCheckpointNumberController;

    public static Action<string> OnMenuDataLoaded;

    private void Awake()
    {
        SetSingleton();

        uploadUserController = FindObjectOfType<UploadUserController>();
        uploadAchivementsController = FindObjectOfType<UploadAchievementsController>();
        getUserAchievementsByUsernameController = FindObjectOfType<GetUserAchievementsByUsernameController>();
        updateAchievementController = FindObjectOfType<UpdateAchievementController>();
        updateCheckpointNumberController = FindObjectOfType<UpdateCheckpointNumberController>();
    }

    private void SetSingleton()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void EnterUser(string username)
    {
        uploadUserController.UploadUser(username, LoadDataToLocal);
    }

    private void DoWhenMenuDataLoaded(string username)
    {
        LoadedDatabaseManager.instance.hasLoadedData = true;

        OnMenuDataLoaded?.Invoke(username);
    }

    public void LoadDataToLocal(string username, UserAchievementsModel userAchievementsModel, CheckpointNumberModel checkpointNumberModel)
    {
        LoadUsernameToLoadedData(username);

        LoadCheckpointNumberToLoadedData(checkpointNumberModel);
        LoadUserAchievementsToLoadedData(userAchievementsModel);

        DoWhenMenuDataLoaded(username);
        OnMenuDataLoaded?.Invoke(username);
    }

    public void LoadUsernameToLoadedData(string username)
    {
        UserManager.instance.username = username;
    }


    public void LoadCheckpointNumberToLoadedData(CheckpointNumberModel checkpointNumberModel)
    {
        UserManager.instance.checkpointNumber = checkpointNumberModel.checkpointNumber;
    }

    public void LoadUserAchievementsToLoadedData(UserAchievementsModel userAchievementsModel)
    {
        foreach (UserAchievementModel userAchievementModel in userAchievementsModel.data)
        {
            LoadUserAchievementToLoadedData(userAchievementModel.achievementNumber, userAchievementModel.isCompleted);
        }
    }

    public void LoadUserAchievementToLoadedData(int achievementNumber, int isCompleted)
    {
        Achievement achievement = Array.Find(AchievementManager.instance.achievementsArray, x => x.achievementNumber == achievementNumber);

        if (achievement == null)
        {
            Debug.Log("Achievement not found -  AchievementNumber: " + achievementNumber);
            return;
        }

        achievement.isCompleted = isCompleted == 1 ? true : false;
    }

    public void UpdateAchievementToDatabase(string username, int achievementNumber, Action<int> OnFinish)
    {
        updateAchievementController.UpdateAchievement(username, achievementNumber, OnFinish);
    }

    public void UpdateCheckpointNumberToDatabase(string username, int checkpointNumber, Action<int> OnFinish)
    {
        updateCheckpointNumberController.UpdateCheckpointNumber(username, checkpointNumber, OnFinish);
    }
}