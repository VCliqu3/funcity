using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadedDatabaseManager : MonoBehaviour
{
    public static LoadedDatabaseManager instance;

    public bool enableDataLoad;
    public bool hasLoadedData = false;

    private void Awake()
    {
        SetSingleton();
    }

    private void SetSingleton()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }

}
