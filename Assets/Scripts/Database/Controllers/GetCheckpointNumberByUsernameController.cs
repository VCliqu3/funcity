using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

public class GetCheckpointNumberByUsernameController : MonoBehaviour
{
    public void GetCheckpointNumberByUsername(string username, UserAchievementsModel userAchievementsModel, Action<string, UserAchievementsModel, CheckpointNumberModel> OnFinish)
    {
        StartCoroutine(GetCheckpointNumberCoroutine(username, userAchievementsModel, OnFinish));
    }

    IEnumerator GetCheckpointNumberCoroutine(string username, UserAchievementsModel userAchievementsModel, Action<string, UserAchievementsModel, CheckpointNumberModel> OnFinish)
    {
        WWWForm form = new WWWForm();
        form.AddField("username", username);

        using (UnityWebRequest www = UnityWebRequest.Post("https://prograjavierprado.samidareno.com/getCheckpointNumberByUsername.php", form))
        {
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ConnectionError
                || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.Log(www.error);
            }
            else
            {
                CheckpointNumberModel checkpointNumberModel = JsonUtility.FromJson<CheckpointNumberModel>(www.downloadHandler.text);
                OnFinish?.Invoke(username, userAchievementsModel, checkpointNumberModel);
            }
        }
    }
}
