using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

public class UploadAchievementsController : MonoBehaviour
{
    private UploadUserAchievementsController uploadUserAchievementsController;

    private void Awake()
    {
        uploadUserAchievementsController = FindObjectOfType<UploadUserAchievementsController>();
    }

    public void UploadAchievements(string username, Action<string, UserAchievementsModel, CheckpointNumberModel> OnFinish)
    {
        int numberOfAchievements = AchievementManager.instance.achievementsArray.Length;

        StartCoroutine(UploadAchievementsCoroutine(username, numberOfAchievements, OnFinish));
    }

    private IEnumerator UploadAchievementsCoroutine(string username, int numberOfAchievements, Action<string, UserAchievementsModel, CheckpointNumberModel> OnFinish)
    {
        for (int i = 0; i < numberOfAchievements; i++)
        {
            string name = AchievementManager.instance.achievementsArray[i].achievementName;
            string description = AchievementManager.instance.achievementsArray[i].achievementDescription;

            WWWForm form = new WWWForm();
            form.AddField("achievementNumber", i);
            form.AddField("achievementName", name);
            form.AddField("achievementDescription", description);

            using (UnityWebRequest www = UnityWebRequest.Post("https://prograjavierprado.samidareno.com/uploadAchievement.php", form))
            {
                yield return www.SendWebRequest();

                if (www.result == UnityWebRequest.Result.ConnectionError
                    || www.result == UnityWebRequest.Result.ProtocolError)
                {
                    Debug.Log(www.error);
                }
                else
                {
                    Debug.Log(www.downloadHandler.text);
                }
            }
        }

        uploadUserAchievementsController.UploadUserAchievements(username, OnFinish);
    }
}
