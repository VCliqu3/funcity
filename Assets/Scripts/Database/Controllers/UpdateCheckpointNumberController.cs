using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

public class UpdateCheckpointNumberController : MonoBehaviour
{
    public void UpdateCheckpointNumber(string username, int checkpointNumber, Action<int> OnFinish)
    {
        StartCoroutine(UpdateCheckpointNumberCoroutine(username, checkpointNumber, OnFinish));
    }

    private IEnumerator UpdateCheckpointNumberCoroutine(string username, int checkpointNumber, Action<int> OnFinish)
    {
        WWWForm form = new WWWForm();
        form.AddField("username", username);
        form.AddField("checkpointNumber", checkpointNumber);

        using (UnityWebRequest www = UnityWebRequest.Post("https://prograjavierprado.samidareno.com/updateCheckpointNumber.php", form))
        {
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ConnectionError
                || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }

        OnFinish?.Invoke(checkpointNumber);
    }
}
