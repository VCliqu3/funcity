using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

public class GetUserAchievementsByUsernameController : MonoBehaviour
{
    private GetCheckpointNumberByUsernameController getCheckpointNumberByUsername;

    private void Awake()
    {
        getCheckpointNumberByUsername = FindObjectOfType<GetCheckpointNumberByUsernameController>();
    }

    public void GetUserAchievementsByUsername(string username, Action<string, UserAchievementsModel, CheckpointNumberModel> OnFinish)
    {
        StartCoroutine(GetUserAchievementsByUsernameCoroutine(username, OnFinish));
    }

    IEnumerator GetUserAchievementsByUsernameCoroutine(string username, Action<string, UserAchievementsModel, CheckpointNumberModel> OnFinish)
    {
        WWWForm form = new WWWForm();
        form.AddField("username", username);

        using (UnityWebRequest www = UnityWebRequest.Post("https://prograjavierprado.samidareno.com/getUserAchievementsByUsername.php", form))
        {
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ConnectionError
                || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.Log(www.error);
            }
            else
            {
                UserAchievementsModel userAchievements = JsonUtility.FromJson<UserAchievementsModel>(www.downloadHandler.text);

                getCheckpointNumberByUsername.GetCheckpointNumberByUsername(username, userAchievements, OnFinish);
            }
        }
    }
}
