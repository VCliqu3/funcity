using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

public class UpdateAchievementController : MonoBehaviour
{
    public void UpdateAchievement(string username, int achievementNumber, Action<int> OnFinish)
    {
        StartCoroutine(UpdateAchievementsCoroutine(username, achievementNumber, OnFinish));
    }

    private IEnumerator UpdateAchievementsCoroutine(string username, int achievementNumber, Action<int> OnFinish)
    {
        WWWForm form = new WWWForm();
        form.AddField("username", username);
        form.AddField("achievementNumber", achievementNumber);

        using (UnityWebRequest www = UnityWebRequest.Post("https://prograjavierprado.samidareno.com/updateUserAchievement.php", form))
        {
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ConnectionError
                || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }

        OnFinish?.Invoke(achievementNumber);
    }
}
