using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

public class UploadUserAchievementsController : MonoBehaviour
{
    private GetUserAchievementsByUsernameController getUserAchievementsByUsernameController;

    private void Awake()
    {
        getUserAchievementsByUsernameController = FindObjectOfType<GetUserAchievementsByUsernameController>();
    }

    public void UploadUserAchievements(string username, Action<string, UserAchievementsModel, CheckpointNumberModel> OnFinish)
    {
        int numberOfAchievements = AchievementManager.instance.achievementsArray.Length;

        StartCoroutine(UploadUserAchievementsCoroutine(username, numberOfAchievements, OnFinish));
    }

    private IEnumerator UploadUserAchievementsCoroutine(string username, int numberOfAchievements, Action<string, UserAchievementsModel, CheckpointNumberModel> OnFinish)
    {
        for (int i = 0; i < numberOfAchievements; i++)
        {
            WWWForm form = new WWWForm();
            form.AddField("username", username);
            form.AddField("achievementNumber", i);

            using (UnityWebRequest www = UnityWebRequest.Post("https://prograjavierprado.samidareno.com/uploadUserAchievement.php", form))
            {
                yield return www.SendWebRequest();

                if (www.result == UnityWebRequest.Result.ConnectionError
                    || www.result == UnityWebRequest.Result.ProtocolError)
                {
                    Debug.Log(www.error);
                }
                else
                {
                    Debug.Log(www.downloadHandler.text);
                }
            }
        }

        getUserAchievementsByUsernameController.GetUserAchievementsByUsername(username, OnFinish);
    }
}
