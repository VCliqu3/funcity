using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Networking;

public class UploadUserController : MonoBehaviour
{
    private UploadAchievementsController uploadAchievementsController;

    public static Action OnDataLoadFailed;

    private void Awake()
    {
        uploadAchievementsController = FindObjectOfType<UploadAchievementsController>();
    }

    public void UploadUser(string username, Action<string, UserAchievementsModel, CheckpointNumberModel> OnFinish)
    {
        StartCoroutine(UploadUserCoroutine(username, OnFinish));
    }

    private IEnumerator UploadUserCoroutine(string username, Action<string, UserAchievementsModel, CheckpointNumberModel> OnFinish)
    {
        WWWForm form = new WWWForm();
        form.AddField("username", username);

        using (UnityWebRequest www = UnityWebRequest.Post("https://prograjavierprado.samidareno.com/uploadUser.php", form))
        {
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ConnectionError
                || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.Log(www.error);
                OnDataLoadFailed?.Invoke();

            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                uploadAchievementsController.UploadAchievements(username, OnFinish);
            }
        }
    }
}
