DROP DATABASE IF EXISTS funcity;

CREATE DATABASE funcity;

USE funcity;

CREATE TABLE users(
    userID INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(100) NOT NULL,
    checkpointNumber INT NOT NULL,
    PRIMARY KEY (userID)
);

CREATE TABLE achievements(
    achievementID INT NOT NULL AUTO_INCREMENT,
    achievementNumber INT NOT NULL,
    achievementName VARCHAR(100) NOT NULL,
    achievementDescription VARCHAR(200) NOT NULL,
    PRIMARY KEY (achievementID)
);

CREATE TABLE users_achievements(
    user_achievementID INT NOT NULL AUTO_INCREMENT,
    achievementID INT NOT NULL,
    userID INT NOT NULL,
    isCompleted BIT NOT NULL,
    PRIMARY KEY (user_achievementID),
    CONSTRAINT fk_users_achievements_achievements
    FOREIGN KEY (achievementID)
    REFERENCES achievements(achievementID),
    CONSTRAINT fk_users_achievements_users
    FOREIGN KEY (userID)
    REFERENCES users(userID)
);

