using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeUserButtonController : MonoBehaviour
{
    [SerializeField] private Button changeUsernameButton;

    private void OnEnable()
    {
        DatabaseManager.OnMenuDataLoaded += EnableButton;
    }
    private void OnDisable()
    {
        DatabaseManager.OnMenuDataLoaded -= EnableButton;
    }

    private void Start()
    {
        changeUsernameButton.gameObject.SetActive(LoadedDatabaseManager.instance.hasLoadedData);
    }

    private void EnableButton(string username)
    {
        changeUsernameButton.gameObject.SetActive(true);
    }
}
