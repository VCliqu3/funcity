using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UsernameTextController : MonoBehaviour
{
    [SerializeField] private TMP_Text usernameText;
    [SerializeField] private Image usernameTextImage;
    private void OnEnable()
    {
        MenuInsertUsername.OnUsernameInserted += ShowUsername;
    }
    private void OnDisable()
    {
        MenuInsertUsername.OnUsernameInserted -= ShowUsername;
    }

    private void Start()
    {
        CheckShowUsername();
    }

    #region ShowUsername

    private void CheckShowUsername()
    {
        if (LoadedDatabaseManager.instance.enableDataLoad)
        {
            if (LoadedDatabaseManager.instance.hasLoadedData)
            {
                ShowUsername(UserManager.instance.username);
            }
            else
            {
                usernameText.text = "";
                usernameTextImage.enabled = false;
            }
        }
        else
        {
            usernameText.text = "Sesi�n sin usuario";
            usernameTextImage.enabled = true;
        }
    }

    private void ShowUsername(string username)
    {
        usernameText.text = username;
        usernameTextImage.enabled = true;
    }

    #endregion
}
