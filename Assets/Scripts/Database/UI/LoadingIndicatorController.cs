using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LoadingIndicatorController : MonoBehaviour
{
    [Header("Blinking Settings")]
    [SerializeField] private bool blinkingEnabled;
    [SerializeField] private float blinkingTime;
    [SerializeField] private float blinkingMinAlpha;
    [SerializeField] private float blinkingMaxAlpha;

    [Header("FadeIn Settings")]
    [SerializeField] private float fadeInTime;
    [SerializeField] private float fadeInMinAlpha;
    [SerializeField] private float fadeInMaxAlpha;

    [Header("FadeOut Settings")]
    [SerializeField] private float fadeOutTime;
    [SerializeField] private float fadeOutMinAlpha;

    [SerializeField] private bool fadeOutMaxAlphaEnabled;
    [SerializeField] private float fadeOutMaxAlpha;

    [SerializeField] private IndicatorState textState = IndicatorState.Idle;
    private enum IndicatorState { Idle, FadingIn, Showing, FadingOut }

    private CanvasGroup canvasGroup;

    private void OnEnable()
    {
        StopAllCoroutines();
        InitializeCanvasGroup();

        MenuInsertUsername.OnTryInsertUsername += OnTryInsertUsername;
        MenuInsertUsername.OnInvalidUsernameInserted += OnInvalidUsernameInserted;
        MenuInsertUsername.OnDataLoadFailed += OnDataLoadFailed;
        MenuInsertUsername.OnPanelFadeOutStart += OnPanelFadeOutStart;
    }
    private void OnDisable()
    {
        MenuInsertUsername.OnTryInsertUsername -= OnTryInsertUsername;
        MenuInsertUsername.OnInvalidUsernameInserted -= OnInvalidUsernameInserted;
        MenuInsertUsername.OnDataLoadFailed -= OnDataLoadFailed;
        MenuInsertUsername.OnPanelFadeOutStart -= OnPanelFadeOutStart;
    }

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    private void Start()
    {
        InitializeCanvasGroup();
    }

    private void InitializeCanvasGroup()
    {
        canvasGroup.alpha = 0;
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;

        SetIndicatorState(IndicatorState.Idle);
    }

    private void SetIndicatorState(IndicatorState state)
    {
        textState = state;
    }

    private void EnableIndicator()
    {
        StopAllCoroutines();
        StartCoroutine(EnableCoroutine());
    }

    private void HideIndicator()
    {
        StopAllCoroutines();
        StartCoroutine(HideCoroutine());
    }

    #region Enable & Hide Coroutines

    private IEnumerator EnableCoroutine()
    {
        SetIndicatorState(IndicatorState.FadingIn);
        yield return StartCoroutine(FadeIn(fadeInTime));

        SetIndicatorState(IndicatorState.Showing);
        StartCoroutine(BlinkingCoroutine());
    }

    private IEnumerator HideCoroutine()
    {
        SetIndicatorState(IndicatorState.FadingOut);
        yield return StartCoroutine(FadeOut(fadeOutTime));

        SetIndicatorState(IndicatorState.Idle);
    }
    #endregion

    #region Blinking
    private IEnumerator BlinkingCoroutine()
    {
        while (true)
        {
            if (!blinkingEnabled) yield break;

            float time = 0f;
            canvasGroup.alpha = blinkingMaxAlpha;

            while (time <= blinkingTime)
            {
                canvasGroup.alpha = blinkingMaxAlpha - time / blinkingTime * (blinkingMaxAlpha - blinkingMinAlpha);

                time += Time.deltaTime;
                yield return null;
            }

            canvasGroup.alpha = blinkingMinAlpha;
            time = 0f;

            while (time <= blinkingTime)
            {
                canvasGroup.alpha = blinkingMinAlpha + time / blinkingTime * (blinkingMaxAlpha - blinkingMinAlpha);

                time += Time.deltaTime;
                yield return null;
            }
        }
    }
    #endregion

    #region FadeCoroutines
    private IEnumerator FadeIn(float fadeInTime)
    {
        canvasGroup.alpha = fadeInMinAlpha;
        float time = 0f;

        while (time <= fadeInTime)
        {
            time += Time.deltaTime;
            canvasGroup.alpha = fadeInMinAlpha + time / fadeInTime * (fadeInMaxAlpha - fadeInMinAlpha);

            yield return null;
        }

        canvasGroup.alpha = fadeInMaxAlpha;
    }

    private IEnumerator FadeOut(float fadeOutTime)
    {
        float startingAlpha = fadeOutMaxAlphaEnabled ? fadeOutMaxAlpha : canvasGroup.alpha;

        float time = 0f;

        while (time <= fadeOutTime)
        {
            time += Time.deltaTime;
            canvasGroup.alpha = startingAlpha - time / fadeOutTime * (startingAlpha - fadeOutMinAlpha);

            yield return null;
        }

        canvasGroup.alpha = fadeOutMinAlpha;
    }
    #endregion

    #region Event Subscriptions
    private void OnTryInsertUsername()
    {
        EnableIndicator();
    }

    private void OnInvalidUsernameInserted()
    {
        HideIndicator();
    }
    private void OnDataLoadFailed()
    {
        HideIndicator();
    }

    private void OnPanelFadeOutStart()
    {
        HideIndicator();
    }
    #endregion
}
