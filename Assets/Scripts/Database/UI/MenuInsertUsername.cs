using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class MenuInsertUsername : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private CanvasGroup insertUsernameCanvasGroup;
    [SerializeField] private TMP_InputField usernameInputField;
    public int minNameCharacters;
    public int maxNameCharacters;

    [Header("Fade Canvas Group Settings")]
    [SerializeField] private float waitFadeTime;
    [SerializeField] private float fadeInTime;
    [SerializeField] private float fadeOutTime;

    [SerializeField] private InsertUsernamePanelState insertUsernamePanelState;
    private enum InsertUsernamePanelState {Disabled, Waiting, FadingIn, FadingOut, Showing, LoadingData}

    private NonGameplayScenesInputHandler nonGameplayScenesInputHandler;

    public static Action<string> OnUsernameInserted;
    public static Action OnMenuInsertUsernamePanelFaded;
    public static Action<int,int> OnUsernameInsertedTooShort;
    public static Action<int, int> OnUsernameInsertedTooLong;

    public static Action OnTryInsertUsername;
    public static Action OnInvalidUsernameInserted;
    public static Action OnDataLoadFailed;
    public static Action OnPanelFadeOutStart;

    private void OnEnable()
    {
        DatabaseManager.OnMenuDataLoaded += DataLoadedLogic;
        UploadUserController.OnDataLoadFailed += DataLoadFailLogic;
    }

    private void OnDisable()
    {
        DatabaseManager.OnMenuDataLoaded -= DataLoadedLogic;
        UploadUserController.OnDataLoadFailed -= DataLoadFailLogic;
    }

    private void Awake()
    {
        nonGameplayScenesInputHandler = FindObjectOfType<NonGameplayScenesInputHandler>();
    }
    private void Start()
    {
        SetState(InsertUsernamePanelState.Disabled);
        CheckStartMenuDatabaseLoad();
    }

    private void Update()
    {
        CheckEnterInput();
    }

    private void CheckStartMenuDatabaseLoad()
    {
        if (LoadedDatabaseManager.instance.hasLoadedData)
        {
            HidePanel();
            return;
        }

        if (LoadedDatabaseManager.instance.enableDataLoad)
        {
            StartingEnablePanel();
        }
        else
        {
            HidePanel();
        }
    }

    private void CheckEnterInput()
    {
        if (!nonGameplayScenesInputHandler.GetSkipInputDown()) return;

        EnterUsername();     
    }

    public void EnterUsername()
    {
        if (insertUsernamePanelState != InsertUsernamePanelState.Showing) return;

        if (usernameInputField.text.Length < minNameCharacters)
        {
            Debug.Log("Usuario inv�lido - Numero de caracteres: " + usernameInputField.text.Length);
            OnUsernameInsertedTooShort?.Invoke(minNameCharacters, usernameInputField.text.Length);

            OnInvalidUsernameInserted?.Invoke();
            return;
        }
        else if(usernameInputField.text.Length > maxNameCharacters)
        {
            Debug.Log("Usuario inv�lido - Numero de caracteres: " + usernameInputField.text.Length);
            OnUsernameInsertedTooLong?.Invoke(maxNameCharacters, usernameInputField.text.Length);

            OnInvalidUsernameInserted?.Invoke();
            return;
        }

        SetState(InsertUsernamePanelState.LoadingData);
        DatabaseManager.instance.EnterUser(usernameInputField.text);

        OnTryInsertUsername?.Invoke();
    }

    public void DataLoadedLogic(string username)
    {
        OnUsernameInserted?.Invoke(username);
        StartCoroutine(FadeOutPanel());
        //HidePanel();
    }

    private void DataLoadFailLogic()
    {
        SetState(InsertUsernamePanelState.Showing);
        OnDataLoadFailed?.Invoke();
    }

    #region SetState
    private void SetState(InsertUsernamePanelState state)
    {
        insertUsernamePanelState = state;
    }
    #endregion

    #region Enable & Hide Panel
    private void StartingEnablePanel()
    {
        if (insertUsernamePanelState != InsertUsernamePanelState.Disabled) return;
        StartCoroutine(StartingFadeInPanel());
    }

    public void EnablePanel()
    {
        if (insertUsernamePanelState != InsertUsernamePanelState.Disabled) return;
        StartCoroutine(FadeInPanel());
    }

    public void HideFadeOutPanel()
    {
        if (insertUsernamePanelState != InsertUsernamePanelState.Showing) return;
        StartCoroutine(FadeOutPanel());
    }

    private void HidePanel()
    {
        insertUsernameCanvasGroup.alpha = 0f;
        insertUsernameCanvasGroup.blocksRaycasts = false;
        insertUsernameCanvasGroup.interactable = false;

        SetState(InsertUsernamePanelState.Disabled);
    }

    private IEnumerator StartingFadeInPanel()
    {
        SetState(InsertUsernamePanelState.Waiting);

        yield return new WaitForSeconds(waitFadeTime);

        StartCoroutine(FadeInPanel());
    }

    private IEnumerator FadeInPanel()
    {
        ClearInputField();

        SetState(InsertUsernamePanelState.FadingIn);

        insertUsernameCanvasGroup.alpha = 0f;
        insertUsernameCanvasGroup.blocksRaycasts = true;
        insertUsernameCanvasGroup.interactable = true;

        float time = 0f;

        while (time <= fadeInTime)
        {
            time += Time.unscaledDeltaTime;
            insertUsernameCanvasGroup.alpha = time / fadeInTime;

            yield return null;
        }

        insertUsernameCanvasGroup.alpha = 1f;

        SetState(InsertUsernamePanelState.Showing);
    }

    private IEnumerator FadeOutPanel()
    {
        OnPanelFadeOutStart?.Invoke();

        SetState(InsertUsernamePanelState.FadingOut);

        insertUsernameCanvasGroup.alpha = 1f;

        float time = 0f;

        while (time <= fadeOutTime)
        {
            time += Time.unscaledDeltaTime;
            insertUsernameCanvasGroup.alpha = 1 - time / fadeOutTime;

            yield return null;
        }

        insertUsernameCanvasGroup.alpha = 0f;
        insertUsernameCanvasGroup.blocksRaycasts = false;
        insertUsernameCanvasGroup.interactable = false;

        SetState(InsertUsernamePanelState.Disabled);

        OnMenuInsertUsernamePanelFaded?.Invoke();
    }
    #endregion

    private void ClearInputField()
    {
        usernameInputField.text = "";
    }
}

