using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExitGameInsertUsernameButtonController : MonoBehaviour
{
    [SerializeField] private Button changeUsernameButton;

    private void OnEnable()
    {
        MenuInsertUsername.OnMenuInsertUsernamePanelFaded += CheckDisableButton;
    }
    private void OnDisable()
    {
        MenuInsertUsername.OnMenuInsertUsernamePanelFaded -= CheckDisableButton;
    }

    private void Start()
    {
        changeUsernameButton.gameObject.SetActive(!LoadedDatabaseManager.instance.hasLoadedData);
    }

    private void CheckDisableButton()
    {
        if (!LoadedDatabaseManager.instance.hasLoadedData) return;
        DisableButton();
    }

    private void DisableButton()
    {
        changeUsernameButton.gameObject.SetActive(false);
    }
}
