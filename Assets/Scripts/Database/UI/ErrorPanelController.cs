using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ErrorPanelController : MonoBehaviour
{
    [SerializeField] private TMP_Text errorPanelTitle;
    [SerializeField] private TMP_Text errorPanelDescription;

    [SerializeField] private float fadeInTime;
    [SerializeField] private float fadeOutTime;
    [SerializeField] private float timeShowing;

    [SerializeField] private ErrorPanelState errorPanelState;
    private enum ErrorPanelState { Disabled, FadingIn, FadingOut, Showing}

    private CanvasGroup canvasGroup;

    private void OnEnable()
    {
        UploadUserController.OnDataLoadFailed += DoWhenDataLoadFailed;
        MenuInsertUsername.OnUsernameInsertedTooShort += DoWhenUsernameInsertedTooShort;
        MenuInsertUsername.OnUsernameInsertedTooLong += DoWhenUsernameInsertedTooLong;
    }
    private void OnDisable()
    {
        UploadUserController.OnDataLoadFailed -= DoWhenDataLoadFailed;
        MenuInsertUsername.OnUsernameInsertedTooShort -= DoWhenUsernameInsertedTooShort;
        MenuInsertUsername.OnUsernameInsertedTooLong -= DoWhenUsernameInsertedTooLong;
    }

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    private void Start()
    {
        SetCanvasGroup();
    }

    private void SetCanvasGroup()
    {
        canvasGroup.alpha = 0f;
        canvasGroup.blocksRaycasts = false;
        canvasGroup.interactable = false;
    }

    #region SetState
    private void SetState(ErrorPanelState state)
    {
        errorPanelState = state;
    }
    #endregion

    private void DoWhenDataLoadFailed()
    {
        if (errorPanelState != ErrorPanelState.Disabled) return;

        errorPanelTitle.text = "Error de conexi�n";
        errorPanelDescription.text = "No se puede conectar con el servidor";

        StartCoroutine(ShowErrorPanel());
    }

    private void DoWhenUsernameInsertedTooShort(int minCharacters, int charactersInserted)
    {
        if (errorPanelState != ErrorPanelState.Disabled) return;

        errorPanelTitle.text = "Usuario inv�lido";
        errorPanelDescription.text = "El nombre de usuario debe tener como m�nimo " + minCharacters + " caracteres";

        StartCoroutine(ShowErrorPanel());
    }

    private void DoWhenUsernameInsertedTooLong(int maxCharacters, int charactersInserted)
    {
        if (errorPanelState != ErrorPanelState.Disabled) return;

        errorPanelTitle.text = "Usuario inv�lido";
        errorPanelDescription.text = "El nombre de usuario debe tener como m�ximo " + maxCharacters + " caracteres";

        StartCoroutine(ShowErrorPanel());
    }

    private void ClearErrorTitle()
    {
        errorPanelTitle.text = "";
    }

    private void ClearErrorDescription()
    {
        errorPanelDescription.text = "";
    }

    private IEnumerator ShowErrorPanel()
    {
        yield return FadeInPanel();
        yield return new WaitForSeconds(timeShowing);
        yield return FadeOutPanel();
    }

    #region FadeCoroutines

    private IEnumerator FadeInPanel()
    {
        SetState(ErrorPanelState.FadingIn);

        canvasGroup.alpha = 0f;
        canvasGroup.blocksRaycasts = true;
        canvasGroup.interactable = true;

        float time = 0f;

        while (time <= fadeInTime)
        {
            time += Time.unscaledDeltaTime;
            canvasGroup.alpha = time / fadeInTime;

            yield return null;
        }

        canvasGroup.alpha = 1f;

        SetState(ErrorPanelState.Showing);
    }

    private IEnumerator FadeOutPanel()
    {
        SetState(ErrorPanelState.FadingOut);

        canvasGroup.alpha = 1f;

        float time = 0f;

        while (time <= fadeOutTime)
        {
            time += Time.unscaledDeltaTime;
            canvasGroup.alpha = 1 - time / fadeOutTime;

            yield return null;
        }

        canvasGroup.alpha = 0f;
        canvasGroup.blocksRaycasts = false;
        canvasGroup.interactable = false;

        SetState(ErrorPanelState.Disabled);

        ClearErrorTitle();
        ClearErrorDescription();
    }

    #endregion
}
