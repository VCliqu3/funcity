using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CloseInsertUsernamePanelButtonController : MonoBehaviour
{
    [SerializeField] private Button changeUsernameButton;

    private void OnEnable()
    {
        MenuInsertUsername.OnMenuInsertUsernamePanelFaded += CheckEnableButton;
    }
    private void OnDisable()
    {
        MenuInsertUsername.OnMenuInsertUsernamePanelFaded -= CheckEnableButton;
    }

    private void Start()
    {
        changeUsernameButton.gameObject.SetActive(LoadedDatabaseManager.instance.hasLoadedData);
    }

    private void CheckEnableButton()
    {
        if (!LoadedDatabaseManager.instance.hasLoadedData) return;
        EnableButton();
    }

    private void EnableButton()
    {
        changeUsernameButton.gameObject.SetActive(true);
    }
}
