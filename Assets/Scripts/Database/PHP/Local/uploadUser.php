<?php
$mysqli = new mysqli("localhost", "root", "", "funcity");
if ($mysqli->connect_error)
{
    echo "Error";
}
else
{
    $username = $_POST["username"];

    $sql = "SELECT userID FROM users WHERE username = ?";
    $query = $mysqli->prepare($sql);
    $query->bind_param("s", $username);
    $query->execute();
    $result = $query->get_result();

    if ($result->num_rows > 0)
    {
        echo "User already on database";
    }
    else
    {
        $checkpointNumber = 0;

        $sql = "INSERT INTO users(userID, username, checkpointNumber) VALUES(NULL,?,?);";
        $query = $mysqli->prepare($sql);
        $query->bind_param("si", $username, $checkpointNumber);

        if ($query->execute())
        {
            echo "User inserted succesfully";
        }
        else
        {
            echo "Error";
        }
    }

    $mysqli->close();
}
