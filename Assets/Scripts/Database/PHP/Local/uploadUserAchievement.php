<?php
$mysqli = new mysqli("localhost", "root", "", "funcity");
if ($mysqli->connect_error)
{
    echo "Error";
}
else
{
    $username = $_POST["username"];
    $achievementNumber = $_POST["achievementNumber"];

    $sql = 
    "SELECT user_achievementID FROM users_achievements 
    INNER JOIN users 
    ON users_achievements.userID = users.userID
    INNER JOIN achievements
    ON users_achievements.achievementID = achievements.achievementID
    WHERE users.username = ?
    AND achievements.achievementNumber =?";
    
    $query = $mysqli->prepare($sql);
    $query->bind_param("si", $username, $achievementNumber);
    $query->execute();
    $result = $query->get_result();

    if ($result->num_rows > 0)
    {
        echo "User Achievement already on database";
    }
    else
    {
        $sql = "SELECT userID FROM users WHERE username = ?";
        $query = $mysqli->prepare($sql);
        $query->bind_param("s", $username);
        $query->execute();
        $result = $query->get_result();

        if ($result->num_rows > 0)
        {
            echo "User found successfully";
            $userID = $result->fetch_assoc()["userID"];

            $sql = "SELECT achievementID FROM achievements WHERE achievementNumber = ?";
            $query = $mysqli->prepare($sql);
            $query->bind_param("i", $achievementNumber);
            $query->execute();
            $result = $query->get_result();
    
            if ($result->num_rows > 0)
            {
                echo "Achievement found successfully";
                $achievementID = $result->fetch_assoc()["achievementID"];
                $isCompleted = 0;

                $sql = "INSERT INTO users_achievements(user_achievementID, achievementID, userID, isCompleted) VALUES(NULL,?, ?, ?);";
                $query = $mysqli->prepare($sql);
                $query->bind_param("iii", $achievementID,$userID, $isCompleted);
        
                if ($query->execute())
                {
                    echo "User Achievement inserted succesfully";
                }
                else
                {
                    echo "Error";
                }
            }
            else
            {
                echo "Achievement not found - Achievement Number: $achievementNumber";
            }
        }
        else
        {
            echo "User not found - Username: $username";
        }       
    }
    
    $mysqli->close();
}
