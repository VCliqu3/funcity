<?php
header("Content-Type: application/json");
$mysqli = new mysqli("localhost", "root", "", "funcity");
if ($mysqli->connect_error)
{
    echo "Error";
}
else
{
    $username = $_POST["username"];

    $sql = 
    "SELECT users.username, achievements.achievementNumber, isCompleted  FROM users_achievements
    INNER JOIN users 
    ON users_achievements.userID = users.userID
    INNER JOIN achievements 
    ON users_achievements.achievementID = achievements.achievementID
    AND users.username = ?";

    $query = $mysqli->prepare($sql);
    $query->bind_param("s", $username);
    $query->execute();
    $result = $query->get_result();
    $data = array();

    if ($result->num_rows > 0)
    {
        $data = $result->fetch_all(MYSQLI_ASSOC);
        echo json_encode(["data" => $data]);
    }
    else
    {
        echo json_encode(["data" => null]);
    }

    $mysqli->close();
}
