<?php
header("Content-Type: application/json");
$mysqli = new mysqli("localhost", "root", "", "funcity");
if ($mysqli->connect_error)
{
    echo "Error";
}
else
{
    $username = $_POST["username"];

    $sql = 
    "SELECT checkpointNumber  FROM users WHERE users.username = ?";

    $query = $mysqli->prepare($sql);
    $query->bind_param("s", $username);
    $query->execute();
    $result = $query->get_result();

    if ($result->num_rows > 0)
    {
        $checkpointNumber = $result->fetch_assoc()["checkpointNumber"];
        echo json_encode(["checkpointNumber" => $checkpointNumber]);
    }
    else
    {
        echo json_encode(["data" => null]);
    }

    $mysqli->close();
}
