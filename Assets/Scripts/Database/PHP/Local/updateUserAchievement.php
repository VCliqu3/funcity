<?php
$mysqli = new mysqli("localhost", "root", "", "funcity");
if ($mysqli->connect_error)
{
    echo "Error";
}
else
{
    $username = $_POST["username"];
    $achievementNumber = $_POST["achievementNumber"];

    $sql = 
    "SELECT user_achievementID FROM users_achievements 
    INNER JOIN users 
    ON users_achievements.userID = users.userID
    INNER JOIN achievements
    ON users_achievements.achievementID = achievements.achievementID
    WHERE users.username = ?
    AND achievements.achievementNumber =?";

    $query = $mysqli->prepare($sql);
    $query->bind_param("si", $username, $achievementNumber);
    $query->execute();
    $result = $query->get_result();

    if ($result->num_rows > 0)
    {
        $user_achievementID = $result->fetch_assoc()["user_achievementID"];
        $isCompleted = 1;

        $sql = "UPDATE users_achievements SET isCompleted = ? WHERE user_achievementID = ?";
        $query = $mysqli->prepare($sql);
        $query->bind_param("ii", $isCompleted, $user_achievementID);
        $query->execute();

        if ($query->execute())
        {
            echo "User Achievement updated successfully - User Achievement ID: $user_achievementID";
        }
        else
        {
            echo "Error";
        }
             
    }
    else
    {

        echo "User Achievement not found - Username: $username, AchievementNumber: $achievementNumber";
    }

    $mysqli->close();
}
