<?php
$mysqli = new mysqli("localhost", "javierprado", "programiercoles23.2", "javierprado_funcity");
if ($mysqli->connect_error)
{
    echo "Error";
}
else
{
    $achievementNumber = $_POST["achievementNumber"];
    $achievementName = $_POST["achievementName"];
    $achievementDescription = $_POST["achievementDescription"];
   
    $sql = 
    "SELECT achievementID FROM achievements WHERE achievementNumber = ?";

    $query = $mysqli->prepare($sql);
    $query->bind_param("i", $achievementNumber);
    $query->execute();
    $result = $query->get_result();

    if ($result->num_rows > 0)
    {
        $achievementID = $result->fetch_assoc()["achievementID"];

        $sql = "UPDATE achievements 
        SET achievementName = ?, achievementDescription = ? 
        WHERE achievementID = ?";

        $query = $mysqli->prepare($sql);
        $query->bind_param("ssi", $achievementName, $achievementDescription, $achievementID);

        if ($query->execute())
        {
            echo "Achievement updated successfully";
        }
        else
        {
            echo "Error";
        }
    }
    else
    {
        $sql = "INSERT INTO achievements(achievementID, achievementNumber, achievementName, achievementDescription) VALUES(NULL,?,?,?);";
        $query = $mysqli->prepare($sql);
        $query->bind_param("iss", $achievementNumber, $achievementName, $achievementDescription);

        if ($query->execute())
        {
            echo "Achievement inserted successfully";
        }
        else
        {
            echo "Error";
        }
    }

    $mysqli->close();
}
