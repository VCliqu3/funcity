<?php
$mysqli = new mysqli("localhost", "javierprado", "programiercoles23.2", "javierprado_funcity");
if ($mysqli->connect_error)
{
    echo "Error";
}
else
{
    $username = $_POST["username"];
    $checkpointNumber = $_POST["checkpointNumber"];

    $sql = 
    "SELECT userID FROM users WHERE username = ?";

    $query = $mysqli->prepare($sql);
    $query->bind_param("s", $username);
    $query->execute();
    $result = $query->get_result();

    if ($result->num_rows > 0)
    {
        $userID = $result->fetch_assoc()["userID"];

        $sql = "UPDATE users SET checkpointNumber = ? WHERE userID = ?";
        $query = $mysqli->prepare($sql);
        $query->bind_param("ii", $checkpointNumber, $userID);
        $query->execute();

        if ($query->execute())
        {
            echo "Checkpoint number updated successfully - Checkpoint Number: $checkpointNumber";
        }
        else
        {
            echo "Error";
        }
             
    }
    else
    {

        echo "Checkpoint not found - Username: $username, Checkpoint Number: $checkpointNumber";
    }

    $mysqli->close();
}
