using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestInteract : InteractableObject
{
    public override void OnPlayerEnterInteractionSightRange()
    {
        //Debug.Log("Interaction Sight Range of" + gameObject.name +"entered");
    }
    public override void OnPlayerLeaveInteractionSightRange()
    {
        //Debug.Log("Interaction Sight Range of" + gameObject.name + "left");
    }
    public override void Interact(PlayerInteract playerInteract)
    {
        base.Interact(playerInteract);
        Debug.Log("Im interacting with " + gameObject.name);
    }
}
