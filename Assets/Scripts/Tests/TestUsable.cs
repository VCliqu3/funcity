using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestUsable : UsableObject
{
    public override void OnPlayerEnterPickUpDetectionRange()
    {
        Debug.Log("Now Im detecting " + gameObject.name);
    }

    public override void OnPlayerLeavePickUpDetectionRange()
    {
        Debug.Log("Now Im not detecting " + gameObject.name);
    }


    public override void OnPlayerEnterPickUpSightRange()
    {
        Debug.Log("PickUp Sight Range of" + gameObject.name +"entered");
    }

    public override void OnPlayerLeavePickUpSightRange()
    {
        Debug.Log("PickUp Sight Range of" + gameObject.name + "left");
    }

    public override void Drop(PlayerPickUp playerPickUp)
    {
        base.Drop(playerPickUp);
        Debug.Log(gameObject.name + " dropped!");
    }
    public override void UseWithoutUsablePlace(PlayerUse playerUse)
    {
        base.UseWithoutUsablePlace(playerUse);
        Debug.Log("Im using " + gameObject.name + " without usable place");
    }
    public override void UseWrong(PlayerUse playerUse, UsablePlace usablePlace)
    {
        base.UseWrong(playerUse, usablePlace);
    }
}
