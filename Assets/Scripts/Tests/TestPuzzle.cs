using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPuzzle : Puzzle
{
    [SerializeField] private UsableObject usableObject1;
    [SerializeField] private UsablePlace usablePlace1;

    protected override void OnEnable()
    {
        base.OnEnable();
        UsableObject.OnObjectUsed += CheckObjectUsedInPlace;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        UsableObject.OnObjectUsed -= CheckObjectUsedInPlace;
    }

    protected override void CheckObjectUsedInPlace(UsableObject usableObject, UsablePlace usablePlace)
    {
        if(usableObject == usableObject1 && usablePlace == usablePlace1)
        {
            OnPuzzleCompleted?.Invoke(this);
            Debug.Log("PuzzleCompleted!");
        }
    }
}
