using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestUsablePlace : UsablePlace
{
    public override void OnPlayerEnterUsablePlaceSightRange() 
    {
        //Debug.Log("UsablePlace Sight Range of" + gameObject.name + "entered");
    }
    public override void OnPlayerLeaveUsablePlaceSightRange() 
    {
        //Debug.Log("UsablePlace Sight Range of" + gameObject.name + "left");
    }
    public override void OnPlaceUsed(PlayerUse playerUse, UsableObject PickableObject)
    {
        Debug.Log(PickableObject.name + " has been used in " + gameObject.name);
    }
    public override void OnPlaceUsedWrong(PlayerUse playerUse, UsableObject usableObject)
    {
        base.OnPlaceUsedWrong(playerUse, usableObject);
    }
}
