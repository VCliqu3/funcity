using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShowroomSwitcher : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (!Input.GetKeyDown(KeyCode.P)) return;

        if(SceneManager.GetActiveScene().name == "Gameplay")
        {
            ScenesManager.instance.StartFadeToTargetScene("Showroom",0.5f);
        }

        if (SceneManager.GetActiveScene().name == "Showroom")
        {
            ScenesManager.instance.StartFadeToTargetScene("Gameplay", 0.5f);
        }
    }
}
