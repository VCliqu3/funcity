using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPlaceablePlace : PlaceablePlace
{
    public override void OnPlayerEnterUsablePlaceSightRange() 
    {
        //Debug.Log("UsablePlace Sight Range of" + gameObject.name + "entered");
    }
    public override void OnPlayerLeaveUsablePlaceSightRange() 
    {
        //Debug.Log("UsablePlace Sight Range of" + gameObject.name + "left");
    }

    public override void OnPlaceUsedWrong(PlayerUse playerUse, UsableObject usableObject)
    {
        base.OnPlaceUsedWrong(playerUse, usableObject);
    }
}
