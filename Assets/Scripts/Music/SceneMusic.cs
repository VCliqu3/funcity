using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneMusic : MonoBehaviour
{
    public AudioClip sceneMusic;

    // Start is called before the first frame update
    void Start()
    {
        AudioManager.instance.PlayMusic(sceneMusic);
    }
}
