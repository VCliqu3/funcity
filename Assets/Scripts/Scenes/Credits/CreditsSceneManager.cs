using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CreditsSceneManager : MonoBehaviour
{
    [Header("Scene Settings")]
    [SerializeField] private float fadeInTime;
    [SerializeField] private float fadeOutTime;

    [Header("Next Scene Settings")]
    [SerializeField] private string nextSceneName;
    [SerializeField] private bool nextSceneTextEnabled;
    [SerializeField] private float timeToEnableNextSceneText;

    [Header("Autoskip Settings")]
    [SerializeField] private bool autoSkipEnabled;
    [SerializeField] private float timeForAutoSkip;

    private float time = 0;
    private bool goingToNextScene;
    private bool nextSceneTextHasBeenEnabled = false;

    public static Action OnEnableNextSceneText;

    private void OnEnable()
    {
        NextSceneTextController.OnNextSceneInput += GoToNextScene;
    }

    private void OnDisable()
    {
        NextSceneTextController.OnNextSceneInput -= GoToNextScene;
    }

    private void Start()
    {
        Time.timeScale = 1f;

        SetNewsPaperScene();
        CreditsFadeIn();
    }

    private void Update()
    {
        time += Time.deltaTime;
        CheckAutoSkip();
        CheckEnableNextSceneText();
    }
    private void SetNewsPaperScene()
    {
        time = 0f;
        goingToNextScene = false;
        nextSceneTextHasBeenEnabled = false;
    }

    #region Scene
    private void CreditsFadeIn()
    {
        ScenesManager.instance.StartFadeInCoroutine(fadeInTime);
    }
    private void CreditsFadeOut(string targetScene)
    {
        ScenesManager.instance.StartFadeToTargetScene(targetScene, fadeOutTime);
    }

    public void FadeOutToNextScene()
    {
        CreditsFadeOut(nextSceneName);
    }

    private void GoToNextScene()
    {
        if (goingToNextScene) return;

        CreditsFadeOut(nextSceneName);
        goingToNextScene = true;
    }
    #endregion

    #region NextSceneText
    private void CheckEnableNextSceneText()
    {
        if (!nextSceneTextEnabled) return;
        if (nextSceneTextHasBeenEnabled) return;

        if (time >= timeToEnableNextSceneText)
        {
            OnEnableNextSceneText?.Invoke();
            nextSceneTextHasBeenEnabled = true;
        }
    }
    #endregion

    #region Autoskip
    private void CheckAutoSkip()
    {
        if (!autoSkipEnabled) return;

        if (time >= timeForAutoSkip)
        {
            GoToNextScene();
        }
    }
    #endregion
}
