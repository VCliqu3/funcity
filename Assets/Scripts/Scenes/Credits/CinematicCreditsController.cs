using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinematicCreditsController : MonoBehaviour
{
    [SerializeField] bool enableNamesSkip;

    [SerializeField] private Animator cameraAnimator;
    [SerializeField] private CanvasGroup blackPanelCanvasGroup;

    [SerializeField] private float fadeInTime;
    [SerializeField] private float fadeOutTime;

    [SerializeField] private float startWaitTime;
    [SerializeField] private float nameShowTime;

    private NonGameplayScenesInputHandler nonGameplayScenesInputHandler;

    private void Awake()
    {
        nonGameplayScenesInputHandler = FindObjectOfType<NonGameplayScenesInputHandler>();
    }

    private void Start()
    {
        StartCoroutine(CinematicCoroutine());
    }

    private IEnumerator CinematicCoroutine()
    {
        yield return new WaitForSeconds(startWaitTime);

        cameraAnimator.Play("CameraAnimation1");

        yield return StartCoroutine(WaitSeconds(nameShowTime));

        yield return FadeOutInAnimation("CameraAnimation2");

        yield return StartCoroutine(WaitSeconds(nameShowTime));

        yield return FadeOutInAnimation("CameraAnimation3");

        yield return StartCoroutine(WaitSeconds(nameShowTime));

        yield return FadeOutInAnimation("CameraAnimation4");

        yield return StartCoroutine(WaitSeconds(nameShowTime));

        yield return FadeOutInAnimation("CameraAnimation5");

        yield return StartCoroutine(WaitSeconds(nameShowTime));

        yield return FadeOutInAnimation("CameraAnimation6");

        yield return StartCoroutine(WaitSeconds(nameShowTime));

        yield return FadeOutInAnimation("CameraAnimation9");

        yield return StartCoroutine(WaitSeconds(nameShowTime));

        yield return FadeOutInAnimation("CameraAnimation8");

        yield return StartCoroutine(WaitSeconds(nameShowTime));

        yield return FadeOutInAnimation("CameraAnimation10");

        yield return StartCoroutine(WaitSeconds(nameShowTime));

        yield return FadeOutInAnimation("CameraAnimation12");

        yield return StartCoroutine(WaitSeconds(nameShowTime));

        yield return FadeOutInAnimation("CameraAnimation13");

        yield return StartCoroutine(WaitSeconds(nameShowTime));

        yield return FadeOutInAnimation("CameraAnimation14");

        yield return StartCoroutine(WaitSeconds(nameShowTime));

        FindObjectOfType<CreditsSceneManager>().FadeOutToNextScene();
    }

    private IEnumerator FadeOutInAnimation(string animationName)
    {
        yield return FadeOut();
        cameraAnimator.Play(animationName);
        yield return FadeIn();
    }

    private IEnumerator WaitSeconds(float waitTime)
    {
        float time = 0;

        while (time < waitTime)
        {
            time += Time.deltaTime;

            if (nonGameplayScenesInputHandler.GetSkipInputDown() && enableNamesSkip)
            {
                break;
            }

            yield return null;
        }
    }

    #region FadePanelCoroutines
    private IEnumerator FadeIn()
    {
        blackPanelCanvasGroup.alpha = 1f;
        blackPanelCanvasGroup.blocksRaycasts = true;
        float time = 0f;

        while (time <= fadeInTime)
        {
            time += Time.deltaTime;
            blackPanelCanvasGroup.alpha -= Time.deltaTime / fadeInTime;

            yield return null;
        }

        blackPanelCanvasGroup.alpha = 0f;
        blackPanelCanvasGroup.blocksRaycasts = false;
    }

    private IEnumerator FadeOut()
    {
        blackPanelCanvasGroup.alpha = 0f;
        blackPanelCanvasGroup.blocksRaycasts = true;
        float time = 0f;

        while (time <= fadeOutTime)
        {
            time += Time.deltaTime;
            blackPanelCanvasGroup.alpha += Time.deltaTime / fadeOutTime;

            yield return null;
        }

        blackPanelCanvasGroup.alpha = 1f;
        blackPanelCanvasGroup.blocksRaycasts = false;
    }
    #endregion
}
