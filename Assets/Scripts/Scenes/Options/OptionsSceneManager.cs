using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsSceneManager : MonoBehaviour
{
    [SerializeField] private float fadeInTime;
    [SerializeField] private float fadeOutTime;

    private void Start()
    {
        Time.timeScale = 1f;
        OptionsFadeIn();
    }

    private void OptionsFadeIn()
    {
        ScenesManager.instance.StartFadeInCoroutine(fadeInTime);
    }

    public void OptionsFadeOut(string targetScene)
    {
        ScenesManager.instance.StartFadeToTargetScene(targetScene, fadeOutTime);
    }
}
