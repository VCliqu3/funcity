using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

[System.Serializable]
public class OptionsButtonPanelLink
{
    public Button button;
    public GameObject panel;
}

public class OptionPanelsController : MonoBehaviour
{
    [SerializeField] private OptionsButtonPanelLink[] buttonPanelLinks;
    [SerializeField] private GameObject defaultPanel;

    private GameObject activePanel;

    private void Start()
    {
        DisableAllPanels();
        EnableDefaultPanel();
    }

    public void OnPauseMenuPanelButtonClicked()
    {
        Button clickedButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();

        if (clickedButton)
        {
            OptionsButtonPanelLink buttonPanelLink = Array.Find(buttonPanelLinks, x => x.button == clickedButton);

            if (buttonPanelLink != null)
            {
                GameObject panel = buttonPanelLink.panel;

                if (panel != activePanel)
                {
                    SwitchActivePanel(panel);
                }
            }
            else
            {
                Debug.Log("Link Not Found");
            }
        }
        else
        {
            Debug.Log("Button Not Found");
        }
    }

    private void SwitchActivePanel(GameObject panel)
    {
        if (activePanel) activePanel.SetActive(false);

        panel.SetActive(true);

        activePanel = panel;
    }

    private void EnableDefaultPanel()
    {
        if (activePanel) return;

        if (defaultPanel)
        {
            defaultPanel.SetActive(true);
            activePanel = defaultPanel;
        }
    }

    private void DisableAllPanels()
    {
        foreach (OptionsButtonPanelLink link in buttonPanelLinks)
        {
            link.panel.SetActive(false);
        }

        activePanel = null;
    }
}
