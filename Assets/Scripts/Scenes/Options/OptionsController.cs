using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsController : MonoBehaviour
{
    [Header("Audio Settings")]
    [SerializeField] private Slider musicSlider;
    [SerializeField] private Slider sfxSlider;
    [SerializeField] private Slider dialogueSlider;

    [Header("SensibilitySettings")]
    [SerializeField] private Slider sensibilityXSlider;
    [SerializeField] private Slider sensibilityYSlider;

    [SerializeField] private Toggle invertXToggle;
    [SerializeField] private Toggle invertYToggle;

    // Start is called before the first frame update
    private void OnEnable()
    {
        LoadMusicVolumeToSlider();
        LoadSFXVolumeToSlider();
        LoadDialogueVolumeToSlider();

        LoadSensibilityXToSlider();
        LoadSensibilityYToSlider();

        LoadInvertXToToggle();
        LoadInvertYToToggle();
    }

    private void OnDisable()
    {
        AudioManager.instance.LoadMusicVolumePlayerPrefs();
        AudioManager.instance.LoadSFXVolumePlayerPrefs();
        AudioManager.instance.LoadDialogueVolumePlayerPrefs();

        SensibilityManager.instance.LoadSensibilityXPlayerPrefs();
        SensibilityManager.instance.LoadSensibilityYPlayerPrefs();

        SensibilityManager.instance.LoadInvertXPlayerPrefs();
        SensibilityManager.instance.LoadInvertYPlayerPrefs();
    }

    #region Audio Settings
    private void LoadMusicVolumeToSlider()
    {
        musicSlider.value = AudioManager.instance.musicVolume;
    }
    private void LoadSFXVolumeToSlider()
    {
        sfxSlider.value = AudioManager.instance.sfxVolume;
    }
    private void LoadDialogueVolumeToSlider()
    {
        dialogueSlider.value = AudioManager.instance.dialogueVolume;
    }
    public void ChangeMusicVolume()
    {
        AudioManager.instance.SetMusicVolume(musicSlider.value);
    }
    public void ChangeSFXVolume()
    {
        AudioManager.instance.SetSFXVolume(sfxSlider.value);
    }
    public void ChangeDialogueVolume()
    {
        AudioManager.instance.SetDialogueVolume(dialogueSlider.value);
    }
    #endregion

    #region Sensibility Settings
    private void LoadSensibilityXToSlider()
    {
        sensibilityXSlider.value = SensibilityManager.sensibilityX;
    }
    private void LoadSensibilityYToSlider()
    {
        sensibilityYSlider.value = SensibilityManager.sensibilityY;
    }
    public void ChangeXSensibility()
    {
        SensibilityManager.instance.SetSensibilityX(sensibilityXSlider.value);
    }

    public void ChangeYSensibility()
    {
        SensibilityManager.instance.SetSensibilityY(sensibilityYSlider.value);
    }

    #endregion

    #region Invert Settings
    private void LoadInvertXToToggle()
    {
        invertXToggle.isOn = SensibilityManager.invertX;
    }
    private void LoadInvertYToToggle()
    {
        invertYToggle.isOn = SensibilityManager.invertY;
    }
    public void ChangeInvertX()
    {
        SensibilityManager.instance.SetInvertX(invertXToggle.isOn);
    }

    public void ChangeInvertY()
    {
        SensibilityManager.instance.SetInvertY(invertYToggle.isOn);
    }

    #endregion

    public void SaveSettings()
    {
        AudioManager.instance.SaveMusicVolumePlayerPrefs();
        AudioManager.instance.SaveSFXVolumePlayerPrefs();
        AudioManager.instance.SaveDialogueVolumePlayerPrefs();

        SensibilityManager.instance.SaveSensibilityXPlayerPrefs();
        SensibilityManager.instance.SaveSensibilityYPlayerPrefs();

        SensibilityManager.instance.SaveInvertXPlayerPrefs();
        SensibilityManager.instance.SaveInvertYPlayerPrefs();
    }
}
