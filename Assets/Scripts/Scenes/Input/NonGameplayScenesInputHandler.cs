using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NonGameplayScenesInputHandler : MonoBehaviour
{
    private bool CanProcessInput()
    {
        return true;
    }
    public bool GetNextSceneInputDown()
    {
        if (CanProcessInput())
        {
            return Input.GetButtonDown(GameConstants.c_ButtonNameNextScene);
        }

        return false;
    }
    public bool GetSkipInputDown()
    {
        if (CanProcessInput())
        {
            return Input.GetButtonDown(GameConstants.c_ButtonNameSkip);
        }

        return false;
    }
}
