using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

[System.Serializable]
public class AchievementUIRelation
{
    public int achievementNumber;
    public GameObject linkedUI;
}
public class AchievementsPanelController : MonoBehaviour
{
    [SerializeField] private AchievementUIRelation[] achievementUIRelations;

    // Start is called before the first frame update
    private void Start()
    {
        InitializeAchievementsUI();
    }

    private void InitializeAchievementsUI()
    {
        foreach(AchievementUIRelation relation in achievementUIRelations)
        {
            InitializeAchievementUI(relation);
        }
    }

    private void InitializeAchievementUI(AchievementUIRelation relation)
    {
        Achievement achievement = Array.Find(AchievementManager.instance.achievementsArray, x => x.achievementNumber == relation.achievementNumber);

        if (achievement == null)
        {
            Debug.Log("Achievement not found -  ID: " + relation.achievementNumber);
            return;
        }

        relation.linkedUI.transform.Find("Name").GetComponent<TMP_Text>().text = achievement.achievementName;
        relation.linkedUI.transform.Find("Description").GetComponent<TMP_Text>().text = achievement.achievementDescription;
        relation.linkedUI.transform.Find("Image").GetComponent<Image>().sprite = achievement.achievementSprite;

        Image lockedPanel = relation.linkedUI.transform.Find("LockedPanel").GetComponent<Image>();

        lockedPanel.enabled = !achievement.isCompleted;
    }
}
