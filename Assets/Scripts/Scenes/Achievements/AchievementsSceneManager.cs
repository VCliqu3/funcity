using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementsSceneManager : MonoBehaviour
{
    [SerializeField] private float fadeInTime;
    [SerializeField] private float fadeOutTime;

    private void Start()
    {
        Time.timeScale = 1f;
        AchievementsFadeIn();
    }

    private void AchievementsFadeIn()
    {
        ScenesManager.instance.StartFadeInCoroutine(fadeInTime);
    }

    public void AchievementsFadeOut(string targetScene)
    {
        ScenesManager.instance.StartFadeToTargetScene(targetScene, fadeOutTime);
    }
}
