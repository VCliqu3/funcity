using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DiarySceneManager : MonoBehaviour
{
    [Header("Scene Settings")]
    [SerializeField] private float fadeInTime;
    [SerializeField] private float fadeOutTime;

    [Header("Next Settings")]
    [SerializeField] private string nextSceneName;

    [Header("Typing Settings")]
    [SerializeField] private float timeToStartTyping;
    [SerializeField] private bool skipTextEnabled;
    [SerializeField] private float timeToEnableSkipText;

    [Header("Autoskip Settings")]
    [SerializeField] private bool autoSkipEnabled;
    [SerializeField] private float timeForAutoSkip;

    private bool goingToNextScene;
    private bool startedTyping;
    private bool skipTypingTextHasBeenEnabled = false;

    public static Action OnEnableNextSceneText;
    public static Action OnStartTyping;
    public static Action OnEnableSkipTypingText;

    private float time = 0f;

    private void OnEnable()
    {
        NextSceneTextController.OnNextSceneInput += GoToNextScene;
        TypewriterController.OnTypingFinished += StartAutoSkip; 
    }

    private void OnDisable()
    {
        NextSceneTextController.OnNextSceneInput -= GoToNextScene;
        TypewriterController.OnTypingFinished -= StartAutoSkip;
    }

    private void Start()
    {
        Time.timeScale = 1f;

        SetDiaryScene();
        DiaryFadeIn();
    }

    private void Update()
    {
        time += Time.deltaTime;

        CheckStartTyping();
        CheckEnableSkipTypingText();
    }

    private void SetDiaryScene()
    {
        time = 0f;

        goingToNextScene = false;
        skipTypingTextHasBeenEnabled = false;
    }

    #region Scene
    private void DiaryFadeIn()
    {
        ScenesManager.instance.StartFadeInCoroutine(fadeInTime);
    }
    private void DiaryFadeOut(string targetScene)
    {
        ScenesManager.instance.StartFadeToTargetScene(targetScene, fadeOutTime);
    }
    private void GoToNextScene()
    {
        if (goingToNextScene) return;

        DiaryFadeOut(nextSceneName);
        goingToNextScene = true;
    }
    #endregion

    #region Typing
    private void CheckStartTyping()
    {
        if (startedTyping) return;

        if (time >= timeToStartTyping)
        {
            OnStartTyping?.Invoke();
            startedTyping = true;
        }
    }

    private void CheckEnableSkipTypingText()
    {
        if (!skipTextEnabled) return;
        if (skipTypingTextHasBeenEnabled) return;

        if (time >= timeToEnableSkipText)
        {
            OnEnableSkipTypingText?.Invoke();
            skipTypingTextHasBeenEnabled = true;
        }
    }
    #endregion

    #region Autoskip
    private void StartAutoSkip()
    {
        if (!autoSkipEnabled) return;

        StartCoroutine(Autoskip());

    }

    private IEnumerator Autoskip()
    {
        yield return new WaitForSeconds(timeForAutoSkip);

        GoToNextScene();
    }
    #endregion
}
