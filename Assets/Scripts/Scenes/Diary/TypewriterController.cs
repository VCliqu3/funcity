using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

[System.Serializable]
public class TMPTextRelation
{
    public TMP_Text textObject;
    public string textToType;
}

public class TypewriterController : MonoBehaviour
{
    [SerializeField] private TMPTextRelation[] _TMPTextRelations;

    [SerializeField] private float timeBetweenCharacters;
    [SerializeField] private float timeBetweenSentences;
    
    [SerializeField] private AudioClip typeAudioClip;

    private AudioSource audioSource;

    [SerializeField] private TypingState typingState = TypingState.Idle;
    private enum TypingState { Idle, Typing, BetweenSentences, Finished}

    public static Action OnTypingFinished;

    private void OnEnable()
    {
        DiarySceneManager.OnStartTyping += TypeSentences;
        SkipTextController.OnSkipInput += SkipTyping;
    }

    private void OnDisable()
    {
        DiarySceneManager.OnStartTyping -= TypeSentences;
        SkipTextController.OnSkipInput -= SkipTyping;
    }

    private void Awake()
    {
        audioSource = GetComponentInChildren<AudioSource>();
    }

    private void Start()
    {
        ClearAllRelationsText();
        SetTypingState(TypingState.Idle);
    }

    private void SetTypingState(TypingState state)
    {
        typingState = state;
    }

    #region Typing
    private void TypeSentences()
    {
        StartCoroutine(TypeSentencesCoroutine());
    }

    private IEnumerator TypeSentencesCoroutine()
    {
        for (int i=0; i< _TMPTextRelations.Length; i++)
        {
            SetTypingState(TypingState.Typing);
            yield return StartCoroutine(TypeSentence(_TMPTextRelations[i]));
            SetTypingState(TypingState.BetweenSentences);

            if(i< _TMPTextRelations.Length -1) yield return new WaitForSeconds(timeBetweenSentences);
        }

        SetTypingState(TypingState.Finished);

        OnTypingFinished?.Invoke();
    }

    private IEnumerator TypeSentence(TMPTextRelation relation)
    {
        ClearText(relation.textObject);

        int characterIndex = 0;

        audioSource.clip = typeAudioClip;
        audioSource.Play();

        while (true)
        {
            characterIndex++;
            
            relation.textObject.text = relation.textToType.Substring(0,characterIndex);
            relation.textObject.text += "<color=#00000000>" + relation.textToType.Substring(characterIndex) + "</color>";

            yield return new WaitForSeconds(timeBetweenCharacters);

            if (characterIndex >= relation.textToType.Length)
            {
                break;
            }
        }

        audioSource.Stop();
        audioSource.clip = null;
    }
    #endregion

    #region Clear
    private void ClearAllRelationsText()
    {
        foreach (TMPTextRelation relation in _TMPTextRelations)
        {
            ClearText(relation.textObject);
        }
    }

    private void ClearText(TMP_Text text)
    {
        text.text = "";
    }
    #endregion

    #region Skip
    private void SkipTyping()
    {
        StopAllCoroutines();

        audioSource.Stop();
        audioSource.clip = null;

        foreach (TMPTextRelation relation in _TMPTextRelations)
        {
            relation.textObject.text = relation.textToType;
        }

        SetTypingState(TypingState.Finished);

        OnTypingFinished?.Invoke();
    }
    #endregion
}
