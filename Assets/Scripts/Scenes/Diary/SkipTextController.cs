using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SkipTextController : MonoBehaviour
{
    [Header("Blinking Settings")]
    [SerializeField] private bool blinkingEnabled;
    [SerializeField] private float blinkingTime;
    [SerializeField] private float blinkingMinAlpha;
    [SerializeField] private float blinkingMaxAlpha;

    [Header("FadeIn Settings")]
    [SerializeField] private float fadeInTime;
    [SerializeField] private float fadeInMinAlpha;
    [SerializeField] private float fadeInMaxAlpha;

    [Header("FadeOut Settings")]
    [SerializeField] private float fadeOutTime;
    [SerializeField] private float fadeOutMinAlpha;

    [SerializeField] private bool fadeOutMaxAlphaEnabled;
    [SerializeField] private float fadeOutMaxAlpha;

    [SerializeField] private TextState textState = TextState.Idle;
    private enum TextState {Idle, FadingIn, Showing, FadingOut}

    private CanvasGroup canvasGroup;
    private NonGameplayScenesInputHandler nonGameplayScenesInputHandler;

    public static Action OnSkipInput;

    private void OnEnable()
    {
        DiarySceneManager.OnEnableSkipTypingText += DoWhenSkipTextEnabled;
        TypewriterController.OnTypingFinished += DoWhenTypingFinished;
    }
    private void OnDisable()
    {
        DiarySceneManager.OnEnableSkipTypingText -= DoWhenSkipTextEnabled;
        TypewriterController.OnTypingFinished -= DoWhenTypingFinished;
    }

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        nonGameplayScenesInputHandler = FindObjectOfType<NonGameplayScenesInputHandler>();
    }

    private void Start()
    {
        InitializeCanvasGroup();
        SetTextState(TextState.Idle);
    }

    private void Update()
    {
        CheckSkipTypingInput();
    }


    private void InitializeCanvasGroup()
    {
        canvasGroup.alpha = 0;
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
    }

    private void SetTextState(TextState state)
    {
        textState = state;
    }

    #region Enable
    private void DoWhenSkipTextEnabled()
    {
        StopAllCoroutines();
        StartCoroutine(EnableCoroutine());
    }

    private IEnumerator EnableCoroutine()
    {
        SetTextState(TextState.FadingIn);
        yield return StartCoroutine(FadeIn(fadeInTime));

        SetTextState(TextState.Showing);
        StartCoroutine(BlinkingCoroutine());
    }
    #endregion

    #region Skip & Hide
    private void CheckSkipTypingInput()
    {
        if (textState != TextState.Showing) return;

        if (nonGameplayScenesInputHandler.GetSkipInputDown())
        {
            HideText();
            OnSkipInput?.Invoke();
        }
    }

    private void HideText()
    {
        StopAllCoroutines();

        SetTextState(TextState.FadingOut);
        StartCoroutine(FadeOut(fadeOutTime));
    }

    private void DoWhenTypingFinished()
    {
        fadeOutMaxAlphaEnabled = false;
        HideText();
    }
    #endregion

    #region Blinking
    private IEnumerator BlinkingCoroutine()
    {
        while (true)
        {
            if (!blinkingEnabled) yield break;

            float time = 0f;
            canvasGroup.alpha = blinkingMaxAlpha;

            while (time <= blinkingTime)
            {
                canvasGroup.alpha = blinkingMaxAlpha - time / blinkingTime * (blinkingMaxAlpha - blinkingMinAlpha);

                time += Time.deltaTime;
                yield return null;
            }

            canvasGroup.alpha = blinkingMinAlpha;
            time = 0f;

            while (time <= blinkingTime)
            {
                canvasGroup.alpha = blinkingMinAlpha + time / blinkingTime * (blinkingMaxAlpha - blinkingMinAlpha);

                time += Time.deltaTime;
                yield return null;
            }
        }
    }
    #endregion

    #region FadeCoroutines
    private IEnumerator FadeIn(float fadeInTime)
    {
        canvasGroup.alpha = fadeInMinAlpha;
        float time = 0f;

        while (time <= fadeInTime)
        {
            time += Time.deltaTime;
            canvasGroup.alpha = fadeInMinAlpha + time / fadeInTime * (fadeInMaxAlpha - fadeInMinAlpha);

            yield return null;
        }

        canvasGroup.alpha = fadeInMaxAlpha;
    }

    private IEnumerator FadeOut(float fadeOutTime)
    {
        float startingAlpha = fadeOutMaxAlphaEnabled ? fadeOutMaxAlpha : canvasGroup.alpha;

        float time = 0f;

        while (time <= fadeOutTime)
        {
            time += Time.deltaTime;
            canvasGroup.alpha = startingAlpha - time / fadeOutTime * (startingAlpha - fadeOutMinAlpha);

            yield return null;
        }

        canvasGroup.alpha = fadeOutMinAlpha;
    }
    #endregion
}
