using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MenuSceneManager : MonoBehaviour
{
    [SerializeField] private float fadeInTime;
    [SerializeField] private float fadeOutTime;

    [SerializeField] private string newspaperScene;
    [SerializeField] private string gameplayScene;

    private void Start()
    {
        Time.timeScale = 1f;
        SetCursorLockedAndVisible(false);
        MenuFadeIn();
    }

    private void MenuFadeIn()
    {
        ScenesManager.instance.StartFadeInCoroutine(fadeInTime);
    }

    public void MenuFadeOut(string targetScene)
    {
        ScenesManager.instance.StartFadeToTargetScene(targetScene,fadeOutTime);
    }

    #region PlayButtonLogic
    private bool CheckUsingDatabase()
    {
        return LoadedDatabaseManager.instance.enableDataLoad && LoadedDatabaseManager.instance.hasLoadedData;
    }

    public void PlayButtonLogic()
    {
        if (CheckUsingDatabase())
        {
            PlayButtonDatabaseLogic();
        }
        else
        {
            PlayButtonPlayerPrefsLogic();
        }
    }

    public void PlayButtonDatabaseLogic()
    {
        if (UserManager.instance.checkpointNumber == 0)
        {
            MenuFadeOut(newspaperScene);
            return;
        }

        MenuFadeOut(gameplayScene);
    }

    public void PlayButtonPlayerPrefsLogic()
    {
        if (!PlayerPrefs.HasKey("CheckpointNumber"))
        {
            MenuFadeOut(newspaperScene);
            return;
        }

        if (PlayerPrefs.GetInt("CheckpointNumber") == 0)
        {
            MenuFadeOut(newspaperScene);
            return;
        }

        MenuFadeOut(gameplayScene);
    }
    #endregion

    private void SetCursorLockedAndVisible(bool isUnlocked)
    {
        Cursor.lockState = isUnlocked ? CursorLockMode.None : CursorLockMode.Locked;
        Cursor.visible = isUnlocked;
    }

    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("Game Quit!");
    }

    public void ClearAllPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }
}
