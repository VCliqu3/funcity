using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlurPanelController : MonoBehaviour
{
    [SerializeField,Range(0.001f, 0.015f)] private float pauseBlur;
    [SerializeField, Range(0.001f, 0.015f)] private float notesBlur;
    [SerializeField, Range(0.001f, 0.015f)] private float inspectionBlur;
    [SerializeField, Range(0.001f, 0.015f)] private float jumpscareBlur;

    [SerializeField] private Camera blurNoteCamera;
    [SerializeField] private Camera blurInspectionCamera;
    [SerializeField] private Camera blurPauseCamera;
    [SerializeField] private Camera jumpscareCamera;

    private Canvas canvas;
    private Image image;

    private bool onNote;
    private bool onInspection;

    private void OnEnable()
    {
        GameManager.OnGameStart += DoWhenGameStart;
        PauseCanvasController.OnPauseMenuOpened += DoWhenPauseMenuOpened;
        PauseCanvasController.OnPauseMenuClosed += DoWhenPauseMenuClosed;

        NoteCanvasController.OnNoteOpened += DoWhenNoteOpened;
        NoteCanvasController.OnNoteClosed += DoWhenNoteClosed;

        InspectionCanvasController.OnInspectionOpened += DoWhenInspectionOpened;
        InspectionCanvasController.OnInspectionClosed += DoWhenInspectionClosed;

        GameManager.OnGameJumpScare += DoWhenJumpscare;
    }
    private void OnDisable()
    {
        GameManager.OnGameStart -= DoWhenGameStart;

        PauseCanvasController.OnPauseMenuOpened -= DoWhenPauseMenuOpened;
        PauseCanvasController.OnPauseMenuClosed -= DoWhenPauseMenuClosed;

        NoteCanvasController.OnNoteOpened -= DoWhenNoteOpened;
        NoteCanvasController.OnNoteClosed -= DoWhenNoteClosed;

        InspectionCanvasController.OnInspectionOpened -= DoWhenInspectionOpened;
        InspectionCanvasController.OnInspectionClosed -= DoWhenInspectionClosed;

        GameManager.OnGameJumpScare -= DoWhenJumpscare;
    }

    private void Awake()
    {
        image = GetComponent<Image>();
        canvas = GetComponentInParent<Canvas>();
    }

    private void DoWhenGameStart()
    {
        onNote = false;
        onInspection = false;
        image.enabled = false;
    }

    #region Pause
    private void DoWhenPauseMenuOpened()
    {
        image.enabled = true;
        SetImageMaterialBlur(pauseBlur);
        SetScreenSpaceCamera(blurPauseCamera);
    }

    private void DoWhenPauseMenuClosed()
    {
        if (onNote)
        {
            SetImageMaterialBlur(notesBlur);
            SetScreenSpaceCamera(blurNoteCamera);
        }
        else if (onInspection)
        {
            SetImageMaterialBlur(inspectionBlur);
            SetScreenSpaceCamera(blurInspectionCamera);
        }
        else
        {
            image.enabled = false;
        }
    }
    #endregion

    #region Notes
    private void DoWhenNoteOpened(InteractableNote interactableNote)
    {
        onNote = true;
        image.enabled = true;
        SetImageMaterialBlur(notesBlur);
        SetScreenSpaceCamera(blurNoteCamera);
    }

    private void DoWhenNoteClosed(InteractableNote interactableNote)
    {
        if (GameManager.instance.gameState == GameManager.GameState.OnJumpScare) return;

        onNote = false;
        image.enabled = false;
    }
    #endregion

    #region Inspection
    private void DoWhenInspectionOpened(PickableObject pickableObject)
    {
        onInspection = true;
        image.enabled = true;
        SetImageMaterialBlur(inspectionBlur);
        SetScreenSpaceCamera(blurInspectionCamera);
    }

    private void DoWhenInspectionClosed(PickableObject pickableObject)
    {
        if (GameManager.instance.gameState == GameManager.GameState.OnJumpScare) return;

        onInspection = false;
        image.enabled = false;
    }
    #endregion

    #region Jumpscare
    private void DoWhenJumpscare()
    {
        image.enabled = true;
        SetImageMaterialBlur(jumpscareBlur);
        SetScreenSpaceCamera(jumpscareCamera);
    }
    #endregion

    private void SetScreenSpaceCamera(Camera camera)
    {
        canvas.worldCamera = camera;
    }

    private void SetImageMaterialBlur(float value)
    {
        image.material.SetFloat("_BlurY", value);
        image.material.SetFloat("_BlurX", value);
    }
}
